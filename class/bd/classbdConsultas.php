<?php
include("../database/classdb.php");

// Clase Principal.
class classbdConsultas {

    var $ObjDb = "";
    var $orderby, $groupby, $limite;

    //var $prefix_table="f5."; //esquerma real
    var $prefix_table = "f5."; //esquerma desarrollo
    function classbdConsultas() {
        // Librerias Comunes
        $this->orderby = array();
        $this->groupby = array();
        $this->limite = array();
    }

    /**
    esta funcion se usa para seleccionar los cargos disponibles.
    */
    function select_cargos($conect, $id_cargo=null){
        $nameTabla[$this->prefix_table.'rrhh_cargos'] = $this->prefix_table."rrhh_cargos";
        $campos['id_cargo'] = "id_cargo";
        $campos['de_cargo'] = "de_cargo";
        $condicion['fecha_expiracion'] = "'2222-12-31'";
        if($id_cargo!=null){
            $condicion['id_cargo'] = $id_cargo;
        }
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $this->orderby, $this->groupby, $this->limite);
    }

    //unificando con el almacen tecnico
    function select_almacen($conect) {
        $nameTabla['almacen_tecnico.descripciones'] = "almacen_tecnico.descripciones";

        $campos['id_descripcion'] = "id_descripcion";
        $campos['descripcion'] = "descripcion";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $this->orderby, $this->groupby, $this->limite);
    }
    /**
    *
    *   esta funcion sirve para determinar los datos de un tiporecurso.
    *
    */
    function select_info_tiporecurso($conect, $idtipomaterial) {
        $nameTabla['inventario.select_servicios'] = "inventario.select_servicios";
        /**
         * tabla:
         * inventario.select_servicios
         * campos:
         * materialtipomaterialinv.idtipomaterial,
         * inventario.concatena(materialtipomaterialinv.idtipoinventario::text) AS idtiposinventario,
         * upper(materialtipomaterialinv.valorinventariado) AS valorinventariado,
         * materialtipomaterialinv.desctipomaterial,
         * materialtipomaterialinv.id_fintipoinventario
         */
        $campos['idtipomaterial'] = "idtipomaterial";
        $campos['idtiposinventario'] = "idtiposinventario";
        $campos['valorinventariado'] = "valorinventariado";
        $campos['desctipomaterial'] = "desctipomaterial";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";
        $condicion['idtipomaterial'] = $idtipomaterial;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $this->orderby, $this->groupby, $this->limite);
    }

    function select_categorias_cargos($conect){
        $nameTabla[$this->prefix_table.'categorias_cargo'] = $this->prefix_table.'categorias_cargo';
        $campos['ids_cargos'] = "ids_cargos";
        $campos['desc_categoria'] = "desc_categoria";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $this->orderby, $this->groupby, $this->limite);
    }

    //deprecated
    function select_servicios2($conect, $tiposervicio="2") {
        $nameTabla['inventario.select_servicios2'] = "inventario.select_servicios2";
        /**
         * tabla:
         * nventario.select_servicios2
         * campos
         *  inventario.concatena(rel_tipomaterial_servicios.idtipomaterial::text) AS idstipomaterial,
         * rel_tipomaterial_servicios.valorinventariado,
         * rel_tipomaterial_servicios.id_fintipoinventario
         *
         */
        $campos['idstipomaterial'] = "idstipomaterial";
        $campos['valorinventariado'] = "valorinventariado";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";
        $condicion['id_fintipoinventario'] = $tiposervicio;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $this->orderby, $this->groupby, $this->limite);
    }

    function get_recursos_humanos($conect,$cedula=null,$id_cargo=null, $id_gerencia=null,$id_division=null, $busqueda=null){
        /*
        SELECT
        rrhh_personal.cedula,
        rrhh_personal.id_gerencia,
        rrhh_personal.id_division,
        rrhh_personal.id_cargo,
        rrhh_rel_categoria_cargo.id_categoria,
        rrhh_personal.nombre,
        rrhh_personal.apellido,
        rrhh_gerencia.desc_gerencia,
        rrhh_division.desc_division,
        rrhh_cargos.de_cargo,
        rrhh_categoria.desc_categoria
        */
        $nameTabla[$this->prefix_table.'rrhh_modulogeneral'] = $this->prefix_table.'rrhh_modulogeneral';
        $campos['cedula'] = "cedula";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['id_division'] = "id_division";
        $campos['id_cargo'] = "id_cargo";
        $campos['id_categoria'] = "id_categoria";
        $campos['nombre'] = "nombre";
        $campos['apellido'] = "apellido";
        $campos['desc_gerencia'] = "desc_gerencia";
        $campos['desc_division'] = "desc_division";
        $campos['de_cargo'] = "de_cargo";
        $campos['desc_categoria'] = "desc_categoria";
        if($cedula!=null){
            $condicion.="cedula=".$cedula;
        }
        if($id_cargo!=null){
            if($condicion!=""){
                $condicion.=" and ";
            }
            $condicion.="id_cargo=".$id_cargo;
        }
        if($busqueda!=null){
            if($condicion!=""){
                $condicion.=" and ";
            }
            $condicion.="busqueda ilike '%".$busqueda."%'";
        }
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    //deprecated
    function get_recursos_sys_inventario($conect, $idmaterialgeneral=null, $idtipoinventario=null, $idtipomaterial=null, $id_gerencia=null, $id_division=null, $id_fintipoinventario=null, $busqueda=null) {
        $nameTabla['inventario.inventariostok'] = "inventario.inventariostok";
        /**
         * tabla
         * inventario.inventariostok
         * campos
         * vistageneral_inventario.idmaterialgeneral,
         * vistageneral_inventario.idtipoinventario,
         * vistageneral_inventario.desctipoinventario,
         * vistageneral_inventario.desctipomaterial,
         * vistageneral_inventario.inventario_valor,
         * vistageneral_inventario.busqueda,
         * vistageneral_inventario.idtipomaterial,
         * vistageneral_inventario.id_gerencia,
         * vistageneral_inventario.id_division,
         * vistageneral_inventario.id_fintipoinventario
         */
        $campos['idmaterialgeneral'] = "idmaterialgeneral";
        $campos['idtipoinventario'] = "idtipoinventario";
        $campos['desctipoinventario'] = "desctipoinventario";
        $campos['desctipomaterial'] = "desctipomaterial";
        $campos['inventario_valor'] = "inventario_valor";
        $campos['idtipomaterial'] = "idtipomaterial";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['id_division'] = "id_division";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";
        $condicion="";
        if ($idmaterialgeneral != null) {
            $condicion="idmaterialgeneral=".$idmaterialgeneral;
        }
        if ($idtipoinventario != null) {

            if($condicion!=""){
                $condicion.=" and ";
            }

            $condicion.="idtipoinventario=".$idtipoinventario;
        }

        if ($idtipomaterial != null) {

            if($condicion!=""){
                $condicion.=" and ";
            }

            $condicion.="idtipomaterial=".$idtipomaterial;
        }
        if ($id_gerencia != null) {

            if($condicion!=""){
                $condicion.=" and ";
            }

            $condicion.="id_gerencia=".$id_gerencia;
        }
        if ($id_division != null) {

            if($condicion!=""){
                $condicion.=" and ";
            }

            $condicion.="id_division=".$id_division;
        }
        if ($id_fintipoinventario != null) {

            if($condicion!=""){
                $condicion.=" and ";
            }

            $condicion.="id_fintipoinventario=".$id_fintipoinventario;
        }

        if($busqueda!=null){
            if($condicion!=""){
                $condicion.=" and ";
            }
            $condicion.="busqueda ilike '%".$busqueda."%'";
        }


        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();

        //echo $condicion."|";
        //$this->ObjDb->imprimesql=true;

        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
        //return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $this->orderby, $this->groupby, $this->limite);
    }

    /**
     * resuelve la informacion del inventario y del recurso seleccionado.
     * @param type $conect ruta de conecion y valores a la base de datos
     * @param type $idmaterialgeneral el idmaterialgeneral del inventario
     * @return type
     */
    function get_info_recurso_inventario($conect, $idmaterialgeneral) {
        $nameTabla['inventario.vistageneral_inventario'] = "inventario.vistageneral_inventario";

        /**
         * Tabla
         * inventario.vistageneral_inventario AS
         * Campos
         * materialgeneral.idmaterialgeneral,
         * materialgeneral.idtipoinventario,
         * tipoinventario.desctipoinventario,
         * tipomaterial.desctipomaterial,
         * inventario.concatena((caracteristica.desccararacteristica || ':'::text) || vista_inventario.valorinventariado) AS inventario_valor,
         * inventario.concatena(((tipomaterial.desctipomaterial || ' '::text) || (caracteristica.desccararacteristica || ':'::text)) || vista_inventario.valorinventariado) AS busqueda,
         * materialgeneral.estado_seleccion,
         * materialgeneral.expiracion_seleccion,
         * tipomaterial.idtipomaterial,
         * tipoinventario.id_gerencia,
         * tipoinventario.id_division,
         * tipoinventario.id_fintipoinventario
         */
        $campos['idmaterialgeneral'] = "idmaterialgeneral";
        $campos['idtipoinventario'] = "idtipoinventario";
        $campos['desctipoinventario'] = "desctipoinventario";
        $campos['desctipomaterial'] = "desctipomaterial";
        $campos['inventario_valor'] = "inventario_valor";
        $campos['estado_seleccion'] = "estado_seleccion";
        $campos['idtipomaterial'] = "idtipomaterial";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['id_division'] = "id_division";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";
        $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $this->orderby, $this->groupby, $this->limite);
    }

    /**
    */
    function guardarcomentarios($conect, $idstatuspauta, $mensaje){
        $nameTabla = $this->prefix_table . 't_estatus_pauta';
        $campos['observaciones'] = $mensaje;
        $condicion['id_estatus_pauta'] = $idstatuspauta;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function desacsignar_recursos($conect, $idpauta, $id_recurso_asignado, $cedula_session) {
        $nameTabla = $this->prefix_table . 't_detalle_servicio';
        $campos['fecha_exp'] = date("Y-m-d");
        $campos['user_exp'] = $cedula_session;
        $condicion['id_pauta'] = $idpauta;
        $condicion['id_recurso_asignado'] = $id_recurso_asignado;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function desacsignar_recursos_rrhh($conect, $idpauta, $id_recurso_asignado, $cedula_session) {
        $nameTabla = $this->prefix_table . 't_detalle_servicio';
        $campos['fecha_exp'] = date("Y-m-d");
        $campos['user_exp'] = $cedula_session;
        $condicion['id_pauta'] = $idpauta;
        $condicion['id_tipo_recurso'] = 2;
        $condicion['id_recurso_asignado'] = $id_recurso_asignado;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function desacsignar_recursos_mat($conect, $idpauta, $id_recurso_asignado, $cedula_session) {
        $nameTabla = $this->prefix_table . 't_detalle_servicio';
        $campos['fecha_exp'] = date("Y-m-d");
        $campos['user_exp'] = $cedula_session;
        $condicion['id_pauta'] = $idpauta;
        $condicion['id_tipo_recurso'] = 1;
        $condicion['id_recurso_asignado'] = $id_recurso_asignado;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }


    function status_recurso($conect, $pauta, $t_inventario, $status){
        $nameTabla[$this->prefix_table . 'recur_val_estatus'] = $this->prefix_table . "recur_val_estatus";
        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";

        $condicion = "id_pauta=".$pauta." and id_fintipoinventario=".$t_inventario;
        //$condicion['id_fintipoinventario'] = $t_inventario;
        if($status!=""){
            $condicion.=" and (";
        }
        $estatus=explode(";",$status);

        for($i=0;$i<count($estatus);$i++){
            if($i>0){
               $condicion.=" or ";
           }
           $condicion.="id_estatus=".$estatus[$i];
       }
       if($i>0){
        $condicion.=")";
}

$this->ObjDb = new classdb($conect);
$this->ObjDb->fdbConectar();
return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
}

    /**
    * determina en que posibles gerencias van los recursos.
    */
    function recursotmaterial_gerencia($conect, $idtipomaterial, $id_gerencia, $id_division=null){
        $nameTabla['inventario.tipomaterial_gerencia_div'] = "inventario.tipomaterial_gerencia_div";
        $campos['idtipomaterial'] = "idtipomaterial";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['id_division'] = "id_division";

        if($idtipomaterial!=null){
            $condicion['idtipomaterial'] = $idtipomaterial;
        }

        if($id_gerencia!=null){
            $condicion['id_gerencia'] = $id_gerencia;
        }

        if($id_division!=null){
            $condicion['id_division'] = $id_division;
        }
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

///////////////////////////////f5_new//////////////////////////////////////////////////
///////////////////////////Aviso de la Intranet///////////////////////////////////////

    function selectrecurasig($conect, $cedula) {
        $nameTabla['inventario.vista_inventario'] = "inventario.vista_inventario";
        $campos['idmaterialgeneral'] = "idmaterialgeneral";
        $condicion['valorinventariado'] = "'" . $cedula . "'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectasigpara($conect, $id) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";
        $campos['id_pauta'] = "id_pauta";
        $condicion['id_recurso_asignado'] = $id;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

///////////////////////////select///////////////////////////////////////////////////
    function selectservicio($conect) {
        $nameTabla[$this->prefix_table . 't_tipo_servicio'] = $this->prefix_table . "t_tipo_servicio";
        $campos['id_tipo_servicio'] = "id_tipo_servicio";
        $campos['descripcion'] = "descripcion";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectpauta($conect) {
        $nameTabla[$this->prefix_table . 't_tipo_pauta'] = $this->prefix_table . "t_tipo_pauta";
        $campos['id_tipo_pauta'] = "id_tipo_pauta";
        $campos['descripcion'] = "descripcion";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectprograma($conect) {
        $nameTabla[$this->prefix_table . 't_programa'] = $this->prefix_table . "t_programa";
        $campos['id_program'] = "id_program";
        $campos['descripcion'] = "descripcion";
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selecttipoevento($conect) {
        $nameTabla[$this->prefix_table . 't_tipo_evento'] = $this->prefix_table . "t_tipo_evento";
        $campos['id_tipo_evento'] = "id_tipo_evento";
        $campos['descripcion'] = "descripcion";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selecttipotraje($conect) {
        $nameTabla[$this->prefix_table . 't_tipo_traje'] = $this->prefix_table . "t_tipo_traje";
        $campos['id_tipo_traje'] = "id_tipo_traje";
        $campos['descripcion'] = "descripcion";
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlocacion($conect) {
        $nameTabla[$this->prefix_table . 't_locacion'] = $this->prefix_table . "t_locacion";
        $campos['id_locacion'] = "id_locacion";
        $campos['descripcion'] = "descripcion";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdirector($conect) {
        $nameTabla[$this->prefix_table . 't_locacion'] = $this->prefix_table . "t_locacion";
        $campos['id_locacion'] = "id_locacion";
        $campos['descripcion'] = "descripcion";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcoordinador($conect) {
        $nameTabla[$this->prefix_table . 't_locacion'] = $this->prefix_table . "t_locacion";
        $campos['id_locacion'] = "id_locacion";
        $campos['descripcion'] = "descripcion";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }


//////////////////////////////insert//////////////////////////////////////////////

    function insertservicio_f5($conect, $descripcion, $id_tipo_servicio) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_servicio";
        // Campos para hacer el insert
        $campos['descripcion'] = $descripcion;
        $campos['id_tipo_servicio'] = $id_tipo_servicio;
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function inserttipotraje($conect, $descripcion) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_tipo_traje";
        // Campos para hacer el insert
        $campos['descripcion'] = $descripcion;
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertprograma($conect, $descripcion) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_programa";
        // Campos para hacer el insert
        $campos['descripcion'] = $descripcion;
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertevento($conect, $descripcion) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_evento";
        // Campos para hacer el insert
        $campos['descripcion'] = $descripcion;
        $campo_return = "id_evento";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

///////////////////////////agregar usuario///////////////////////////////////////

    function selectexiste($conect, $cedula) {
        $nameTabla['usuario.t_datos_personales'] = "usuario.t_datos_personales";
        $campos['cedula'] = "cedula";
        $condicion['cedula'] = $cedula;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    /*
     * revisa en tabla datos del usuario.
     */

    function selectdatosexiste($conect, $cedula) {
        $nameTabla['usuario.t_datos_personales'] = "usuario.t_datos_personales";
        $campos['correo'] = "correo";
        $campos['telefono1'] = "telefono1";
        $campos['telefono2'] = "telefono2";
        $condicion['cedula'] = $cedula;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectexisteaccesof5($conect, $cedula, $id_aplicacion) {
        $nameTabla['usuario.t_acceso'] = "usuario.t_acceso";
        $campos['cedula'] = "cedula";
        $condicion['cedula'] = "'" . $cedula . "'";
        $condicion['id_aplicacion'] = "'" . $id_aplicacion . "'";
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function updatecorreo($conect, $cedula, $correo) {
        // Tabla para hacer la consulta
        $nameTabla = 'usuario.t_datos_personales';
        $campos['correo'] = $correo;
        $condicion['cedula'] = $cedula;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatetlf1($conect, $cedula, $telefono1) {
        // Tabla para hacer la consulta
        $nameTabla = 'usuario.t_datos_personales';
        //$campos['telefono1']=$telefono1;
        if ($telefono1 != "") {
            $campos['telefono1'] = $telefono1;
        }
        $condicion['cedula'] = $cedula;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatetlf2($conect, $cedula, $telefono2) {
        // Tabla para hacer la consulta
        $nameTabla = 'usuario.t_datos_personales';
        //$campos['telefono2']=$telefono2;
        if ($telefono2 != "") {
            $campos['telefono2'] = $telefono2;
        }
        $condicion['cedula'] = $cedula;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    /**
    * funcion que selecciona a los usuarios del sistema.
    */
    function selectusuariof5($conect, $cedula) {
        $nameTabla[ $this->prefix_table .'usuarios'] =  $this->prefix_table ."usuarios";
        $campos['nombre_usuario'] = "nombre_usuario";
        $campos['apellido_usuario'] = "apellido_usuario";
        $campos['correo'] = "correo";
        $campos['desc_usuario']= "desc_usuario";
        $campos['de_gerencia']= "de_gerencia";
        $campos['de_division']= "de_division";

        $condicion['cedula'] = "'" . $cedula . "'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidtipousuariof5($conect, $cedula) {
        $nameTabla['usuario.t_acceso'] = "usuario.t_acceso";
        $campos['id_tipo_usuario'] = "id_tipo_usuario";
        $condicion['cedula'] = "'" . $cedula . "'";
        $condicion['id_aplicacion'] = 12;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selecttipousuariof5($conect, $idtipousuario) {
        $nameTabla['usuario.t_tipo_usuario'] = "usuario.t_tipo_usuario";
        $campos['desc_usuario'] = "desc_usuario";
        $condicion['id_tipo_usuario'] = "'" . $idtipousuario . "'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function insertusuarios($conect, $nombre, $apellido, $cedula, $correo, $telefono1, $telefono2) {
        // Nombre de la Tabla
        $nameTabla = "usuario.t_datos_personales";
        // Campos para hacer el insert
        $campos['nombre_usuario'] = $nombre;
        $campos['apellido_usuario'] = $apellido;
        $campos['cedula'] = $cedula;
        $campos['correo'] = $correo;
        if ($telefono1 != "") {
            $campos['telefono1'] = $telefono1;
        }
        if ($telefono2 != "") {
            $campos['telefono2'] = $telefono2;
        }
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertclaveusuarios($conect, $cedula, $clave1, $id_modulo, $ced_sesion, $tipousuario, $idgerencia, $iddivision) {
        // Nombre de la Tabla
        $nameTabla = "usuario.t_acceso";
        // Campos para hacer el insert
        $campos['cedula'] = $cedula;
        $campos['clave'] = $clave1;
        $campos['id_modulo'] = $id_modulo;
        $campos['id_aplicacion'] = 12;
        $campos['niv_con'] = 1;
        $campos['niv_eli '] = 1;
        $campos['niv_inc '] = 1;
        $campos['niv_mod '] = 1;
        $campos['ced_trans '] = $ced_sesion;
        $campos['fecha_trans '] = date("Y-m-d");
        $campos['id_tipo_usuario '] = $tipousuario;
        $campos['idgerencia'] = $idgerencia;
        $campos['iddivision'] = $iddivision;
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

///////////////////////eliminar usuario ////////////////////////////////////////

    function updateusuariof5($conect, $cedula, $id_aplicacion) {
        // Tabla para hacer la consulta
        $nameTabla = 'usuario.t_acceso';
        $campos['fecha_exp'] = date("Y-m-d");
        $condicion['cedula'] = $cedula;
        $condicion['id_aplicacion'] = $id_aplicacion;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatedelistatraje($conect, $id) {
        // Tabla para hacer la consulta
        $nameTabla = 'f5.t_tipo_traje';
        $campos['fecha_exp'] = date("Y-m-d");
        $condicion['id_tipo_traje'] = $id;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatedelistaprog($conect, $id) {
        // Tabla para hacer la consulta
        $nameTabla = 'f5.t_programa';
        $campos['fecha_exp'] = date("Y-m-d");
        $condicion['id_program'] = $id;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

///////////////////////cambiar perfil del usuario ////////////////////////////////////////

    function selectperfilactual($conect, $cedula) {
        $nameTabla['usuario.t_acceso'] = "usuario.t_acceso";
        $campos['id_tipo_usuario'] = "id_tipo_usuario";
        $condicion['cedula'] = "'" . $cedula . "'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

//////////////////////////reinicio y consulta de clave/////////////////////////////////

    function updateclavef5($conect, $cedula) {
        // Tabla para hacer la consulta
        $nameTabla = 'usuario.t_acceso';
        $campos['clave'] = 123456;
        $condicion['cedula'] = $cedula;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function selectclave($conect, $cedula) {
        $nameTabla['usuario.t_acceso'] = "usuario.t_acceso";
        $campos['clave'] = "clave";
        $condicion['cedula'] = "'" . $cedula . "'";
        $condicion['id_aplicacion'] = 12;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

////////////////////////////////////////////////////////////////////////////

    function updatecambiarperfil($conect, $cedula, $perfilact, $perfilmod) {
        // Tabla para hacer la consulta
        $nameTabla = 'usuario.t_acceso';
        $campos['id_tipo_usuario'] = $perfilmod;

        $condicion['cedula'] = $cedula;
        $condicion['id_tipo_usuario'] = $perfilact;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function selectdescperfil($conect, $perfilmod) {
        $nameTabla['usuario.t_tipo_usuario'] = "usuario.t_tipo_usuario";

        $campos['desc_usuario'] = "desc_usuario";

        $condicion['id_tipo_usuario'] = $perfilmod;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }
    /*
    function selectdescgerencia($conect, $perfilmod) {
        $nameTabla['usuario.t_tipo_usuario'] = "usuario.t_tipo_usuario";

        $campos['desc_gerencia'] = "desc_gerencia";

        $condicion['id_gerencia'] = $idgerencia;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }*/
////////////////////////////guardarpauta_f5/////////////////////////////////////

    function insertlugar($conect, $lugar) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_lugar";
        // Campos para hacer el insert
        $campos['descripcion'] = $lugar;

        $campo_return = "id_lugar";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function insertcitacion($conect, $fecha1, $hora1, $id_lugarcitacion) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_citacion";
        // Campos para hacer el insert
        $campos['fecha'] = $fecha1;
        $campos['hora'] = $hora1;
        $campos['id_lugar'] = $id_lugarcitacion;

        $campo_return = "id_citacion";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function insertmontaje($conect, $fecha2, $hora2, $id_lugarmontaje) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_montaje";
        // Campos para hacer el insert
        $campos['fecha'] = $fecha2;
        $campos['hora'] = $hora2;
        //$campos['id_lugar']=$id_lugarmontaje;

        $campo_return = "id_montaje";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function insertemision($conect, $fecha3, $hora3, $id_lugaremision) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_emision_grabacion";
        // Campos para hacer el insert
        $campos['fecha'] = $fecha3;
        $campos['hora'] = $hora3;
        //$campos['id_lugar']=$id_lugaremision;

        $campo_return = "id_emision_grabacion";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function insertretorno($conect, $fecha4, $hora4, $id_lugarretorno) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_retorno";
        // Campos para hacer el insert
        $campos['fecha'] = $fecha4;
        $campos['hora'] = $hora4;
        //$campos['id_lugar']=$id_lugarretorno;

        $campo_return = "id_retorno";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function insertdatospauta($conect, $ced_sesion, $pauta, $locacion, $tipotraje, $programa, $evento, $id_citacion, $id_montaje, $id_emision, $id_retorno, $tipoevento, $lugar_pauta, $des_evento) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_pauta";
        // Campos para hacer el insert
        $campos['fecha_reg'] = date("Y-m-d");
        $campos['user_reg'] = $ced_sesion;
        $campos['id_tipo_pauta'] = $pauta;
        $campos['id_locacion'] = $locacion;
        $campos['id_tipo_traje'] = $tipotraje;
        $campos['id_program'] = $programa;
        $campos['id_citacion'] = $id_citacion;
        $campos['id_montaje'] = $id_montaje;
        $campos['id_emision_grabacion'] = $id_emision;
        $campos['id_retorno'] = $id_retorno;
        $campos['id_tipo_evento'] = $tipoevento;
        $campos['lugar'] = strtoupper($lugar_pauta);
        $campos['descripcion'] = strtoupper($des_evento);

        $campo_return = "id_pauta";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function insertestatus($conect, $id_pauta, $estatus, $cedula, $observaciones=null) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_pauta";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = $estatus;
        $campos['user_reg'] = $cedula;
        if($observaciones!=null){
            $campos['observaciones'] = $observaciones;
        }
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

/////////////////////////////Mostrar Pauta////////////////////////////////////////////////


    function selectservicios($conect) {
        $nameTabla['inventario.materialtipomaterialinv'] = "inventario.materialtipomaterialinv";
        $campos['idtipomaterial'] = "idtipomaterial";
        $campos['valorinventariado'] = "valorinventariado";
        $campos['desctipomaterial'] = "desctipomaterial";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidspauta($conect, $pauta) {
        $nameTabla[$this->prefix_table . 't_pauta'] = $this->prefix_table . "t_pauta";
        $campos['id_pauta'] = "id_pauta";
        $campos['id_tipo_pauta'] = "id_tipo_pauta";
        $campos['id_locacion'] = "id_locacion";
        $campos['id_tipo_traje'] = "id_tipo_traje";
        $campos['id_program'] = "id_program";
        $campos['id_citacion'] = "id_citacion";
        $campos['id_montaje'] = "id_montaje";
        $campos['id_emision_grabacion'] = "id_emision_grabacion";
        $campos['id_retorno'] = "id_retorno";
        $campos['id_tipo_evento'] = "id_tipo_evento";
        $campos['user_reg'] = "user_reg";
        $campos['lugar'] = "lugar";
        $campos['descripcion'] = "descripcion";
        $condicion['id_pauta'] = $pauta;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function updatecitacion($conect, $id_citacion, $fecha, $hora, $id_lugar) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_citacion';
        $campos['fecha'] = $fecha;
        $campos['hora'] = $hora;
        $campos['id_lugar'] = $id_lugar;

        $condicion['id_citacion'] = $id_citacion;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updateemision($conect, $id_emision_grabacion, $fecha, $hora) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_emision_grabacion';
        $campos['fecha'] = $fecha;
        $campos['hora'] = $hora;
        $condicion['id_emision_grabacion'] = $id_emision_grabacion;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatemontaje($conect, $id_montaje, $fecha, $hora) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_montaje';
        $campos['fecha'] = $fecha;
        $campos['hora'] = $hora;
        $condicion['id_montaje'] = $id_montaje;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updateretorno($conect, $id_retorno, $fecha, $hora) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_retorno';
        $campos['fecha'] = $fecha;
        $campos['hora'] = $hora;
        $condicion['id_retorno'] = $id_retorno;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatepauta($conect, $id_pauta, $id_tipo_pauta, $id_locacion, $id_tipo_traje, $id_program, $id_tipo_evento, $lugar, $descripcion) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_pauta';
        $campos['id_tipo_pauta'] = $id_tipo_pauta;
        $campos['id_locacion'] = $id_locacion;
        $campos['id_tipo_traje'] = $id_tipo_traje;
        $campos['id_program'] = $id_program;
        $campos['id_tipo_evento'] = $id_tipo_evento;
        $campos['lugar'] = strtoupper($lugar);
        $campos['descripcion'] = strtoupper($descripcion);
        $condicion['id_pauta'] = $id_pauta;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function selectdescprograma($conect, $id_program) {
        $nameTabla[$this->prefix_table . 't_programa'] = $this->prefix_table . "t_programa";

        $campos['id_program'] = "id_program";
        $campos['descripcion'] = "descripcion";
        $condicion['id_program'] = $id_program;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, null, $this->groupby, $this->limite);
    }

    function selectdesclocacion($conect, $id_locacion) {
        $nameTabla[$this->prefix_table . 't_locacion'] = $this->prefix_table . "t_locacion";

        $campos['id_locacion'] = "id_locacion";
        $campos['descripcion'] = "descripcion";
        $condicion['id_locacion'] = $id_locacion;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdesctipotraje($conect, $id_tipo_traje) {
        $nameTabla[$this->prefix_table . 't_tipo_traje'] = $this->prefix_table . "t_tipo_traje";

        $campos['descripcion'] = "descripcion";
        $condicion['id_tipo_traje'] = $id_tipo_traje;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdescproductor($conect, $cedula) {
        $nameTabla['sigai.grlpersona'] = "sigai.grlpersona";

        $campos['de_corto'] = "de_corto";
        $condicion['nu_documen'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectnombreyapellido($conect, $cedula) {
        $nameTabla['sigai.grlpersona'] = "sigai.grlpersona";

        $campos['de_corto'] = "de_corto";
        $condicion['nu_documen'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectnombreyapellidof5($conect, $cedula) {
        $nameTabla['usuario.t_datos_personales'] = "usuario.t_datos_personales";

        $campos['nombre_usuario'] = "nombre_usuario";
        $campos['apellido_usuario'] = "apellido_usuario";
        $condicion['cedula'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcargoquienaprobo($conect, $cedula) {
        $nameTabla['sigai.datos_trabajador'] = "sigai.datos_trabajador";

        $campos['de_cargo'] = "de_cargo";
        $condicion['nu_documen'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdesctipoevento($conect, $id_tipo_evento) {
        $nameTabla[$this->prefix_table . 't_tipo_evento'] = $this->prefix_table . "t_tipo_evento";

        $campos['id_tipo_evento'] = "id_tipo_evento";
        $campos['descripcion'] = "descripcion";
        $condicion['id_tipo_evento'] = $id_tipo_evento;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcitacion($conect, $id_citacion) {
        $nameTabla[$this->prefix_table . 't_citacion'] = $this->prefix_table . "t_citacion";

        $campos['id_citacion'] = "id_citacion";
        $campos['fecha'] = "fecha";
        $campos['hora'] = "hora";
        $campos['id_lugar'] = "id_lugar";
        $condicion['id_citacion'] = $id_citacion;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectemision($conect, $id_emision_grabacion) {
        $nameTabla[$this->prefix_table . 't_emision_grabacion'] = $this->prefix_table . "t_emision_grabacion";

        $campos['id_emision_grabacion'] = "id_emision_grabacion";
        $campos['fecha'] = "fecha";
        $campos['hora'] = "hora";
        //$campos['id_lugar']="id_lugar";
        $condicion['id_emision_grabacion'] = $id_emision_grabacion;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectmontaje($conect, $id_montaje) {
        $nameTabla[$this->prefix_table . 't_montaje'] = $this->prefix_table . "t_montaje";

        $campos['id_montaje'] = "id_montaje";
        $campos['fecha'] = "fecha";
        $campos['hora'] = "hora";
        //$campos['id_lugar']="id_lugar";
        $condicion['id_montaje'] = $id_montaje;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectretorno($conect, $id_retorno) {
        $nameTabla[$this->prefix_table . 't_retorno'] = $this->prefix_table . "t_retorno";

        $campos['id_retorno'] = "id_retorno";
        $campos['fecha'] = "fecha";
        $campos['hora'] = "hora";
        //$campos['id_lugar']="id_lugar";
        $condicion['id_retorno'] = $id_retorno;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlugar($conect, $id_lugar) {
        $nameTabla[$this->prefix_table . 't_lugar'] = $this->prefix_table . "t_lugar";

        $campos['id_lugar'] = "id_lugar";
        $campos['descripcion'] = "descripcion";
        $condicion['id_lugar'] = $id_lugar;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlugar2($conect, $citacion="") {
        $nameTabla['f5.t_lugar'] = "f5.t_lugar";

        $campos['id_lugar'] = "id_lugar";
        $campos['descripcion'] = "descripcion";
        if ($citacion != "") {
            $condicion['id_lugar <'] = 2;
        } else {
            $condicion['id_lugar >'] = 3;
        }

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

/////////////////////////////////////////////////////////////////////////////////////////
    function selectdatospauta($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 'sys_main'] = $this->prefix_table . "sys_main";

        $campos['tipo_pauta'] = "tipo_pauta";
        $campos['nombre_programa'] = "nombre_programa";
        $campos['descripcion'] = "descripcion";
        $campos['productor'] = "productor";
        $campos['pauta_locacion'] = "pauta_locacion";
        $campos['tipo_evento'] = "tipo_evento";
        $campos['pauta_traje'] = "pauta_traje";
        $campos['citacion_lugar'] = "citacion_lugar";
        $campos['fecha_citacion'] = "fecha_citacion";
        $campos['hora_citacion'] = "hora_citacion";
        $campos['fecha_montaje'] = "fecha_montaje";
        $campos['hora_montaje'] = "hora_montaje";
        $campos['fecha_emision'] = "fecha_emision";
        $campos['hora_emision'] = "hora_emision";
        $campos['fecha_retorno'] = "fecha_retorno";
        $campos['hora_retorno'] = "hora_retorno";
        $campos['lugar'] = "lugar";


        $condicion['id_pauta'] = $id_pauta;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

/////////////////////////////consulta de estatus/////////////////////////////////////////

    function selectestatus($conect, $pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_pauta'] = $this->prefix_table . "t_estatus_pauta";

        $campos['id_estatus'] = "id_estatus";
        $campos['id_estatus_pauta'] = "id_estatus_pauta";

        $condicion['id_pauta'] = $pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdescestatus($conect, $estatus) {
        $nameTabla[$this->prefix_table . 't_estatus'] = $this->prefix_table . "t_estatus";

        $campos['descripcion'] = "descripcion";
        $condicion['id_estatus'] = $estatus;


        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistadepautas($conect, $cedula_sesion, $limiteinicio=0, $limitefin=null) {
        $nameTabla[$this->prefix_table . 't_pauta'] = $this->prefix_table . "t_pauta";
        $campos['id_pauta'] = "id_pauta";
        $condicion['user_reg'] = "'" . $cedula_sesion . "'";
        $orderby['id_pauta'] = "id_pauta DESC";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        if ($limitefin != null) {
            $limite['INICIO'] = $limiteinicio;
            $limite['FIN'] = $limitefin;
        }
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $limite);
    }

    function selectdescpauta($conect, $id) {
        $nameTabla[$this->prefix_table . 't_pauta'] = $this->prefix_table . "t_pauta";

        $campos['id_program'] = "id_program";
        $campos['id_evento'] = "id_evento";
        $condicion['id_pauta'] = $id;


        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectestatuspauta($conect, $id) {
        $nameTabla[$this->prefix_table . 't_estatus_pauta'] = $this->prefix_table . "t_estatus_pauta";

        $campos['id_estatus'] = "id_estatus";
        $condicion['id_pauta'] = $id;


        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectnomevento($conect, $evento) {
        $nameTabla[$this->prefix_table . 't_evento'] = $this->prefix_table . "t_evento";

        $campos['descripcion'] = "descripcion";
        $condicion['id_evento'] = $evento;


        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectnomprog($conect, $prog) {
        $nameTabla[$this->prefix_table . 't_programa'] = $this->prefix_table . "t_programa";

        $campos['descripcion'] = "descripcion";
        $condicion['id_program'] = $prog;


        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectnomestatus($conect, $est) {
        $nameTabla[$this->prefix_table . 't_estatus'] = $this->prefix_table . "t_estatus";

        $campos['descripcion'] = "descripcion";
        $condicion['id_estatus'] = $est;


        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

////////////////////////////Materiales///////////////////////////////////////////////

    /**
     *
     * @param type $conect ruta de conexion a la base de datos..
     * @param type $material el id del material
     * @param type $id_pauta el id de la pauta
     * @param type $ced_sesion la cedula de la persona que ejecuta la session
     * @param type $id_recurso_asignado id del recurso seleccionado
     * @return type id que retorna la insercion del query
     */
    function insertmaterial($conect, $id_recurso_asignado, $id_pauta, $ced_sesion,$id_tipo_rec, $material=null,$id_gerencia,$id_division) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_detalle_servicio";
        // Campos para hacer el insert
        $campos['id_recurso'] = $material;
        $campos['id_pauta'] = $id_pauta;
        $campos['id_tipo_recurso'] = $id_tipo_rec;
        $campos['id_gerencia'] = $id_gerencia;
        $campos['id_division'] = $id_division;
        $campos['fecha_reg'] = date("Y-m-d");
        $campos['user_reg'] = $ced_sesion;
        //permite agregar tambien recursos asignados....
        if ($id_recurso_asignado != null) {
            $campos['id_recurso_asignado'] = $id_recurso_asignado;
        }

        $campo_return = "id_detalle_servicio";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function insertpendiente($conect,$id_pauta,$id_articulo) {
        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_solicitudes_f5";
        // Campos para hacer el insert
        $campos['id_articulo'] = $id_articulo;
        $campos['id_pauta'] = $id_pauta;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos, $campo_return);
    }

    function selectlistmat($conect, $pauta) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['id_recurso'] = "id_recurso";

        //$campos['cantidad']="cantidad";
        $condicion['id_pauta'] = $pauta;
        $condicion['id_recurso_asignado'] = "'1'";
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmat_group($conect, $pauta=null, $id_recurso=null) {
        $nameTabla[$this->prefix_table . 'selectlistmat_group'] = $this->prefix_table . "selectlistmat_group";

        $campos['ids_detalles_servicios'] = "ids_detalles_servicios";
        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad']="cantidad";

        if($pauta!=null){
            $condicion['id_pauta'] = $pauta;
        }

        if($id_recurso!=null){
            $condicion['id_recurso'] = $id_recurso;
        }
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }


    function selectlistmatdesc($conect, $id_recurso) {
        $nameTabla['inventario.materialtipomaterialinv'] = "inventario.materialtipomaterialinv";
        $campos['desctipomaterial'] = "desctipomaterial";
        $condicion['idtipomaterial'] = $id_recurso;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatdescnuevo($conect, $id_recurso) {
        $nameTabla['almacen_tecnico.t_descripciones'] = "almacen_tecnico.t_descripciones";
        $campos['descripcion'] = "descripcion";

        $condicion['id_descripcion'] = $id_recurso;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdetalles($conect, $id_recurso) {
        $nameTabla['inventario.inventariostok'] = "inventario.inventariostok";

        $campos['desctipomaterial'] = "desctipomaterial";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";

        $condicion['idtipomaterial'] = $id_recurso;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatger($conect, $id_recurso) {
        $nameTabla['inventario.inventariostok'] = "inventario.inventariostok";

        $campos['id_gerencia'] = "id_gerencia";

        $condicion['idtipomaterial'] = $id_recurso;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    //borra un recurso de la lista de recursos pedidos.
    function updatedelista($conect, $id_detalle_servicio, $cedula_sesion) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_detalle_servicio';
        $campos['fecha_exp'] = date("Y-m-d");
        $campos['user_exp'] = $cedula_sesion;


        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $condicion['fecha_exp'] = '2222-12-31';

        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatedelistaasignados($conect, $id_detalle_servicio, $caracteristicas, $cedula=null, $id_gerencia=null, $id_division=null) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_detalle_servicio';
        $campos[' id_recurso_asignado'] = "$caracteristicas";
        if($cedula!=null){
            $campos['user_reg'] = "$cedula";
        }

        if($id_gerencia!=null){
            $campos['id_gerencia'] = $id_gerencia;
        }

        if($id_division!=null){
            $campos['id_division'] = $id_division;
        }


        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $condicion['fecha_exp'] = '2222-12-31';

        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updateestalmtec($conect, $caracteristica, $estatus) {
        // Tabla para hacer la consulta
        $nameTabla='almacen_tecnico.t_articulo';
        $campos['id_estatus'] = $estatus;

        $condicion['id_articulo'] = $caracteristica;
        $condicion['fecha_exp'] = '2222-12-31';

        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatesolicitudes($conect, $id_pauta, $id_recurso_asignado) {
        // Tabla para hacer la consulta
        $nameTabla='almacen_tecnico.t_solicitudes_f5';
        $campos['fecha_exp'] = date("Y-m-d");

        $condicion['id_articulo'] = $id_recurso_asignado;
        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = '2222-12-31';

        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }
///////////////////////cambio de estatus de los recursos/////////////////////////////////////


    function insertestatusrecurso($conect, $id_pauta, $id_detalle_servicio, $id_estatus, $cedula_sesion, $id_recurso_asignado) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_detalle_servicio";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = $id_estatus;
        $campos['id_detalle_servicio'] = $id_detalle_servicio;
        $campos['id_recurso_asignado'] = $id_recurso_asignado;
        $campos['user_exp'] = $cedula_sesion;


        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function updatesolalmtecf5($conect, $id_pauta, $id_prestamo, $id_articulo) {

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_solicitudes_f5";
        // Campos para hacer el insert
        $campos['id_prestamo'] = $id_prestamo;

        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = '2222-12-31';
        $condicion['id_articulo'] = $id_articulo;
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos,$condicion);
    }

    function updatestart($conect,$id_articulo,$estatus) {

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_articulo";
        // Campos para hacer el insert
        $campos['id_estatus'] = $estatus;

        $condicion['id_articulo'] = $id_articulo;
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos,$condicion);
    }


    function insertsolalmtec($conect, $id_destino,$id_desc_destino,
            $fecha_reg,$id_estatus,$observaciones,$tipo_prestamo,$hora_reg,$resp_prestamo,$fecha_sol,
            $hora_prestamo,$user_reg,$id_remoto) {

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_prestamo";
        // Campos para hacer el insert
        $campos['id_destino'] = $id_destino;
        $campos['id_desc_dest'] = $id_desc_destino;
        $campos['fecha_reg'] = $fecha_reg;
        $campos['id_estatus_prestamo'] = $id_estatus;
        $campos['observacion'] = $observaciones;
        $campos['tipo_prestamo'] = $tipo_prestamo;
        $campos['hora_reg'] = $hora_reg;
        $campos['resp_prestamo'] = $resp_prestamo;
        $campos['fecha_sol'] = $fecha_sol;
        $campos['hora_prestamo'] = $hora_prestamo;
        $campos['user_reg'] = $user_reg;
        $campos['id_remoto'] = $id_remoto;


        $campo_return = "id_prestamo";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function insertmatalmtec($conect, $id_prestamo,$id_recurso,$fecha_sol,$user_reg) {

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_material_sol";
        // Campos para hacer el insert
        $campos['id_solicitud'] = $id_prestamo;
        $campos['id_articulo'] = $id_recurso;
        $campos['fecha_sol'] = $fecha_sol;
        $campos['user_reg'] = $user_reg;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function selectverrecursoasignado($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['id_division'] = "id_division";


        $condicion['id_detalle_servicio'] = $id_detalle_servicio;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatcit($conect, $id_citacion) {
        $nameTabla[$this->prefix_table . 't_citacion'] = $this->prefix_table . "t_citacion";

        $campos['fecha'] = "fecha";
        $campos['hora'] = "hora";

        $condicion['id_citacion'] = $id_citacion;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectresppauta($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_pauta'] = $this->prefix_table . "t_pauta";

        $campos['user_reg'] = "user_reg";
        $campos['id_citacion'] = "id_citacion";

        $condicion['id_pauta'] = $id_pauta;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

/////////////////////////////mostrar recursos////////////////////////////////////////////////

    function updatematerialasignado($conect, $id_pauta, $id_detalle, $caracteristicas) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_detalle_servicio';
        $campos['id_recurso_asignado'] = "caracteristicas";

        $condicion['id_pauta'] = $id_pauta;
        $condicion['id_detalle_servicio'] = $id_detalle;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function selectmostrarrecursosger($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'recur_val_ger'] = $this->prefix_table . "recur_val_ger";

        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";

        //$campos['cantidad']="cantidad";
        $condicion['id_pauta'] = $pauta;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    //funcion que muestra los recursos del productor... status 13
    function selectmostrarrecursos($conect, $pauta, $t_inventario="2") {
        $nameTabla[$this->prefix_table . 'recur_val_prodjefe'] = $this->prefix_table . "recur_val_prodjefe";
        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";

        $condicion['id_pauta'] = $pauta;
        $condicion['id_fintipoinventario'] = $t_inventario;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    //funcion que muestra los datos agregados por el jefe de area  status 20
    function selectmostrarrecursosjef_jef($conect, $pauta, $t_inventario="2") {
        $nameTabla[$this->prefix_table . 'recur_val_jefejefe'] = $this->prefix_table . "recur_val_jefejefe";
        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";

        $condicion['id_pauta'] = $pauta;
        $condicion['id_fintipoinventario'] = $t_inventario;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectmostrarrecursosual($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'recur_val_ual'] = $this->prefix_table . "recur_val_ual";
        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";
        $condicion['id_pauta'] = $pauta;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectrecursosvalidados($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'recur_val_ger'] = $this->prefix_table . "recur_val_ger";
        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";
        $condicion['id_pauta'] = $pauta;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistdeeliminados($conect, $pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";
        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['user_exp'] = "user_exp";

        $condicion['id_pauta'] = $pauta;
        $condicion['id_estatus'] = 16; // eliminados por la ual,solo este estatus por que son las modificaciones de la ual.
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }
    function selectlistdeeliminadosual($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'recur_elim_ual'] = $this->prefix_table . "recur_elim_ual";
        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";


        $condicion['id_pauta'] = $pauta;
        //$condicion['id_estatus'] = 16; // eliminados por la ual,solo este estatus por que son las modificaciones de la ual.
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistaeliminados($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";
        $campos['id_recurso'] = "id_recurso";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";
        //$campos['cantidad']="cantidad";
        $condicion['id_detalle_servicio'] = $id_detalle_servicio;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

///////////////////////asignar recursos////////////////////////////////////////////////


    function selectcaracteristicas($conect, $id_recurso) {
        $nameTabla['inventario.inventariostok'] = "inventario.inventariostok";

        $campos['idmaterialgeneral'] = "idmaterialgeneral";
        $campos['inventario_valor'] = "inventario_valor";

        //$campos['cantidad']="cantidad";
        $condicion['idtipomaterial'] = $id_recurso;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatoscedula($conect, $idmaterialgeneral) {
        $nameTabla['inventario.vistageneral_inventario_f5'] = "inventario.vistageneral_inventario_f5";
        $campos['valorinventariado'] = "valorinventariado";
        $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        $condicion['idcaracteristica'] = 27;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatosrecasig($conect, $id_rec_asig) {
        $nameTabla['almacen_tecnico.t_articulo'] = "almacen_tecnico.t_articulo";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['bien_nac'] = "bien_nac";


        $condicion['id_articulo'] = $id_rec_asig;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }



    function selectdatospnombre($conect, $idmaterialgeneral) {
        $nameTabla['inventario.vistageneral_inventario_f5'] = "inventario.vistageneral_inventario_f5";


        $campos['valorinventariado'] = "valorinventariado";

        $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        $condicion['idcaracteristica'] = 23;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatospapellido($conect, $idmaterialgeneral) {
        $nameTabla['inventario.vistageneral_inventario_f5'] = "inventario.vistageneral_inventario_f5";


        $campos['valorinventariado'] = "valorinventariado";

        $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        $condicion['idcaracteristica'] = 25;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatosserial($conect, $idmaterialgeneral) {
        $nameTabla['inventario.vistageneral_inventario_f5'] = "inventario.vistageneral_inventario_f5";


        $campos['valorinventariado'] = "valorinventariado";

        $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        $condicion['idcaracteristica'] = 5;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatosmarca($conect, $idmaterialgeneral) {
        $nameTabla['inventario.vistageneral_inventario_f5'] = "inventario.vistageneral_inventario_f5";


        $campos['valorinventariado'] = "valorinventariado";

        $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        $condicion['idcaracteristica'] = 3;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatosagregados($conect, $idmaterialgeneral) {
        $nameTabla['inventario.vistageneral_inventario_f5'] = "inventario.vistageneral_inventario_f5";


        $campos['valorinventariado'] = "valorinventariado";

        $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        $condicion['idcaracteristica'] = 19;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatosbiennac($conect, $idmaterialgeneral) {
        $nameTabla['inventario.vistageneral_inventario_f5'] = "inventario.vistageneral_inventario_f5";


        $campos['valorinventariado'] = "valorinventariado";

        $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        $condicion['idcaracteristica'] = 4;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcaracteristicas2($conect, $id_recurso) {
        $nameTabla['inventario.inventariostok'] = "inventario.inventariostok";

        $campos['idmaterialgeneral'] = "idmaterialgeneral";
        $campos['inventario_valor'] = "inventario_valor";

        //$campos['cantidad']="cantidad";
        $condicion['idmaterialgeneral'] = $id_recurso;
        $orderby['inventario_valor'] = "inventario_valor ASC";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    /**
     *
     * @param type $conect para pasar el archivo de conexion de la base de datos
     * @param type $pauta para pasar el idpauta
     * @param type $id_recurso para pasar el tipo de recurso del inventario
     * @return type query //resultado de la base de datos.
     */

    function selectrecsinasig($conect,$pauta){
        $nameTabla[$this->prefix_table . 'recur_sin_asig'] = $this->prefix_table . "recur_sin_asig";

        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";

        $condicion['id_pauta'] = $pauta;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);


    }
    function selectlistmatnoasig($conect, $pauta, $id_recurso=null) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['id_recurso'] = "id_recurso";

        $condicion['id_pauta'] = $pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $condicion['id_recurso_asignado'] = "'1'";

        //filtar tambien por id_recurso.
        if ($id_recurso != null) {
            $condicion['id_recurso'] = $id_recurso;
        }

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectrecnoasig($conect, $pauta, $id_recurso=null) {
        $nameTabla[$this->prefix_table . 'recur_sin_asig'] = $this->prefix_table . "recur_sin_asig";

        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";

        $condicion['id_pauta'] = $pauta;

        //filtar tambien por id_recurso.
        if ($id_recurso != null) {
            $condicion['id_recurso'] = $id_recurso;
        }

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }


    function selectlistmatasig($conect, $pauta=null, $id_recurso_asignado=null) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_recurso'] = "id_recurso";
        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";

        if ($pauta != null) {
            $condicion['id_pauta'] = $pauta;
        }

        if ($id_recurso_asignado == null) {
            $condicion['id_recurso_asignado >'] = 2;
        } else {
            $condicion['id_recurso_asignado'] = $id_recurso_asignado;
        }

        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatasignuevo($conect, $pauta=null, $id_recurso_asignado=null) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_recurso'] = "id_recurso";
        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";

        if ($pauta != null) {
            $condicion['id_pauta'] = $pauta;
        }

        if ($id_recurso_asignado == null) {
            $condicion['id_recurso_asignado >'] = 2;
        } else {
            $condicion['id_recurso_asignado'] = $id_recurso_asignado;
        }

        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function select_recursoasignadonoterminado($conect, $pauta=null, $id_recurso_asignado=null){
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $condicion['id_recurso_asignado'] = $id_recurso_asignado;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $condicion['id_estatus!']="35 AND id_estatus!=16 AND id_estatus!=15 AND id_estatus!=33";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function rrhh_personal_asignado($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'rrhh_asignados_pauta'] = $this->prefix_table . "rrhh_asignados_pauta";
        /*
        SELECT
        t_detalle_servicio.id_detalle_servicio AS serial,
        t_detalle_servicio.id_pauta,
        t_detalle_servicio.fecha_reg,
        t_detalle_servicio.user_reg,
        t_detalle_servicio.fecha_exp,
        t_detalle_servicio.user_exp,
        t_detalle_servicio.id_recurso,
        t_detalle_servicio.id_recurso_asignado,
        t_detalle_servicio.id_tipo_recurso
        */
        $campos['serial'] = "serial";
        $campos['id_pauta'] = "id_pauta";
        $campos['id_recurso'] = "id_recurso";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";

        //$campos['cantidad']="cantidad";
        $condicion['id_pauta'] = $pauta;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }


    function invmaterial_asignados_pauta($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'invmaterial_asignados_pauta'] = $this->prefix_table . "invmaterial_asignados_pauta";
        /*
        SELECT
        t_detalle_servicio.id_detalle_servicio AS serial,
        t_detalle_servicio.id_pauta,
        t_detalle_servicio.fecha_reg,
        t_detalle_servicio.user_reg,
        t_detalle_servicio.fecha_exp,
        t_detalle_servicio.user_exp,
        t_detalle_servicio.id_recurso,
        t_detalle_servicio.id_recurso_asignado,
        t_detalle_servicio.id_tipo_recurso
        */
        $campos['serial'] = "serial";
        $campos['id_pauta'] = "id_pauta";
        $campos['id_recurso'] = "id_recurso";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";

        //$campos['cantidad']="cantidad";
        $condicion['id_pauta'] = $pauta;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }




    function selectlistmatasignados($conect, $pauta, $gerencia, $iddivision=null) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_recurso'] = "id_recurso";
        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";

        $condicion['id_pauta'] = $pauta;
        if($gerencia!=null){
            $condicion['id_gerencia'] = $gerencia;
        }

        if($iddivision!=null){
            $condicion['id_division'] = $iddivision;
        }


        //que pasa cuando areas operativas modifican un recurso y le ponen valor 1 al recurso asignado..
        //$condicion['id_recurso_asignado >'] = 2;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistreccambiados($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";

        $condicion['id_pauta'] = $id_pauta;
        $condicion['(id_estatus'] = "11 or id_estatus=13 or id_estatus=17 or id_estatus=20) ";
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatasig2($conect, $pauta) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_recurso'] = "id_recurso";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";

        $condicion['id_pauta'] = $pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatcambiado($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_recurso'] = "id_recurso";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";

        $condicion['id_detalle_servicio'] = $id_detalle_servicio;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistnojust($conect, $id_detalle_servicio, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";
        $campos['id_estatus'] = "id_estatus";
        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatosrec($conect, $pauta) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";
        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        //$campos['cantidad']="cantidad";
        $condicion['id_pauta'] = $pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectrecsol($conect, $pauta) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";
        $campos['id_recurso'] = "id_recurso";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";
        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        //$campos['cantidad']="cantidad";

        $condicion['id_pauta'] = $pauta;
        $condicion['id_recurso_asignado'] = 1;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();

        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectrecsolporest($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'rec_por_estatus_cant'] = $this->prefix_table . "rec_por_estatus_cant";
        $campos['id_recurso'] = "id_recurso";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";
        $campos['cantidad']="cantidad";

        $condicion="id_pauta=".$pauta;
        //$condicion['id_estatus'] = $estatus;
        //$condicion['fecha_exp'] = "'2222-12-31'";
        $condicion.= " and id_estatus in (13,20) ";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectrecsolind($conect, $pauta, $id_recurso) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";
        $campos['id_recurso'] = "id_recurso";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";
        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        //$campos['cantidad']="cantidad";

        $condicion['id_pauta'] = $pauta;
        $condicion['id_recurso'] = $id_recurso;
        $condicion['id_recurso_asignado'] = 1;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }


    function selecttipo_rec($conect, $pauta,$id_recurso) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";
        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        //$campos['cantidad']="cantidad";

        $condicion['id_pauta'] = $pauta;
        $condicion['id_recurso'] = $id_recurso;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectrecsolporcant($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'cantidades_recursos'] = $this->prefix_table . "cantidades_recursos";
        $campos['id_recurso']="id_recurso";
        $campos['cantidad']="cantidad";

        $condicion['id_pauta'] = $pauta;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcaractrec($conect, $id_descripcion) {
        $nameTabla['almacen_tecnico.t_articulo'] = "almacen_tecnico.t_articulo";
        $campos['id_articulo'] = "id_articulo";
        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['bien_nac'] = "bien_nac";

        //$campos['cantidad']="cantidad";
        $condicion['id_descripcion'] = $id_descripcion;
        $condicion['id_estatus'] = 1;
        $condicion['id_grupo'] = "'GP1'";
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdesctipo($conect, $id_descripcion) {
        $nameTabla['almacen_tecnico.t_descripciones'] = "almacen_tecnico.t_descripciones";
        $campos['descripcion'] = "descripcion";

        $condicion['id_descripcion'] = $id_descripcion;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

/////////////////////reportes///////////////////////////////////////////
    function selectcontpautas($conect, $fechaini, $fechafin) {
        $nameTabla[$this->prefix_table . 'pautas_por_fechas'] = $this->prefix_table . "pautas_por_fechas";
        $campos['cantidad'] = "cantidad";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectrecursopersonal($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'datos_recurso_inv'] = $this->prefix_table . "datos_recurso_inv";
        $campos['id_recurso'] = "id_recurso";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $condicion['id_pauta'] = $pauta;
        $condicion['id_fintipoinventario'] = 1;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectrecursomaterial($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'datos_recurso_inv'] = $this->prefix_table . "datos_recurso_inv";
        $campos['id_recurso'] = "id_recurso";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $condicion['id_pauta'] = $pauta;
        $condicion['id_fintipoinventario'] = 2;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectrecurso($conect, $pauta, $id_recurso_asignado="1"){
        $nameTabla[$this->prefix_table . 'datos_recurso_inv'] = $this->prefix_table . "datos_recurso_inv";
        $campos['id_recurso'] = "id_recurso";
        $campos['desctipomaterial'] = 'desctipomaterial';
        $campos['id_detalle_servicio'] = 'id_detalle_servicio';
        $condicion['id_pauta'] = $pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $condicion['id_recurso_asignado'] = $id_recurso_asignado;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistdeasig($conect, $pauta, $estatus) {
        $nameTabla[$this->prefix_table . 'recur_asig'] = $this->prefix_table . "recur_asig";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['user_exp'] = "user_exp";

        $condicion['id_pauta'] = $pauta;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatprod($conect, $pauta, $estatus) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['user_exp'] = "user_exp";

        $condicion['id_pauta']=$pauta;
        $condicion['id_estatus']=$estatus;


        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatsolicitados($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'cont_recur_por_estatus_sol'] = $this->prefix_table . "cont_recur_por_estatus_sol";

        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";
        $condicion['id_pauta']=$pauta;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatsolicitadosual($conect, $pauta, $estatus) {
        $nameTabla[$this->prefix_table . 'cont_recur_por_estatus'] = $this->prefix_table . "cont_recur_por_estatus";

        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";
        $condicion['id_pauta']=$pauta;
        $condicion['id_estatus']=$estatus;


        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }



    function selectlistmatact($conect, $pauta, $estatus) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['fecha_reg'] = "fecha_reg";

        //$campos['cantidad']="cantidad";
        $condicion['id_pauta'] = $pauta;
        $condicion['id_estatus'] = $estatus;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatactrep($conect, $pauta, $estatus) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['user_exp'] = "user_exp";

        //$campos['cantidad']="cantidad";
        $condicion['id_pauta'] = $pauta;
        $condicion['id_estatus'] = $estatus;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidsmat($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_recurso'] = "id_recurso";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";

        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidsmatelim($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_recurso'] = "id_recurso";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";

        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $condicion['fecha_exp'] != "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidgerencia($conect, $id_recurso, $id_gerencia=null) {
        $nameTabla[$this->prefix_table . 'lista_recursos_por_gerencia'] = $this->prefix_table . "lista_recursos_por_gerencia";

        $campos['de_gerencia'] = "de_gerencia";
        $condicion['id_recurso'] = $id_recurso;

        if($id_gerencia!=null){
            $condicion['id_gerencia'] = $id_gerencia;
        }

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidmat($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_recurso'] = "id_recurso";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";



        $condicion['id_detalle_servicio'] = $id_detalle_servicio;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    /*
    * funcion para seleccionar gerencia y divisiones
    */
    function selectgerendiv($conect, $id_recurso) {
        $nameTabla[$this->prefix_table . 'rrhh_personal'] = $this->prefix_table . "rrhh_personal";

        $campos['id_gerencia'] = "id_gerencia";
        $campos['id_division'] = "id_division";

        $condicion['cedula'] = $id_recurso;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }



    function selectquienaprobo($conect, $id_pauta, $id_estatus) {
        $nameTabla[$this->prefix_table . 't_estatus_pauta'] = $this->prefix_table . "t_estatus_pauta";

        $campos['user_reg'] = "user_reg";

        $condicion['id_estatus'] = $id_estatus;
        $condicion['id_pauta'] = $id_pauta;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectquienasigno($conect, $id_pauta, $id_recurso_asignado) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";

        $campos['user_exp'] = "user_exp";
        $campos['fecha_reg'] = "fecha_reg";


        $condicion['id_recurso_asignado'] = $id_recurso_asignado;
        $condicion['id_pauta'] = $id_pauta;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function fechaestatusanterior($conect,$id_pauta) {
        $nameTabla['f5.t_estatus_pauta'] = "f5.t_estatus_pauta";

        $campos['id_estatus_pauta'] = "id_estatus_pauta";
        $campos['fecha_exp'] = "fecha_exp";
        $campos['id_estatus'] = "id_estatus";

        $condicion['id_pauta'] = 60;
        $orderby['id_estatus_pauta'] = "id_estatus_pauta DESC";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

///////////////////////////correos usuario///////////////////////////////////////////////


    function selectdatocorreo($conect, $cedula) {
        $nameTabla['usuario.t_datos_personales'] = "usuario.t_datos_personales";

        $campos['correo'] = "correo";

        $condicion['cedula'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

//////////////////////correos de la gerencia ///////////////////////////////////
    function selectgerencia($conect, $gerencia=null) {
        $nameTabla['f5.rrhh_gerencia'] = "f5.rrhh_gerencia";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['desc_gerencia'] = "desc_gerencia";
        //$campos['cantidad']="cantidad";
        if ($gerencia != null) {
            $condicion['id_gerencia'] = $gerencia;
        }
        $condicion['fecha_expiracion'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdivision($conect, $id_gerencia, $id_division=null) {
        $nameTabla['f5.rrhh_division'] = "f5.rrhh_division";

        $campos['id_division'] = "id_division";
        $campos['desc_division'] = "desc_division";
        $campos['id_gerencia'] = "id_gerencia";

        //$campos['cantidad']="cantidad";
        if ($id_gerencia != null) {
            $condicion['id_gerencia'] = $id_gerencia;
        }

        if ($id_division != null) {
            $condicion['id_division'] = $id_division;
        }

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function insertcorreos($conect, $idger, $ger, $cedula) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_correo";
        // Campos para hacer el insert
        $campos['id_gerencia'] = $idger;
        $campos['correo'] = $ger;
        $campos['user_reg'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertcorreosdiv($conect, $idger, $iddiv, $div, $cedula) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_correo";
        // Campos para hacer el insert
        $campos['id_gerencia'] = $idger;
        $campos['id_division'] = $iddiv;
        $campos['correo'] = $div;
        $campos['user_reg'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function selectverificarcorreos($conect, $idger, $iddiv=null) {
        $nameTabla[$this->prefix_table . 't_correo'] = $this->prefix_table . "t_correo";
        $campos['correo'] = "correo";
        $condicion['id_gerencia'] = $idger;
        $condicion['fecha_exp'] = "'2222-12-31'";
        if ($iddiv != null) {
            $condicion['id_division'] = $iddiv;
        }
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function updateexpcorreos($conect, $idger) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_correo';
        $campos['fecha_exp'] = date("Y-m-d");

        $condicion['id_gerencia'] = $idger;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function selectcorreosger($conect, $idger) {
        $nameTabla[$this->prefix_table . 't_correo'] = $this->prefix_table . "t_correo";
        $campos['correo'] = "correo";
        $condicion['id_gerencia'] = $idger;

        $condicion['id_division'] = 0;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcorreousuario($conect, $idger=null, $iddivision=null, $idtipousuario=null) {
        $nameTabla[$this->prefix_table . 'usuarios'] = $this->prefix_table . "usuarios";
        $campos['correo'] = "correo";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['iddivision'] = "iddivision";
        if ($idger != null) {
            $condicion['id_gerencia'] = $idger;
        }
        if ($iddivision != null) {
            $condicion['iddivision'] = $iddivision;
        }
        if ($idtipousuario != null) {
            $condicion['id_tipo_usuario'] = $idtipousuario;
        }
        $condicion['correo !'] = "''";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function tributa_a($conect, $idger=null, $iddivision=null, $idtipousuario=null) {
        $nameTabla[$this->prefix_table . 'usuarios'] = $this->prefix_table . "usuarios";
        $campos['cedula'] = "cedula";
        $campos['nombre_usuario'] = "nombre_usuario";
        $campos['apellido_usuario'] = "apellido_usuario";
        $campos['de_gerencia'] = 'de_gerencia';
        $campos['de_division'] = 'de_division';
        $campos['desc_usuario'] ="desc_usuario";
        if ($idger != null) {
            $condicion['id_gerencia'] = $idger;
        }
        if ($iddivision != null) {
            $condicion['iddivision'] = $iddivision;
        }
        if ($idtipousuario != null) {
            $condicion['id_tipo_usuario'] = $idtipousuario;
        }
        $condicion['correo !'] = "''";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcorreosdiv($conect, $idger, $iddiv) {
        $nameTabla[$this->prefix_table . 't_correo'] = $this->prefix_table . "t_correo";
        $campos['correo'] = "correo";
        $condicion['id_gerencia'] = $idger;
        $condicion['id_division'] = $iddiv;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidsgerencia($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 'lista_pauta_gerencia'] = $this->prefix_table . "lista_pauta_gerencia";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['iddivision'] = "iddivision";
        $condicion['id_pauta'] = $id_pauta;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidsgerencia_ao_sinasignacion($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 'lista_pauta_gerencia_ao_sinasignacion'] = $this->prefix_table . "lista_pauta_gerencia_ao_sinasignacion";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['id_division'] = "id_division";
        $condicion['id_pauta'] = $id_pauta;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidsdivision($conect, $id_gerencia, $id_pauta) {
        $nameTabla[$this->prefix_table . 'pauta_gerencia'] = $this->prefix_table . "pauta_gerencia";
        $campos['id_division'] = "id_division";
        $condicion['id_gerencia'] = $id_gerencia;
        $condicion['id_pauta'] = $id_pauta;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

///////////////////////////estatus///////////////////////////////////////////////////////

    function verificarasignacion($conect, $id_recurso_asignado, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $condicion['id_recurso_asignado'] = $id_recurso_asignado;
        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function verificarestatus($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";
        $campos['id_estatus'] = "id_estatus";
        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $orderby['id_detalle_servicio'] = "id_detalle_servicio DESC";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function eliminadopor($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";
        $campos['user_exp'] = "user_exp";
        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function asignadopor($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";
        $campos['user_exp'] = "user_exp";
        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatousuario($conect, $cedula) {
        $nameTabla['usuario.t_datos_personales'] = "usuario.t_datos_personales";
        $campos['nombre_usuario'] = "nombre_usuario";
        $campos['apellido_usuario'] = "apellido_usuario";
        $campos['correo'] = "correo";
        $campos['telefono1'] = "telefono1";
        $campos['telefono2'] = "telefono2";
        $condicion['cedula'] = $cedula;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function verificarestatuspauta($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_pauta'] = $this->prefix_table . "t_estatus_pauta";
        $campos['id_estatus'] = "id_estatus";
        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function updateexpestatus($conect, $id_pauta, $observaciones="") {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_estatus_pauta';
        $campos['fecha_exp'] = date("Y-m-d");
        if ($observaciones != "") {
            $campos['observaciones'] = $observaciones;
        }
        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updateexpestatusrecursos($conect, $id_detalle_servicio) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_estatus_detalle_servicio';
        $campos['fecha_exp'] = date("Y-m-d");
        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function insertestatusnuevo($conect, $id_pauta, $estatuspauta, $cedula, $observaciones=null) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_pauta";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = $estatuspauta;
        $campos['user_reg'] = $cedula;
        if($observaciones!=null){
            $campos['observaciones'] = $observaciones;
        }
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertestatusmodif($conect, $id_pauta, $id_estatus, $cedula) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_pauta";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = $id_estatus;
        $campos['user_reg'] = $cedula;
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertestatusmod($conect, $id_pauta, $cedula) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_pauta";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = 7;
        $campos['user_reg'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertestatusmod3($conect, $id_pauta, $cedula) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_pauta";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = 25;
        $campos['user_reg'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertestatusnuevorech($conect, $id_pauta, $estatuspauta, $obs, $cedula) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_pauta";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = $estatuspauta;
        $campos['observaciones'] = $obs;
        $campos['user_reg'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertestatusnuevoanulada($conect, $id_pauta, $estatuspauta, $obs) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_pauta";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = $estatuspauta;
        $campos['observaciones'] = $obs;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function selectrecursosdelapauta($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";

        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $orderby['id_detalle_servicio'] = "id_detalle_servicio DESC";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectrecursosdelapautaeditada($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";

        $condicion = "id_pauta=" . $id_pauta . " and id_estatus in (27) and fecha_exp='2222-12-31'";

        $orderby['id_detalle_servicio'] = "id_detalle_servicio DESC";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function insertestatusrecursoanulado($conect, $id_pauta, $ced_sesion, $id_detalle_servicio, $estatus_recurso, $id_recurso_asignado) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_detalle_servicio";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = $estatus_recurso;
        $campos['id_detalle_servicio'] = $id_detalle_servicio;
        $campos['id_recurso_asignado'] = $id_recurso_asignado;
        $campos['user_exp'] = $ced_sesion;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function selectobsrech($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_pauta'] = $this->prefix_table . "t_estatus_pauta";

        $campos['observaciones'] = "observaciones";
        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectestatuspautas($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_pauta'] = $this->prefix_table . "t_estatus_pauta";

        $campos['id_estatus'] = "id_estatus";
        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatjust($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_informe'] = $this->prefix_table . "t_informe";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['id_informe'] = "id_informe";
        $campos['id_estado_informe'] = "id_estado_informe";
        $campos['observaciones'] = "observaciones";

        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidrecurso($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_recurso'] = "id_recurso";
        $campos['id_tipo_recurso'] = "id_tipo_recurso";

        $condicion['id_detalle_servicio'] = $id_detalle_servicio;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    /**
     * SIRVE PARA DEFINIR LOS ESTADOS DE LOS INFORMES..... USADO MUCHO EN EL ULTIMO PROCESO DE PAUTAS.
     * @param type $conect
     * @param type $id_estado_informe
     * @return type
     */
    function selectestadoinf($conect, $id_estado_informe) {
        $nameTabla[$this->prefix_table . 't_estado_informe'] = $this->prefix_table . "t_estado_informe";
        $campos['descripcion'] = "descripcion";
        $condicion['id_estado_informe'] = $id_estado_informe;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function insertguardar($conect, $id_pauta, $id_detalle_servicio, $asistio, $observaciones, $ced_sesion) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_informe";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_detalle_servicio'] = $id_detalle_servicio;
        $campos['id_estado_informe'] = $asistio;
        $campos['observaciones'] = $observaciones;
        $campos['user_reg'] = $ced_sesion;
        $campos['fecha_reg'] = date("Y-m-d");
        $campos['fecha_exp'] = "2222-12-31";


        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function updateexpestatusinf($conect, $id_informe, $ced_sesion) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_informe';
        $campos['fecha_exp'] = date("Y-m-d");
        $campos['user_exp'] = $ced_sesion;

        $condicion['id_informe'] = $id_informe;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updateexpestatusrecursosf5($conect, $id_pauta, $id_detalle_servicio) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_estatus_detalle_servicio';
        $campos['fecha_exp'] = date("Y-m-d");

        $condicion['id_pauta'] = $id_pauta;
        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function SelectFotoCarnet($conect, $ced) {
        $nameTabla['carnet.t_foto'] = "carnet.t_foto";

        $campos['id_foto'] = "id_foto";
        $campos['descripcion'] = "descripcion";
        $campos['nombre'] = "nombre";
        $campos['archivo_bytea'] = "coalesce(archivo_bytea,'-1') as archivo_bytea";
        $campos['mime'] = "mime";
        $campos['size'] = "size";


        $condicion['descripcion'] = "'" . $ced . "'";
        $condicion['fecha_expiracion'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function usuario($conect, $cedula, $clave, $id_aplicacion, $id_modulo, $id_tipo_usuario, $id_submodulo, $idgerencia=null, $iddivision=null) {
        // Tabla para hacer la consulta
        $nameTabla['usuario.t_acceso'] = "usuario.t_acceso";
        // Campos para seleccionar
        $campos['id_acceso'] = "id_acceso";
        $campos['cedula'] = "cedula";
        $campos['clave'] = "clave";
        $campos['id_modulo'] = "id_modulo";
        $campos['id_aplicacion'] = "id_aplicacion";
        $campos['niv_con'] = "niv_con";
        $campos['niv_eli'] = "niv_eli";
        $campos['niv_inc'] = "niv_inc";
        $campos['niv_mod'] = "niv_mod";
        $campos['ced_trans'] = "ced_trans";
        $campos['fecha_trans'] = "fecha_trans";
        $campos['fecha_exp'] = "fecha_exp";
        $campos["id_tipo_usuario"] = "id_tipo_usuario";
        $campos["id_submodulo"] = "id_submodulo";

        $campos["idgerencia"] = "idgerencia";
        $campos["iddivision"] = "iddivision";

        // Condicion
        if ($cedula != "") {
            $condicion['cedula'] = "'" . $cedula . "'";
        }

        if ($clave != "") {
            $condicion['clave'] = "'" . $clave . "'";
        }

        if ($id_aplicacion != "") {
            $condicion['id_aplicacion'] = $id_aplicacion;
        }

        if ($id_modulo != "") {
            $condicion['id_modulo'] = $id_modulo;
        }

        if ($id_tipo_usuario != "") {
            $condicion['id_tipo_usuario >'] = $id_tipo_usuario;
        }

        if ($id_submodulo != "") {
            $condicion['id_submodulo'] = $id_submodulo;
        }

        if ($idgerencia != null) {
            $condicion['idgerencia'] = $idgerencia;
        }

        if ($iddivision != null) {
            $condicion['iddivision'] = $iddivision;
        }
        $condicion['fecha_exp'] = "'2222-12-31'";

        $orderby['cedula'] = "cedula";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function updateexpirausuario($conect, $cedula, $clave, $id_aplicacion, $id_modulo, $id_tipo_usuario, $id_submodulo) {
        // Tabla para hacer la consulta
        $nameTabla = 'usuario.t_acceso';
        $campos['fecha_exp'] = date("Y-m-d");

        if ($cedula != "") {
            $condicion['cedula'] = $cedula;
        }

        if ($clave != "") {
            $condicion['clave'] = $clave;
        }

        if (($id_aplicacion != "")) {
            $condicion['id_aplicacion'] = $id_aplicacion;
        }

        if (($id_modulo != "")) {
            $condicion['id_modulo'] = $id_modulo;
        }

        if (($id_tipo_usuario != "")) {
            $condicion['id_tipo_usuario'] = $id_tipo_usuario;
        }

        if (($id_submodulo != "")) {
            $condicion['id_submodulo'] = $id_submodulo;
        }

        $condicion['fecha_exp'] = "2222-12-31";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function insertusuario($conect, $cedula, $clave, $id_aplicacion, $id_modulo, $id_tipo_usuario, $id_submodulo , $id_gerencia, $id_division) {

        // Nombre de la Tabla
        $nameTabla = "usuario.t_acceso";
        // Campos para hacer el insert
        $campos['cedula'] = $cedula;
        $campos['clave'] = $clave;
        $campos['id_modulo'] = $id_modulo;
        $campos['id_aplicacion'] = $id_aplicacion;
        $campos['niv_con'] = 1;
        $campos['niv_eli'] = 1;
        $campos['niv_inc'] = 1;
        $campos['niv_mod'] = 1;
        $campos['ced_trans'] = $cedula;
        $campos['fecha_trans'] = date("Y-m-d");
        $campos['id_tipo_usuario'] = $id_tipo_usuario;
        if ($id_submodulo != "") {
            $campos['id_submodulo'] = $id_submodulo;
        }

        $campos['idgerencia'] = $id_gerencia;
        $campos['iddivision'] = $id_division;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function datospersonalesusuario($conect, $cedula) {
        // Tabla para hacer la consulta
        $nameTabla['usuario.t_datos_personales'] = "usuario.t_datos_personales";
        // Campos para seleccionar
        $campos['cedula'] = "cedula";
        $campos['nombre_usuario'] = "nombre_usuario";
        $campos['apellido_usuario'] = "apellido_usuario";
        // Condicon
        if ($cedula != "") {
            $condicion['cedula'] = $cedula;
        }
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function datosgerenciadelusuario($conect, $cedula) {
        // Tabla para hacer la consulta
        $nameTabla['sigai.datos_trabajador'] = "sigai.datos_trabajador";
        // Campos para seleccionar
        $campos['id_gerencia'] = "id_gerencia";
        $campos['id_division'] = "id_division";

        // Condicon
        if ($cedula != "") {
            $condicion['nu_documen'] = $cedula;
        }
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

/*
lo comento porque ya no se usara estas funciones para requerir datos de tabajadores

    function datostrabajadorsigai($conect, $cedula, $id_gerencia="", $id_cargo="") {
        // Tabla para hacer la consulta
        $nameTabla['sigai.datos_trabajador'] = "sigai.datos_trabajador";
        // Campos para seleccionar
        $campos['nu_documen'] = "nu_documen";
        $campos['no_primernombre'] = "no_primernombre";
        $campos['no_segundonombre'] = "no_segundonombre";
        $campos['no_primerapellido'] = "no_primerapellido";
        $campos['no_segundoapellido'] = "no_segundoapellido";
        $campos['de_cargo'] = "de_cargo";
        $campos['de_gerencia'] = "de_gerencia";
        $campos['de_division'] = "de_division";
        $campos['id_cargo'] = "id_cargo";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['id_division'] = "id_division";
        // Condicon
        if ($cedula != "") {
            $d_cedula = 'nu_documen=' . $cedula;
            $and = ' and ';
        } else {
            $d_cedula = "";
            $and = ' ';
        }
        if ($id_gerencia != "") {
            $d_gerencia = $and . 'id_gerencia=' . $id_gerencia;
            $and = ' and ';
        } else {
            $d_gerencia = "";
            $and = ' ';
        }
        if ($id_cargo != "") {
            if ($cedula != "") {
                $and = ' and ';
            }
            $d_cargo = $and . 'id_cargo in' . $id_cargo;
        } else {
            $d_cargo = "";
        }
        $condicion = $d_cedula . $d_gerencia . $d_cargo;

        $orderby['nu_documen'] = "nu_documen";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }*/

/*
    function datostrabajadortelefonico($conect, $cedula, $id_persona="", $minimo="NO") {
        // Tabla para hacer la consulta
        $nameTabla['directorio2.datostrabajadortelefonico'] = "directorio2.datostrabajadortelefonico";
        // Campos para seleccionar
        $campos['id_persona'] = "id_persona";
        $campos['cedula'] = "cedula";
        $campos['nombres'] = "nombres";
        $campos['apellidos'] = "apellidos";
        $campos['desc_gerencia'] = "desc_gerencia";
        $campos['desc_division'] = "desc_division";
        $campos['desc_cargo'] = "desc_cargo";
        $campos['id_cargo'] = "id_cargo";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['id_division'] = "id_division";
        // Condicon
        if ($cedula != "") {
            $condicion['cedula'] = $cedula;
        }

        if ($id_persona != "") {
            $condicion['id_persona'] = $id_persona;
        }

        if ($minimo === 'NO') {
            $limite = $this->limite;
        } else {
            if (strcmp($minimo, "") != 0) {
                $limite['INICIO'] = $minimo;
                $limite['FIN'] = 10;
            } else {
                $limite = array();
            }
        }


        $orderby['cedula'] = "cedula";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $limite);
    }*/

    function datosusuariocambioclave($conect, $cedula, $id_persona="", $minimo="NO") {
        // Tabla para hacer la consulta
        $nameTabla['sigai.datos_trabajador'] = "sigai.datos_trabajador";
        // Campos para seleccionar
        $campos['no_primernombre'] = "no_primernombre";
        $campos['no_segundonombre'] = "no_segundonombre";
        $campos['no_primerapellido'] = "no_primerapellido";
        $campos['no_segundoapellido'] = "no_segundoapellido";
        $campos['de_gerencia'] = "de_gerencia";
        $campos['de_division'] = "de_division";
        $campos['de_cargo'] = "de_cargo";
        // Condicon
        if ($cedula != "") {
            $condicion['nu_documen'] = $cedula;
        }


        if ($minimo === 'NO') {
            $limite = $this->limite;
        } else {
            if (strcmp($minimo, "") != 0) {
                $limite['INICIO'] = $minimo;
                $limite['FIN'] = 10;
            } else {
                $limite = array();
            }
        }


        $orderby['nu_documen'] = "nu_documen";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $limite);
    }

    ///la regla de la consulta relacional son 3 parametros
    function busqueda($conect, $vista_tabla, $campo_rn, $campo_bq, $busqueda, $consulta_relacional, $minimo="") {
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        if ($busqueda != "") {
            // Tabla para hacer la consulta
            $nameTabla[$vista_tabla] = $vista_tabla;
            // Campos para seleccionar
            $campos[$campo_rn] = $campo_rn;
            // Condicon
            $busqueda = explode(" ", trim($busqueda));
            for ($i = 0; $i < count($busqueda); $i++) {
                if ($i > 0) {
                    $separador.=" AND ";
                }
                $separador.=$campo_bq . " ILIKE '%" . $busqueda[$i] . "%'";
            }
            $condicion = $separador;
            $orderby[$campo_rn] = $campo_rn;
            $resultado = $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
            $arreglo = array();
            $x = 1;
            foreach ($resultado as $llave => $valor) {
                $arreglo = $this->$consulta_relacional($conect, "", $valor[1], $minimo);
                $arreglo2[$x++] = $arreglo[1];
            }
            return $arreglo2;
        } else {
            return $this->$consulta_relacional($conect, "", "", $minimo);
        }
    }

    function busqueda2($conect, $vista_tabla, $campo_rn, $campo_bq, $busqueda, $consulta_relacional, $minimo) {
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        // Tabla para hacer la consulta
        $nameTabla[$vista_tabla] = $vista_tabla;
        // Campos para seleccionar
        //$campos[$campo_rn]=$campo_rn;
        $campo_rn = explode(",", trim($campo_rn));
        for ($ii = 0; $ii < count($campo_rn); $ii++) {
            $campos[$campo_rn[$ii]] = $campo_rn[$ii];
        }

        if ($busqueda != "") { // Condicon
            $busqueda = explode(" ", trim($busqueda));
            for ($i = 0; $i < count($busqueda); $i++) {
                if ($i > 0) {
                    $separador.=" AND ";
                }
                $separador.=$campo_bq . " ILIKE '%" . $busqueda[$i] . "%'";
            }
            $condicion = $separador;
            //$orderby[$campo_rn]=$campo_rn;
        } else {
            $condicion == array();
        }

        if ($minimo === 'NO') {
            $limite = $this->limite;
        } else {
            if (strcmp($minimo, "") != 0) {
                $limite['INICIO'] = $minimo;
                $limite['FIN'] = 10;
            } else {
                $limite = array();
            }
        }

        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $limite);
    }

    function busquedasys_main($conect, $vista_tabla, $campo_rn, $campo_bq, $busqueda, $consulta_relacional, $minimo) {
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        // Tabla para hacer la consulta
        $nameTabla[$vista_tabla] = $vista_tabla;
        // Campos para seleccionar
        //$campos[$campo_rn]=$campo_rn;
        $campo_rn = explode(",", trim($campo_rn));
        for ($ii = 0; $ii < count($campo_rn); $ii++) {
            $campos[$campo_rn[$ii]] = $campo_rn[$ii];
        }

        if ($busqueda != "") { // Condicon
            $busqueda = explode(" ", trim($busqueda));
            for ($i = 0; $i < count($busqueda); $i++) {
                if ($i > 0) {
                    $separador.=" AND ";
                }
                $separador.=$campo_bq . " ILIKE '%" . mb_strtoupper($busqueda[$i]) . "%'";
            }
            //$condicion=$separador;
            if ($consulta_relacional != "") {
                $condicion = $separador . " AND " . $consulta_relacional;
            }else{
                $condicion = $separador ;
            }
        } else {
            //$condicion==array();
            $condicion = $consulta_relacional;
        }

        if ($minimo === 'NO') {
            $limite = $this->limite;
        } else {
            if (strcmp($minimo, "") != 0) {
                $limite['INICIO'] = $minimo;
                $limite['FIN'] = 10;
            } else {
                $limite = array();
            }
        }
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $this->orderby, $this->groupby, $limite);
    }

    function selectpautasfinalizadas($conect,$fecha_ini, $fecha_fin, $gerencia) {
        $nameTabla[$this->prefix_table . 'lista_de_pautas_por_gerencias'] = $this->prefix_table . "lista_de_pautas_por_gerencias";

        $campos['descripcion'] = "descripcion";
        $campos['nombre_programa'] = "nombre_programa";
        $campos['nom_prod'] = "nom_prod";
        $campos['gerencia_prod'] = "gerencia_prod";
        $campos['fecha_citacion'] = "fecha_citacion";
        $campos['id_pauta'] = "id_pauta";


        $condicion = "id_estatus in (26) and fecha_citacion between '" . $fecha_ini . "' and '" . $fecha_fin . "' and id_gerencia=".$gerencia."";

        $orderby['fecha_citacion'] = "fecha_citacion DESC";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectpautasnofinalizadas($conect,$fecha_ini, $fecha_fin, $gerencia) {
        $nameTabla[$this->prefix_table . 'lista_de_pautas_por_gerencias'] = $this->prefix_table . "lista_de_pautas_por_gerencias";

        $campos['descripcion'] = "descripcion";
        $campos['nombre_programa'] = "nombre_programa";
        $campos['nom_prod'] = "nom_prod";
        $campos['gerencia_prod'] = "gerencia_prod";
        $campos['fecha_citacion'] = "fecha_citacion";
        $campos['id_pauta'] = "id_pauta";

        $condicion = "id_estatus in (36) and fecha_citacion between '" . $fecha_ini . "' and '" . $fecha_fin . "' and id_gerencia=".$gerencia."";

        $orderby['fecha_citacion'] = "fecha_citacion DESC";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectpautasinformesenproceso($conect,$fecha_ini, $fecha_fin, $gerencia) {
        $nameTabla[$this->prefix_table . 'lista_de_pautas_por_gerencias'] = $this->prefix_table . "lista_de_pautas_por_gerencias";

        $campos['descripcion'] = "descripcion";
        $campos['nombre_programa'] = "nombre_programa";
        $campos['nom_prod'] = "nom_prod";
        $campos['gerencia_prod'] = "gerencia_prod";
        $campos['fecha_citacion'] = "fecha_citacion";
        $campos['id_pauta'] = "id_pauta";

        $condicion = "id_estatus in (25) and fecha_citacion between '" . $fecha_ini . "' and '" . $fecha_fin . "' and id_gerencia=".$gerencia."";

        $orderby['fecha_citacion'] = "fecha_citacion DESC";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectpautascaducas($conect) {
        $nameTabla[$this->prefix_table . 'sys_main'] = $this->prefix_table . "sys_main";

        $campos['nombre_programa'] = "nombre_programa";
        $campos['descripcion'] = "descripcion";
        $campos['fecha_citacion'] = "fecha_citacion";
        $campos['hora_citacion'] = "hora_citacion";
        $campos['id_estatus'] = "id_estatus";
        $campos['id_pauta'] = "id_pauta";

        $condicion = "id_estatus not in (21,25,26,36) and fecha_citacion = '".date("Y-m-d")."' and hora_citacion <= '".date("H:i").":00'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, null, $this->groupby, $this->limite);
    }

    function updateestatus($conect, $pauta, $mensaje){
        $nameTabla = $this->prefix_table . 't_estatus_pauta';

        $campos['observaciones'] = $mensaje;
        $campos['fecha_exp'] = date("Y-m-d");

        $condicion['id_pauta'] = $pauta;
        $condicion['fecha_exp'] = '2222-12-31';

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

}
?>
