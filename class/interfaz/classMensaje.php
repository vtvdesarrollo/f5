<?php

class classMensaje {

    var $tituloFrom = "";
    var $classEstilo = "";

    function classMensaje($tituloFrom="", $classEstiloAux="") {
        $this->tituloFrom = $tituloFrom;
        $this->classEstilo = $classEstiloAux;
    }

    function InterfazDirectorio($tituloSistema, $tituloPagina, $ficherosJs, $usuario, $elements, $menu="") {
        $codHtml.="<h5>
        <div class=\"derecha-usuario\">" . $usuario . "</div>
        </h5>
        <div id=\"formulariodeBusqueda\" name=\"formulariodeBusqueda\" >
        <table class=\"tabla\">
        <tbody>
        <tr>
        <th>Buscar</th>
        <td align=\"left\">" . $elements[input_buscar] . "</td>
        </tr>
        <tr>
        <td colspan=\"2\">
        <div id=\"clickdegerencia\"
        onclick=\"
        initgerencias();
        document.getElementById('clickdegerencia').style.display='none';
        document.getElementById('mostrargerencias').style.display='block'
        document.getElementById('gui_gerencias').style.display='block';
        \">
        <h6>Click <b>Aqu&iacute;</b> para realizar b&uacute;queda por gerencias</h6>
        </div>
        <table>
        <tr>
        <td>
        <font id=\"mostrargerencias\" style=\"display:none;\"><b>Gerencias:</b></font><div id=\"gui_gerencias\" onclick=\"document.getElementById('mostrardependencias').style.display='block';\"></div>
        </td>
        <td>
        <font id=\"mostrardependencias\" style=\"display:none;\"><b>Dependencias:</b></font><div id=\"gui_divisiones\"></div>
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div>
        <div id=\"resultadosdirectorio\" style=\"display:none\">
        <div id=\"search_algorithm_result\" class=\"tabla\" style=\"max-height: 400px;width:820px;  overflow-x: hidden; overflow-y: auto;\">RESULTADOS DE LA BUSQUEDA</div>
        </div>
        <table class='tabla'  >
        <tr><th colspan='7' class='titulo' >Servicios al usuario:</th></tr>
        <tr>
        <th ><div align='center'>Servicio</div></th>
        <th ><div align='center'>Extension</div></th>
        <th ><div align='center'>Correo<br>@vtv.gob.ve</div></th>
        <th rowspan='" . $elements["serv_rowspan"] . "' class='titulo'><div align='center'> </div></th>
        <th ><div align='center'>Servicio</div></th>
        <th ><div align='center'>Extension</div></th>
        <th ><div align='center'>Correo<br>@vtv.gob.ve</div></th>
        </tr>
        " . $elements[datosserv] . "</table>";
        $codHtml.="
        </td>
        </tr>
        ";
        return $codHtml; //devuelve o retorna la variable que contiene el codigo html
    }


    function InterfazDatosPersonales($cedula_buscar, $botonA, $botonC, $botonB, $irParaPagina) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='form' align='center'>
        <div id='loadiing' align='center'></div>
        <table class='tabla'>
        <tr><th class='titulo' colspan='2'>INGRESO DE C&Eacute;DULA DE IDENTIDAD</th></tr>
        <tr><th>C&Eacute;DULA</th><td>" . $cedula_buscar . "&nbsp;&nbsp;&nbsp;" . $botonB . "</td></tr>
        </table>
        <BR>
        <div id='loading' align='center'></div>
        <div id='datosp' name='datosp' align='center'></div>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function InterfazListaPersona($campo_busqueda, $botonB) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='form' align='center'>
        <div id='loadiing' align='center'></div>
        <table class='tabla'>
        <tr><th class='titulo' colspan='2'>BUSQUEDA</th></tr>
        <tr><td colspan='2'>" . $campo_busqueda . "&nbsp;&nbsp;&nbsp;" . $botonB . "</td></tr>
        </table>
        <BR>
        <div id='loading' align='center'></div>
        <div id='datosp' name='datosp' align='center'></div>
        </div>
        <br>
        ";
        return $codHtml;
    }

    /**
    * PLANTILLA: que genera la estructura basica de una pagina
    */
    function interfazbienvenida($nombresyapellidos, $gerencia, $division, $tipousuario, $jefesArray=null, $mensaje=null) {
        $codHtml = "";
        $codHtml ='<table border="0" class="tabla" style="width: 100%; height:70px; float: left">
        <thead>
        <tr>
        <th colspan="2">Datos de usuario</th>
        <th style="vertical-align: top;">Usted es responsable de:</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td rowspan="4" colspan="1" style="text-align: center;"><img width="70px" height="70px" src="http://intranet/sistemas/directorio/paginas/download_foto.php?id='.base64_encode($_SESSION['cedula']).'" /></td>
        <td><b>Nombres y Apellidos:</b>'.$nombresyapellidos.'</td>
        <td style="vertical-align: top; width:350px;" rowspan="4" colspan="1">
        <div style="padding-left: 15px;padding-right: 10px; border-left: 5px ridge brown; width:90%; height:90px; overflow:auto" >
        ';
        $codHtml.=$mensaje;
        $codHtml.=    '
        </div>
        </td>
        </tr>
        <tr>
        <td><b>Gerencia:</b>'.$gerencia.'</td>
        </tr>
        <tr>
        <td><b>Division:</b>'.$division.'</td>
        </tr>
        <tr>
        <td><b>Perfil:</b>'.$tipousuario.'</td>
        </tr>
        </tbody>
        </table>
        ';
        if($jefesArray!=null){
            $codHtml.='<div class = "tableContainer">
            <table style="width: 100%;" border="0" class="tabla">
            <thead >
            <tr>
            <th>Usted afecta a:</th>
            </tr>
            </thead>
            <tbody >
            ';
            if($jefesArray!=null){
                for($i=1;$i<=count($jefesArray);$i++){
                    $codHtml.="<tr>
                    <td rowspan='5' style='text-align: center;'><img width='70px' height='70px' src='http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($jefesArray[$i][1])."' /></td>
                    </tr>
                    <tr>
                    <td style='text-align: left;'><b>Nombres: </b> ".utf8_decode($jefesArray[$i][2])." ".utf8_decode($jefesArray[$i][3])."</td>
                    </tr>
                    <tr>
                    <td style='text-align: left;'><b>Gerencia: </b>".utf8_decode($jefesArray[$i][4])."</td>
                    </tr>
                    <tr>
                    <td style='text-align: left;'><b>Division: </b>".utf8_decode($jefesArray[$i][5])."</td>
                    </tr>
                    <tr>
                    <td style='text-align: left;'><b>Perfil: </b>".utf8_decode($jefesArray[$i][6])."</td>
                    </tr>";
                }
            }
            $codHtml.=      '
            </tbody>
            </table>
            </div>';
        }else{
                //mesaje diciendo que tiene poderes de super vaca!.
        }

        return $codHtml;
    }

    function interfazreporte($anio, $mess, $gerencia, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='form' align='center'>
        <div id='loadiing' align='center'></div>
        <table class='tabla'>
        <tr><th class='titulo' colspan='2'>REPORTE</th></tr>
        <tr><th>A&Ntilde;O:</th><td>" . $anio . "</td></tr>
        <tr><th>MES</th><td>" . $mess . "</td></tr>
        <tr><th>GERENCIA</th><td>" . $gerencia . "</td></tr>
        </table>
        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        <BR>
        <div id='loading' align='center'></div>
        <div id='datosp' name='datosp' align='center'></div>
        </div>
        <br>
        ";
        return $codHtml;
    }




    function interfazagregarservicio($tipo_grupo, $nombre, $extension, $correo, $titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla'>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>GRUPO</th><td>" . $tipo_grupo . "</td></tr>
        <tr><th>NOMBRE</th><td>" . $nombre . "</td></tr>
        <tr><th>CORREO ELECTRONICO</th><td>" . $correo . "</td></tr>
        <tr><th>EXTENSION</th><td>" . $extension . "</td></tr>
        </table>
        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazRegistro($error) {
        $plantilla = '
        ' . $error . '
        <br>
        <div name="loading" align="center" id="loading"></div>
        <div  align="center" id="form1" name="form1">
        <table class="tabla" style="width:500px;">
        <tr><th colspan="2" class="titulo">INGRESO</th></tr>
        <tr><td valign="top"><table width="90%" border="0" align="center" valign="middle" cellpadding="0" cellspacing="5">
        <td width="50%"><table width="100%"  border="0" cellspacing="8" cellpadding="0">
        <tr><td class="text2"><div align="right">Introduzca su cedula y clave de usuario haga click en "entrar" o presione "enter" Espere confirmaci&oacute;n</div></td></tr>
        <tr><td class="text2"><div align="center"><a href="classolvidocontrasena.php">Olvido su clave de usuario?</a></div></td></tr>
        </table></td>
        <form  method="post" name="form_registro" id="form_registro">
        <td width="50%"><table width="100%"  border="0" cellspacing="8" cellpadding="0">
        <tr>
        <td class="text2">Introduzca su <b>Cedula</b></td></tr>
        <tr><td> <input type="text" maxlength="8"  name="login" id="login" onkeypress="return validaN(event)"  /></td></tr>
        <tr><td class="text2">Introduzca su <b>clave de usuario</b></td></tr>
        <tr><td>  <input type="password" name="clave" id="clave" onkeypress="return enter(event,validaringreso)"></td></tr>
        <tr><td><button onclick="validaringreso()" type="button" class="boton2" value="entrar"></button></td></tr>
        <tr><td class="text2"><input name="validarUsuario" type="hidden" value="validarUsuario" /></td></tr>
        </table></td>
        </form>
        </tr>
        </table></td>
        </tr>
        </table>
        </td>
        </tr>
        </table>
        </div><br>';
        return $plantilla;
    }

    function interfazregexito($titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:400px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>El usuario fue registrado con exito!</div></th></tr>
        <table class='tabla' style='width:400px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfaznoregexito($titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:400px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>El usuario no fue registrado con exito, intente de nuevo.</div></th></tr>
        <table class='tabla' style='width:400px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    ////////////////////////SOLICITUD///////////////////////////////////////////////

    function interfazsft($programa, $gerencia, $envivo, $grabacion, $titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:450px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>Programa:</th><td>" . $programa . "</td></tr>
        <tr><th>Gerencia:</th><td>" . $gerencia . "</td></tr>
        <tr><th>Tipo de Grabacion:</th><td>En vivo&nbsp;" . $envivo . "Grabado&nbsp;" . $grabacion . "</td></tr>
        </table>
        <table class='tabla' style='width:450px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function form_solfactecn_bas($pauta,$locacion,$tipoevento,$tipotraje){
        $html="<table class='tabla' style='width:850px';>
        <tr><th class='titulo' colspan='4'>".utf8_decode("SOLICITUD DE FACILIDADES T&Eacute;CNICAS")."</th></tr>
        <tr><th><div align='center'>Tipo Pauta</div></th>
        <th><div align='center'>Locaci&oacute;n</div></th>
        <th><div align='center'>Tipo de Evento</div></th>
        <th><div align='center'>Tipo de Traje</div></th><tr>
        <tr><td><div align='center'>" . $pauta . "</div></td>
        <td><div align='center'>" . $locacion . "</div></td>
        <td><div align='center'>" . $tipoevento . "</div></td>
        <td><div align='center'>" . $tipotraje . "</div></td></tr>
        <tr><th colspan='2'>
        <div align='center'>Descripci&oacute;n</div></th>
        <th colspan='2'><div align='center'>Lugar</div></th>
        </tr>
        <tr><th colspan='2'>
        <div id='cargando4' align='center'></div><div id='div_tipopauta' align='left'></div></th>
        <th colspan='2'><div id='cargando_locac' align='center'></div><div id='div_locacpauta' align='left'></div></th>
        </tr>
        </table>";
        return $html;
    }

    function form_solfactecn_bas_citaciones($fecha1,$fecha2,$fecha3,$fecha4,$hora1,$hora2,$hora3,$hora4,$min1,$min2,$min3,$min4, $lugar_citacion_lef){
        $html="<table class='tabla' style='width:850px';>
        <thead>
        <tr>
        <th style='width:150px;'>Tipo&nbsp;&nbsp;&nbsp;&nbsp;</th>
        <th><div align='center'>Fecha / Hora</div></th>
        <th><div align='center' style='width:150px;'>Sitio</div></th>
        </tr>
        </thead>
        <tbody>
        <tr class='tr_fecha'><th style='width:150px;'>Citaci&oacute;n:</th><td style='width:300px;'><div align='center'>" . $fecha1 . "&nbsp;&nbsp;&nbsp;" . $hora1 . ":" . $min1 . "</div></td><th><div align='left'>" . $lugar_citacion_lef . "</div></th></tr>
        <tr class='tr_fecha'><th style='width:150px;'>Montaje:</th><td style='width:300px;'><div align='center'>" . $fecha2 . "&nbsp;&nbsp;&nbsp;" . $hora2 . ":" . $min2 . "</div></td><th></th></tr>
        <tr class='tr_fecha'><th style='width:150px;'><div style='display:block' id='envivo_div' >Emisi&oacute;n:</div><div style='display:none' id='grabado_div'>Grabaci&oacute;n</div></th><td style='width:300px;'><div align='center'>" . $fecha3 . "&nbsp;&nbsp;&nbsp;" . $hora3 . ":" . $min3 . "</div></td><th></th></tr>
        <tr class='tr_fecha'><th style='width:150px;'>Retorno</th><td style='width:300px;'><div align='center'>" . $fecha4 . "&nbsp;&nbsp;&nbsp;" . $hora4 . ":" . $min4 . "</div></td><th></th></tr>
        </tbody>
        </table>";
        return $html;
    }

    function tablabotones($botones){
        $html="<table class='tabla'>
        <tr><th colspan='2'><div align='center'>$botones</div></tr>
        </table>";
        return $html;
    }

    function interfazprocesotabla($nombreaccion, $posicion=1, $tabla){
        $clase="class='tituloproceso'";
        if($tabla == 1){
            if($posicion==1){
                $posicion1=$clase;
            }
            if($posicion==2){
                $posicion2=$clase;
            }
            if($posicion==3){
                $posicion3=$clase;
            }
            $pasos = "<table class='tablaproceso'>
            <tr><th class='titulo' colspan='10' align=\"center\">PASOS DEL PROCESO</th></tr>
            <tr align=\"center\"><th><td><img src='../estilos/imagenes/informe.jpeg' style=\"width:25px;height:25px;border-width:none;border-style:none;\" ></td></th>
            <th><td><img src='../estilos/imagenes/agre_al_carro.jpeg' style=\"width:25px;height:25px;border-width:none;border-style:none;\" ></th></td>
            <th><td><img src='../estilos/imagenes/estatus/exito.png' style=\"width:25px;height:25px;border-width:none;border-style:none;\" ></td></th>
            </tr>
            <tr align=\"center\"><th><td ".$posicion1.">".$nombreaccion." Datos Generales</td></th>
            <th><td ".$posicion2.">".$nombreaccion." Recursos Necesarios</td></th>
            <th><td ".$posicion3.">Fin</td></th>
            </tr>
            </table>";
        }else{
            if($posicion==1){
                $posicion1=$clase;
            }
            if($posicion==2){
                $posicion2=$clase;
            }
            if($posicion==3){
                $posicion3=$clase;
            }
            if($posicion==4){
                $posicion4=$clase;
            }
            if($posicion==5){
                $posicion5=$clase;
            }

            $pasos = "<table class='tablaproceso'>
            <tr><th class='titulo' colspan='10' align=\"center\">PASOS DEL PROCESO</th></tr>
            <tr align=\"center\"><th><td><img src='../estilos/imagenes/asig_pers.jpeg' style=\"width:30px;height:30px;border-width:none;border-style:none;\" ></td></th>
            <th><td><img src='../estilos/imagenes/agre_al_carro.jpeg' style=\"width:30px;height:30px;border-width:none;border-style:none;\" ></td></th>
            <th><td><img src='../estilos/imagenes/infousuario.jpeg' style=\"width:30px;height:30px;border-width:none;border-style:none;\" ></th></td>
            <th><td><img src='../estilos/imagenes/aprob2.jpeg' style=\"width:35px;height:35px;border-width:none;border-style:none;\" ></td></th>
            <th><td><img src='../estilos/imagenes/estatus/fin.jpg' style=\"width:40px;height:40px;border-width:none;border-style:none;\" ></td></th>
            </tr>
            <tr align=\"center\"><th><td ".$posicion1.">Asignacion del Personal</td></th>
            <th><td ".$posicion2.">Asignacion de los Recursos Materiales</td></th>
            <th><td ".$posicion3.">Informar a las Areas Operativas</td></th>
            <th><td ".$posicion4.">Aprobacion</td></th>
            <th><td ".$posicion5.">Fin</td></th>
            </tr>
            </table>";
        }
        return $pasos;
    }

    function interfazpauta_f5_moficar($pauta, $locacion, $tipotraje, $tipoevento, $hora1, $min1, $hora2, $min2, $hora3, $min3, $hora4, $min4, $fecha1, $fecha2, $fecha3, $fecha4, $lugar_lef, $lugar_citacion_lef, $lugar_pt_lef, $evento_programa_, $titulo, $botonA, $botonC, $pasos) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        ".$pasos."
        <table class='tabla' style='width:850px';>
        <tr><th class='titulo' colspan='4'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>Tipo Pauta</div></th>
        <th><div align='center'>Locaci&oacute;n</div></th>
        <th><div align='center'>Tipo de Evento</div></th>
        <th><div align='center'>Tipo de Traje</div></th><tr>
        <tr><td><div align='center'>" . $pauta . "</div></td>
        <td><div align='center'>" . $locacion . "</div></td>
        <td><div align='center'>" . $tipoevento . "</div></td>
        <td><div align='center'>" . $tipotraje . "</div></td></tr>
        <tr><th colspan='2'>
        <div align='center'>Descripci&oacute;n</div></th>
        <th colspan='2'><div align='center'>Lugar</div></th>
        </tr>
        <tr><th colspan='2'>
        <div id='cargando4' align='center'></div><div id='div_tipopauta' align='left'>" . $evento_programa_ . "</div></th>
        <th colspan='2'><div id='cargando_locac' align='center'></div><div id='div_locacpauta' align='left'>" . $lugar_pt_lef . " " . $lugar_lef . "</div></th>
        </tr>
        </table>
        <table class='tabla' style='width:850px';>
        <thead>
        <tr>
        <th style='width:150px;'>Tipo&nbsp;&nbsp;&nbsp;&nbsp;</th>
        <th><div align='center'>Fecha / Hora</div></th>
        <th><div align='center' style='width:150px;'>Sitio</div></th>
        </tr>
        </thead>
        <tbody>
        <tr class='tr_fecha'><th style='width:150px;'>Citaci&oacute;n:</th><td style='width:300px;'><div align='center'>" . $fecha1 . "&nbsp;&nbsp;&nbsp;" . $hora1 . ":" . $min1 . "</div></td><th><div align='left'>" . $lugar_citacion_lef . "</div></th></tr>
        <tr class='tr_fecha'><th style='width:150px;'>Montaje:</th><td style='width:300px;'><div align='center'>" . $fecha2 . "&nbsp;&nbsp;&nbsp;" . $hora2 . ":" . $min2 . "</div></td><th></th></tr>
        <tr class='tr_fecha'><th style='width:150px;'><div style='display:block' id='envivo_div' >Emisi&oacute;n:</div><div style='display:none' id='grabado_div'>Grabaci&oacute;n</div></th><td style='width:300px;'><div align='center'>" . $fecha3 . "&nbsp;&nbsp;&nbsp;" . $hora3 . ":" . $min3 . "</div></td><th></th></tr>
        <tr class='tr_fecha'><th style='width:150px;'>Retorno</th><td style='width:300px;'><div align='center'>" . $fecha4 . "&nbsp;&nbsp;&nbsp;" . $hora4 . ":" . $min4 . "</div></td><th></th></tr>
        </tbody>
        </table>
        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }


    function interfazsolicitarservicios2($pasos, $tablapautabasica, $tablaservicios, $materiales, $tablacomentario, $botones) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        ".$pasos."
        ".$tablapautabasica."
        ".$tablaservicios."
        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $materiales . "
        </div>
        ".$tablacomentario."
        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>".$botones."</div></tr>
        </table>
        </div>
        <br>";
        return $codHtml;
    }

    function interfazparavalidarual($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $evento, $materiales, $titulo1, $titulo2, $titulo3, $botonA, $botonR, $botonC, $botonAg) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <table class='tabla' style='width:850px';>
        <tr><th class='titulo' colspan='8'>" . $titulo1 . "</th></tr>
        <tr><th>N&deg; de pauta:</th><td>" . $pauta . "</td></tr>
        <tr>" . $evento . "</tr>
        <tr><th>Productor:</th><td>" . $descripcionprod . "</td></tr>
        <tr><th>Locaci&oacute;n:</th><td>" . $descripcionloc . "</td></tr>
        <tr><th>Tipo de evento:</th><td>" . $descripcionteven . "</td></tr>
        <tr><th>Tipo de traje:</th><td>" . $descripciontraje . "</td></tr>
        <tr><th>Citaci&oacute;n:</th><td>" . $descripcitacion . "</td><th>Fecha:</th><td>" . $fechacitacion . "</td><th>Hora:</th><td>" . $horacitacion . "</td></tr>
        <tr><th>Montaje:</th><td>" . $descripmontaje . "</td><th>Fecha:</th><td>" . $fechamontaje . "</td><th>Hora:</th><td>" . $horamontaje . "</td></tr>
        <tr><th>Emisi&oacute;n:</th><td>" . $descripemision . "</td><th>Fecha:</th><td>" . $fechaemision . "</td><th>Hora:</th><td>" . $horaemision . "</td></tr>
        " . $descripretorno . "
        </table>
        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $materiales . "
        </div>
        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonR . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazparavalidar($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $evento, $materiales, $titulo1, $titulo2, $titulo3, $botonA, $botonR, $botonC, $botonAg) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <table class='tabla' style='width:850px';>
        <tr><th class='titulo' colspan='8'>" . $titulo1 . "</th></tr>
        <tr><th>N&deg; de pauta:</th><td>" . $pauta . "</td></tr>
        <tr>" . $evento . "</tr>
        <tr><th>Productor:</th><td>" . $descripcionprod . "</td></tr>
        <tr><th>Locaci&oacute;n:</th><td>" . $descripcionloc . "</td></tr>
        <tr><th>Tipo de evento:</th><td>" . $descripcionteven . "</td></tr>
        <tr><th>Tipo de traje:</th><td>" . $descripciontraje . "</td></tr>
        <tr><th>Citaci&oacute;n:</th><td>" . $descripcitacion . "</td><th>Fecha:</th><td>" . $fechacitacion . "</td><th>Hora:</th><td>" . $horacitacion . "</td></tr>
        <tr><th>Montaje:</th><td>" . $descripmontaje . "</td><th>Fecha:</th><td>" . $fechamontaje . "</td><th>Hora:</th><td>" . $horamontaje . "</td></tr>
        <tr><th>Emisi&oacute;n:</th><td>" . $descripemision . "</td><th>Fecha:</th><td>" . $fechaemision . "</td><th>Hora:</th><td>" . $horaemision . "</td></tr>
        " . $descripretorno . "
        </table>

        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $materiales . "
        </div>

        <!--<div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <table class='tabla'>
        <tr><th style='width:55%';>C&oacute;digo de autorizaci&oacute;n:</th><td>" . $codval . "</td></tr>
        </table>
        </div>-->

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonR . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }


    function interfazasignarrecursos($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $servicios, $cantidad, $evento, $materiales, $asignados, $titulo1, $titulo2, $titulo3, $titulo4, $botonA, $botonC, $botonAg, $tablaAdmPers, $tablaRecursoAsig, $pasos, $tablacomentario) {

        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        ".$pasos."
        <table class='tabla' style='width:850px';>
        <tr><th class='titulo' colspan='8'>" . $titulo1 . "</th></tr>
        <tr><th>N&deg; de pauta:</th><td>" . $pauta . "</td></tr>
        <tr>" . $evento . "</tr>
        <tr><th>Productor:</th><td>" . $descripcionprod . "</td></tr>
        <tr><th>Locaci&oacute;n:</th><td>" . $descripcionloc . "</td></tr>
        <tr><th>Tipo de evento:</th><td>" . $descripcionteven . "</td></tr>
        <tr><th>Tipo de traje:</th><td>" . $descripciontraje . "</td></tr>
        <tr><th>Citaci&oacute;n:</th><td>" . $descripcitacion . "</td><th>Fecha:</th><td>" . $fechacitacion . "</td><th>Hora:</th><td>" . $horacitacion . "</td></tr>
        <tr><th>Montaje:</th><td>" . $descripmontaje . "</td><th>Fecha:</th><td>" . $fechamontaje . "</td><th>Hora:</th><td>" . $horamontaje . "</td></tr>
        <tr><th>Emisi&oacute;n:</th><td>" . $descripemision . "</td><th>Fecha:</th><td>" . $fechaemision . "</td><th>Hora:</th><td>" . $horaemision . "</td></tr>
        " . $descripretorno . "
        </table>";
        $codHtml .= $tablaAdmPers.$tablaRecursoAsig."

        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $materiales . "
        " . $asignados . "
        </div>
        ".$tablacomentario."
        <div id='agregar' style='display:none'>
        <table class='tabla'>
        <thead><tr><th colspan='2'>ASIGNACION PERSONAL</th></tr></thead>
        <tbody><tr><th>TIPO DE RECURSOS</th><td><div id='div_cargo' name='div_cargo'></div></td></tr>
        <tr><th colspan='2'>RECURSOS DISPONIBLE</th></tr>
        <tr><td colspan='2'>
        <div class='demo ui-widget ui-helper-clearfix'>
        <ul id='gallery' class='gallery ui-helper-reset ui-helper-clearfix' style='overflow: auto; height: 300px'>
        </ul>
        <div id='trash' class='ui-widget-content ui-state-default' style='overflow: auto; height: 300px'>
        <h4 class='ui-widget-header'><span class='ui-icon ui-icon-plus'>Personal Asignado</span> Asignados</h4>

        </div>
        </div>
        </td></tr>
        </tbody>
        <tfoot>
        <tr>
        <td colspan='2'>
        <form>
        <button class='close boton' type='button'>Listo</button>
        </form>
        </td>
        </tr>
        </tfoot>
        </table>
        </div>
        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazasignarrecursos2($pasos, $tabladatosbasicos, $tablaAdmPers, $tablaRecursoAsig,$materiales,$asignados,$tablacomentario,$botones) {

        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        ".$pasos;

        $codHtml.=$tabladatosbasicos;
        $codHtml.= $tablaAdmPers.$tablaRecursoAsig."

        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $materiales . "
        " . $asignados . "
        </div>
        ".$tablacomentario."
        <div id='agregar' style='display:none'>
        <table class='tabla'>
        <thead><tr><th colspan='2'>ASIGNACION PERSONAL</th></tr></thead>
        <tbody><tr><th>TIPO DE RECURSOS</th><td><div id='div_cargo' name='div_cargo'></div></td></tr>
        <tr><th>Buscar: </th><td><input type='text' id='buscarEmpl' name='buscarEmpl' onkeydown='' onkeyup='check_data_personal_asignado(null, ".$_GET['id_pauta'].", true, this.value, true)';/></td></tr>
        <tr><th colspan='2'>RECURSOS DISPONIBLE</th></tr>
        <tr><td colspan='2'>
        <div class='demo ui-widget ui-helper-clearfix'>
        <ul id='gallery' class='gallery ui-helper-reset ui-helper-clearfix' style='overflow: auto; height: 300px'>
        </ul>
        <div id='trash' class='ui-widget-content ui-state-default' style='overflow: auto; height: 300px'>
        <h4 class='ui-widget-header'><span class='ui-icon ui-icon-plus'>Personal Asignado</span> Asignados</h4>
        </div>
        </div>
        </td></tr>
        </tbody>
        <tfoot>
        <tr>
        <td colspan='2'>
        <form>
        <button class='close boton' type='button'>Listo</button>
        </form>
        </td>
        </tr>
        </tfoot>
        </table>
        </div>
        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" .$botones. "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }



    function interfazenviarao($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $evento, $materiales, $asignados, $titulo1, $titulo2, $titulo3, $botonC, $botonA) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <table class='tabla' style='width:850px';>
        <tr><th class='titulo' colspan='8'>" . $titulo1 . "</th></tr>
        <tr><th>N&deg; de pauta:</th><td>" . $pauta . "</td></tr>
        <tr>" . $evento . "</tr>
        <tr><th>Productor:</th><td>" . $descripcionprod . "</td></tr>
        <tr><th>Locaci&oacute;n:</th><td>" . $descripcionloc . "</td></tr>
        <tr><th>Tipo de evento:</th><td>" . $descripcionteven . "</td></tr>
        <tr><th>Tipo de traje:</th><td>" . $descripciontraje . "</td></tr>
        <tr><th>Citaci&oacute;n:</th><td>" . $descripcitacion . "</td><th>Fecha:</th><td>" . $fechacitacion . "</td><th>Hora:</th><td>" . $horacitacion . "</td></tr>
        <tr><th>Montaje:</th><td>" . $descripmontaje . "</td><th>Fecha:</th><td>" . $fechamontaje . "</td><th>Hora:</th><td>" . $horamontaje . "</td></tr>
        <tr><th>Emisi&oacute;n:</th><td>" . $descripemision . "</td><th>Fecha:</th><td>" . $fechaemision . "</td><th>Hora:</th><td>" . $horaemision . "</td></tr>
        " . $descripretorno . "
        </table>

        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $materiales . "
        " . $asignados . "
        </div>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazasignadopara($tabla, $botonA) {
        $codHtml = "";
        $codHtml = "
        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $tabla . "
        </div>
        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "</div></tr>
        </table>
        <br>
        ";
        return $codHtml;
    }

    function interfazmostrarpautagenerada($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $obs, $evento, $rec_solicitados, $titulo1, $titulo2, $titulo3, $botonA, $botonC, $reportes_assoc) {
        $codHtml = "";
        $codHtml = "

        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <table class='tabla' style='width:850px';>
        <tr><th class='titulo' colspan='8'>" . $titulo1 . "</th></tr>
        <tr><th>N&deg; de pauta:</th><td>" . $pauta . "</td></tr>
        <tr>" . $evento . "</tr>
        <tr><th>Productor:</th><td>" . $descripcionprod . "</td></tr>
        <tr><th>Locaci&oacute;n:</th><td>" . $descripcionloc . "</td></tr>
        <tr><th>Tipo de evento:</th><td>" . $descripcionteven . "</td></tr>
        <tr><th>Tipo de traje:</th><td>" . $descripciontraje . "</td></tr>
        <tr><th>Citaci&oacute;n:</th><td>" . $descripcitacion . "</td><th>Fecha:</th><td>" . $fechacitacion . "</td><th>Hora:</th><td>" . $horacitacion . "</td></tr>
        <tr><th>Montaje:</th><td>" . $descripmontaje . "</td><th>Fecha:</th><td>" . $fechamontaje . "</td><th>Hora:</th><td>" . $horamontaje . "</td></tr>
        <tr><th>Emisi&oacute;n:</th><td>" . $descripemision . "</td><th>Fecha:</th><td>" . $fechaemision . "</td><th>Hora:</th><td>" . $horaemision . "</td></tr>
        " . $descripretorno . "
        <tr><th class='titulo' colspan='8'>" . $titulo3 . "</th></tr>
        " . $rec_solicitados . "
        </table>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazmostrarpauta($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $obs, $evento, $materiales, $titulo1, $titulo2, $titulo3, $botonA, $botonC, $reportes_assoc) {
        $codHtml = "";
        $codHtml = "

        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <table class='tabla' style='width:850px';>
        <tr><th class='titulo' colspan='8'>" . $titulo1 . "</th></tr>
        <tr><th>N&deg; de pauta:</th><td>" . $pauta . "</td></tr>
        <tr>" . $evento . "</tr>
        <tr><th>Productor:</th><td>" . $descripcionprod . "</td></tr>
        <tr><th>Locaci&oacute;n:</th><td>" . $descripcionloc . "</td></tr>
        <tr><th>Tipo de evento:</th><td>" . $descripcionteven . "</td></tr>
        <tr><th>Tipo de traje:</th><td>" . $descripciontraje . "</td></tr>
        <tr><th>Citaci&oacute;n:</th><td>" . $descripcitacion . "</td><th>Fecha:</th><td>" . $fechacitacion . "</td><th>Hora:</th><td>" . $horacitacion . "</td></tr>
        <tr><th>Montaje:</th><td>" . $descripmontaje . "</td><th>Fecha:</th><td>" . $fechamontaje . "</td><th>Hora:</th><td>" . $horamontaje . "</td></tr>
        <tr><th>Emisi&oacute;n:</th><td>" . $descripemision . "</td><th>Fecha:</th><td>" . $fechaemision . "</td><th>Hora:</th><td>" . $horaemision . "</td></tr>
        " . $descripretorno . "
        </table>

        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $reportes_assoc . "
        " . $materiales . "
        </div>

        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        " . $obs . "
        </div>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazmostrarpautaconasig($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $obs, $evento, $materiales, $asignados, $modao, $titulo1, $titulo2, $titulo3, $botonC, $reportes_assoc=null) {
        $codHtml = "";
        $codHtml = "

        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <table class='tabla' style='width:850px';>
        <tr><th class='titulo' colspan='8'>" . $titulo1 . "</th></tr>
        <tr><th>N&deg; de pauta:</th><td>" . $pauta . "</td></tr>
        <tr>" . $evento . "</tr>
        <tr><th>Productor:</th><td>" . $descripcionprod . "</td></tr>
        <tr><th>Locaci&oacute;n:</th><td>" . $descripcionloc . "</td></tr>
        <tr><th>Tipo de evento:</th><td>" . $descripcionteven . "</td></tr>
        <tr><th>Tipo de traje:</th><td>" . $descripciontraje . "</td></tr>
        <tr><th>Citaci&oacute;n:</th><td>" . $descripcitacion . "</td><th>Fecha:</th><td>" . $fechacitacion . "</td><th>Hora:</th><td>" . $horacitacion . "</td></tr>
        <tr><th>Montaje:</th><td>" . $descripmontaje . "</td><th>Fecha:</th><td>" . $fechamontaje . "</td><th>Hora:</th><td>" . $horamontaje . "</td></tr>
        <tr><th>Emisi&oacute;n:</th><td>" . $descripemision . "</td><th>Fecha:</th><td>" . $fechaemision . "</td><th>Hora:</th><td>" . $horaemision . "</td></tr>
        " . $descripretorno . "
        </table>

        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $materiales . "
        " . $reportes_assoc . "
        " . $asignados . "
        " . $modao . "
        </div>

        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        " . $obs . "
        </div>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazmostrarpautaconasigual($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $evento, $materiales, $asignados, $titulo1, $titulo2, $titulo3, $botonC, $botonA,$reportes_assoc, $pasos) {
        $codHtml = "";
        $codHtml = "

        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        ".$pasos."
        <table class='tabla' style='width:850px';>
        <tr><th class='titulo' colspan='8'>" . $titulo1 . "</th></tr>
        <tr><th>N&deg; de pauta:</th><td>" . $pauta . "</td></tr>
        <tr>" . $evento . "</tr>
        <tr><th>Productor:</th><td>" . $descripcionprod . "</td></tr>
        <tr><th>Locaci&oacute;n:</th><td>" . $descripcionloc . "</td></tr>
        <tr><th>Tipo de evento:</th><td>" . $descripcionteven . "</td></tr>
        <tr><th>Tipo de traje:</th><td>" . $descripciontraje . "</td></tr>
        <tr><th>Citaci&oacute;n:</th><td>" . $descripcitacion . "</td><th>Fecha:</th><td>" . $fechacitacion . "</td><th>Hora:</th><td>" . $horacitacion . "</td></tr>
        <tr><th>Montaje:</th><td>" . $descripmontaje . "</td><th>Fecha:</th><td>" . $fechamontaje . "</td><th>Hora:</th><td>" . $horamontaje . "</td></tr>
        <tr><th>Emisi&oacute;n:</th><td>" . $descripemision . "</td><th>Fecha:</th><td>" . $fechaemision . "</td><th>Hora:</th><td>" . $horaemision . "</td></tr>
        " . $descripretorno . "
        </table>

        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $reportes_assoc . "
        " . $materiales . "
        " . $asignados . "
        </div>


        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazmostrarpautaconasigao($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $obs, $evento, $materiales, $asignados, $titulo1, $titulo2, $botonC, $reportes_assoc) {
        $codHtml = "";
        $codHtml = "

        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <table class='tabla' style='width:850px';>
        <tr><th class='titulo' colspan='8'>" . $titulo1 . "</th></tr>
        <tr><th>N&deg; de pauta:</th><td>" . $pauta . "</td></tr>
        <tr>" . $evento . "</tr>
        <tr><th>Productor:</th><td>" . $descripcionprod . "</td></tr>
        <tr><th>Locaci&oacute;n:</th><td>" . $descripcionloc . "</td></tr>
        <tr><th>Tipo de evento:</th><td>" . $descripcionteven . "</td></tr>
        <tr><th>Tipo de traje:</th><td>" . $descripciontraje . "</td></tr>
        <tr><th>Citaci&oacute;n:</th><td>" . $descripcitacion . "</td><th>Fecha:</th><td>" . $fechacitacion . "</td><th>Hora:</th><td>" . $horacitacion . "</td></tr>
        <tr><th>Montaje:</th><td>" . $descripmontaje . "</td><th>Fecha:</th><td>" . $fechamontaje . "</td><th>Hora:</th><td>" . $horamontaje . "</td></tr>
        <tr><th>Emisi&oacute;n:</th><td>" . $descripemision . "</td><th>Fecha:</th><td>" . $fechaemision . "</td><th>Hora:</th><td>" . $horaemision . "</td></tr>
        " . $descripretorno . "
        </table>

        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $reportes_assoc . "
        " . $materiales . "
        " . $asignados . "
        </div>

        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        " . $obs . "
        </div>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazmostrarinforme($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $obs, $evento, $asignados="", $justificados, $titulo1, $titulo2, $titulo3, $botonC,$reportes_assoc) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <table class='tabla' style='width:850px';>
        <tr><th class='titulo' colspan='8'>" . $titulo1 . "</th></tr>
        <tr><th>N&deg; de pauta:</th><td>" . $pauta . "</td></tr>
        <tr>" . $evento . "</tr>
        <tr><th>Productor:</th><td>" . $descripcionprod . "</td></tr>
        <tr><th>Locaci&oacute;n:</th><td>" . $descripcionloc . "</td></tr>
        <tr><th>Tipo de evento:</th><td>" . $descripcionteven . "</td></tr>
        <tr><th>Tipo de traje:</th><td>" . $descripciontraje . "</td></tr>
        <tr><th>Citaci&oacute;n:</th><td>" . $descripcitacion . "</td><th>Fecha:</th><td>" . $fechacitacion . "</td><th>Hora:</th><td>" . $horacitacion . "</td></tr>
        <tr><th>Montaje:</th><td>" . $descripmontaje . "</td><th>Fecha:</th><td>" . $fechamontaje . "</td><th>Hora:</th><td>" . $horamontaje . "</td></tr>
        <tr><th>Emisi&oacute;n:</th><td>" . $descripemision . "</td><th>Fecha:</th><td>" . $fechaemision . "</td><th>Hora:</th><td>" . $horaemision . "</td></tr>
        " . $descripretorno . "
        </table>

        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $reportes_assoc . "
        " . $justificados . "
        </div>


        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazgenerarinforme($nsolicitud, $titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>Ingrese el N# de la pauta:</th><td>" . $nsolicitud . "</td></tr>
        </table>

        <table class='tabla' style='width:500px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazinformedeprod($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $obs, $evento, $asignados, $justificados, $titulo1, $titulo2, $titulo3, $botonC, $botonA) {
        $codHtml = "";
        $codHtml = "

        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <table class='tabla' style='width:850px';>
        <tr><th class='titulo' colspan='8'>" . $titulo1 . "</th></tr>
        <tr><th>N&deg; de pauta:</th><td>" . $pauta . "</td></tr>
        <tr>" . $evento . "</tr>
        <tr><th>Productor:</th><td>" . $descripcionprod . "</td></tr>
        <tr><th>Locaci&oacute;n:</th><td>" . $descripcionloc . "</td></tr>
        <tr><th>Tipo de evento:</th><td>" . $descripcionteven . "</td></tr>
        <tr><th>Tipo de traje:</th><td>" . $descripciontraje . "</td></tr>
        <tr><th>Citaci&oacute;n:</th><td>" . $descripcitacion . "</td><th>Fecha:</th><td>" . $fechacitacion . "</td><th>Hora:</th><td>" . $horacitacion . "</td></tr>
        <tr><th>Montaje:</th><td>" . $descripmontaje . "</td><th>Fecha:</th><td>" . $fechamontaje . "</td><th>Hora:</th><td>" . $horamontaje . "</td></tr>
        <tr><th>Emisi&oacute;n:</th><td>" . $descripemision . "</td><th>Fecha:</th><td>" . $fechaemision . "</td><th>Hora:</th><td>" . $horaemision . "</td></tr>
        " . $descripretorno . "
        </table>

        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $asignados . "
        " . $justificados . "
        </div>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazmostrarpautamod($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $obs, $evento, $materiales, $asignados, $eliminados, $titulo1, $titulo2, $titulo3, $botonC,$reportes_assoc) {
        $codHtml = "";
        $codHtml = "

        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <table class='tabla' style='width:850px';>
        <tr><th class='titulo' colspan='8'>" . $titulo1 . "</th></tr>
        <tr><th>N&deg; de pauta:</th><td>" . $pauta . "</td></tr>
        <tr>" . $evento . "</tr>
        <tr><th>Productor:</th><td>" . $descripcionprod . "</td></tr>
        <tr><th>Locaci&oacute;n:</th><td>" . $descripcionloc . "</td></tr>
        <tr><th>Tipo de evento:</th><td>" . $descripcionteven . "</td></tr>
        <tr><th>Tipo de traje:</th><td>" . $descripciontraje . "</td></tr>
        <tr><th>Citaci&oacute;n:</th><td>" . $descripcitacion . "</td><th>Fecha:</th><td>" . $fechacitacion . "</td><th>Hora:</th><td>" . $horacitacion . "</td></tr>
        <tr><th>Montaje:</th><td>" . $descripmontaje . "</td><th>Fecha:</th><td>" . $fechamontaje . "</td><th>Hora:</th><td>" . $horamontaje . "</td></tr>
        <tr><th>Emisi&oacute;n:</th><td>" . $descripemision . "</td><th>Fecha:</th><td>" . $fechaemision . "</td><th>Hora:</th><td>" . $horaemision . "</td></tr>
        " . $descripretorno . "
        </table>

        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $reportes_assoc . "
        " . $materiales . "
        " . $asignados . "
        " . $eliminados . "
        </div>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazmostrarreppautamod($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $obs, $evento, $solprod, $elimprod, $agreual, $asignados, $eliminados, $modao, $titulo1, $titulo2, $titulo3, $botonC) {
        $codHtml = "";
        $codHtml = "

        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <table class='tabla' style='width:850px';>
        <tr><th class='titulo' colspan='8'>" . $titulo1 . "</th></tr>
        <tr><th>N&deg; de pauta:</th><td>" . $pauta . "</td></tr>
        <tr>" . $evento . "</tr>
        <tr><th>Productor:</th><td>" . $descripcionprod . "</td></tr>
        <tr><th>Locaci&oacute;n:</th><td>" . $descripcionloc . "</td></tr>
        <tr><th>Tipo de evento:</th><td>" . $descripcionteven . "</td></tr>
        <tr><th>Tipo de traje:</th><td>" . $descripciontraje . "</td></tr>
        <tr><th>Citaci&oacute;n:</th><td>" . $descripcitacion . "</td><th>Fecha:</th><td>" . $fechacitacion . "</td><th>Hora:</th><td>" . $horacitacion . "</td></tr>
        <tr><th>Montaje:</th><td>" . $descripmontaje . "</td><th>Fecha:</th><td>" . $fechamontaje . "</td><th>Hora:</th><td>" . $horamontaje . "</td></tr>
        <tr><th>Emisi&oacute;n:</th><td>" . $descripemision . "</td><th>Fecha:</th><td>" . $fechaemision . "</td><th>Hora:</th><td>" . $horaemision . "</td></tr>
        " . $descripretorno . "
        </table>

        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $solprod . "
        " . $elimprod . "
        " . $agreual . "
        " . $eliminados . "
        " . $asignados . "
        " . $modao . "
        </div>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazmostrarpautaadm($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $evento, $obs, $materiales, $titulo1, $titulo2, $titulo3, $botonC, $reportes_assoc=null) {
        $codHtml = "";
        $codHtml = "

        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <table class='tabla' style='width:850px';>
        <tr><th class='titulo' colspan='8'>" . $titulo1 . "</th></tr>
        <tr><th>N&deg; de pauta:</th><td>" . $pauta . "</td></tr>
        <tr>" . $evento . "</tr>
        <tr><th>Productor:</th><td>" . $descripcionprod . "</td></tr>
        <tr><th>Locaci&oacute;n:</th><td>" . $descripcionloc . "</td></tr>
        <tr><th>Tipo de evento:</th><td>" . $descripcionteven . "</td></tr>
        <tr><th>Tipo de traje:</th><td>" . $descripciontraje . "</td></tr>
        <tr><th>Citaci&oacute;n:</th><td>" . $descripcitacion . "</td><th>Fecha:</th><td>" . $fechacitacion . "</td><th>Hora:</th><td>" . $horacitacion . "</td></tr>
        <tr><th>Montaje:</th><td>" . $descripmontaje . "</td><th>Fecha:</th><td>" . $fechamontaje . "</td><th>Hora:</th><td>" . $horamontaje . "</td></tr>
        <tr><th>Emisi&oacute;n:</th><td>" . $descripemision . "</td><th>Fecha:</th><td>" . $fechaemision . "</td><th>Hora:</th><td>" . $horaemision . "</td></tr>
        " . $descripretorno . "
        </table>

        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $reportes_assoc . "
        " . $materiales . "
        </div>

        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        " . $obs . "
        </div>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazreportedeauditoria($reporte, $titulo3, $botonC) {
        $codHtml = "";
        $codHtml = "

        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <div id='datosp2' align='center'>
        <div id='cargando2' align='center'></div>
        " . $reporte . "
        </div>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazcitacion($lugar1, $lugar2, $lugar3, $lugar4, $hora1, $min1, $hora2, $min2, $hora3, $min3, $hora4, $min4, $dia1, $mes1, $anio1, $dia2, $mes2, $anio2, $dia3, $mes3, $anio3, $dia4, $mes4, $anio4, $fecha2, $fecha3, $fecha4, $titulo1, $titulo2, $titulo3, $titulo4, $locacion, $tipotraje, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:450px;'>
        <tr><th class='titulo' colspan='2'>" . $titulo1 . "</th></tr>
        <tr><th>Lugar:</th><td>" . $lugar1 . "</td></tr>
        <tr><th>Hora:</th><td>" . $hora1 . ":" . $min1 . "</td></tr>
        <tr><th>Fecha:</th><td>" . $dia1 . "&nbsp;" . $mes1 . "&nbsp;" . $anio1 . "</td></tr>
        </table>

        <table class='tabla' style='width:450px;'>
        <tr><th class='titulo' colspan='2'>" . $titulo2 . "</th></tr>
        <tr><th>Lugar:</th><td>" . $lugar2 . "</td></tr>
        <tr><th>Hora:</th><td>" . $hora2 . ":" . $min2 . "</td></tr>
        <tr><th>Fecha:</th><td>" . $dia2 . "&nbsp;" . $mes2 . "&nbsp;" . $anio2 . "</td></tr>
        </table>

        <table class='tabla' style='width:450px;'>
        <tr><th class='titulo' colspan='2'>" . $titulo3 . "</th></tr>
        <tr><th>Lugar:</th><td>" . $lugar3 . "</td></tr>
        <tr><th>Hora:</th><td>" . $hora3 . ":" . $min3 . "</td></tr>
        <tr><th>Fecha:</th><td>" . $dia3 . "&nbsp;" . $mes3 . "&nbsp;" . $anio3 . "</td></tr>
        </table>

        <table class='tabla' style='width:450px;'>
        <tr><th class='titulo' colspan='2'>" . $titulo4 . "</th></tr>
        <tr><th>Lugar:</th><td>" . $lugar4 . "</td></tr>
        <tr><th>Hora:</th><td>" . $hora4 . ":" . $min4 . "</td></tr>
        <tr><th>Fecha:</th><td>" . $dia4 . "&nbsp;" . $mes4 . "&nbsp;" . $anio4 . "</td></tr>
        </table>
        <table class='tabla' style='width:450px;'>
        <tr><th>Locaci&oacute;n:</th><td>" . $locacion . "</td></tr>
        <tr><th>Tipo de traje:</th><td>" . $tipotraje . "</td></tr>
        </table>

        <table class='tabla' style='width:450px;'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazcodaut($codigo, $titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'  >
        <div id='loading' align='center'></div>
        <table class='tabla'style='width:700px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>Se requiere del c&oacute;digo de autorizaci&oacute;n para el envio de la solicitud</th><td>" . $codigo . "</td></tr>
        </table>

        <table class='tabla' style='width:700px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazsolguardada($titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:400px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>La solicitud fue guardada con exito!</div></th></tr>

        <table class='tabla' style='width:400px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfaznosolguardada($titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:400px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>La solicitud no se pudo guardar con exito, intente de nuevo.</div></th></tr>

        <table class='tabla' style='width:400px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

///////////////////////listas/////////////////////////////////


    function Interfazlistadepautas($campo_busqueda, $botonB) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='form' align='center'>
        <div id='loadiing' align='center'></div>
        <table class='tabla'>
        <tr><th class='titulo' colspan='2'>BUSQUEDA</th></tr>
        <tr><td colspan='2'>" . $campo_busqueda . "&nbsp;&nbsp;&nbsp;" . $botonB . "</td></tr>
        </table>
        <BR>
        <div id='loading' align='center'></div>
        <div id='datosp' name='datosp' align='center'></div>
        </div>
        <br>
        ";
        return $codHtml;
    }

///////////////////////ingreso de correos de la gerencia///////////////////////

    function interfazingresodecorreos($titulo1, $desc_ger, $campgerencia, $divisiones, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' id='gerencia' style='width:450px';>
        <tr><th class='titulo' colspan='2'>" . $titulo1 . "</th></tr>
        <tr><th>" . $desc_ger . "</th><td>" . $campgerencia . "</td></tr>
        " . $divisiones . "
        </table>

        <table class='tabla' style='width:450px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

///////////////////////////UAL////////////////////////////////////

    function interfazx() {
        $codHtml = "";
        $codHtml = "
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        <br><br><br>
        ";
        return $codHtml;
    }

    function interfazual($tbpersona, $titulo, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla'>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>" . $tbpersona . "</th></tr>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazsolicitudual($programa, $gerencia, $tevento, $productor, $director, $coordinador, $lugarc, $fechac, $horac, $lugare, $fechae, $horae, $lugarp, $fechap, $horap, $obs, $aprobada, $rechazada, $titulo, $titulo1, $titulo2, $titulo3, $titulo4, $titulo5, $titulo6, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>Programa:</th><td>" . $programa . "</td></tr>
        <tr><th>Gerencia:</th><td>" . $gerencia . "</td></tr>
        <tr><th>Evento:</th><td>" . $tevento . "</td></tr>
        <tr><th>Productor:</th><td>" . $productor . "</td></tr>
        <tr><th>Director:</th><td>" . $director . "</td></tr>
        <tr><th>Coordinador:</th><td>" . $coordinador . "</td></tr>
        </table>

        <table class='tabla'  style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo1 . "</th></tr>
        <tr><th>Lugar:</th><td>" . $lugarc . "</td></tr>
        <tr><th>Fecha:</th><td>" . $fechac . "</td></tr>
        <tr><th>Hora:</th><td>" . $horac . "</td></tr>
        </table>

        <table class='tabla'  style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo2 . "</th></tr>
        <tr><th>Lugar:</th><td>" . $lugare . "</td></tr>
        <tr><th>Fecha:</th><td>" . $fechae . "</td></tr>
        <tr><th>Hora:</th><td>" . $horae . "</td></tr>
        </table>

        <table class='tabla'  style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo3 . "</th></tr>
        <tr><th>Lugar:</th><td>" . $lugarp . "</td></tr>
        <tr><th>Fecha:</th><td>" . $fechap . "</td></tr>
        <tr><th>Hora:</th><td>" . $horap . "</td></tr>
        </table>

        <table class='tabla'  style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo4 . "</th></tr>
        </table>

        <table class='tabla'  style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo5 . "</th></tr>
        </table>

        <table class='tabla'  style='width:500px';>
        <tr><th>Observaciones:</th><td>" . $obs . "</td></tr>
        </table>

        <table class='tabla'  style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo6 . "</th></tr>
        <tr><th ></th><td align='center'>Aprobada&nbsp;" . $aprobada . "Rechazada&nbsp;" . $rechazada . "</td></tr>
        </table>


        <table class='tabla'  style='width:500px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

////////////////////////ESTATUS//////////////////////////////////77

    function interfazestatus($nsolicitud, $titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>Ingrese N# de la pauta:</th><td>" . $nsolicitud . "</td></tr>
        </table>

        <table class='tabla' style='width:500px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazlistapautas($pautas, $titulo, $botonC, $paginas) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla'>
        <thead>
        <tr><th class='titulo' colspan='7' >" . $titulo . "</th></tr>
        </thead>
        <tbody>
        <tr><th><div align='center'  style='width:150%';>Id</div></th><th><div align='center' style='width:150%';>Descripci&oacute;n</div></th><th><div align='center'  style='width:150%';>Estatus</div></th></tr>
        <tr><th>" . $pautas . "</th></tr>
        </tbody>

        </table>

        <table class='tabla'><tr style=''><td colspan='7' >" . $paginas . "</td></tr></table>
        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazeespera($titulo, $botonA) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:400px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>Su solicitud esta en espera</div></th></tr>

        <table class='tabla' style='width:400px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazeproceso($titulo, $botonA) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:400px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>Su solicitud esta en proceso</div></th></tr>

        <table class='tabla' style='width:400px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazeaprobada($titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:400px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>Su solicitud esta aprobada</div></th></tr>

        <table class='tabla'  style='width:400px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazerechazada($titulo, $botonA, $botonC, $obs) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>Observaciones:</th><td>" . $obs . "</td></tr>

        <table class='tabla';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazanularpautas($obs, $titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:500px'; >
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <!--<tr><th>N&deg; de la pauta :</th><td>" . $npauta . "</td></tr>-->
        <tr><th>Observaciones:</th><td>" . $obs . "</td></tr>
        </table>

        <table class='tabla' style='width:500px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazverestatus($programa, $gerencia, $tevento, $productor, $director, $coordinador, $lugarc, $fechac, $horac, $lugare, $fechae, $horae, $lugarp, $fechap, $horap, $obs, $aprobada, $rechazada, $titulo, $titulo1, $titulo2, $titulo3, $titulo4, $titulo5, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>Programa:</th><td>" . $programa . "</td></tr>
        <tr><th>Gerencia:</th><td>" . $gerencia . "</td></tr>
        <tr><th>Evento:</th><td>" . $tevento . "</td></tr>
        <tr><th>Productor:</th><td>" . $productor . "</td></tr>
        <tr><th>Director:</th><td>" . $director . "</td></tr>
        <tr><th>Coordinador:</th><td>" . $coordinador . "</td></tr>
        </table>
        <table class='tabla' style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo1 . "</th></tr>
        <tr><th>Lugar:</th><td>" . $lugarc . "</td></tr>
        <tr><th>Fecha:</th><td>" . $fechac . "</td></tr>
        <tr><th>Hora:</th><td>" . $horac . "</td></tr>
        </table>
        <table class='tabla' style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo2 . "</th></tr>
        <tr><th>Lugar:</th><td>" . $lugare . "</td></tr>
        <tr><th>Fecha:</th><td>" . $fechae . "</td></tr>
        <tr><th>Hora:</th><td>" . $horae . "</td></tr>
        </table>
        <table class='tabla' style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo3 . "</th></tr>
        <tr><th>Lugar:</th><td>" . $lugarp . "</td></tr>
        <tr><th>Fecha:</th><td>" . $fechap . "</td></tr>
        <tr><th>Hora:</th><td>" . $horap . "</td></tr>
        </table>
        <table class='tabla' style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo4 . "</th></tr>
        </table>
        <table class='tabla' style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo5 . "</th></tr>
        </table>
        <table class='tabla' style='width:500px';>
        <tr><th>Observaciones:</th><td>" . $obs . "</td></tr>
        </table>
        <table class='tabla' style='width:500px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

///////////////////OLVIDO CONTRASEñA//////////////////////////////////////////
    function interfazolvidocontrasena($cedula, $titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th align='center'>C&eacute;dula:</th><td>" . $cedula . "</td></tr>
        <!--<tr><th>Ingrese su correo electr&oacute;nico:</th><td>" . $correo . "</td></tr>-->
        </table>

        <table class='tabla' style='width:500px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazcambioexitoso($titulo, $botonA) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:450px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>Se ha cambiado con exito su clave usuario!</div></th></tr>

        <table class='tabla' style='width:450px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfaznocambioexitoso($titulo, $botonA) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla'  style='width:450px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>No se pudo realizar el cambio de su clave usuario!</div></th></tr>

        <table class='tabla'  style='width:450px';	>
        <tr><th colspan='2'><div align='center'>" . $botonA . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazcorreocontraseña($titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:400px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>El correo con su clave usuario, fue enviado con exito!</div></th></tr>

        <table class='tabla' style='width:400px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfaznocorreocontraseña($titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:400px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>El correo con su clave usuario no pudo ser enviado, intente de nuevo.</div></th></tr>

        <table class='tabla' style='width:400px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

//////////////////////ADMINISTRADOR/////////////////////////////////////

    function interfazadministrador($agregar, $eliminar, $perfil, $titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center' >
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:450px;' 	>
        <tr align='center'><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr align='center'><th></th><td>" . $agregar . "</td></tr>
        <tr align='center'><th></th><td>" . $eliminar . "</td></tr>
        <tr align='center'><th></th><td>" . $perfil . "</td></tr>

        </table>

        <table class='tabla' style='width:450px;'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazadmeliminar($cedula, $titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center' >
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:450px;' >
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>C&eacute;dula:</th><td>" . $cedula . "</td></tr>

        </table>

        <table class='tabla' style='width:450px;' >
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazadmperfil($ced, $nombre, $apellido, $tipousuario, $perfilmod, $titulo1, $titulo2, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>

        <table class='tabla' style='width:450px;'>
        <tr><th class='titulo' colspan='2'>" . $titulo1 . "</th></tr>
        <tr><th>Nombre:</th><td>" . $nombre . "</td></tr>
        <tr><th>Apellido:</th><td>" . $apellido . "</td></tr>
        <tr><th>C&eacute;dula:</th><td>" . $ced . "</td></tr>
        </table>

        <table class='tabla' style='width:450px;'>
        <tr><th class='titulo' colspan='2'>" . $titulo2 . "</th></tr>
        <tr><th>Perfil actual:</th><td>" . $tipousuario . "</td></tr>
        <tr><th>Nuevo perfil:</th><td>" . $perfilmod . "</td></tr>
        </table>

        <table class='tabla' style='width:450px;'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazadmagregar($tipousuario, $nombre, $apellido, $cedula, $telefono1, $telefono2, $correo, $clave1, $clave2, $titulo, $botonA, $botonC, $firma, $gerencia=null, $division=null) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' align='center' style='width:500px;' >
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>C&eacute;dula:</th><td>" . $cedula . "</td></tr>
        <tr><th>Nombre:</th><td>" . $nombre . "</td></tr>
        <tr><th>Apellido:</th><td>" . $apellido . "</td></tr>
        <tr><th>Tipo de usuario:</th><td>" . $tipousuario . "</td></tr>";

        if ($gerencia != null) {
            $codHtml .= "<tr><th>Gerencia:</th><td>" . $gerencia . "</td></tr>
            <tr><th>Divisi&oacute;n:</th><td><div id='div_division1'>Seleccione una gerencia</div></td></tr>";
        }else{
            $controlesOcultos="<input type='hidden' name='idgerencia' id='idgerencia' value='{$_SESSION['gerencia']}' />
            <input type='hidden' name='iddivision' id='iddivision' value='{$_SESSION['division']}' />";
        }

        $codHtml .= "
        <!-- <tr><th>Cargo:</th><td>" . $cargo . "</td></tr>-->
        <tr><th>Telefono cel():</th><td>" . $telefono1 . "</td></tr>
        <tr><th>Telefono alternativo:</th><td>" . $telefono2 . "</td></tr>
        <tr><th>Correo:</th><td>" . $correo . "</td></tr>
        <tr><th>Clave:</th><td>" . $clave1 . "</td></tr>
        <tr><th>Confirmar clave:</th><td>" . $clave2 . "</td></tr>
        <!--<tr><th>C&oacute;digo:</th><td><div id='cargando3'></div><div id='codigoval_1'></div></td></tr>-->
        <!--<tr><th>Firma Digital:</th><td>$firma</td></tr>-->
        </table>
        {$controlesOcultos}

        <table class='tabla' style='width:500px;'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazagrexito($titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:400px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>El usuario $nombre fue registrado con exito!</div></th></tr>

        <table class='tabla' style='width:400px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazelimexito($titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center' >
        <div id='loading' align='center'></div>
        <table class='tabla'style='width:400px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>El usuario $nombre fue eliminado con exito!</div></th></tr>

        <table class='tabla' style='width:400px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazperfilexito($titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:400px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>El nuevo perfil del usuario $nombre es: $perfil</div></th></tr>

        <table class='tabla' style='width:400px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfaznoperfilexito($titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:400px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>No se pudo realizar el cambio de perfil, intente de nuevo.</div></th></tr>

        <table class='tabla' style='width:400px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfaznoagrexito($titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:400px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>No se pudo registrar el usuario con exito, intente de nuevo.</div></th></tr>

        <table class='tabla' style='width:400px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfaznoelimexito($titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:400px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th><div align='center'>No se pudo eliminar el usuario con exito, intente de nuevo.</div></th></tr>

        <table class='tabla' style='width:400px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

/////////////////AREAS OPERATIVAS////////////////

    function interfazao($tbpersona, $titulo, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla'>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>" . $tbpersona . "</th></tr>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazsolicitudao($programa, $gerencia, $tevento, $productor, $director, $coordinador, $lugarc, $fechac, $horac, $lugare, $fechae, $horae, $lugarp, $fechap, $horap, $obs, $aprobada, $rechazada, $titulo, $titulo1, $titulo2, $titulo3, $titulo4, $titulo5, $titulo6, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>Programa:</th><td>" . $programa . "</td></tr>
        <tr><th>Gerencia:</th><td>" . $gerencia . "</td></tr>
        <tr><th>Evento:</th><td>" . $tevento . "</td></tr>
        <tr><th>Productor:</th><td>" . $productor . "</td></tr>
        <tr><th>Director:</th><td>" . $director . "</td></tr>
        <tr><th>Coordinador:</th><td>" . $coordinador . "</td></tr>
        </table>

        <table class='tabla' style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo1 . "</th></tr>
        <tr><th>Lugar:</th><td>" . $lugarc . "</td></tr>
        <tr><th>Fecha:</th><td>" . $fechac . "</td></tr>
        <tr><th>Hora:</th><td>" . $horac . "</td></tr>
        </table>

        <table class='tabla' style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo2 . "</th></tr>
        <tr><th>Lugar:</th><td>" . $lugare . "</td></tr>
        <tr><th>Fecha:</th><td>" . $fechae . "</td></tr>
        <tr><th>Hora:</th><td>" . $horae . "</td></tr>
        </table>

        <table class='tabla' style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo3 . "</th></tr>
        <tr><th>Lugar:</th><td>" . $lugarp . "</td></tr>
        <tr><th>Fecha:</th><td>" . $fechap . "</td></tr>
        <tr><th>Hora:</th><td>" . $horap . "</td></tr>
        </table>

        <table class='tabla' style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo4 . "</th></tr>
        </table>

        <table class='tabla' style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo5 . "</th></tr>
        </table>

        <table class='tabla' style='width:500px';>
        <tr><th>Observaciones:</th><td>" . $obs . "</td></tr>
        </table>

        <table class='tabla' style='width:500px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

///////////////////ALMACEN/////////////////////////

    function interfazalmacen($reporte, $recepcion, $titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla'>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr align='center'><th></th><td>" . $reporte . "</td></tr>
        <tr align='center'><th></th><td>" . $recepcion . "</td></tr>
        </table>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazatrecepciones($tbpersona, $titulo, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla'>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>" . $tbpersona . "</th></tr>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazatentregas($tbpersona, $titulo, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla'>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>" . $tbpersona . "</th></tr>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

////////////////REPORTES/////////////////////////////////////

    function interfazreportegenerales($tbsolicitud, $titulo, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla'>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>" . $tbsolicitud . "</th></tr>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazreporteauditoria($nsolicitud, $titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla' style='width:400px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>Ingrese el N# de la pauta:</th><td>" . $nsolicitud . "</td></tr>
        </table>

        <table class='tabla' style='width:400px';>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazreporteentrega($tbsolicitud, $titulo, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla'>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>" . $tbsolicitud . "</th></tr>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazreporterecepcion($tbsolicitud, $titulo, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla'>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>" . $tbsolicitud . "</th></tr>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

///////////////////////Consultas////////////////////////////////////////

    function interfazservicio($descripcion, $tiposerv, $titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <table class='tabla'>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>Tipo de servicio:</th><td>" . $tiposerv . "</td></tr>
        <tr><th>Descripci&oacute;n:</th><td>" . $descripcion . "</td></tr>

        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfaztipotraje($traje, $titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <table class='tabla'>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>Tipo de traje:</th><td>" . $traje . "</td></tr>


        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function interfazprograma($programa, $titulo, $botonA, $botonC) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='cargando' align='center'></div>
        <table class='tabla'>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>Programa:</th><td>" . $programa . "</td></tr>


        <table class='tabla'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table>
        </div>
        <br>
        ";
        return $codHtml;
    }

    function InterfazExitosamente($mensaje, $pasos= null) {
        $codHtml = "";
        $codHtml = "<br>
        <div align='center'>
        ".$pasos."
        <table class='tabla' style='width:93%;align:center'>
        <tr><th class='titulo' colspan='2'>MENSAJE</th></tr>
        <tr><td><div align='center'>" . $mensaje . "</div></td></tr>
        </table>
        </div><br>";
        return $codHtml;
    }

    function interfazdatospersonalestelf($cedula, $d_nombres, $d_apellidos, $de_gerencia, $de_division, $de_cargo, $titulo, $tabla_consumo) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla'  style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>C&Eacute;DULA:</th><td>" . $cedula . "</td></tr>
        <tr><th>NOMBRES:</th><td>" . $d_nombres . "</td></tr>
        <tr><th>APELLIDOS:</th><td>" . $d_apellidos . "</td></tr>
        <tr><th>GERENCIA:</th><td>" . $de_gerencia . "</td></tr>
        <tr><th>DIVISI&Oacute;N:</th><td>" . $de_division . "</td></tr>
        <tr><th>CARGO:</th><td>" . $de_cargo . "</td></tr>
        </table>" . $tabla_consumo;
        return $codHtml;
    }

    /**
    *interfaz de cambio de clave acorde con el sistema f5
    */
    function interfazcambioclave($titulo, $cedula, $d_nombres, $d_apellidos, $correo, $telefono1, $telefono2, $codHtml2) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla'  style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>C&Eacute;DULA:</th><td>" . $cedula . "</td></tr>
        <tr><th>NOMBRES:</th><td>" . $d_nombres . "</td></tr>
        <tr><th>APELLIDOS:</th><td>" . $d_apellidos . "</td></tr>
        <tr><th>CORREO:</th><td>" . $correo . "</td></tr>
        <tr><th>TELEFONO:</th><td>" . $telefono1 . "</td></tr>
        <tr><th>TELEFONO ALT:</th><td>" . $telefono2 . "</td></tr>
        </table>".$codHtml2;
        return $codHtml;
    }

    function interfazdatosusuariof5($cedula, $d_pnombres, $d_snombres, $d_papellidos, $d_sapellidos, $de_gerencia, $de_division, $de_cargo, $titulo, $tabla_consumo) {
        $codHtml = "";
        $codHtml = "
        <br><br>
        <div id='datosp' align='center'>
        <div id='loading' align='center'></div>
        <table class='tabla'  style='width:500px';>
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>C&Eacute;DULA:</th><td>" . $cedula . "</td></tr>
        <tr><th>NOMBRES:</th><td>" . $d_pnombres . "," . $d_snombres . "</td></tr>
        <tr><th>APELLIDOS:</th><td>" . $d_papellidos . "," . $d_sapellidos . "</td></tr>
        <tr><th>GERENCIA:</th><td>" . $de_gerencia . "</td></tr>
        <tr><th>DIVISI&Oacute;N:</th><td>" . $de_division . "</td></tr>
        <tr><th>CARGO:</th><td>" . $de_cargo . "</td></tr>
        </table>" . $tabla_consumo;
        return $codHtml;
    }

    function interfazelegirprod($selectProductor){
        $html="<table class='tabla' style='width:850px';>
        <thead>
        <tr>
        <th style='width:150px;'>Elige a tu Productor:</th>
        </tr>
        </thead>
        <tbody>
        <tr><td>" . $selectProductor . "</td></tr>
        </tbody>
        </table>";
        return $html;
    }
}
?>