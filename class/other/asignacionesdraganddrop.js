(function($){
    $.support.ajax = !!(window.XMLHttpRequest);
    var needActiveXFix = false;
    if(window.ActiveXObject){
        try{
            new ActiveXObject("Microsoft.XMLHTTP");
            $.support.ajax = true;
        } catch(e){
            $.support.ajax = false;
            if(window.XMLHttpRequest){
                $.ajaxSetup({
                    xhr: function(){
                        return new XMLHttpRequest();
                    }
                });
                $.support.ajax = true;
            }
        }
    }
    $.manageAjax = (function(){
        var cache 			= {},
        queues			= {},
        presets 		= {},
        activeRequest 	= {},
        allRequests 	= {},
        triggerEndCache = {},
        defaults 		= {
            queue: true, //clear
            maxRequests: 1,
            abortOld: false,
            preventDoubbleRequests: true,
            cacheResponse: false,
            complete: function(){},
            error: function(ahr, status){
                var opts = this;
                if(status && status.indexOf('error') != -1){
                    setTimeout(function(){
                        var errStr = status +': ';
                        if(ahr.status){
                            errStr += 'status: '+ ahr.status +' | ';
                        }
                        errStr += 'URL: '+ opts.url;
                        throw new Error(errStr);
                    }, 1);
                }
            },
            success: function(){},
            abort: function(){}
        }
        ;

        function create(name, settings){
            var publicMethods = {};
            presets[name] = presets[name] ||
            {};

            $.extend(true, presets[name], $.ajaxSettings, defaults, settings);

            if(!allRequests[name]){
                allRequests[name] 	= {};
                activeRequest[name] = {};
                activeRequest[name].queue = [];
                queues[name] 		= [];
                triggerEndCache[name] = [];
            }
            $.each($.manageAjax, function(fnName, fn){
                if($.isFunction(fn) && fnName.indexOf('_') !== 0){
                    publicMethods[fnName] = function(param, param2){
                        if(param2 && typeof param === 'string'){
                            param = param2;
                        }
                        fn(name, param);
                    };
                }
            });
            return publicMethods;
        }

        function complete(opts, args){

            if(args[1] == 'success' || args[1] == 'notmodified'){
                opts.success.apply(opts, [args[0].successData, args[1]]);
                if (opts.global) {
                    $.event.trigger("ajaxSuccess", args);
                }
            }

            if(args[1] === 'abort'){
                opts.abort.apply(opts, args);
                if(opts.global){
                    $.active--;
                    $.event.trigger("ajaxAbort", args);
                }
            }

            opts.complete.apply(opts, args);

            if (opts.global) {
                $.event.trigger("ajaxComplete", args);
            }

            if (opts.global && ! $.active){
                $.event.trigger("ajaxStop");
            }
        //args[0] = null;
    }

    function proxy(oldFn, fn){
        return function(xhr, s, e){
            fn.call(this, xhr, s, e);
            oldFn.call(this, xhr, s, e);
            xhr = null;
            e = null;
        };
    }


    function callQueueFn(name){
        var q = queues[name];
        if(q && q.length){
            var fn = q.shift();
            if(fn){
                fn();
            }
        }
    }


    function add(name, opts){
        if(!presets[name]){
            create(name, opts);
        }
        opts = $.extend({}, presets[name], opts);
            //aliases
            var allR 	= allRequests[name],
            activeR = activeRequest[name],
            queue	= queues[name];

            var id 				= opts.type +'_'+ opts.url.replace(/\./g, '_'),
            triggerStart 	= true,
            oldComplete 	= opts.complete,
            ajaxFn 			= function(){
                activeR.queue.push(id);
                activeR[id] = {
                    xhr: false,
                    ajaxManagerOpts: opts
                };
                activeR[id].xhr = $.ajax(opts);
                return id;
            }
            ;

            if(opts.data){
                id += (typeof opts.data == 'string') ? opts.data : $.param(opts.data);
            }

            if(opts.preventDoubbleRequests && allRequests[name][id]){
                return false;
            }

            allR[id] = true;

            opts.complete = function(xhr, s, e){
                var triggerEnd = true;
                if(opts.abortOld){
                    $.each(activeR.queue, function(i, activeID){
                        if(activeID == id){
                            return false;
                        }
                        abort(name, activeID);
                        return activeID;
                    });
                }
                oldComplete.call(this, xhr, s, e);
                //stop memory leak
                if(activeRequest[name][id]){
                    if(activeRequest[name][id] && activeRequest[name][id].xhr){
                        activeRequest[name][id].xhr = null;
                    }
                    activeRequest[name][id] = null;
                }
                triggerEndCache[name].push({
                    xhr: xhr,
                    status: s
                });
                xhr = null;
                activeRequest[name].queue = $.grep(activeRequest[name].queue, function(qid){
                    return (qid !== id);
                });
                allR[id] = false;

                e = null;

                delete activeRequest[name][id];

                $.each(activeR, function(id, queueRunning){
                    if(id !== 'queue' || queueRunning.length){
                        triggerEnd = false;
                        return false;
                    }
                });

                if(triggerEnd){
                    $.event.trigger(name +'End', [triggerEndCache[name]]);
                    $.each(triggerEndCache[name], function(i, cached){
                        cached.xhr = null; //memory leak
                    });
                    triggerEndCache[name] = [];
                }
            };

            if(cache[id]){
                ajaxFn = function(){
                    activeR.queue.push(id);
                    complete(opts, cache[id]);
                    return id;
                };
            } else if(opts.cacheResponse){
                opts.complete = proxy(opts.complete, function(xhr, s){
                    if( s !== "success" && s !== "notmodified" ){
                        return false;
                    }
                    cache[id][0].responseXML 	= xhr.responseXML;
                    cache[id][0].responseText 	= xhr.responseText;
                    cache[id][1] 				= s;
                    //stop memory leak
                    xhr = null;
                    return id; //strict
                });

                opts.success = proxy(opts.success, function(data, s){
                    cache[id] = [{
                        successData: data,
                        ajaxManagerOpts: opts
                    }, s];
                    data = null;
                });
            }

            ajaxFn.ajaxID = id;

            $.each(activeR, function(id, queueRunning){
                if(id !== 'queue' || queueRunning.length){
                    triggerStart = false;
                    return false;
                }
            });

            if(triggerStart){
                $.event.trigger(name +'Start');
            }
            if(opts.queue){
                opts.complete = proxy(opts.complete, function(){

                    callQueueFn(name);
                });

                if(opts.queue === 'clear'){
                    queue = clear(name);
                }

                queue.push(ajaxFn);

                if(activeR.queue.length < opts.maxRequests){
                    callQueueFn(name);
                }
                return id;
            }
            return ajaxFn();
        }

        function clear(name, shouldAbort){
            $.each(queues[name], function(i, fn){
                allRequests[name][fn.ajaxID] = false;
            });
            queues[name] = [];

            if(shouldAbort){
                abort(name);
            }
            return queues[name];
        }

        function getXHR(name, id){
            var ar = activeRequest[name];
            if(!ar || !allRequests[name][id]){
                return false;
            }
            if(ar[id]){
                return ar[id].xhr;
            }
            var queue = queues[name],
            xhrFn;
            $.each(queue, function(i, fn){
                if(fn.ajaxID == id){
                    xhrFn = [fn, i];
                    return false;
                }
                return xhrFn;
            });
            return xhrFn;
        }

        function abort(name, id){
            var ar = activeRequest[name];
            if(!ar){
                return false;
            }
            function abortID(qid){
                if(qid !== 'queue' && ar[qid] && ar[qid].xhr){
                    try {
                        ar[qid].xhr.abort();
                    } catch(e){}
                    complete(ar[qid].ajaxManagerOpts, [ar[qid].xhr, 'abort']);
                }
                return null;
            }
            if(id){
                return abortID(id);
            }
            return $.each(ar, abortID);
        }

        function unload(){
            $.each(presets, function(name){
                clear(name, true);
            });
            cache = {};
        }

        return {
            defaults: 		defaults,
            add: 			add,
            create: 		create,
            cache: 			cache,
            abort: 			abort,
            clear: 			clear,
            getXHR: 		getXHR,
            _activeRequest: activeRequest,
            _complete: 		complete,
            _allRequests: 	allRequests,
            _unload: 		unload
        };
    })();
    //stop memory leaks
    $(window).unload($.manageAjax._unload);
})(jQuery);

(function($) {
    $.alerts = {
        // These properties can be read/written by accessing $.alerts.propertyName from your scripts at any time
        verticalOffset: -75,                // vertical offset of the dialog from center screen, in pixels
        horizontalOffset: 0,                // horizontal offset of the dialog from center screen, in pixels/
        repositionOnResize: true,           // re-centers the dialog on window resize
        overlayOpacity: .01,                // transparency level of overlay
        overlayColor: '#FFF',               // base color of overlay
        draggable: true,                    // make the dialogs draggable (requires UI Draggables plugin)
        okButton: '&nbsp;OK&nbsp;',         // text for the OK button
        cancelButton: '&nbsp;Cancel&nbsp;', // text for the Cancel button
        dialogClass: null,                  // if specified, this class will be applied to all dialogs

        // Public methods

        alert: function(message, title, callback) {
            if( title == null ) title = 'Alert';
            $.alerts._show(title, message, null, 'alert', function(result) {
                if( callback ) callback(result);
            });
        },

        confirm: function(message, title, callback) {
            if( title == null ) title = 'Confirm';
            $.alerts._show(title, message, null, 'confirm', function(result) {
                if( callback ) callback(result);
            });
        },

        prompt: function(message, value, title, callback) {
            if( title == null ) title = 'Prompt';
            $.alerts._show(title, message, value, 'prompt', function(result) {
                if( callback ) callback(result);
            });
        },

        // Private methods

        _show: function(title, msg, value, type, callback) {

            $.alerts._hide();
            $.alerts._overlay('show');

            $("BODY").append(
                '<div id="popup_container">' +
                '<h1 id="popup_title"></h1>' +
                '<div id="popup_content">' +
                '<div id="popup_message"></div>' +
                '</div>' +
                '</div>');

            if( $.alerts.dialogClass ) $("#popup_container").addClass($.alerts.dialogClass);

            // IE6 Fix
            var pos = ($.browser.msie && parseInt($.browser.version) <= 6 ) ? 'absolute' : 'fixed';

            $("#popup_container").css({
                position: pos,
                zIndex: 99999,
                padding: 0,
                margin: 0
            });

            $("#popup_title").text(title);
            $("#popup_content").addClass(type);
            $("#popup_message").text(msg);
            $("#popup_message").html( $("#popup_message").text().replace(/\n/g, '<br />') );

            $("#popup_container").css({
                minWidth: $("#popup_container").outerWidth(),
                maxWidth: $("#popup_container").outerWidth()
            });

            $.alerts._reposition();
            $.alerts._maintainPosition(true);

            switch( type ) {
                case 'alert':
                $("#popup_message").after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '" id="popup_ok" /></div>');
                $("#popup_ok").click( function() {
                    $.alerts._hide();
                    callback(true);
                });
                $("#popup_ok").focus().keypress( function(e) {
                    if( e.keyCode == 13 || e.keyCode == 27 ) $("#popup_ok").trigger('click');
                });
                break;
                case 'confirm':
                $("#popup_message").after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '" id="popup_ok" /> <input type="button" value="' + $.alerts.cancelButton + '" id="popup_cancel" /></div>');
                $("#popup_ok").click( function() {
                    $.alerts._hide();
                    if( callback ) callback(true);
                });
                $("#popup_cancel").click( function() {
                    $.alerts._hide();
                    if( callback ) callback(false);
                });
                $("#popup_ok").focus();
                $("#popup_ok, #popup_cancel").keypress( function(e) {
                    if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
                    if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
                });
                break;
                case 'prompt':
                $("#popup_message").append('<br /><input type="text" size="30" id="popup_prompt" />').after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '" id="popup_ok" /> <input type="button" value="' + $.alerts.cancelButton + '" id="popup_cancel" /></div>');
                $("#popup_prompt").width( $("#popup_message").width() );
                $("#popup_ok").click( function() {
                    var val = $("#popup_prompt").val();
                    $.alerts._hide();
                    if( callback ) callback( val );
                });
                $("#popup_cancel").click( function() {
                    $.alerts._hide();
                    if( callback ) callback( null );
                });
                $("#popup_prompt, #popup_ok, #popup_cancel").keypress( function(e) {
                    if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
                    if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
                });
                if( value ) $("#popup_prompt").val(value);
                $("#popup_prompt").focus().select();
                break;
            }

            // Make draggable
            if( $.alerts.draggable ) {
                try {
                    $("#popup_container").draggable({
                        handle: $("#popup_title")
                    });
                    $("#popup_title").css({
                        cursor: 'move'
                    });
                } catch(e) { /* requires jQuery UI draggables */ }
            }
        },

        _hide: function() {
            $("#popup_container").remove();
            $.alerts._overlay('hide');
            $.alerts._maintainPosition(false);
        },

        _overlay: function(status) {
            switch( status ) {
                case 'show':
                $.alerts._overlay('hide');
                $("BODY").append('<div id="popup_overlay"></div>');
                $("#popup_overlay").css({
                    position: 'absolute',
                    zIndex: 99998,
                    top: '0px',
                    left: '0px',
                    width: '100%',
                    height: $(document).height(),
                    background: $.alerts.overlayColor,
                    opacity: $.alerts.overlayOpacity
                });
                break;
                case 'hide':
                $("#popup_overlay").remove();
                break;
            }
        },

        _reposition: function() {
            var top = (($(window).height() / 2) - ($("#popup_container").outerHeight() / 2)) + $.alerts.verticalOffset;
            var left = (($(window).width() / 2) - ($("#popup_container").outerWidth() / 2)) + $.alerts.horizontalOffset;
            if( top < 0 ) top = 0;
            if( left < 0 ) left = 0;

            // IE6 fix
            if( $.browser.msie && parseInt($.browser.version) <= 6 ) top = top + $(window).scrollTop();

            $("#popup_container").css({
                top: top + 'px',
                left: left + 'px'
            });
            $("#popup_overlay").height( $(document).height() );
        },

        _maintainPosition: function(status) {
            if( $.alerts.repositionOnResize ) {
                switch(status) {
                    case true:
                    $(window).bind('resize', $.alerts._reposition);
                    break;
                    case false:
                    $(window).unbind('resize', $.alerts._reposition);
                    break;
                }
            }
        }

    }

    // Shortuct functions
    jAlert = function(message, title, callback) {
        $.alerts.alert(message, title, callback);
    }

    jConfirm = function(message, title, callback) {
        $.alerts.confirm(message, title, callback);
    };

    jPrompt = function(message, value, title, callback) {
        $.alerts.prompt(message, value, title, callback);
    };

})(jQuery);

var Base64 = {
    // private property
    _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    // public method for encoding
    encode : function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;
        input = Base64._utf8_encode(input);
        while (i < input.length) {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output = output +
            this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
            this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
        }
        return output;
    },

    // public method for decoding
    decode : function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (i < input.length) {
            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));
            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }
        output = Base64._utf8_decode(output);
        return output;
    },

    // private method for UTF-8 encoding
    _utf8_encode : function (string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    // private method for UTF-8 decoding
    _utf8_decode : function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while ( i < utftext.length ) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }

        return string;
    }
}
//configuracion de variable de comunicacion a archivos
var classcoreurl="../";
//overlays triggers
var triggers="";

//function para crear objetos select
function crearselect(nombre,data,defaultsid_selected, namedefault){
    var selec=$("<select>").attr("name",nombre).attr("id",nombre).css("display","none");
    if(defaultsid_selected==undefined){
        var opcciondefecto=$("<option>")
        .attr("value","")
        .attr("selected","selected");
        if(namedefault!=undefined){
            opcciondefecto.html(namedefault);
        }else{
            opcciondefecto.html("-------");
        }
        opcciondefecto.appendTo(selec);
    }
    //alert(data.length);
    if(data!=null){
        for(i=0;i<data.length;i++){
            var opccionselect=$("<option>");
            opccionselect.attr("value",data[i]['id']).html(data[i]['valor']);
            if(defaultsid_selected!=undefined){
                if(defaultsid_selected==data[i]['id']){
                    opccionselect.attr("selected","selected");
                }
            }
            opccionselect.appendTo(selec);
        }
    }
    return selec;
}

//listarecursosdisponibles
//funcion para crear selects libres
//funcion que sirve para crear selects de los objetos.
function selectlibre_ajax(nombre,contenedor,argobj,callback,defaultsid_selected, perspectiva,ndefault){
    if(perspectiva!=undefined){
        $("#"+perspectiva).find("#"+contenedor).find("#"+nombre).remove();
    }else{
        $("#"+contenedor).find("#"+nombre).remove();
    }

    manejadorAjax.add({
        beforeSend: function(datos){
            if(perspectiva!=undefined){
                $("#"+perspectiva).find("#"+contenedor).html('<img id="carga_'+contenedor+'" src="../css/imagenes/loading.gif" />');
            }else{
                $("#"+contenedor).html('<img id="carga_'+contenedor+'" src="../css/imagenes/loading.gif" />');
            }
        },
        success: function(html1) {
            html1=eval("(" + html1 + ")")
            var selec=crearselect(nombre,html1,defaultsid_selected,ndefault);
            //los callbacks
            if(callback!=undefined){
                selec.change(callback);
            }
            if(perspectiva!=undefined){
                selec.appendTo($("#"+perspectiva).find("#"+contenedor));
                $("#"+perspectiva).find("#"+contenedor).find("#"+nombre).fadeIn();
            }else{
                selec.appendTo($("#"+contenedor));
                $("#"+contenedor).find("#"+nombre).fadeIn();
            }
        },
        complete: function(datos){

            if(perspectiva!=undefined){
                $("#"+perspectiva).find("#"+contenedor).find("#carga_"+contenedor).fadeOut('slow',function(){
                    $(this).remove();
                });
            }else{
                $("#"+contenedor).find("#carga_"+contenedor).fadeOut('slow',function(){
                    $(this).remove();
                });
            }
        },
        url: 'classDirectorioFunciones.php',
        data: argobj
    });
}

/*servicio ajax y conexion a datos*/
function ajaxserv(argobj,callback,msgwait,abortcallback, noprocess){
    manejadorAjax.add({
        beforeSend: function(datos){
            if(noprocess==undefined){
                if(msgwait==undefined){
                    msgwait="PROCESANDO";
                }

                jAlert(msgwait+":<br><img src=\"../estilos/imagenes/estatus/loadingAnimation.gif\" alt=\"Procesando\"/>","proceso",function(){
                    manejadorAjax.abort();
                    if(abortcallback!=undefined){
                        abortcallback();
                    }
                });
                $("#popup_ok").val("cancelar");
            }
        },
        success: function(html1) {
            $.alerts._hide();
            if(callback!=undefined){
                callback(html1);
            }
        },
        type: "POST",
        url: 'classDirectorioFunciones.php',
        data: argobj
    });



}

//valida la entrada de numeros en un campo de texto
function solonum(evt){
    var keyPressed = (evt.which) ? evt.which : evt.keyCode;
    //alert(keyPressed);
    if(keyPressed==45){
        return true;
    }else{
        return !(keyPressed > 31 && (keyPressed < 48 || keyPressed > 57));
    }
}

//manejador de servicio ajax para acciones de sistema core.
function coreajaxserv(arg, callback,msgwait,abortcallback){
    ajaxserv("&cx=conexion_postgree&class=classbdConsultas&archconx=conect_sistemas"+arg,function(data){
        if(callback!=undefined){
            callback(data);
        }
    },msgwait,function(){
        if(abortcallback!=undefined){
            abortcallback();
        }
    });
}

var manejadorAjax = $.manageAjax.create('coreAjaxManejador', {
    abortOld: true,
    cacheResponse: false,
    type: "post"
});

//manejador de servicio ajax para acciones de sistema.
function servidorajax(arg, callback,msgwait,abortcallback, noprocess){
    ajaxserv(arg,function(data){
        if(callback!=undefined){
            callback(data);
        }
    },msgwait,function(){
        if(abortcallback!=undefined){
            abortcallback();
        }
    }, noprocess);
}

function rt_serv_update_asig(idpauta){
    servidorajax("obtenerFuncion=select_asignados&idpauta="+idpauta,function(data){
        data=eval("("+data+")");

        if(data!=undefined){
            $('ul',$("#trash")).length ? $('ul',$("#trash")) : $('<ul class="gallery ui-helper-reset"/>').attr('id','ul_asignado').appendTo($("#trash"));

            $array_personal_asignado=new Array();
            $array_personal_asignado_nuevo=new Array();
            $("#ul_asignado").find("li").each(function(){
                $array_personal_asignado[$array_personal_asignado.length] = $(this).attr("id");
            })


            for(i=0;i<data.length;i++){
                $array_personal_asignado_nuevo[$array_personal_asignado_nuevo.length]=data[i][1];

                $('#'+data[i][1], $("#ul_asignado")).length ?  $('#'+data[i][1], $("#gallery")).remove():
                $("<li>").addClass('ui-widget-content ui-corner-tr ui-draggable').attr('id',data[i][1]).attr("idtmat",data[i][4]).css("display","list-item").css("width","45")
                .append($("<h5>").addClass('ui-widget-header'))
                .append($("<img>").attr('src','http://intranet/sistemas/directorio/paginas/download_foto.php?id='+Base64.encode(data[i][3])).attr('width','45').attr('height','34').attr('alt',data[i][2]))
                .append($("<a>").addClass('ui-icon ui-icon-refresh').attr('title','Quitar de la asignación').text('quitar'))
                .appendTo('#ul_asignado')
            }


            for(i=0;i<$array_personal_asignado.length;i++){
                if ($.inArray($array_personal_asignado[i],$array_personal_asignado_nuevo)==-1){
                    $("#"+$array_personal_asignado[i]).remove();
                    check_data_personal_asignado($("#id_cargo").val(), idpauta, "nomensajeproceso");
                }
            }

            initefects(idpauta);
        }else{
            $("#ul_asignado li").remove();
        }


    //causa crash
    /* if($("#id_cargo").val()!=""){
            check_data_personal_asignado($("#id_cargo").val(), idpauta, "nomensajeproceso");
        }*/
    },undefined,undefined,"nomensajeproceso")
}

var intervalID = "";


function check_data_personal_asignado(id_cargo, id_pauta, noprogress, busquedaEmp, borrarImg){

    if(busquedaEmp!=undefined){
        if(busquedaEmp.length==0 || busquedaEmp.length<3){
            $("#gallery").html("");
            busquedaEmp='null';
        }
    }


    servidorajax("obtenerFuncion=recursosdragandrog&idtipomaterial="+id_cargo+"&busqueda="+busquedaEmp,function(data){
        data=eval("("+data+")");
        if(data!=undefined){
            if(borrarImg){
                $("#gallery").html("");
            }
            for(i=0;i<data.length;i++)
            {
                var arreglo_pers_asig=-1;
                if($('#'+data[i][1]).length==0){
                    $("<li>").addClass('ui-widget-content ui-corner-tr').attr('id',data[i][1]).attr("idtmat",data[i][4])
                    .append($("<h5>").addClass('ui-widget-header'))
                    .append($("<img>").attr('src','http://intranet/sistemas/directorio/paginas/download_foto.php?id='+Base64.encode(data[i][3])).attr('width','85').attr('height','70').attr('alt',data[i][2]))
                    .append($("<a>").addClass('ui-icon ui-icon-plus').attr('title','Asignar').text('Asignar'))
                    .appendTo('#gallery')
                    $('.ui-widget-content IMG').each(function(){
                        var $this = $(this);
                        $this.mousedown(function(e){
                            e.preventDefault();
                        });
                    })
                    $('#'+data[i][1]+" img").qtip({
                        content: data[i][2],
                        show: {
                            when: 'mouseover',
                            solo: true
                        },
                        hide: {
                            when: 'mouseout'
                        },
                        position: {
                            corner: {
                                target: 'topMiddle',
                                tooltip: 'bottomMiddle'
                            }
                        },
                        style: {
                            width: 400,
                            padding: 0,
                            color: 'black',
                            textAlign: 'left',
                            border: {
                                width: 1,
                                radius: 4,
                                color: '#AD1515'
                            },
                            tip: {
                                corner: 'bottomMiddle',
                                color: "#AD1515",
                                size: {
                                    x: 30,
                                    y : 15
                                }
                            }
                        }
                    });
                }

}
initefects(id_pauta);
}
},undefined, undefined, noprogress);
}

function callback_select_personal(id_cargo,id_pauta,nointerval){
    if(nointerval==undefined){
        rt_serv_update_asig(id_pauta);
        intervalID = setInterval(function(){
            rt_serv_update_asig(id_pauta);
        }, 2000); // will alert every second
    }
    // clearInterval(intervalID); // will clear the timer

    $('#gallery').html("");
    check_data_personal_asignado(id_cargo, id_pauta);

    $('.ui-widget-content IMG').each(function(){
        var $this = $(this);
        $this.mousedown(function(e){
            e.preventDefault();
        });
    })

    select_cargos("id_cargo","div_cargo","agregar",id_cargo,function (data){
        callback_select_personal($(this).val(), id_pauta, "nointerval");
    });
}


function initefects(idpauta){
    var $gallery = $('#gallery'), $trash = $('#trash');
    var idpauta=idpauta;
    // let the gallery items be draggable
    $('li',$gallery).draggable({
        cancel: 'a.ui-icon',// clicking an icon won't initiate dragging
        revert: 'invalid', // when not dropped, the item will revert back to its initial position
        containment: $('#demo-frame').length ? '#demo-frame' : 'document', // stick to demo-frame if present
        helper: 'clone',
        cursor: 'move'
    });

    $('li',$trash).draggable({
        cancel: 'a.ui-icon',// clicking an icon won't initiate dragging
        revert: 'invalid', // when not dropped, the item will revert back to its initial position
        containment: $('#demo-frame').length ? '#demo-frame' : 'document', // stick to demo-frame if present
        helper: 'clone',
        cursor: 'move'
    });

    // let the trash be droppable, accepting the gallery items
    $trash.droppable({
        accept: '#gallery > li',
        activeClass: 'ui-state-highlight',
        drop: function(ev, ui) {
            deleteImage(ui.draggable,idpauta);
        }
    });

    // let the gallery be droppable as well, accepting items from the trash
    $gallery.droppable({
        accept: '#trash li',
        activeClass: 'custom-state-active',
        drop: function(ev, ui) {

            recycleImage(ui.draggable,idpauta);
        }
    });

    // image deletion function
    var recycle_icon = '<a href="link/to/recycle/script/when/we/have/js/off" title="Quitar de la asignación" class="ui-icon ui-icon-refresh">quitar</a>';
    function deleteImage($item,idpauta) {
        servidorajax("obtenerFuncion=guardardrop&idmaterialgeneral="+$item.attr("id")+"&idpauta="+idpauta+"&idtmat="+$item.attr("idtmat"),function(data){
            $item.fadeOut(function() {
                var $list = $('ul',$trash).length ? $('ul',$trash) : $('<ul class="gallery ui-helper-reset"/>').attr('id','ul_asignado').appendTo($trash);
                $item.find('a.ui-icon-plus').remove();
                $item.find('a.ui-icon-refresh').remove();
                $item.append(recycle_icon).appendTo($list).fadeIn(function() {
                    $item.animate({
                        width: '45px'
                    }).find('img').animate({
                        height: '34px'
                    });
                });
            });
        });
    }

    // image recycle function
    var trash_icon = '<a href="link/to/trash/script/when/we/have/js/off" title="Asignar" class="ui-icon ui-icon-plus">Asignar</a>';
    function recycleImage($item,idpauta) {
        servidorajax("obtenerFuncion=eliminardop&idmaterialgeneral="+$item.attr("id")+"&idpauta="+idpauta+"&idtmat="+$item.attr("idtmat"),function(data){
            $item.fadeOut(function() {
                $item.find('a.ui-icon-plus').remove();
                $item.find('a.ui-icon-refresh').remove();
                $item.css('width','85px').append(trash_icon).find('img').css('height','72px').end().appendTo($gallery).fadeIn();
                if($("#id_cargo").val()!=""){
                    check_data_personal_asignado($("#id_cargo").val(), idpauta, "nomensajeproceso");
                }
            });
        });
    }

    // image preview function, demonstrating the ui.dialog used as a modal window
    function viewLargerImage($link) {
        var src = $link.attr('href');
        var title = $link.siblings('img').attr('alt');
        var $modal = $('img[src="'+src+'"]');

        //jAlert($modal.length);

        if ($modal.length) {
            $modal.dialog('open')
        } else {
            var img = $('<img alt="'+title+'" width="384" height="288" style="display:none;padding: 8px;" />')
            .attr('src',src).appendTo('body');
            setTimeout(function() {
                img.dialog({
                    title: title,
                    width: 400,
                    modal: true
                });
            }, 1);
        }
    }

    // resolve the icons behavior with event delegation
    $('ul.gallery > li').click(function(ev) {
        var $item = $(this);
        var $target = $(ev.target);

        if ($target.is('a.ui-icon-plus')) {
            deleteImage($item, idpauta);
        } else if ($target.is('a.ui-icon-zoomin')) {
            viewLargerImage($target);
        } else if ($target.is('a.ui-icon-refresh')) {
            recycleImage($item, idpauta);
        }

        return false;
    });
}

function select_cargos(nombre,contenedor,perspectiva,idselected,callback){
    selectlibre_ajax(nombre,contenedor,"obtenerFuncion=listarecursosdisponibles",callback,idselected,perspectiva);
}
"use strict"; // Enable ECMAScript "strict" operation for this function. See more: http://ejohn.org/blog/ecmascript-5-strict-mode-json-and-more/
/*jslint browser: true, onevar: true, undef: true, nomen: true, eqeqeq: true, bitwise: true, regexp: true, strict: true, newcap: true, immed: true */

/*global window: false, jQuery: false */
(function ($) {
    // Assign cache and event initialisation on document load
    $(document).ready(function () {
        // Adjust positions of the tooltips on window resize or scroll if enabled
        var i;
        $(window).bind('resize scroll', function (event) {
            for (i = 0; i < $.fn.qtip.interfaces.length; i++) {
                // Access current elements API
                var api = $.fn.qtip.interfaces[i];

                // Update position if resize or scroll adjustments are enabled
                if(api && api.status && api.status.rendered && api.options.position.type !== 'static' && api.elements.tooltip.is(':visible') &&
                    (api.options.position.adjust.scroll && event.type === 'scroll' || api.options.position.adjust.resize && event.type === 'resize')) {
                    // Queue the animation so positions are updated correctly
                api.updatePosition(event, true);
            }
        }
    });

        // Hide unfocus toolipts on document mousedown
        $(document).bind('mouseenter.qtip', function (event) {
            if($(event.target).parents('div.qtip').length === 0) {
                var tooltip = $('.qtipSelector'),
                api = tooltip.qtip('api');

                // Only hide if its visible and not the tooltips target
                if(tooltip.is(':visible') && api && api.status && !api.status.disabled && $(event.target).add(api.elements.target).length > 1) {
                    api.hide(event);
                }
            }
        });
    });

    // Corner object parser
    function Corner(corner) {
        if(!corner){
            return false;
        }

        this.x = String(corner).replace(/middle/i, 'center').match(/left|right|center/i)[0].toLowerCase();
        this.y = String(corner).replace(/middle/i, 'center').match(/top|bottom|center/i)[0].toLowerCase();
        this.offset = {
            left: 0,
            top: 0
        };
        this.precedance = (corner.charAt(0).search(/^(t|b)/) > -1) ? 'y' : 'x';
        this.string = function(){
            return (this.precedance === 'y') ? this.y+this.x : this.x+this.y;
        };
    }

    // Tip coordinates calculator
    function calculateTip(corner, width, height) {
        // Define tip coordinates in terms of height and width values
        var tips = {
            bottomright: [[0, 0], [width, height], [width, 0]],
            bottomleft: [[0, 0], [width, 0], [0, height]],
            topright: [[0, height], [width, 0], [width, height]],
            topleft: [[0, 0], [0, height], [width, height]],
            topcenter: [[0, height], [width / 2, 0], [width, height]],
            bottomcenter: [[0, 0], [width, 0], [width / 2, height]],
            rightcenter: [[0, 0], [width, height / 2], [0, height]],
            leftcenter: [[width, 0], [width, height], [0, height / 2]]
        };
        tips.lefttop = tips.bottomright;
        tips.righttop = tips.bottomleft;
        tips.leftbottom = tips.topright;
        tips.rightbottom = tips.topleft;

        return tips[corner];
    }

    // Border coordinates calculator
    function calculateBorders(radius) {
        var borders;

        // Use canvas element if supported
        if($('<canvas />').get(0).getContext) {
            borders = {
                topLeft: [radius, radius],
                topRight: [0, radius],
                bottomLeft: [radius, 0],
                bottomRight: [0, 0]
            };
        }

        // Canvas not supported - Use VML (IE)
        else if($.browser.msie) {
            borders = {
                topLeft: [-90, 90, 0],
                topRight: [-90, 90, -radius],
                bottomLeft: [90, 270, 0],
                bottomRight: [90, 270, -radius]
            };
        }

        return borders;
    }


    // Build a jQuery style object from supplied style object
    function jQueryStyle(style, sub) {
        var styleObj, i;

        styleObj = $.extend(true, {}, style);
        for (i in styleObj) {
            if(sub === true && (/(tip|classes)/i).test(i)) {
                delete styleObj[i];
            }
            else if(!sub && (/(width|border|tip|title|classes|user)/i).test(i)) {
                delete styleObj[i];
            }
        }

        return styleObj;
    }

    // Sanitize styles
    function sanitizeStyle(style) {
        if(typeof style.tip !== 'object') {
            style.tip = {
                corner: style.tip
            };
        }
        if(typeof style.tip.size !== 'object') {
            style.tip.size = {
                width: style.tip.size,
                height: style.tip.size
            };
        }
        if(typeof style.border !== 'object') {
            style.border = {
                width: style.border
            };
        }
        if(typeof style.width !== 'object') {
            style.width = {
                value: style.width
            };
        }
        if(typeof style.width.max === 'string') {
            style.width.max = parseInt(style.width.max.replace(/([0-9]+)/i, "$1"), 10);
        }
        if(typeof style.width.min === 'string') {
            style.width.min = parseInt(style.width.min.replace(/([0-9]+)/i, "$1"), 10);
        }

        // Convert deprecated x and y tip values to width/height
        if(typeof style.tip.size.x === 'number') {
            style.tip.size.width = style.tip.size.x;
            delete style.tip.size.x;
        }
        if(typeof style.tip.size.y === 'number') {
            style.tip.size.height = style.tip.size.y;
            delete style.tip.size.y;
        }

        return style;
    }

    // Build styles recursively with inheritance
    function buildStyle() {
        var self, i, styleArray, styleExtend, finalStyle, ieAdjust;
        self = this;

        // Build style options from supplied arguments
        styleArray = [true, {}];
        for(i = 0; i < arguments.length; i++){
            styleArray.push(arguments[i]);
        }
        styleExtend = [$.extend.apply($, styleArray)];

        // Loop through each named style inheritance
        while(typeof styleExtend[0].name === 'string') {
            // Sanitize style data and append to extend array
            styleExtend.unshift(sanitizeStyle($.fn.qtip.styles[styleExtend[0].name]));
        }

        // Make sure resulting tooltip className represents final style
        styleExtend.unshift(true, {
            classes: {
                tooltip: 'qtip-' + (arguments[0].name || 'defaults')
            }
        }, $.fn.qtip.styles.defaults);

        // Extend into a single style object
        finalStyle = $.extend.apply($, styleExtend);

        // Adjust tip size if needed (IE 1px adjustment bug fix)
        ieAdjust = ($.browser.msie) ? 1 : 0;
        finalStyle.tip.size.width += ieAdjust;
        finalStyle.tip.size.height += ieAdjust;

        // Force even numbers for pixel precision
        if(finalStyle.tip.size.width % 2 > 0) {
            finalStyle.tip.size.width += 1;
        }
        if(finalStyle.tip.size.height % 2 > 0) {
            finalStyle.tip.size.height += 1;
        }

        // Sanitize final styles tip corner value
        if(finalStyle.tip.corner === true) {
            if(self.options.position.corner.tooltip === 'center' && self.options.position.corner.target === 'center') {
                finalStyle.tip.corner = false;
            }
            else {
                finalStyle.tip.corner = self.options.position.corner.tooltip;
            }
        }

        return finalStyle;
    }

    // Border canvas draw method
    function drawBorder(canvas, coordinates, radius, color) {
        // Create corner
        var context = canvas.get(0).getContext('2d');
        context.fillStyle = color;
        context.beginPath();
        context.arc(coordinates[0], coordinates[1], radius, 0, Math.PI * 2, false);
        context.fill();
    }

    // Create borders using canvas and VML
    function createBorder() {
        var self, i, width, radius, color, coordinates, containers, size, betweenWidth, betweenCorners, borderTop, borderBottom, borderCoord, sideWidth, vertWidth;
        self = this;

        // Destroy previous border elements, if present
        self.elements.wrapper.find('.qtip-borderBottom, .qtip-borderTop').remove();

        // Setup local variables
        width = self.options.style.border.width;
        radius = self.options.style.border.radius;
        color = self.options.style.border.color || self.options.style.tip.color;

        // Calculate border coordinates
        coordinates = calculateBorders(radius);

        // Create containers for the border shapes
        containers = {};
        for (i in coordinates) {
            // Create shape container
            containers[i] = '<div rel="' + i + '" style="' + ((/Left/).test(i) ? 'left' : 'right') + ':0; ' + 'position:absolute; height:' + radius + 'px; width:' + radius + 'px; overflow:hidden; line-height:0.1px; font-size:1px">';

            // Canvas is supported
            if($('<canvas />').get(0).getContext) {
                containers[i] += '<canvas height="' + radius + '" width="' + radius + '" style="vertical-align: top"></canvas>';
            }

            // No canvas, but if it's IE use VML
            else if($.browser.msie) {
                size = radius * 2 + 3;
                containers[i] += '<v:arc stroked="false" fillcolor="' + color + '" startangle="' + coordinates[i][0] + '" endangle="' + coordinates[i][1] + '" ' + 'style="width:' + size + 'px; height:' + size + 'px; margin-top:' + ((/bottom/).test(i) ? -2 : -1) + 'px; ' + 'margin-left:' + ((/Right/).test(i) ? coordinates[i][2] - 3.5 : -1) + 'px; ' + 'vertical-align:top; display:inline-block; behavior:url(#default#VML)"></v:arc>';

            }

            containers[i] += '</div>';
        }

        // Create between corners elements
        betweenWidth = self.getDimensions().width - (Math.max(width, radius) * 2);
        betweenCorners = '<div class="qtip-betweenCorners" style="height:' + radius + 'px; width:' + betweenWidth + 'px; ' + 'overflow:hidden; background-color:' + color + '; line-height:0.1px; font-size:1px;">';

        // Create top border container
        borderTop = '<div class="qtip-borderTop" dir="ltr" style="height:' + radius + 'px; ' + 'margin-left:' + radius + 'px; line-height:0.1px; font-size:1px; padding:0;">' + containers.topLeft + containers.topRight + betweenCorners;
        self.elements.wrapper.prepend(borderTop);

        // Create bottom border container
        borderBottom = '<div class="qtip-borderBottom" dir="ltr" style="height:' + radius + 'px; ' + 'margin-left:' + radius + 'px; line-height:0.1px; font-size:1px; padding:0;">' + containers.bottomLeft + containers.bottomRight + betweenCorners;
        self.elements.wrapper.append(borderBottom);

        // Draw the borders if canvas were used (Delayed til after DOM creation)
        if($('<canvas />').get(0).getContext) {
            self.elements.wrapper.find('canvas').each(function () {
                borderCoord = coordinates[$(this).parent('[rel]:first').attr('rel')];
                drawBorder.call(self, $(this), borderCoord, radius, color);
            });
        }

        // Create a phantom VML element (IE won't show the last created VML element otherwise)
        else if($.browser.msie) {
            self.elements.tooltip.append('<v:image style="behavior:url(#default#VML);"></v:image>');
        }

        // Setup contentWrapper border
        sideWidth = Math.max(radius, (radius + (width - radius)));
        vertWidth = Math.max(width - radius, 0);
        self.elements.contentWrapper.css({
            border: '0px solid ' + color,
            borderWidth: vertWidth + 'px ' + sideWidth + 'px'
        });
    }

    // Canvas tip drawing method
    function drawTip(canvas, coordinates, color) {
        // Setup properties
        var context = canvas.get(0).getContext('2d');
        context.fillStyle = color;

        // Create tip
        context.beginPath();
        context.moveTo(coordinates[0][0], coordinates[0][1]);
        context.lineTo(coordinates[1][0], coordinates[1][1]);
        context.lineTo(coordinates[2][0], coordinates[2][1]);
        context.fill();
    }

    function positionTip(corner) {
        var self, ieAdjust, positionAdjust, paddingCorner, paddingSize, newMargin;
        self = this;

        // Return if tips are disabled or tip is not yet rendered
        if(self.options.style.tip.corner === false || !self.elements.tip) {
            return;
        }
        if(!corner) {
            corner = new Corner(self.elements.tip.attr('rel'));
        }

        // Setup adjustment variables
        ieAdjust = positionAdjust = ($.browser.msie) ? 1 : 0;

        // Set initial position
        self.elements.tip.css(corner[corner.precedance], 0);

        // Set position of tip to correct side
        if(corner.precedance === 'y') {
            // Adjustments for IE6 - 0.5px border gap bug
            if($.browser.msie) {
                if(parseInt($.browser.version.charAt(0), 10) === 6) {
                    positionAdjust = corner.y === 'top' ? -3 : 1;
                }
                else {
                    positionAdjust = corner.y === 'top' ? 1 : 2;
                }
            }

            if(corner.x === 'center') {
                self.elements.tip.css({
                    left: '50%',
                    marginLeft: -(self.options.style.tip.size.width / 2)
                });
            }
            else if(corner.x === 'left') {
                self.elements.tip.css({
                    left: self.options.style.border.radius - ieAdjust
                });
            }
            else {
                self.elements.tip.css({
                    right: self.options.style.border.radius + ieAdjust
                });
            }

            if(corner.y === 'top') {
                self.elements.tip.css({
                    top: -positionAdjust
                });
            }
            else {
                self.elements.tip.css({
                    bottom: positionAdjust
                });
            }

        }
        else {
            // Adjustments for IE6 - 0.5px border gap bug
            if($.browser.msie) {
                positionAdjust = (parseInt($.browser.version.charAt(0), 10) === 6) ? 1 : (corner.x === 'left' ? 1 : 2);
            }

            if(corner.y === 'center') {
                self.elements.tip.css({
                    top: '50%',
                    marginTop: -(self.options.style.tip.size.height / 2)
                });
            }
            else if(corner.y === 'top') {
                self.elements.tip.css({
                    top: self.options.style.border.radius - ieAdjust
                });
            }
            else {
                self.elements.tip.css({
                    bottom: self.options.style.border.radius + ieAdjust
                });
            }

            if(corner.x === 'left') {
                self.elements.tip.css({
                    left: -positionAdjust
                });
            }
            else {
                self.elements.tip.css({
                    right: positionAdjust
                });
            }
        }

        // Adjust tooltip padding to compensate for tip
        paddingCorner = 'padding-' + corner[corner.precedance];
        paddingSize = self.options.style.tip.size[corner.precedance === 'x' ? 'width' : 'height'];
        self.elements.tooltip.css('padding', 0).css(paddingCorner, paddingSize);

        // Match content margin to prevent gap bug in IE6 ONLY
        if($.browser.msie && parseInt($.browser.version.charAt(0), 6) === 6) {
            newMargin = parseInt(self.elements.tip.css('margin-top'), 10) || 0;
            newMargin += parseInt(self.elements.content.css('margin-top'), 10) || 0;

            self.elements.tip.css({
                marginTop: newMargin
            });
        }
    }

    // Create tip using canvas and VML
    function createTip(corner) {
        var self, color, coordinates, coordsize, path, tip;
        self = this;

        // Destroy previous tip, if there is one
        if(self.elements.tip !== null) {
            self.elements.tip.remove();
        }

        // Setup color and corner values
        color = self.options.style.tip.color || self.options.style.border.color;
        if(self.options.style.tip.corner === false) {
            return;
        }
        else if(!corner) {
            corner = new Corner(self.options.style.tip.corner);
        }

        // Calculate tip coordinates
        coordinates = calculateTip(corner.string(), self.options.style.tip.size.width, self.options.style.tip.size.height);

        // Create tip element
        self.elements.tip = '<div class="' + self.options.style.classes.tip + '" dir="ltr" rel="' + corner.string() + '" style="position:absolute; ' + 'height:' + self.options.style.tip.size.height + 'px; width:' + self.options.style.tip.size.width + 'px; ' + 'margin:0 auto; line-height:0.1px; font-size:1px;"></div>';

        // Attach new tip to tooltip element
        self.elements.tooltip.prepend(self.elements.tip);

        // Use canvas element if supported
        if($('<canvas />').get(0).getContext) {
            tip = '<canvas height="' + self.options.style.tip.size.height + '" width="' + self.options.style.tip.size.width + '"></canvas>';
        }

        // Canvas not supported - Use VML (IE)
        else if($.browser.msie) {
            // Create coordize and tip path using tip coordinates
            coordsize = self.options.style.tip.size.width + ',' + self.options.style.tip.size.height;
            path = 'm' + coordinates[0][0] + ',' + coordinates[0][1];
            path += ' l' + coordinates[1][0] + ',' + coordinates[1][1];
            path += ' ' + coordinates[2][0] + ',' + coordinates[2][1];
            path += ' xe';

            // Create VML element
            tip = '<v:shape fillcolor="' + color + '" stroked="false" filled="true" path="' + path + '" coordsize="' + coordsize + '" ' + 'style="width:' + self.options.style.tip.size.width + 'px; height:' + self.options.style.tip.size.height + 'px; ' + 'line-height:0.1px; display:inline-block; behavior:url(#default#VML); ' + 'vertical-align:' + (corner.y === 'top' ? 'bottom' : 'top') + '"></v:shape>';

            // Create a phantom VML element (IE won't show the last created VML element otherwise)
            tip += '<v:image style="behavior:url(#default#VML);"></v:image>';

            // Prevent tooltip appearing above the content (IE z-index bug)
            self.elements.contentWrapper.css('position', 'relative');
        }

        // Create element reference and append vml/canvas
        self.elements.tip = self.elements.tooltip.find('.' + self.options.style.classes.tip).eq(0);
        self.elements.tip.html(tip);

        // Draw the canvas tip (Delayed til after DOM creation)
        if($('<canvas  />').get(0).getContext) {
            drawTip.call(self, self.elements.tip.find('canvas:first'), coordinates, color);
        }

        // Fix IE small tip bug
        if(corner.y === 'top' && $.browser.msie && parseInt($.browser.version.charAt(0), 10) === 6) {
            self.elements.tip.css({
                marginTop: -4
            });
        }

        // Set the tip position
        positionTip.call(self, corner);
    }

    // Create title bar for content
    function createTitle() {
        var self = this;

        // Destroy previous title element, if present
        if(self.elements.title !== null) {
            self.elements.title.remove();
        }

        // Append new ARIA attribute to tooltip
        self.elements.tooltip.attr('aria-labelledby', 'qtip-' + self.id + '-title');

        // Create title element
        self.elements.title = $('<div id="qtip-' + self.id + '-title" class="' + self.options.style.classes.title + '"></div>').css(jQueryStyle(self.options.style.title, true)).css({
            zoom: ($.browser.msie) ? 1 : 0
        }).prependTo(self.elements.contentWrapper);

        // Update title with contents if enabled
        if(self.options.content.title.text) {
            self.updateTitle.call(self, self.options.content.title.text);
        }

        // Create title close buttons if enabled
        if(self.options.content.title.button !== false && typeof self.options.content.title.button === 'string') {
            self.elements.button = $('<a class="' + self.options.style.classes.button + '" role="button" style="float:right; position: relative"></a>').css(jQueryStyle(self.options.style.button, true)).html(self.options.content.title.button).prependTo(self.elements.title).click(function (event) {
                if(!self.status.disabled) {
                    self.hide(event);
                }
            });
        }
    }

    // Assign hide and show events
    function assignEvents() {
        var self, showTarget, hideTarget, inactiveEvents;
        self = this;

        // Setup event target variables
        showTarget = self.options.show.when.target;
        hideTarget = self.options.hide.when.target;

        // Add tooltip as a hideTarget is its fixed
        if(self.options.hide.fixed) {
            hideTarget = hideTarget.add(self.elements.tooltip);
        }

        // Define events which reset the 'inactive' event handler
        inactiveEvents = ['click', 'dblclick', 'mousedown', 'mouseup', 'mousemove',
        'mouseout', 'mouseenter', 'mouseleave', 'mouseover'];

        // Define 'inactive' event timer method
        function inactiveMethod(event) {
            if(self.status.disabled === true) {
                return;
            }

            //Clear and reset the timer
            clearTimeout(self.timers.inactive);
            self.timers.inactive = setTimeout(function () {
                // Unassign 'inactive' events
                $(inactiveEvents).each(function () {
                    hideTarget.unbind(this + '.qtip-inactive');
                    self.elements.content.unbind(this + '.qtip-inactive');
                });

                // Hide the tooltip
                self.hide(event);
            }, self.options.hide.delay);
        }

        // Check if the tooltip is 'fixed'
        if(self.options.hide.fixed === true) {
            self.elements.tooltip.bind('mouseover.qtip', function () {
                if(self.status.disabled === true) {
                    return;
                }

                // Reset the hide timer
                clearTimeout(self.timers.hide);
            });
        }

        // Define show event method
        function showMethod(event) {
            if(self.status.disabled === true) {
                return;
            }

            // If set, hide tooltip when inactive for delay period
            if(self.options.hide.when.event === 'inactive') {
                // Assign each reset event
                $(inactiveEvents).each(function () {
                    hideTarget.bind(this + '.qtip-inactive', inactiveMethod);
                    self.elements.content.bind(this + '.qtip-inactive', inactiveMethod);
                });

                // Start the inactive timer
                inactiveMethod();
            }

            // Clear hide timers
            clearTimeout(self.timers.show);
            clearTimeout(self.timers.hide);

            // Start show timer
            if(self.options.show.delay > 0) {
                self.timers.show = setTimeout(function () {
                    self.show(event);
                }, self.options.show.delay);
            }
            else {
                self.show(event);
            }
        }

        // Define hide event method
        function hideMethod(event) {
            if(self.status.disabled === true) {
                return;
            }

            // Prevent hiding if tooltip is fixed and event target is the tooltip
            if(self.options.hide.fixed === true && (/mouse(out|leave)/i).test(self.options.hide.when.event) && $(event.relatedTarget).parents('div.qtip[id^="qtip"]').length > 0) {
                // Prevent default and popagation
                event.stopPropagation();
                event.preventDefault();

                // Reset the hide timer
                clearTimeout(self.timers.hide);
                return false;
            }

            // Clear timers and stop animation queue
            clearTimeout(self.timers.show);
            clearTimeout(self.timers.hide);
            self.elements.tooltip.stop(true, true);

            // If tooltip has displayed, start hide timer
            self.timers.hide = setTimeout(function () {
                self.hide(event);
            }, self.options.hide.delay);
        }

        // If mouse is the target, update tooltip position on mousemove
        if(self.options.position.target === 'mouse' && self.options.position.type !== 'static') {
            showTarget.bind('mousemove.qtip', function (event) {
                // Set the new mouse positions if adjustment is enabled
                self.cache.mouse = {
                    left: event.pageX,
                    top: event.pageY
                };

                // Update the tooltip position only if the tooltip is visible and adjustment is enabled
                if(self.status.disabled === false && self.options.position.adjust.mouse === true && self.options.position.type !== 'static' && self.elements.tooltip.css('display') !== 'none') {
                    self.updatePosition(event);
                }
            });
        }

        // Both events and targets are identical, apply events using a toggle
        if((self.options.show.when.target.add(self.options.hide.when.target).length === 1 &&
            self.options.show.when.event === self.options.hide.when.event && self.options.hide.when.event !== 'inactive') ||
            self.options.hide.when.event === 'unfocus') {
            self.cache.toggle = 0;
            // Use a toggle to prevent hide/show conflicts
            showTarget.bind(self.options.show.when.event + '.qtip', function (event) {
                if(self.cache.toggle === 0) {
                    showMethod(event);
                }
                else {
                    hideMethod(event);
                }
            });
        }

        // Events are not identical, bind normally
        else {
            showTarget.bind(self.options.show.when.event + '.qtip', showMethod);

            // If the hide event is not 'inactive', bind the hide method
            if(self.options.hide.when.event !== 'inactive') {
                hideTarget.bind(self.options.hide.when.event + '.qtip', hideMethod);
            }
        }

        // Focus the tooltip on mouseover
        if((/(fixed|absolute)/).test(self.options.position.type)) {
            self.elements.tooltip.bind('mouseover.qtip', self.focus);
        }
    }

    // BGIFRAME JQUERY PLUGIN ADAPTION
    //   Special thanks to Brandon Aaron for this plugin
    //   http://plugins.jquery.com/project/bgiframe
    function bgiframe() {
        var self, html, dimensions;
        self = this;
        dimensions = self.getDimensions();

        // Setup iframe HTML string
        html = '<iframe class="qtip-bgiframe" frameborder="0" tabindex="-1" src="javascript:false" ' + 'style="display:block; position:absolute; z-index:-1; filter:alpha(opacity=\'0\'); border: 1px solid red; ' + 'height:' + dimensions.height + 'px; width:' + dimensions.width + 'px" />';

        // Append the new HTML and setup element reference
        self.elements.bgiframe = self.elements.wrapper.prepend(html).children('.qtip-bgiframe:first');
    }

    // Define primary construct function
    function construct() {
        var self, content, url, data, method;
        self = this;

        // Call API method
        self.beforeRender.call(self);

        // Set rendered status to true
        self.status.rendered = 2;

        // Create initial tooltip elements
        self.elements.tooltip = '<div qtip="' + self.id + '" id="qtip-' + self.id + '" role="tooltip" ' + 'aria-describedby="qtip-' + self.id + '-content" class="qtip ' + (self.options.style.classes.tooltip || self.options.style) + '" ' + 'style="display:none; -moz-border-radius:0; -webkit-border-radius:0; border-radius:0; position:' + self.options.position.type + ';"> ' + '  <div class="qtip-wrapper" style="position:relative; overflow:hidden; text-align:left;"> ' + '    <div class="qtip-contentWrapper" style="overflow:hidden;"> ' + '       <div id="qtip-' + self.id + '-content" class="qtip-content ' + self.options.style.classes.content + '"></div> ' + '</div></div></div>';

        // Append to container element
        self.elements.tooltip = $(self.elements.tooltip);
        self.elements.tooltip.appendTo(self.options.position.container);

        // Setup tooltip qTip data
        self.elements.tooltip.data('qtip', {
            current: 0,
            interfaces: [self]
        });

        // Setup element references
        self.elements.wrapper = self.elements.tooltip.children('div:first');
        self.elements.contentWrapper = self.elements.wrapper.children('div:first');
        self.elements.content = self.elements.contentWrapper.children('div:first').css(jQueryStyle(self.options.style));

        // Apply IE hasLayout fix to wrapper and content elements
        if($.browser.msie) {
            self.elements.wrapper.add(self.elements.content).css({
                zoom: 1
            });
        }

        // Setup tooltip attributes
        if(self.options.hide.when.event === 'unfocus') {
            self.elements.tooltip.attr('unfocus', true);
        }

        // If an explicit width is set, updateWidth prior to setting content to prevent dirty rendering
        if(typeof self.options.style.width.value === 'number') {
            self.updateWidth();
        }

        // Create borders and tips if supported by the browser
        if($('<canvas />').get(0).getContext || $.browser.msie) {
            // Create border
            if(self.options.style.border.radius > 0) {
                createBorder.call(self);
            }
            else {
                self.elements.contentWrapper.css({
                    border: self.options.style.border.width + 'px solid ' + self.options.style.border.color
                });
            }

            // Create tip if enabled
            if(self.options.style.tip.corner !== false) {
                createTip.call(self);
            }
        }

        // Neither canvas or VML is supported, tips and borders cannot be drawn!
        else {
            // Set defined border width
            self.elements.contentWrapper.css({
                border: self.options.style.border.width + 'px solid ' + self.options.style.border.color
            });

            // Reset border radius and tip
            self.options.style.border.radius = 0;
            self.options.style.tip.corner = false;
        }

        // Use the provided content string or DOM array
        if((typeof self.options.content.text === 'string' && self.options.content.text.length > 0) || (self.options.content.text.jquery && self.options.content.text.length > 0)) {
            content = self.options.content.text;
        }

        // Check for valid title and alt attributes
        else {
            content = ' ';
        }

        // Set the tooltips content and create title if enabled
        if(self.options.content.title.text !== false) {
            createTitle.call(self);
        }
        self.updateContent(content, false);

        // Assign events and toggle tooltip with focus
        assignEvents.call(self);
        if(self.options.show.ready === true) {
            self.show();
        }

        // Retrieve ajax content if provided
        if(self.options.content.url !== false) {
            url = self.options.content.url;
            data = self.options.content.data;
            method = self.options.content.method || 'get';
            self.loadContent(url, data, method);
        }

        // Call API method and log event
        self.status.rendered = true;
        self.onRender.call(self);
    }

    // Instantiator
    function QTip(target, options, id) {
        // Declare this reference
        var self = this;

        // Setup class attributes
        self.id = id;
        self.options = options;
        self.status = {
            animated: false,
            rendered: false,
            disabled: false,
            focused: false
        };
        self.elements = {
            target: target.addClass(self.options.style.classes.target),
            tooltip: null,
            wrapper: null,
            content: null,
            contentWrapper: null,
            title: null,
            button: null,
            tip: null,
            bgiframe: null
        };
        self.cache = {
            attr: false,
            mouse: {},
            toggle: 0,
            overflow: {
                left: false,
                top: false
            }
        };
        self.timers = {};

        // Define exposed API methods
        $.extend(self, self.options.api, {
            show: function (event) {
                var returned, solo;

                // Make sure tooltip is rendered and if not, return
                if(!self.status.rendered) {
                    return false;
                }

                // Only continue if element is visible
                if(self.elements.tooltip.css('display') !== 'none') {
                    return self;
                }

                // Clear animation queue
                self.elements.tooltip.stop(true, false);

                // Call API method and if return value is false, halt
                returned = self.beforeShow.call(self, event);
                if(returned === false) {
                    return self;
                }

                // Define afterShow callback method
                function afterShow() {
                    // Set ARIA hidden status attribute
                    self.elements.tooltip.attr('aria-hidden', true);

                    // Call API method and focus if it isn't static
                    if(self.options.position.type !== 'static') {
                        self.focus();
                    }
                    self.onShow.call(self, event);

                    // Prevent antialias from disappearing in IE7 by removing filter and opacity attribute
                    if($.browser.msie) {
                        var ieStyle = self.elements.tooltip.get(0).style;
                        ieStyle.removeAttribute('filter');
                        ieStyle.removeAttribute('opacity');
                    }
                    else {
                        self.elements.tooltip.css({
                            opacity: ''
                        });
                    }
                }

                // Maintain toggle functionality if enabled
                self.cache.toggle = 1;

                // Update tooltip position if it isn't static
                if(self.options.position.type !== 'static') {
                    self.updatePosition(event, (self.options.show.effect.length > 0 && self.rendered !== 2));
                }

                // Hide other tooltips if tooltip is solo
                if(typeof self.options.show.solo === 'object') {
                    solo = $(self.options.show.solo);
                }
                else if(self.options.show.solo === true) {
                    solo = $('div.qtip').not(self.elements.tooltip);
                }
                if(solo) {
                    solo.each(function () {
                        if($(this).qtip('api').status.rendered === true) {
                            $(this).qtip('api').hide();
                        }
                    });
                }

                // Show tooltip
                if(typeof self.options.show.effect.type === 'function') {
                    self.options.show.effect.type.call(self.elements.tooltip, self.options.show.effect.length);
                    self.elements.tooltip.queue(function () {
                        afterShow();
                        $(this).dequeue();
                    });
                }
                else {
                    switch (self.options.show.effect.type.toLowerCase()) {
                        case 'fade':
                        self.elements.tooltip.fadeIn(self.options.show.effect.length, afterShow);
                        break;

                        case 'slide':
                        self.elements.tooltip.slideDown(self.options.show.effect.length, function () {
                            afterShow();
                            if(self.options.position.type !== 'static') {
                                self.updatePosition(event, true);
                            }
                        });
                        break;

                        case 'grow':
                        self.elements.tooltip.show(self.options.show.effect.length, afterShow);
                        break;

                        default:
                        self.elements.tooltip.show(null, afterShow);
                        break;
                    }

                    // Add active class to tooltip
                    self.elements.tooltip.addClass(self.options.style.classes.active);
                }

                // Log event and return
                return self;
            },

            hide: function (event) {
                var returned;

                // Make sure tooltip is rendered and if not, return
                if(!self.status.rendered) {
                    return false;
                }

                // Only continue if element is visible
                else if(self.elements.tooltip.css('display') === 'none') {
                    return self;
                }

                // Stop show timer and animation queue
                clearTimeout(self.timers.show);
                self.elements.tooltip.stop(true, false);

                // Call API method and if return value is false, halt
                returned = self.beforeHide.call(self, event);
                if(returned === false) {
                    return self;
                }

                // Define afterHide callback method
                function afterHide() {
                    // Set ARIA hidden status attribute
                    self.elements.tooltip.attr('aria-hidden', true);

                    // Remove opacity attribute
                    if($.browser.msie) {
                        self.elements.tooltip.get(0).style.removeAttribute('opacity');
                    }
                    else {
                        self.elements.tooltip.css({
                            opacity: ''
                        });
                    }

                    // Call API callback
                    self.onHide.call(self, event);
                }

                // Maintain toggle functionality if enabled
                self.cache.toggle = 0;

                // Hide tooltip
                if(typeof self.options.hide.effect.type === 'function') {
                    self.options.hide.effect.type.call(self.elements.tooltip, self.options.hide.effect.length);
                    self.elements.tooltip.queue(function () {
                        afterHide();
                        $(this).dequeue();
                    });
                }
                else {
                    switch (self.options.hide.effect.type.toLowerCase()) {
                        case 'fade':
                        self.elements.tooltip.fadeOut(self.options.hide.effect.length, afterHide);
                        break;

                        case 'slide':
                        self.elements.tooltip.slideUp(self.options.hide.effect.length, afterHide);
                        break;

                        case 'grow':
                        self.elements.tooltip.hide(self.options.hide.effect.length, afterHide);
                        break;

                        default:
                        self.elements.tooltip.hide(null, afterHide);
                        break;
                    }

                    // Remove active class to tooltip
                    self.elements.tooltip.removeClass(self.options.style.classes.active);
                }

                // Log event and return
                return self;
            },

            toggle: function (event, state) {
                var condition = /boolean|number/.test(typeof state) ? state : !self.elements.tooltip.is(':visible');

                self[condition ? 'show' : 'hide'](event);

                return self;
            },

            updatePosition: function (event, animate) {
                if(!self.status.rendered) {
                    return false;
                }

                var posOptions = options.position,
                target = $(posOptions.target),
                elemWidth = self.elements.tooltip.outerWidth(),
                elemHeight = self.elements.tooltip.outerHeight(),
                targetWidth, targetHeight, position,
                my = posOptions.corner.tooltip,
                at = posOptions.corner.target,
                returned,
                coords, i, mapName, imagePos,
                adapt = {
                    left: function () {
                        var leftEdge = $(window).scrollLeft(),
                        rightEdge = $(window).width() + $(window).scrollLeft(),
                        myOffset = my.x === 'center' ? elemWidth/2 : elemWidth,
                        atOffset = my.x === 'center' ? targetWidth/2 : targetWidth,
                        borderAdjust = (my.x === 'center' ? 1 : 2) * self.options.style.border.radius,
                        offset = -2 * posOptions.adjust.x,
                        pRight = position.left + elemWidth,
                        adj;

                        // Cut off by right side of window
                        if(pRight > rightEdge) {
                            adj = offset - myOffset - atOffset + borderAdjust;

                            // Shifting to the left will make whole qTip visible, or will minimize how much is cut off
                            if(position.left + adj > leftEdge || leftEdge - (position.left + adj) < pRight - rightEdge) {
                                return {
                                    adjust: adj,
                                    tip: 'right'
                                };
                            }
                        }
                        // Cut off by left side of window
                        if(position.left < leftEdge) {
                            adj = offset + myOffset + atOffset - borderAdjust;

                            // Shifting to the right will make whole qTip visible, or will minimize how much is cut off
                            if(pRight + adj < rightEdge || pRight + adj - rightEdge < leftEdge - position.left) {
                                return {
                                    adjust: adj,
                                    tip: 'left'
                                };
                            }
                        }

                        return {
                            adjust: 0,
                            tip: my.x
                        };
                    },
                    top: function () {
                        var topEdge = $(window).scrollTop(),
                        bottomEdge = $(window).height() + $(window).scrollTop(),
                        myOffset = my.y === 'center' ? elemHeight/2 : elemHeight,
                        atOffset = my.y === 'center' ? targetHeight/2 : targetHeight,
                        borderAdjust = (my.y === 'center' ? 1 : 2) * self.options.style.border.radius,
                        offset = -2 * posOptions.adjust.y,
                        pBottom = position.top + elemHeight,
                        adj;

                        // Cut off by bottom of window
                        if(pBottom > bottomEdge) {
                            adj = offset - myOffset - atOffset + borderAdjust;

                            // Shifting to the top will make whole qTip visible, or will minimize how much is cut off
                            if(position.top + adj > topEdge || topEdge - (position.top + adj) < pBottom - bottomEdge) {
                                return {
                                    adjust: adj,
                                    tip: 'bottom'
                                };
                            }
                        }
                        // Cut off by top of window
                        if(position.top < topEdge) {
                            adj = offset + myOffset + atOffset - borderAdjust;

                            // Shifting to the top will make whole qTip visible, or will minimize how much is cut off
                            if(pBottom + adj < bottomEdge || pBottom + adj - bottomEdge < topEdge - position.top) {
                                return {
                                    adjust: adj,
                                    tip: 'top'
                                };
                            }
                        }

                        return {
                            adjust: 0,
                            tip: my.y
                        };
                    }
                };

                if(event && options.position.target === 'mouse') {
                    // Force left top to allow flipping
                    at = {
                        x: 'left',
                        y: 'top'
                    };
                    targetWidth = targetHeight = 0;

                    // Use cached mouse coordiantes if not available
                    if(!event.pageX) {
                        position = self.cache.mouse;
                    }
                    else {
                        position = {
                            top: event.pageY,
                            left: event.pageX
                        };
                    }
                }
                else {
                    if(target[0] === document) {
                        targetWidth = target.width();
                        targetHeight = target.height();
                        position = {
                            top: 0,
                            left: 0
                        };
                    }
                    else if(target[0] === window) {
                        targetWidth = target.width();
                        targetHeight = target.height();
                        position = {
                            top: target.scrollTop(),
                            left: target.scrollLeft()
                        };
                    }
                    else if(target.is('area')) {
                        // Retrieve coordinates from coords attribute and parse into integers
                        coords = self.options.position.target.attr('coords').split(',');
                        for(i = 0; i < coords.length; i++) {
                            coords[i] = parseInt(coords[i], 10);
                        }

                        // Setup target position object
                        mapName = self.options.position.target.parent('map').attr('name');
                        imagePos = $('img[usemap="#' + mapName + '"]:first').offset();
                        position = {
                            left: Math.floor(imagePos.left + coords[0]),
                            top: Math.floor(imagePos.top + coords[1])
                        };

                        // Determine width and height of the area
                        switch (self.options.position.target.attr('shape').toLowerCase()) {
                            case 'rect':
                            targetWidth = Math.ceil(Math.abs(coords[2] - coords[0]));
                            targetHeight = Math.ceil(Math.abs(coords[3] - coords[1]));
                            break;

                            case 'circle':
                            targetWidth = coords[2] + 1;
                            targetHeight = coords[2] + 1;
                            break;

                            case 'poly':
                            targetWidth = coords[0];
                            targetHeight = coords[1];

                            for (i = 0; i < coords.length; i++) {
                                if(i % 2 === 0) {
                                    if(coords[i] > targetWidth) {
                                        targetWidth = coords[i];
                                    }
                                    if(coords[i] < coords[0]) {
                                        position.left = Math.floor(imagePos.left + coords[i]);
                                    }
                                }
                                else {
                                    if(coords[i] > targetHeight) {
                                        targetHeight = coords[i];
                                    }
                                    if(coords[i] < coords[1]) {
                                        position.top = Math.floor(imagePos.top + coords[i]);
                                    }
                                }
                            }

                            targetWidth = targetWidth - (position.left - imagePos.left);
                            targetHeight = targetHeight - (position.top - imagePos.top);
                            break;
                        }

                        // Adjust position by 2 pixels (Positioning bug?)
                        targetWidth -= 2;
                        targetHeight -= 2;
                    }
                    else {
                        targetWidth = target.outerWidth();
                        targetHeight = target.outerHeight();

                        if(!self.elements.tooltip.is(':visible')) {
                            self.elements.tooltip.css({
                                left: '-10000000em'
                            }).show();
                        }

                        // Account for tooltips offset parent if necessary
                        if(self.elements.tooltip.offsetParent()[0] === document.body) {
                            position = target.offset();
                        }
                        else {
                            // Account for offset parent and it's scroll positions
                            position = target.position();
                            position.top += target.offsetParent().scrollTop();
                            position.left += target.offsetParent().scrollLeft();
                        }
                    }

                    // Adjust position relative to target
                    position.left += at.x === 'right' ? targetWidth : at.x === 'center' ? targetWidth / 2 : 0;
                    position.top += at.y === 'bottom' ? targetHeight : at.y === 'center' ? targetHeight / 2 : 0;
                }

                // Adjust position relative to tooltip
                position.left += posOptions.adjust.x + (my.x === 'right' ? -elemWidth : my.x === 'center' ? -elemWidth / 2 : 0);
                position.top += posOptions.adjust.y + (my.y === 'bottom' ? -elemHeight : my.y === 'center' ? -elemHeight / 2 : 0);

                // Adjust for border radius
                if(self.options.style.border.radius > 0) {
                    if(my.x === 'left') {
                        position.left -= self.options.style.border.radius;
                    }
                    else if(my.x === 'right') {
                        position.left += self.options.style.border.radius;
                    }

                    if(my.y === 'top') {
                        position.top -= self.options.style.border.radius;
                    }
                    else if(my.y === 'bottom') {
                        position.top += self.options.style.border.radius;
                    }
                }

                // Adjust tooltip position if screen adjustment is enabled
                if(posOptions.adjust.screen) {
                    (function() {
                        var adjusted = {
                            x: 0,
                            y: 0
                        },
                        adapted = {
                            x: adapt.left(),
                            y: adapt.top()
                        },
                        tip = new Corner(options.style.tip.corner);

                        if(self.elements.tip && tip) {
                            // Adjust position according to adjustment that took place
                            if(adapted.y.adjust !== 0) {
                                position.top += adapted.y.adjust;
                                tip.y = adjusted.y = adapted.y.tip;
                            }
                            if(adapted.x.adjust !== 0) {
                                position.left += adapted.x.adjust;
                                tip.x = adjusted.x = adapted.x.tip;
                            }

                            // Update overflow cache
                            self.cache.overflow = {
                                left: adjusted.x === false,
                                top: adjusted.y === false
                            };

                            // Update and redraw the tip
                            if(self.elements.tip.attr('rel') !== tip.string()) {
                                createTip.call(self, tip);
                            }
                        }
                    }());
}

                // Initiate bgiframe plugin in IE6 if tooltip overlaps a select box or object element
                if(!self.elements.bgiframe && $.browser.msie && parseInt($.browser.version.charAt(0), 10) === 6) {
                    bgiframe.call(self);
                }

                // Call API method and if return value is false, halt
                returned = self.beforePositionUpdate.call(self, event);
                if(returned === false) {
                    return self;
                }

                // Check if animation is enabled
                if(options.position.target !== 'mouse' && animate === true) {
                    // Set animated status
                    self.status.animated = true;

                    // Animate and reset animated status on animation end
                    self.elements.tooltip.stop().animate(position, 200, 'swing', function () {
                        self.status.animated = false;
                    });
                }

                // Set new position via CSS
                else {
                    self.elements.tooltip.css(position);
                }

                // Call API method and log event if its not a mouse move
                self.onPositionUpdate.call(self, event);

                return self;
            },

            updateWidth: function (newWidth) {
                // Make sure tooltip is rendered and width is a number
                if(!self.status.rendered || (newWidth && typeof newWidth !== 'number')) {
                    return false;
                }

                // Setup elements which must be hidden during width update
                var hidden = self.elements.contentWrapper.siblings().add(self.elements.tip).add(self.elements.button),
                zoom = self.elements.wrapper.add(self.elements.contentWrapper.children()),
                tooltip = self.elements.tooltip,
                max = self.options.style.width.max,
                min = self.options.style.width.min;

                // Calculate the new width if one is not supplied
                if(!newWidth) {
                    // Explicit width is set
                    if(typeof self.options.style.width.value === 'number') {
                        newWidth = self.options.style.width.value;
                    }

                    // No width is set, proceed with auto detection
                    else {
                        // Set width to auto initally to determine new width and hide other elements
                        self.elements.tooltip.css({
                            width: 'auto'
                        });
                        hidden.hide();

                        // Set the new calculated width and if width has not numerical, grab new pixel width
                        tooltip.width(newWidth);

                        // Set position and zoom to defaults to prevent IE hasLayout bug
                        if($.browser.msie) {
                            zoom.css({
                                zoom: ''
                            });
                        }

                        // Set the new width
                        newWidth = self.getDimensions().width;

                        // Make sure its within the maximum and minimum width boundries
                        if(!self.options.style.width.value) {
                            newWidth = Math.min(Math.max(newWidth, min), max);
                        }
                    }
                }

                // Adjust newWidth by 1px if width is odd (IE6 rounding bug fix)
                if(newWidth % 2) {
                    newWidth += 1;
                }

                // Set the new calculated width and unhide other elements
                self.elements.tooltip.width(newWidth);
                hidden.show();

                // Set the border width, if enabled
                if(self.options.style.border.radius) {
                    self.elements.tooltip.find('.qtip-betweenCorners').each(function (i) {
                        $(this).width(newWidth - (self.options.style.border.radius * 2));
                    });
                }

                // IE only adjustments
                if($.browser.msie) {
                    // Reset position and zoom to give the wrapper layout (IE hasLayout bug)
                    zoom.css({
                        zoom: 1
                    });

                    // Set the new width
                    self.elements.wrapper.width(newWidth);

                    // Adjust BGIframe height and width if enabled
                    if(self.elements.bgiframe) {
                        self.elements.bgiframe.width(newWidth).height(self.getDimensions.height);
                    }
                }

                // Log event and return
                return self;
            },

            updateStyle: function (name) {
                var tip, borders, context, corner, coordinates;

                // Make sure tooltip is rendered and style is defined
                if(!self.status.rendered || typeof name !== 'string' || !$.fn.qtip.styles[name]) {
                    return false;
                }

                // Set the new style object
                self.options.style = buildStyle.call(self, $.fn.qtip.styles[name], self.options.user.style);

                // Update initial styles of content and title elements
                self.elements.content.css(jQueryStyle(self.options.style));
                if(self.options.content.title.text !== false) {
                    self.elements.title.css(jQueryStyle(self.options.style.title, true));
                }

                // Update CSS border colour
                self.elements.contentWrapper.css({
                    borderColor: self.options.style.border.color
                });

                // Update tip color if enabled
                if(self.options.style.tip.corner !== false) {
                    if($('<canvas />').get(0).getContext) {
                        // Retrieve canvas context and clear
                        tip = self.elements.tooltip.find('.qtip-tip canvas:first');
                        context = tip.get(0).getContext('2d');
                        context.clearRect(0, 0, 300, 300);

                        // Draw new tip
                        corner = tip.parent('div[rel]:first').attr('rel');
                        coordinates = calculateTip(corner, self.options.style.tip.size.width, self.options.style.tip.size.height);
                        drawTip.call(self, tip, coordinates, self.options.style.tip.color || self.options.style.border.color);
                    }
                    else if($.browser.msie) {
                        // Set new fillcolor attribute
                        tip = self.elements.tooltip.find('.qtip-tip [nodeName="shape"]');
                        tip.attr('fillcolor', self.options.style.tip.color || self.options.style.border.color);
                    }
                }

                // Update border colors if enabled
                if(self.options.style.border.radius > 0) {
                    self.elements.tooltip.find('.qtip-betweenCorners').css({
                        backgroundColor: self.options.style.border.color
                    });

                    if($('<canvas />').get(0).getContext) {
                        borders = calculateBorders(self.options.style.border.radius);
                        self.elements.tooltip.find('.qtip-wrapper canvas').each(function () {
                            // Retrieve canvas context and clear
                            context = $(this).get(0).getContext('2d');
                            context.clearRect(0, 0, 300, 300);

                            // Draw new border
                            corner = $(this).parent('div[rel]:first').attr('rel');
                            drawBorder.call(self, $(this), borders[corner], self.options.style.border.radius, self.options.style.border.color);
                        });
                    }
                    else if($.browser.msie) {
                        // Set new fillcolor attribute on each border corner
                        self.elements.tooltip.find('.qtip-wrapper [nodeName="arc"]').each(function () {
                            $(this).attr('fillcolor', self.options.style.border.color);
                        });
                    }
                }

                // Log event and return
                return self;
            },

            updateContent: function (content, reposition) {
                var parsedContent, images, loadedImages;

                function afterLoad() {
                    // Update the tooltip width
                    self.updateWidth();

                    // If repositioning is enabled, update positions
                    if(reposition !== false) {
                        // Update position if tooltip isn't static
                        if(self.options.position.type !== 'static') {
                            self.updatePosition(self.elements.tooltip.is(':visible'), true);
                        }

                        // Reposition the tip if enabled
                        if(self.options.style.tip.corner !== false) {
                            positionTip.call(self);
                        }
                    }
                }

                // Make sure content is defined if not, return
                if(!content) {
                    return false;
                }

                // Call API method and set new content if a string is returned
                parsedContent = self.beforeContentUpdate.call(self, content);
                if(typeof parsedContent === 'string') {
                    content = parsedContent;
                }
                else if(parsedContent === false) {
                    return;
                }

                // Continue normally if rendered, but if not set options.content.text instead
                if(self.status.rendered) {
                    // Set position and zoom to defaults to prevent IE hasLayout bug
                    if($.browser.msie) {
                        self.elements.contentWrapper.children().css({
                            zoom: 'normal'
                        });
                    }

                    // Append new content if its a DOM array and show it if hidden
                    if(content.jquery && content.length > 0) {
                        content.clone(true).appendTo(self.elements.content).show();
                    }

                    // Content is a regular string, insert the new content
                    else {
                        self.elements.content.html(content);
                    }

                    // Check if images need to be loaded before position is updated to prevent mis-positioning
                    images = self.elements.content.find('img[complete=false]');
                    if(images.length > 0) {
                        loadedImages = 0;
                        images.each(function (i) {
                            $('<img src="' + $(this).attr('src') + '" />').load(function () {
                                if(++loadedImages === images.length) {
                                    afterLoad();
                                }
                            });
                        });
                    }
                    else {
                        afterLoad();
                    }
                }
                else {
                    self.options.content.text = content;
                }

                // Call API method and log event
                self.onContentUpdate.call(self);
                return self;
            },

            loadContent: function (url, data, method) {
                var returned;

                function setupContent(content) {
                    // Call API method and log event
                    self.onContentLoad.call(self);

                    // Update the content
                    self.updateContent(content);
                }

                // Make sure tooltip is rendered and if not, return
                if(!self.status.rendered) {
                    return false;
                }

                // Call API method and if return value is false, halt
                returned = self.beforeContentLoad.call(self);
                if(returned === false) {
                    return self;
                }

                // Load content using specified request type
                if(method === 'post') {
                    $.post(url, data, setupContent);
                }
                else {
                    $.get(url, data, setupContent);
                }

                return self;
            },

            updateTitle: function (content) {
                var returned;

                // Make sure tooltip is rendered and content is defined
                if(!self.status.rendered || !content) {
                    return false;
                }

                // Call API method and if return value is false, halt
                returned = self.beforeTitleUpdate.call(self);
                if(returned === false) {
                    return self;
                }

                // Set the new content and reappend the button if enabled
                if(self.elements.button) {
                    self.elements.button = self.elements.button.clone(true);
                }
                self.elements.title.html(content);
                if(self.elements.button) {
                    self.elements.title.prepend(self.elements.button);
                }

                // Call API method and log event
                self.onTitleUpdate.call(self);
                return self;
            },

            focus: function (event) {
                var curIndex, newIndex, elemIndex, returned;

                // Make sure tooltip is rendered and if not, return
                if(!self.status.rendered || self.options.position.type === 'static') {
                    return false;
                }

                // Set z-index variables
                curIndex = parseInt(self.elements.tooltip.css('z-index'), 10);
                newIndex = 15000 + $('div.qtip[id^="qtip"]').length - 1;

                // Only update the z-index if it has changed and tooltip is not already focused
                if(!self.status.focused && curIndex !== newIndex) {
                    // Call API method and if return value is false, halt
                    returned = self.beforeFocus.call(self, event);
                    if(returned === false) {
                        return self;
                    }

                    // Loop through all other tooltips
                    $('div.qtip[id^="qtip"]').not(self.elements.tooltip).each(function () {
                        if($(this).qtip('api').status.rendered === true) {
                            elemIndex = parseInt($(this).css('z-index'), 10);

                            // Reduce all other tooltip z-index by 1
                            if(typeof elemIndex === 'number' && elemIndex > -1) {
                                $(this).css({
                                    zIndex: parseInt($(this).css('z-index'), 10) - 1
                                });
                            }

                            // Set focused status to false
                            $(this).qtip('api').status.focused = false;
                        }
                    });

                    // Set the new z-index and set focus status to true
                    self.elements.tooltip.css({
                        zIndex: newIndex
                    });
                    self.status.focused = true;

                    // Call API method and log event
                    self.onFocus.call(self, event);
                }

                return self;
            },

            disable: function (state) {
                self.status.disabled = state ? true : false;
                return self;
            },

            destroy: function () {
                var i, returned, interfaces,
                oldattr = self.elements.target.data('old'+self.cache.attr[0]);

                // Call API method and if return value is false, halt
                returned = self.beforeDestroy.call(self);
                if(returned === false) {
                    return self;
                }

                // Check if tooltip is rendered
                if(self.status.rendered) {
                    // Remove event handlers and remove element
                    self.options.show.when.target.unbind('mousemove.qtip', self.updatePosition);
                    self.options.show.when.target.unbind('mouseout.qtip', self.hide);
                    self.options.show.when.target.unbind(self.options.show.when.event + '.qtip');
                    self.options.hide.when.target.unbind(self.options.hide.when.event + '.qtip');
                    self.elements.tooltip.unbind(self.options.hide.when.event + '.qtip');
                    self.elements.tooltip.unbind('mouseover.qtip', self.focus);
                    self.elements.tooltip.remove();
                }

                // Tooltip isn't yet rendered, remove render event
                else {
                    self.options.show.when.target.unbind(self.options.show.when.event + '.qtip-' + self.id + '-create');
                }

                // Check to make sure qTip data is present on target element
                if(typeof self.elements.target.data('qtip') === 'object') {
                    // Remove API references from interfaces object
                    interfaces = self.elements.target.data('qtip').interfaces;
                    if(typeof interfaces === 'object' && interfaces.length > 0) {
                        // Remove API from interfaces array
                        for(i = 0; i < interfaces.length - 1; i++) {
                            if(interfaces[i].id === self.id) {
                                interfaces.splice(i, 1);
                            }
                        }
                    }
                }
                $.fn.qtip.interfaces.splice(self.id, 1);

                // Set qTip current id to previous tooltips API if available
                if(typeof interfaces === 'object' && interfaces.length > 0) {
                    self.elements.target.data('qtip').current = interfaces.length - 1;
                }
                else {
                    self.elements.target.removeData('qtip');
                }

                // Reset old title attribute if removed
                if(oldattr) {
                    self.elements.target.attr(self.cache.attr[0], oldattr);
                }

                // Call API method and log destroy
                self.onDestroy.call(self);

                return self.elements.target;
            },

            getPosition: function () {
                var show, offset;

                // Make sure tooltip is rendered and if not, return
                if(!self.status.rendered) {
                    return false;
                }

                show = (self.elements.tooltip.css('display') !== 'none') ? false : true;

                // Show and hide tooltip to make sure coordinates are returned
                if(show) {
                    self.elements.tooltip.css({
                        visiblity: 'hidden'
                    }).show();
                }
                offset = self.elements.tooltip.offset();
                if(show) {
                    self.elements.tooltip.css({
                        visiblity: 'visible'
                    }).hide();
                }

                return offset;
            },

            getDimensions: function () {
                var show, dimensions;

                // Make sure tooltip is rendered and if not, return
                if(!self.status.rendered) {
                    return false;
                }

                show = (!self.elements.tooltip.is(':visible')) ? true : false;

                // Show and hide tooltip to make sure dimensions are returned
                if(show) {
                    self.elements.tooltip.css({
                        visiblity: 'hidden'
                    }).show();
                }
                dimensions = {
                    height: self.elements.tooltip.outerHeight(),
                    width: self.elements.tooltip.outerWidth()
                };
                if(show) {
                    self.elements.tooltip.css({
                        visiblity: 'visible'
                    }).hide();
                }

                return dimensions;
            }
        });
}

    // Implementation
    $.fn.qtip = function (options, blanket) {
        var i, id, interfaces, opts, obj, command, config, api;

        // Return API / Interfaces if requested
        if(typeof options === 'string') {
            if($(this).data('qtip')) {
                // Return requested object
                if(options === 'api') {
                    return $(this).data('qtip').interfaces[$(this).data('qtip').current];
                }
                else if(options === 'interfaces') {
                    return $(this).data('qtip').interfaces;
                }
            }
            else {
                return $(this);
            }
        }

        // Validate provided options
        else {
            // Set null options object if no options are provided
            if(!options) {
                options = {};
            }

            // Sanitize option data
            if(typeof options.content !== 'object' || (options.content.jquery && options.content.length > 0)) {
                options.content = {
                    text: options.content
                };
            }
            if(typeof options.content.title !== 'object') {
                options.content.title = {
                    text: options.content.title
                };
            }
            if(typeof options.position !== 'object') {
                options.position = {
                    corner: options.position
                };
            }
            if(typeof options.position.corner !== 'object') {
                options.position.corner = {
                    target: options.position.corner,
                    tooltip: options.position.corner
                };
            }
            if(typeof options.show !== 'object') {
                options.show = {
                    when: options.show
                };
            }
            if(typeof options.show.when !== 'object') {
                options.show.when = {
                    event: options.show.when
                };
            }
            if(typeof options.show.effect !== 'object') {
                options.show.effect = {
                    type: options.show.effect
                };
            }
            if(typeof options.hide !== 'object') {
                options.hide = {
                    when: options.hide
                };
            }
            if(typeof options.hide.when !== 'object') {
                options.hide.when = {
                    event: options.hide.when
                };
            }
            if(typeof options.hide.effect !== 'object') {
                options.hide.effect = {
                    type: options.hide.effect
                };
            }
            if(typeof options.style !== 'object') {
                options.style = {
                    name: options.style
                };
            }

            // Sanitize option styles
            options.style = sanitizeStyle(options.style);

            // Build main options object
            opts = $.extend(true, {}, $.fn.qtip.defaults, options);

            // Inherit all style properties into one syle object and include original options
            opts.style = buildStyle.call({
                options: opts
            }, opts.style);
            opts.user = $.extend(true, {}, options);
        }

        // Iterate each matched element
        return $(this).each(function () // Return original elements as per jQuery guidelines
        {
            var self = $(this), content = false;

            // Check for API commands
            if(typeof options === 'string') {
                command = options.toLowerCase();
                interfaces = $(this).qtip('interfaces');

                // Make sure API data exists
                if(typeof interfaces === 'object') {
                    // Check if API call is a BLANKET DESTROY command
                    if(blanket === true && command === 'destroy') {
                        for(i = interfaces.length - 1; i > -1; i--) {
                            if('object' === typeof interfaces[i]) {
                                interfaces[i].destroy();
                            }
                        }
                    }

                    // API call is not a BLANKET DESTROY command
                    else {
                        // Check if supplied command effects this tooltip only (NOT BLANKET)
                        if(blanket !== true) {
                            interfaces = [$(this).qtip('api')];
                        }

                        // Execute command on chosen qTips
                        for (i = 0; i < interfaces.length; i++) {
                            // Destroy command doesn't require tooltip to be rendered
                            if(command === 'destroy') {
                                interfaces[i].destroy();
                            }

                            // Only call API if tooltip is rendered and it wasn't a destroy call
                            else if(interfaces[i].status.rendered === true) {
                                if(command === 'show') {
                                    interfaces[i].show();
                                }
                                else if(command === 'hide') {
                                    interfaces[i].hide();
                                }
                                else if(command === 'focus') {
                                    interfaces[i].focus();
                                }
                                else if(command === 'disable') {
                                    interfaces[i].disable(true);
                                }
                                else if(command === 'enable') {
                                    interfaces[i].disable(false);
                                }
                                else if(command === 'update') {
                                    interfaces[i].updatePosition();
                                }
                            }
                        }
                    }
                }
            }

            // No API commands, continue with qTip creation
            else {
                // Create unique configuration object
                config = $.extend(true, {}, opts);
                config.hide.effect.length = opts.hide.effect.length;
                config.show.effect.length = opts.show.effect.length;

                // Sanitize target options
                if(config.position.container === false) {
                    config.position.container = $(document.body);
                }
                if(config.position.target === false) {
                    config.position.target = $(this);
                }
                if(config.show.when.target === false) {
                    config.show.when.target = $(this);
                }
                if(config.hide.when.target === false) {
                    config.hide.when.target = $(this);
                }

                // Parse corner options
                config.position.corner.tooltip = new Corner(config.position.corner.tooltip);
                config.position.corner.target = new Corner(config.position.corner.target);

                // If no content is provided, check title and alt attributes for fallback
                if(!config.content.text.length) {
                    $(['title', 'alt']).each(function(i, attr) {
                        var val = self.attr(attr);
                        if(val && val.length) {
                            content = [attr, val];
                            self.data('old'+attr, val).removeAttr(attr);
                            config.content.text = val.replace(/\n/gi, '<br />');
                            return false;
                        }
                    });
                }

                // Determine tooltip ID (Reuse array slots if possible)
                id = $.fn.qtip.interfaces.length;
                for (i = 0; i < id; i++) {
                    if(typeof $.fn.qtip.interfaces[i] === 'undefined') {
                        id = i;
                        break;
                    }
                }

                // Instantiate the tooltip
                obj = new QTip($(this), config, id);

                // Add API references and cache content if present
                $.fn.qtip.interfaces[id] = obj;
                obj.cache.attr = content;

                // Check if element already has qTip data assigned
                if(typeof $(this).data('qtip') === 'object' && $(this).data('qtip')) {
                    // Set new current interface id
                    if(typeof $(this).attr('qtip') === 'undefined') {
                        $(this).data('qtip').current = $(this).data('qtip').interfaces.length;
                    }

                    // Push new API interface onto interfaces array
                    $(this).data('qtip').interfaces.push(obj);
                }

                // No qTip data is present, create now
                else {
                    $(this).data('qtip', {
                        current: 0,
                        interfaces: [obj]
                    });
                }

                // If prerendering is disabled, create tooltip on showEvent
                if(config.content.prerender === false && config.show.when.event !== false && config.show.ready !== true) {
                    config.show.when.target.bind(config.show.when.event + '.qtip-' + id + '-create', {
                        qtip: id
                    }, function (event) {
                        // Retrieve API interface via passed qTip Id
                        api = $.fn.qtip.interfaces[event.data.qtip];

                        // Unbind show event and cache mouse coords
                        api.options.show.when.target.unbind(api.options.show.when.event + '.qtip-' + event.data.qtip + '-create');
                        api.cache.mouse = {
                            left: event.pageX,
                            top: event.pageY
                        };

                        // Render tooltip and start the event sequence
                        construct.call(api);
                        api.options.show.when.target.trigger(api.options.show.when.event);
                    });
                }

                // Prerendering is enabled, create tooltip now
                else {
                    // Set mouse position cache to top left of the element
                    obj.cache.mouse = {
                        left: config.show.when.target.offset().left,
                        top: config.show.when.target.offset().top
                    };

                    // Construct the tooltip
                    construct.call(obj);
                }
            }
        });
};

    // Define qTip API interfaces array
    $.fn.qtip.interfaces = [];

    /* Add intermediary method to the 'attr' class to allow other plugins to successfully
    retrieve the title of an element with a qTip applied */
    $.fn.qtip.fn = {
        attr: $.fn.attr
    };
    $.fn.attr = function(attr) {
        var api = $(this).qtip('api');

        return (arguments.length === 1 && (/title|alt/i).test(attr) && api.status && api.status.rendered === true)
        ? $(this).data('old' + api.cache.attr[0])
        : $.fn.qtip.fn.attr.apply(this, arguments);
    };

    // Define configuration defaults
    $.fn.qtip.defaults = {
        // Content
        content: {
            prerender: false,
            text: false,
            url: false,
            data: null,
            title: {
                text: false,
                button: false
            }
        },
        // Position
        position: {
            target: false,
            corner: {
                target: 'bottomRight',
                tooltip: 'topLeft'
            },
            adjust: {
                x: 0,
                y: 0,
                mouse: true,
                screen: false,
                scroll: true,
                resize: true
            },
            type: 'absolute',
            container: false
        },
        // Effects
        show: {
            when: {
                target: false,
                event: 'mouseover'
            },
            effect: {
                type: 'fade',
                length: 100
            },
            delay: 140,
            solo: false,
            ready: false
        },
        hide: {
            when: {
                target: false,
                event: 'mouseout'
            },
            effect: {
                type: 'fade',
                length: 100
            },
            delay: 0,
            fixed: false
        },
        // Callbacks
        api: {
            beforeRender: function () {},
            onRender: function () {},
            beforePositionUpdate: function () {},
            onPositionUpdate: function () {},
            beforeShow: function () {},
            onShow: function () {},
            beforeHide: function () {},
            onHide: function () {},
            beforeContentUpdate: function () {},
            onContentUpdate: function () {},
            beforeContentLoad: function () {},
            onContentLoad: function () {},
            beforeTitleUpdate: function () {},
            onTitleUpdate: function () {},
            beforeDestroy: function () {},
            onDestroy: function () {},
            beforeFocus: function () {},
            onFocus: function () {}
        }
    };

    $.fn.qtip.styles = {
        defaults: {
            background: 'white',
            color: '#111',
            overflow: 'hidden',
            textAlign: 'left',
            width: {
                min: 0,
                max: 250
            },
            padding: '5px 9px',
            border: {
                width: 1,
                radius: 0,
                color: '#d3d3d3'
            },
            tip: {
                corner: false,
                color: false,
                size: {
                    width: 13,
                    height: 13
                },
                opacity: 1
            },
            title: {
                background: '#e1e1e1',
                fontWeight: 'bold',
                padding: '7px 12px'
            },
            button: {
                cursor: 'pointer'
            },
            classes: {
                target: '',
                tip: 'qtip-tip',
                title: 'qtip-title',
                button: 'qtip-button',
                content: 'qtip-content',
                active: 'qtip-active'
            }
        },
        cream: {
            border: {
                width: 3,
                radius: 0,
                color: '#F9E98E'
            },
            title: {
                background: '#F0DE7D',
                color: '#A27D35'
            },
            background: '#FBF7AA',
            color: '#A27D35',

            classes: {
                tooltip: 'qtip-cream'
            }
        },
        light: {
            border: {
                width: 3,
                radius: 0,
                color: '#E2E2E2'
            },
            title: {
                background: '#f1f1f1',
                color: '#454545'
            },
            background: 'white',
            color: '#454545',

            classes: {
                tooltip: 'qtip-light'
            }
        },
        dark: {
            border: {
                width: 3,
                radius: 0,
                color: '#303030'
            },
            title: {
                background: '#404040',
                color: '#f3f3f3'
            },
            background: '#505050',
            color: '#f3f3f3',

            classes: {
                tooltip: 'qtip-dark'
            }
        },
        red: {
            border: {
                width: 3,
                radius: 0,
                color: '#CE6F6F'
            },
            title: {
                background: '#f28279',
                color: '#9C2F2F'
            },
            background: '#F79992',
            color: '#9C2F2F',

            classes: {
                tooltip: 'qtip-red'
            }
        },
        green: {
            border: {
                width: 3,
                radius: 0,
                color: '#A9DB66'
            },
            title: {
                background: '#b9db8c',
                color: '#58792E'
            },
            background: '#CDE6AC',
            color: '#58792E',

            classes: {
                tooltip: 'qtip-green'
            }
        },
        blue: {
            border: {
                width: 3,
                radius: 0,
                color: '#ADD9ED'
            },
            title: {
                background: '#D0E9F5',
                color: '#5E99BD'
            },
            background: '#E5F6FE',
            color: '#4D9FBF',

            classes: {
                tooltip: 'qtip-blue'
            }
        }
    };
}(jQuery));
