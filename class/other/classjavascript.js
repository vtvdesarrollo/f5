function validaringreso(){

    if($("#login").val()==""){
        jAlert('Disculpe debe escribir la cedula');
        $("#login").focus();
        return false;
    }

    if($("#clave").val()==""){
        jAlert('Disculpe debe escribir la clave');
        $("#clave").focus();
        return false;
    }


    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "login="+$("#login").val()
        +"&clave="+$("#clave").val()
        +"&obtenerFuncion=validaringresousuario",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#form1");
            $("#form1").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}



function customRange(input) {
    return {minDate: "today"};
}

function customRange_2(input) {
    return {minDate: $("#fecha1").datepick("getDate")};
}

function customRange_3(input) {
    return {minDate: $("#fecha2").datepick("getDate")};
}

function customRange_4(input) {
    return {minDate: $("#fecha3").datepick("getDate")};
}

function customRange_estadistica(input){
    if($("#fecha_ini").datepick("getDate")!=null){
        return {minDate: $("#fecha_ini").datepick("getDate")};
    }else{
        return {minDate: "today"};
    }
}

function validarfechaestatistica(dateText, inst){
    var fechaIni=$("#fecha_ini").val().split("/");
    var fechaFin=$("#fecha_fin").val().split("/");

    var fechainicial=fechaIni[2]+fechaIni[1]+fechaIni[0];
    var fechafinal=fechaFin[2]+fechaFin[1]+fechaFin[0];

    if($(this).attr('id')=="fecha_ini"){
        if(fechafinal<fechainicial){
            //$("#fecha_ini").val($("#fecha_fin").val())
            $("#fecha_fin").val($("#fecha_ini").val())
        }
    }
}

function validadorfechas(dateText, inst){
        //alert($(this).attr('id'));
        if($(this).attr('id')=="fecha1"){
            var fechaIni=$("#fecha1").val().split("/");
            var fechaFin1=$("#fecha2").val().split("/");
            var fechaFin2=$("#fecha3").val().split("/");
            var fechaFin3=$("#fecha4").val().split("/");
            var fechainicial=fechaIni[2]+fechaIni[1]+fechaIni[0];
            var fechafinal1=fechaFin1[2]+fechaFin1[1]+fechaFin1[0];
            var fechafinal2=fechaFin2[2]+fechaFin2[1]+fechaFin2[0];
            var fechafinal3=fechaFin3[2]+fechaFin3[1]+fechaFin3[0];
            if(parseInt(fechainicial)>parseInt(fechafinal1)){
                $("#fecha2").val($("#fecha1").val());
                if(parseInt(fechainicial)>parseInt(fechafinal2)){
                    $("#fecha3").val($("#fecha1").val());
                }
                if(parseInt(fechainicial)>parseInt(fechafinal3)){
                    $("#fecha4").val($("#fecha1").val());
                }
            }else if(parseInt(fechainicial)>parseInt(fechafinal2)){
                $("#fecha3").val($("#fecha1").val());
            }else if(parseInt(fechainicial)>parseInt(fechafinal3)){
                $("#fecha4").val($("#fecha1").val());
            }
        }

        if($(this).attr('id')=="fecha2"){
            var fechaIni=$("#fecha2").val().split("/");
            var fechaFin1=$("#fecha3").val().split("/");
            var fechaFin2=$("#fecha4").val().split("/");
            var fechainicial=fechaIni[2]+fechaIni[1]+fechaIni[0];
            var fechafinal1=fechaFin1[2]+fechaFin1[1]+fechaFin1[0];
            var fechafinal2=fechaFin2[2]+fechaFin2[1]+fechaFin2[0];
            if(parseInt(fechainicial)>parseInt(fechafinal1)){
                $("#fecha3").val($("#fecha2").val());
                if(parseInt(fechainicial)>parseInt(fechafinal2)){
                    $("#fecha4").val($("#fecha2").val());
                }
            }else if(parseInt(fechainicial)>parseInt(fechafinal2)){
                $("#fecha4").val($("#fecha2").val());
            }
        }

        if($(this).attr('id')=="fecha3"){
            var fechaIni=$("#fecha3").val().split("/");
            var fechaFin=$("#fecha4").val().split("/");
            var fechainicial=fechaIni[2]+fechaIni[1]+fechaIni[0];
            var fechafinal=fechaFin[2]+fechaFin[1]+fechaFin[0];
            if(parseInt(fechainicial)>parseInt(fechafinal)){
                $("#fecha4").val($("#fecha3").val());
            }
        }
        validarhoras($(this));
    }

    function validarhoras(obj){
        var fila=new Array();
        var fecha=new Array();
        var c=0;
        $(obj).parents("tbody").find("tr").each(function(){
            fila[c]=new Array();
            fila[c]['hora']="";
            $(this).find("select[class='campo_vl']").each(function(){
                if($(this).val()!="60" && $(this).val()!="24"){
                    if(parseInt($(this).val())<10){
                        fila[c]['hora']=fila[c]['hora']+"0"+$(this).val()
                    }else{
                        fila[c]['hora']=fila[c]['hora']+$(this).val()
                    }
                }else{
                    fila[c]['hora']=fila[c]['hora']+"00"
                }
            })
            var fechaexplod=undefined;
            fechaexplod=$(this).find("input[name*='fecha']").val().split("/");
            fecha[c]=fechaexplod[2]+fechaexplod[1]+fechaexplod[0];
            c++;
        })

        if(parseInt(fecha[0]+fila[0]['hora'])>parseInt(fecha[1]+fila[1]['hora'])){
            $("#hora2").val($("#hora1").val());
            $("#min2").val($("#min1").val());
            if(parseInt(fecha[0]+fila[0]['hora'])>parseInt(fecha[2]+fila[2]['hora'])){
                $("#hora3").val($("#hora1").val());
                $("#min3").val($("#min1").val());
            }

            if(parseInt(fecha[0]+fila[0]['hora'])>parseInt(fecha[3]+fila[3]['hora'])){
                $("#hora4").val($("#hora1").val());
                $("#min4").val($("#min1").val());
            }
        }else if(parseInt(fecha[1]+fila[1]['hora'])>parseInt(fecha[2]+fila[2]['hora'])){
            $("#hora3").val($("#hora2").val());
            $("#min3").val($("#min2").val());
            if(parseInt(fecha[1]+fila[1]['hora'])>parseInt(fecha[3]+fila[3]['hora'])){
                $("#hora4").val($("#hora2").val());
                $("#min4").val($("#min2").val());
            }
        }else if(parseInt(fecha[2]+fila[2]['hora'])>parseInt(fecha[3]+fila[3]['hora'])){
            $("#hora4").val($("#hora3").val());
            $("#min4").val($("#min3").val());
        }
    }

    function cambianomenclatura(obj){
        if($(obj).val()=="1"){
            $("#grabado_div").fadeOut(function(){
                $("#envivo_div").fadeIn();
            })
        }else{
            $("#envivo_div").fadeOut(function(){
                $("#grabado_div").fadeIn();
            })
        }
    }

    function ir_a(url){
        window.top.location.href = url
    }


// JavaScript Document
function aparecer(div){
    //div es el id del div que se le va a colocar le efecto
    $(div).hide();

    $(div).animate({
        opacity: 0.2,
        height: 'toggle'
    }, 1000 );

    $(div).animate({
        opacity: 1,
    //height: 'toggle'
}, 1000);
}

function serialize(obj)
{
    var returnVal;
    if(obj != undefined){
        switch(obj.constructor)
        {
            case Array:
            var vArr="[";
            for(var i=0;i<obj.length;i++)
            {
                if(i>0) vArr += ",";
                vArr += serialize(obj[i]);
            }
            vArr += "]"
            return vArr;
            case String:
            returnVal = escape("'" + obj + "'");
            return returnVal;
            case Number:
            returnVal = isFinite(obj) ? obj.toString() : null;
            return returnVal;
            case Date:
            returnVal = "#" + obj + "#";
            return returnVal;
            default:
            if(typeof obj == "object"){
                var vobj=[];
                for(attr in obj)
                {
                    if(typeof obj[attr] != "function")
                    {
                        vobj.push('"' + attr + '":' + serialize(obj[attr]));
                    }
                }
                if(vobj.length >0)
                    return "{" + vobj.join(",") + "}";
                else
                    return "{}";
            }
            else
            {
                return obj.toString();
            }
        }
    }
    return null;
}

function olvidocontrasena ()
{

    if($("#cedula").val()==""){
        jAlert('Disculpe debe ingresar su numero de cedula');
        $("#cedula").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "cedula="+$("#cedula").val()
        +"&obtenerFuncion=olvidocontrasena",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp2");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });

}
/////////////////////////////////////Guardar pauta/////////////////////////////////////////////////

function guardarpauta_f5()
{

    if($("#pauta").val()==0){
        jAlert('Disculpe debe seleccionar un tipo de pauta');
        $("#pauta").focus();
        return false;
    }

    if($("#pauta").val()==1){
        if($("#programa").val()==0){
            jAlert('Disculpe debe seleccionar el programa');
            $("#programa").focus();
            return false;
        }else{
            var programa=$("#programa").val();
            var evento="";
        }
    }

    if($("#pauta").val()==2){
        if($("#evento").val()==""){
            jAlert('Disculpe debe escribir el evento');
            $("#evento").focus();
            return false;
        }else{
            var evento=$("#evento").val();
            var programa=0;
        }
    }

    if($("#locacion").val()==0){
        jAlert('Disculpe debe seleccionar locaci&oacute;n');
        $("#locacion").focus();
        return false;
    }

    if($("#locacion").val()==1){
        if($("#lugar_pt").val()=="") {
            jAlert('Disculpe debe escribir el lugar');
            $("#lugar_pt").focus();
            return false;
        }else{
            var lugar_pauta=$("#lugar_pt").val();
        }
    }else{
        if($("#lugar_lef").val()==0){
            jAlert('Disculpe debe seleccionar el lugar');
            $("#lugar_lef").focus();
            return false;
        }else{
            var lugar_pauta=$("#lugar_lef").val();
        }
    }



    if($("#tipoevento").val()==0){
        jAlert('Disculpe debe seleccionar un tipo de evento');
        $("#tipoevento").focus();
        return false;
    }

    if($("#tipotraje").val()==0){
        jAlert('Disculpe debe seleccionar un tipo de traje');
        $("#tipotraje").focus();
        return false;
    }

    if($("#fecha1").val()==""){
        jAlert('Disculpe debe seleccionar la fecha de la citaci&oacute;n');
        $("#fecha1").focus();
        return false;
    }

    if($("#hora1").val()=="24"){
        jAlert('Disculpe debe seleccionar la hora de la citaci&oacute;n');
        $("#hora1").focus();
        return false;
    }
    if($("#min1").val()=="60"){
        jAlert('Disculpe debe seleccionar los minutos de la citaci&oacute;n');
        $("#min1").focus();
        return false;
    }

    if($("#lugar_citacion_lef").val()==0){
        jAlert('Disculpe debe seleccionar el lugar de la citacion');
        $("#lugar_citacion_lef").focus();
        return false;
    }


    if($("#fecha2").val()==""){
        jAlert('Disculpe debe seleccionar la fecha del montaje');
        $("#fecha2").focus();
        return false;
    }
    if($("#hora2").val()=="24"){
        jAlert('Disculpe debe seleccionar la hora del montaje');
        $("#hora2").focus();
        return false;
    }
    if($("#min2").val()=="60"){
        jAlert('Disculpe debe seleccionar los minutos del montaje');
        $("#min2").focus();
        return false;
    }

    if($("#fecha3").val()==""){
        jAlert('Disculpe debe seleccionar la fecha de la emisi&oacute;n');
        $("#fecha3").focus();
        return false;
    }
    if($("#hora3").val()=="24"){
        jAlert('Disculpe debe seleccionar la hora de la emisi&oacute;n');
        $("#hora3").focus();
        return false;
    }
    if($("#min3").val()=="60"){
        jAlert('Disculpe debe seleccionar los minutos de la emisi&oacute;n');
        $("#min3").focus();
        return false;
    }


    if($("#locacion").val()==1){
        if($("#fecha4").val()==""){
            jAlert('Disculpe debe seleccionar la fecha del retorno');
            $("#fecha4").focus();
            return false;
        }
        if($("#hora4").val()=="24"){
            jAlert('Disculpe debe seleccionar la hora del retorno');
            $("#hora4").focus();
            return false;
        }
        if($("#min4").val()=="60"){
            jAlert('Disculpe debe seleccionar los minutos del retorno');
            $("#min4").focus();
            return false;
        }
    }

    try{
        productor=$("#selectProductor").val();
        if(productor!="0"){
            productor="&cedulaproductor="+productor
        }else{
            productor="";
        }
    }catch(err){
    }

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "pauta_aux="+$("#pauta").val()
        +"&programa_aux="+programa
        +"&evento_aux="+evento
        +"&locacion_aux="+$("#locacion").val()
        +"&tipoevento_aux="+$("#tipoevento").val()
        +"&tipotraje_aux="+$("#tipotraje").val()
        +"&lugar_pauta="+lugar_pauta
        +"&lugar_citacion="+$("#lugar_citacion_lef").val()
        +"&fecha1_aux="+$("#fecha1").val()
        +"&hora1_aux="+$("#hora1").val()
        +"&min1_aux="+$("#min1").val()
        +"&fecha2_aux="+$("#fecha2").val()
        +"&hora2_aux="+$("#hora2").val()
        +"&min2_aux="+$("#min2").val()
        +"&fecha3_aux="+$("#fecha3").val()
        +"&hora3_aux="+$("#hora3").val()
        +"&min3_aux="+$("#min3").val()
        +"&fecha4_aux="+$("#fecha4").val()
        +"&hora4_aux="+$("#hora4").val()
        +"&min4_aux="+$("#min4").val()
        +productor
        +"&obtenerFuncion=guardarpauta_f5",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            window.location = datos;
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });
}

function modificarpauta_f5(){

    if($("#pauta").val()==0){
        jAlert('Disculpe debe seleccionar un tipo de pauta');
        $("#pauta").focus();
        return false;
    }

    if($("#pauta").val()==1){
        if($("#programa").val()==0){
            jAlert('Disculpe debe seleccionar el programa');
            $("#programa").focus();
            return false;
        }else{
            var programa=$("#programa").val();
            var evento="";
        }
    }

    if($("#pauta").val()==2){
        if($("#evento").val()==""){
            jAlert('Disculpe debe escribir el evento');
            $("#evento").focus();
            return false;
        }else{
            var evento=$("#evento").val();
            var programa=0;
        }
    }

    if($("#locacion").val()==0){
        jAlert('Disculpe debe seleccionar locaci&oacute;n');
        $("#locacion").focus();
        return false;
    }

    if($("#locacion").val()==1){
        if($("#lugar_pt").val()=="") {
            jAlert('Disculpe debe escribir el lugar');
            $("#lugar_pt").focus();
            return false;
        }else{
            var lugar_pauta=$("#lugar_pt").val();
        }
    }else{
        if($("#lugar_lef").val()==0){
            jAlert('Disculpe debe seleccionar el lugar');
            $("#lugar_lef").focus();
            return false;
        }else{
            var lugar_pauta=$("#lugar_lef").val();
        }
    }

    if($("#tipoevento").val()==0){
        jAlert('Disculpe debe seleccionar un tipo de evento');
        $("#tipoevento").focus();
        return false;
    }

    if($("#tipotraje").val()==0){
        jAlert('Disculpe debe seleccionar un tipo de traje');
        $("#tipotraje").focus();
        return false;
    }

    if($("#fecha1").val()==""){
        jAlert('Disculpe debe seleccionar la fecha de la citaci&oacute;n');
        $("#fecha1").focus();
        return false;
    }

    if($("#hora1").val()=="24"){
        jAlert('Disculpe debe seleccionar la hora de la citaci&oacute;n');
        $("#hora1").focus();
        return false;
    }
    if($("#min1").val()=="60"){
        jAlert('Disculpe debe seleccionar los minutos de la citaci&oacute;n');
        $("#min1").focus();
        return false;
    }

    if($("#lugar_citacion_lef").val()==0){
        jAlert('Disculpe debe seleccionar el lugar de la citacion');
        $("#lugar_citacion_lef").focus();
        return false;
    }


    if($("#fecha2").val()==""){
        jAlert('Disculpe debe seleccionar la fecha del montaje');
        $("#fecha2").focus();
        return false;
    }
    if($("#hora2").val()=="24"){
        jAlert('Disculpe debe seleccionar la hora del montaje');
        $("#hora2").focus();
        return false;
    }
    if($("#min2").val()=="60"){
        jAlert('Disculpe debe seleccionar los minutos del montaje');
        $("#min2").focus();
        return false;
    }


    if($("#fecha3").val()==""){
        jAlert('Disculpe debe seleccionar la fecha de la emisi&oacute;n');
        $("#fecha3").focus();
        return false;
    }
    if($("#hora3").val()=="24"){
        jAlert('Disculpe debe seleccionar la hora de la emisi&oacute;n');
        $("#hora3").focus();
        return false;
    }
    if($("#min3").val()=="60"){
        jAlert('Disculpe debe seleccionar los minutos de la emisi&oacute;n');
        $("#min3").focus();
        return false;
    }


    if($("#locacion").val()==1)
    {

        if($("#fecha4").val()==""){
            jAlert('Disculpe debe seleccionar la fecha del retorno');
            $("#fecha4").focus();
            return false;
        }
        if($("#hora4").val()=="24"){
            jAlert('Disculpe debe seleccionar la hora del retorno');
            $("#hora4").focus();
            return false;
        }

        if($("#min4").val()=="60"){
            jAlert('Disculpe debe seleccionar los minutos del retorno');
            $("#min4").focus();
            return false;
        }
    }
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "pauta_aux="+$("#pauta").val()
        +"&programa_aux="+programa
        +"&evento_aux="+evento
        +"&locacion_aux="+$("#locacion").val()
        +"&tipoevento_aux="+$("#tipoevento").val()
        +"&tipotraje_aux="+$("#tipotraje").val()
        +"&lugar_pauta="+lugar_pauta
        +"&lugar_citacion="+$("#lugar_citacion_lef").val()
        +"&fecha1_aux="+$("#fecha1").val()
        +"&hora1_aux="+$("#hora1").val()
        +"&min1_aux="+$("#min1").val()
        +"&fecha2_aux="+$("#fecha2").val()
        +"&hora2_aux="+$("#hora2").val()
        +"&min2_aux="+$("#min2").val()
        +"&fecha3_aux="+$("#fecha3").val()
        +"&hora3_aux="+$("#hora3").val()
        +"&min3_aux="+$("#min3").val()
        +"&fecha4_aux="+$("#fecha4").val()
        +"&hora4_aux="+$("#hora4").val()
        +"&min4_aux="+$("#min4").val()
        +"&id_pauta="+$("#id_pauta").val()
        +"&obtenerFuncion=modificarpauta_f5",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
           window.location = datos;
            //aparecer("#datosp");
            //$("#datosp").html(datos);
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });
}

function agregarenlista_f5(obj,id_pauta){
    if($("#servicios").val()==0){
        jAlert('Disculpe debe seleccionar el material que desea agregar');
        $("#servicios").focus();
        return false;
    }

    if($("#cantidad").val()==""){
        jAlert('Disculpe debe escribir la cantidad del recurso');
        $("#cantidad").focus();
        return false;
    }
    if($("#cantidad").val()==0){
        jAlert('Disculpe la cantidad del recurso debe ser mayor que cero');
        $("#cantidad").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "servicios_aux="+$("#servicios").val()
        +"&id_pauta="+id_pauta

        +"&cantidad_aux="+$("#cantidad").val()
        +"&obtenerFuncion=agregarenlista_f5",
        beforeSend:function(datos){
            $("#cargando2").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            $("#servicios").val("");
            $("#cantidad").val("");
            reload_serviciosrequeridos(id_pauta);
        },
        complete:function(datos){
            $("#cargando2").fadeOut("slow");
        }
    });
}

function agregarenlista_f5_rh(obj,id_pauta)
{
    if($("#servicios2").val()==0){
        jAlert('Disculpe debe seleccionar el material que desea agregar');
        $("#servicios").focus();
        return false;
    }

    if($("#cantidad2").val()==""){
        jAlert('Disculpe debe escribir la cantidad del recurso');
        $("#cantidad").focus();
        return false;
    }
    if($("#cantidad2").val()==0){
        jAlert('Disculpe la cantidad del recurso debe ser mayor que cero');
        $("#cantidad").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "servicios_aux="+$("#servicios2").val()
        +"&id_tipo_rec=2"
        +"&id_pauta="+id_pauta

        +"&cantidad_aux="+$("#cantidad2").val()
        +"&obtenerFuncion=agregarenlista_f5",
        beforeSend:function(datos){
            $("#cargando2").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            $("#servicios2").val("");
            $("#cantidad2").val("");
            reload_serviciosrequeridos(id_pauta);
        },
        complete:function(datos){
            $("#cargando2").fadeOut("slow");
        }
    });
}


function eliminardelista_f5(obj,id_pauta,id_rec)
{
    jConfirm('Esta usted seguro de realizar esta accion', 'Confirmacion', function(r) {
        if(r==true){
            id_recurso=$(obj).parents("tr").attr("id");
            $.ajax({
                type: "GET",
                url: "classDirectorioFunciones.php",
                data:
                "id_pauta="+id_pauta
                +"&id_rec="+id_rec
                +"&id_recurso="+id_recurso
                +"&obtenerFuncion=eliminardelista_f5",
                beforeSend:function(datos){
                    $("#cargando2").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
                },
                success:function(datos){
                    $("#servicios").val("");
                    $("#cantidad").val("");
                    reload_serviciosrequeridos(id_pauta);
                },
                complete:function(datos){
                    $("#cargando2").fadeOut("slow");
                }
            });

        }else{
            return false;
        }
    });
}

function modcantidad(id_pauta, tipo_material, accion, tipo_recurso){
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "idtipomaterial="+tipo_material
        +"&id_pauta="+id_pauta
        +"&accion="+accion
        +"&tipo_recurso="+tipo_recurso
        +"&obtenerFuncion=modcantidad",
        success:function(datos){
            reload_serviciosrequeridos(id_pauta);
        }
    });
}

function textsum(obj,$operador){
    if($(obj).parents("tr").find("input[type='text']").val()=="" || $(obj).parents("tr").find("input[type='text']").val()==undefined || $(obj).parents("tr").find("input[type='text']").val()=="0"){
        $valoractual=0;
    }else{
        $valoractual=parseInt($(obj).parents("tr").find("input[type='text']").val())
    }
    eval("$valornuevo="+$valoractual+$operador+"1");
    if($valornuevo>=0){
        $(obj).parents("tr").find("input[type='text']").val($valornuevo);
    }
}

function reload_serviciosrequeridos(id_pauta){
   $.ajax({
    type: "GET",
    url: "classDirectorioFunciones.php",
    data:
    "id_pauta="+id_pauta
    +"&html=imprimir"
    +"&obtenerFuncion=tablarecursos_solicitados",
    beforeSend:function(datos){
        $("#cargando2").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
    },
    success:function(datos){
        $("#hb_solicitarmateriales").html("");
        $("#hb_solicitarmateriales").html(datos);
    },
    complete:function(datos){
        $("#cargando2").fadeOut("slow");
    }
});
}

function agregarenlalista(id_pauta, clave_tmaterial)
{
    if($("#servicios").val()==0){
        jAlert('Disculpe debe seleccionar el material que desea agregar');
        $("#servicios").focus();
        return false;
    }

    if($("#cantidad").val()==""){
        jAlert('Disculpe debe escribir la cantidad del recurso');
        $("#cantidad").focus();
        return false;
    }
    if($("#cantidad").val()==0){
        jAlert('Disculpe la cantidad del recurso debe ser mayor que cero');
        $("#cantidad").focus();
        return false;
    }

    if(($("#listarecursos").find('tr[id="'+$("#servicios").val()+'"]').length)>=1){
        jAlert('Disculpe, ese recurso ya fue agregado',function(){
            $("#servicios").val("");
            $("#cantidad").val("");
        });
    }else{
        servidorajax("obtenerFuncion=agregarenlalista"
            +"&servicios_aux="+$("#servicios").val()
            +"&id_pauta="+id_pauta
            +"&cantidad_aux="+$("#cantidad").val()
            ,function(data){
                aparecer("#datosp2");
                $("#datosp2").html(data);
                $("#servicios").val("");
                $("#cantidad").val("");
                $.msDropDown.create("body select");
                reload_materiales(id_pauta, 'materiales_porasignar', clave_tmaterial);
                reload_asignaciones(id_pauta,clave_tmaterial);
            })
    }
}


function agregarenlista_asignar(id_pauta,id_detalle_servicio,tipo_recurso)
{
    if($("#caracteristicas"+id_detalle_servicio).val()==0){
        jAlert('Disculpe debe seleccionar la caracteristica que desea agregar');
        $("#caracteristicas").focus();
        return false;
    }
    /*if($("#caracteristicas"+id_detalle_servicio).val()==''){
        jAlert('Disculpe no hay mas recursos para asignar');
        $("#caracteristicas").focus();
        return false;
    }*/

    //alert($("#caracteristicas"+id_detalle_servicio).val());
    servidorajax("obtenerFuncion=agregarenlista_asignar"
        +"&caracteristicas="+$("#caracteristicas"+id_detalle_servicio).val()
        +"&id_pauta="+id_pauta
        +"&id_detalle_servicio="+id_detalle_servicio
        ,function(data){
            //alert(data);
            $("#caracteristicas"+id_detalle_servicio).parents("tr").remove();
            reload_materiales(id_pauta, 'materiales_porasignar', tipo_recurso);
            if(tipo_recurso==1){
                reload_asignaciones(id_pauta,"rm");
            }else{
                reload_asignaciones(id_pauta);
            }
        })
}

function reload_asignaciones(id_pauta, clave_tmaterial){
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:"obtenerFuncion=reload_asignados"
        +"&id_pauta="+id_pauta
        +"&tm="+clave_tmaterial,
        beforeSend:function(datos){
            $("#cargando2").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            //
            $("#recursos_hb_asignados").html("");
            $("#recursos_hb_asignados").html(datos);
        },
        complete:function(datos){
            $("#cargando2").fadeOut("slow");
        }
    });
}

function reload_materiales(id_pauta, contenedor, clave_tmaterial){
    var contenedor = contenedor;
    servidorajax("obtenerFuncion=get_selectsmateriales"
        +"&id_pauta="+id_pauta
        +"&tm="+clave_tmaterial
        ,function(datos){
            $("#materiales_porasignar").html("");
            $("#materiales_porasignar").html(datos);
            try {
                $.msDropDown.create("body select");
            }
            catch(e) {
                alert("Error: "+e.message);
            }
        })


}


function agregarenlista_cambiado(id_pauta,id_detalle_servicio)
{
    if($("#caracteristicas"+id_detalle_servicio).val()==0){
        jAlert('Disculpe debe seleccionar la caracteristica que desea agregar');
        $("#caracteristicas").focus();
        return false;
    }
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "caracteristicas="+$("#caracteristicas"+id_detalle_servicio).val()
        +"&id_pauta="+id_pauta
        +"&id_detalle_servicio="+id_detalle_servicio
        +"&obtenerFuncion=agregarenlista_cambiado",
        beforeSend:function(datos){
            $("#cargando2").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp2");
            $("#datosp2").html(datos);
            $("#servicios").val("");
            $("#cantidad").val("");
            $.msDropDown.create("body select");
        },
        complete:function(datos){
            $("#cargando2").fadeOut("slow");
        }
    });
}

function eliminardelalista(id_pauta,id_detalle_servicio,id_tipo_rec){
    jConfirm('Esta usted seguro de realizar esta accion', 'Confirmacion', function(r) {
        if(r==true){
            id_recurso=$(id_detalle_servicio).parents("tr").attr("id");
            servidorajax("obtenerFuncion=select_asignados&id_pauta="+id_pauta
                +"&id_detalle_servicio="+id_detalle_servicio
                +"&id_recurso="+id_recurso
                +"&id_tipo_rec="+id_tipo_rec
                +"&obtenerFuncion=eliminardelalista"
                ,function(data){
                    //reload_materiales(id_pauta, 'materiales_porasignar', id_tipo_rec);
                    //reload_asignaciones(id_pauta);
                    //$("#materiales_porasignar").append(data);
                    $("#"+id_detalle_servicio).remove();
                })
        }else{
            return false;
        }
    });
}

function eliminarmaterialasignadohb(id_recurso_asignado, idpauta, id_tipo_rec){
    servidorajax("obtenerFuncion=eliminardop&idmaterialgeneral="+id_recurso_asignado
        +"&idpauta="+idpauta
        +"&id_tipo_rec="+id_tipo_rec,
        function(data){
        $("#"+id_recurso_asignado).parents("tr").remove();
        reload_materiales(idpauta, 'materiales_porasignar',id_tipo_rec);
        reload_asignaciones(idpauta,id_tipo_rec);
    });
}

function generarinforme(){
    if($("#nsolicitud").val()==""){
        jAlert('Disculpe debe agregar las observaciones');
        $("#nsolicitud").focus();
        return false;
    }
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "id_pauta="+$("#nsolicitud").val()
        +"&obtenerFuncion=generarinforme",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp2");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });
}



function guardarinforme(id_pauta,id_detalle_servicio,llave,ced_sesion)
{
    if($("#asistio"+llave).val()==0){
        jAlert('Disculpe debe seleccionar la asistencia/estado');
        $("#asistio"+llave).focus();
        return false;
    }

    if($("#observaciones"+llave).val()==""){
        jAlert('Disculpe debe las observaciones ');
        $("#observaciones"+llave).focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "asistio="+$("#asistio"+llave).val()
        +"&observaciones="+$("#observaciones"+llave).val()
        +"&id_pauta="+id_pauta
        +"&ced_sesion="+ced_sesion
        +"&id_detalle_servicio="+id_detalle_servicio
        +"&obtenerFuncion=guardarinforme",
        beforeSend:function(datos){
            $("#cargando2").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            window.location.reload();
        },
        complete:function(datos){
            $("#cargando2").fadeOut("slow");
        }
    });
}

function modificarinforme(id_pauta,id_informe,id_detalle_servicio,llave,ced_sesion){
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "id_pauta="+id_pauta
        +"&ced_sesion="+ced_sesion
        +"&id_informe="+id_informe
        +"&id_detalle_servicio="+id_detalle_servicio
        +"&obtenerFuncion=modificarinforme",
        beforeSend:function(datos){
            $("#cargando2").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            window.location.reload();
        },
        complete:function(datos){
            $("#cargando2").fadeOut("slow");
        }
    });
}

function modificarasignado(id_pauta,id_detalle_servicio,id_tipo_rec,ced_sesion){
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "id_pauta="+id_pauta
        +"&ced_sesion="+ced_sesion
        +"&id_detalle_servicio="+id_detalle_servicio
        +"&id_tipo_rec="+id_tipo_rec
        +"&obtenerFuncion=modificarasignado",
        beforeSend:function(datos){
            $("#cargando2").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp2");
            $("#datosp2").html(datos);
            $("#servicios").val("");
            $("#cantidad").val("");
            $.msDropDown.create("body select");

            $('.modalInput2').overlay({
                    mask: {color: '#ddd', loadSpeed: 200,opacity: 0.9},
                    closeOnClick: false,
                    oneInstance: false,
                    closeOnEsc: false
            });
            $('#agregar button').click(function(e){
                clearInterval(intervalID);
                window.location.reload();
            });

        },
        complete:function(datos){
            $("#cargando2").fadeOut("slow");
        }
    });
}

function guardarcorreos(){
    var gerencias=$("#gerencia input");
    var divisiones=$("#divisiones input");
    var gerenciaarray=Array();
    var divisionesarray=Array();
    var re=/^[0-9a-z_\-\.]+@[0-9a-z\-\.]+\.[a-z]{2,4}$/i
    var cont=0;
    gerencias.each(function(){
        if($(this).val()!=''){
            if(re.exec($(this).val())){
                gerenciaarray[cont]=$(this).attr('id')+';'+$(this).val();
                cont++;
            }
        }
    })
    var cont=0;
    divisiones.each(function(){
        if($(this).val()!=''){
            if(re.exec($(this).val())){
                divisionesarray[cont]=$(this).attr('id')+';'+$(this).val();
                cont++;
            }
        }
    })
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "gerencias="+gerenciaarray
        +"&divisiones="+divisionesarray
        +"&obtenerFuncion=guardarcorreos",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });

}

//funcion que sirve para enviar pauta.
function enviarsolicitud(pauta,estatus_pt,mod){
    jConfirm("&iquest;Esta seguro(a) de querer enviar la solicitud?", "Confirme", function(r){
        if(r==true){
            $.ajax({
                type: "GET",
                url: "classDirectorioFunciones.php",
                data:
                "id_pauta="+pauta
                +"&estatus_pt="+estatus_pt
                +"&mod="+mod
                +"&obtenerFuncion=enviarsolicitud",
                beforeSend:function(datos){
                    $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
                    $("#cargando").fadeIn();
                },
                success:function(datos){
                //aparecer("#datosp");
                $("#datosp").html(datos);
                $("#datosp2").remove();
                $("#datosp3").remove();
                $("#datosp4").remove();
            },
            complete:function(datos){
                $("#cargando").fadeOut("slow");
            }
        });
        }else{
            return false;
        }
    })
}

/*
function enviarsolicitud2(pauta){
    // datosrec datosrec 36
    // datosrecasig datosrecasig
    if((parseInt($("#datosrec").val()))==0){
        jAlert ("usted debe asignar por lo menos un servicio");
        return false;
    }

    var resta=(parseInt($("#datosrec").val())-parseInt($("#datosrecasig").val()));
    if(resta>0 ){
        jAlert ("Disculpe le faltan "+resta+" recursos por asignar");
        return false;
    }else if($("#listarecursos tr").length>2 || $("#listarecursosH tr").length>2){
        jAlert("debe regresar y asignar el recurso faltante");
        return false;
    }

    if(confirm("&iquest;Esta seguro(a) de querer enviar la solicitud?")){
        $.ajax({
            type: "GET",
            url: "classDirectorioFunciones.php",
            data:
            "id_pauta="+pauta
            +"&obtenerFuncion=enviarsolicitud2",
            beforeSend:function(datos){
                $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
                $("#cargando").fadeIn();
            },
            success:function(datos){
                aparecer("#datosp");
                $("#datosp").html(datos);
            },
            complete:function(datos){
                $("#cargando").fadeOut("slow");
            }
        });
    }
}*/

function enviarsolicitud_sinasignacion(pauta){
    var valor_porasignar=parseInt($("#datosrec").val());
    var valor_asignados=parseInt($("#datosasig").val());

    if(valor_porasignar>0  || valor_asignados==0)
    {
        if(valor_asignados>=1){
            alert ("Disculpe le faltan "+parseInt($("#datosrec").val())+" recursos por asignar");
        }else{
            alert ("Disculpe la pauta no esta completa, debe asignar por lo menos 1 recurso humano o material");
        }

        return false;
    }
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "id_pauta="+pauta
        +"&obtenerFuncion=enviarsolicitud2",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });

}

function enviarsolicitud3(pauta){
    var resta=parseInt($("#canttotal").val())-parseInt($("#cantjust").val());
    if(resta>0)
    {
        alert ("Disculpe le faltan "+resta+" recursos por justificar");
        return false;
    }

    jConfirm("&iquest;Esta seguro(a) de querer enviar la evaluacion?", "Confirme", function(r){
        if(r==true){
            $.ajax({
                type: "GET",
                url: "classDirectorioFunciones.php",
                data:
                "id_pauta="+pauta
                +"&obtenerFuncion=enviarsolicitud3",
                beforeSend:function(datos){
                    $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
                },
                success:function(datos){
                    aparecer("#datosp");
                    $("#datosp").html(datos);
                },
                complete:function(datos){
                    $("#cargando").fadeOut("slow");
                }
            });
        }else{
            return false;
        }
    })
}

function enviarsolicitud1(pauta,lista){

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "id_pauta="+pauta
        +"&lista="+lista
        +"&obtenerFuncion=enviarsolicitud1",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp2");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });

}

function cambiarestatus(id_pauta,estatuspauta){

    jConfirm("&iquest;Esta seguro(a) de querer enviar la solicitud final?", "Confirme", function(r){
        if(r==true){
            $.ajax({
                type: "GET",
                url: "classDirectorioFunciones.php",
                data:
                "id_pauta="+id_pauta
                +"&estatuspauta="+estatuspauta
                +"&obtenerFuncion=cambiarestatus",
                beforeSend:function(datos){
                    $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
                },
                success:function(datos){
                    try{
                        $("#datosp2").remove();
                        $("#datosp3").remove();
                        $("#datosp4").remove();
                        $(".tablaproceso").eq(0).remove();
                    }catch(err){
                    }
                    $("#datosp").html(datos);
                },
                complete:function(datos){
                    $("#cargando").fadeOut("slow");
                }
            });
        }else{
            return false;
        }
    })
}

function cambiarestatusrech(id_pauta,estatuspauta,lista){

    //alert ("id_pauta"+id_pauta );
    //alert ("estatuspauta"+estatuspauta );

    if($("#obs").val()==""){
        jAlert('Disculpe debe agregar las observaciones');
        $("#obs").focus();
        return false;
    }
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "id_pauta="+id_pauta
        +"&estatuspauta="+estatuspauta
        +"&lista="+lista
        +"&obs_aux="+$("#obs").val()
        +"&obtenerFuncion=cambiarestatusrech",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp2");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });
}

function suspenderpauta(id_pauta,estatuspauta,lista){

    if($("#obs").val()==""){
        jAlert('Disculpe debe agregar las observaciones');
        $("#obs").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "id_pauta="+id_pauta
        +"&estatuspauta="+estatuspauta
        +"&lista="+lista
        +"&obs_aux="+$("#obs").val()
        +"&obtenerFuncion=suspenderpauta",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp2");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });

}

///////////////////////////////////////anular pauta //////////////////////////////////

function cambiarestatusanulada(){


    if($("#npauta").val()==0){
        jAlert('Disculpe debe ingresar el numero de la pauta que desea anular');
        $("#npauta").focus();
        return false;
    }

    if($("#obs").val()==""){
        jAlert('Disculpe debe agregar las observaciones');
        $("#obs").focus();
        return false;
    }



    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "npauta="+$("#npauta").val()
        +"&obs="+$("#obs").val()
        +"&obtenerFuncion=cambiarestatusanulada",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp2");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });



}

function reportejs(url){
    window.open(url+"?fini="+$("#busqueda").find("input").eq(0).val()+"&fefin="+$("#busqueda").find("input").eq(1).val());
}


function rep_estad(){


    if($("#fecha_ini").val()==0){
        jAlert('Disculpe debe ingresar fecha de inicio');
        $("#fecha_ini").focus();
        return false;
    }

    if($("#fecha_fin").val()==0){
        jAlert('Disculpe debe ingresar la fecha final');
        $("#fecha_fin").focus();
        return false;
    }



    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "fecha_ini="+$("#fecha_ini").val()
        +"&fecha_fin="+$("#fecha_fin").val()
        +"&obtenerFuncion=rep_estad",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
            $("#p1").fadeOut();
            $("#busqueda").fadeOut();
        },
        success:function(datos){
            aparecer("#datosp2");
            $("#p2").fadeIn();
            $("#datosp2").append(datos);
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });



}


//////////////////////////////////////consulta de estatus///////////////////////////////////////////////////////////////



function estatus_f5(){

    if($("#nsolicitud").val()==0){
        jAlert('Disculpe debe ingresar el numero de la solicitud que desea consultar');
        $("#nsolicitud").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "nsolicitud_aux="+$("#nsolicitud").val()
        +"&obtenerFuncion=estatus_f5",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });
}
///////////////////////////////////////agregar usuario ////////////////////////////////////////////////////////////

function guardarusuario_f5(){

    if($("#cedula").val()==""){
        jAlert('Disculpe debe agregar la cedula del usuario');
        $("#cedula").focus();
        return false;
    }

    if($("#nombre").val()==""){
        jAlert('Disculpe debe agregar el nombre del usuario');
        $("#nombre").focus();
        return false;
    }

    if($("#apellido").val()==""){
        jAlert('Disculpe debe agregar el apellido del usuario');
        $("#apellido").focus();
        return false;
    }


    if($("#tipousuario").val()==0){
        jAlert('Disculpe debe seleccionar el tipo de usuario');
        $("#tipousuario").focus();
        return false;
    }

    if($("#idgerencia").val()==0){
        jAlert('Disculpe debe seleccionar una gerencia');
        $("#idgerencia").focus();
        return false;
    }

    if($("#iddivision").val()==0){
        jAlert('Disculpe debe seleccionar una division');
        $("#iddivision").focus();
        return false;
    }

    if(($("#telefono1").val()=="")&&($("#telefono2").val()=="")){
        jAlert('Disculpe debe agregar al menos un numero de contacto');
        $("#telefono1").focus();
        return false;
    }

    if($("#correo").val()!=""){
        re=/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/
        if(!re.exec($("#correo").val())){
            jAlert('Disculpe direccion de correo electronico invalido');
            return false;
        }
    }
    else{
        jAlert('Disculpe debe agregar el correo del usuario');
        $("#correo").focus();
        return false;
    }

    if($("#clave1").val()==""){
        jAlert('Disculpe debe agregar la clave del usuario');
        $("#clave1").focus();
        return false;
    }

    if($("#clave2").val()==""){
        jAlert('Disculpe debe confirmar la clave del usuario');
        $("#clave2").focus();
        return false;
    }

    if($("#clave2").val()!=$("#clave1").val()){
        jAlert('Disculpe la clave no es igual a la confirmacion');
        $("#clave1").val("");
        $("#clave2").val("");
        $("#clave2").focus();

        return false;
    }

    if($("#telefono1").val()!="undefined"){
        var telefono1=$("#op_telefono1").val()+$("#telefono1").val();
    }

    if($("#telefono2").val()!="undefined"){
        var telefono2=$("#op_telefono2").val()+$("#telefono2").val();
    }

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "tipousuario="+$("#tipousuario").val()
        +"&nombre="+$("#nombre").val()
        +"&apellido="+$("#apellido").val()
        +"&cedula="+$("#cedula").val()
        +"&idgerencia="+$("#idgerencia").val()
        +"&iddivision="+$("#iddivision").val()
        +"&telefono1="+telefono1
        +"&telefono2="+telefono2
        +"&clave1="+$("#clave1").val()
        +"&correo="+$("#correo").val()
        +"&obtenerFuncion=guardarusuario_f5",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp2");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });
}


//////////////////////////////////////listas///////////////////////////////////////////////////////////////////////

function listar(modulo){
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion="+modulo,
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}


function buscarreportesvalidacion()
{

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listadereportesvalidacion",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}


function buscarreportesinformes()
{

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listadereportesinformes",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}


function buscarreportesrecurasig(){

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listadereportesrecurasig",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function buscarreportesrecur(){
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listadereportesrecur",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function buscarreportespers(){
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listadereportespers",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function buscarreportes(){
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listadereportes",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}


function buscarusuarios(){
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listadeusuarios",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}


function pag_listaprogra(){
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listaprog",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function buscarpautas()
{

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listadepautas",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}


function buscarpautasger(){

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listadepautasger",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}


function buscarpautasjarea(){

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listadepautasjarea",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}



function buscarpautasual(){
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listadepautasual",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function buscarpautasao(){
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listadepautasao",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function buscarinformes(){

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listadeinformes",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

////////////////////////////////////////////////reportes///////////////////////////////////////////////////////

function reporteauditoria(){
    if($("#nsolicitud").val()==0){
        jAlert('Disculpe debe ingresar el numero de la pauta');
        $("#nsolicitud").focus();
        return false;
    }
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "nsolicitud="+$("#nsolicitud").val()
        +"&obtenerFuncion=reporteauditoria",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });
}


/////////////////////////////////////Agregar servicios///////////////////////////////////////////////////////////////


function guardarservicio_f5()
{

    if($("#tiposerv").val()==0){
        jAlert('Disculpe debe seleccionar el servicio');
        $("#tiposerv").focus();
        return false;
    }

    if($("#descripcion").val()==0){
        jAlert('Disculpe debe escribir la descripción del servicio');
        $("#descripcion").focus();
        return false;
    }



    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "tiposerv_aux="+$("#tiposerv").val()
        +"&descripcion_aux="+$("#descripcion").val()
        +"&obtenerFuncion=guardarservicio_f5",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });

}


function guardartipotraje()
{

    if($("#traje").val()==0){
        jAlert('Disculpe debe escribir el tipo de traje');
        $("#traje").focus();
        return false;
    }



    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "tipotraje_aux="+$("#traje").val()
        +"&obtenerFuncion=guardartipotraje",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });

}


function guardarprograma()
{

    if($("#programa").val()==0){
        jAlert('Disculpe debe escribir el tipo de traje');
        $("#programa").focus();
        return false;
    }




    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "programa_aux="+$("#programa").val()
        +"&obtenerFuncion=guardarprograma",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });

}
////////////////////////////reiniciar clave////////////////////////////////////////////

function reiniciar_clave(cedula)
{

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "cedula="+cedula
        +"&obtenerFuncion=reiniciar_clave",
        beforeSend:function(datos){
            $("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#form");
            $("#form").html(datos);
        },
        complete:function(datos){
            $("#loadiing").fadeOut("slow");
        }
    });

}

/////////////////////////////cambio de perfil /////////////////////////////////////////

function cambioperfil_f5(cedula,id_tipousuario)
{

    if($("#perfilmod").val()==0){
        jAlert('Disculpe debe seleccionar el perfil al cual desea cambiar');
        $("#perfilmod").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "cedula="+cedula
        +"&perfilmod="+$("#perfilmod").val()
        +"&perfilact="+id_tipousuario
        +"&obtenerFuncion=cambioperfil_f5",
        beforeSend:function(datos){
            $("#cargando4").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#cargando4").fadeOut("slow");
        }
    });

}
///////////////////////////////////eliminar usuarios////////////////////////////////////////////

function eliminarusuario_f5(cedula){

    jConfirm('Esta usted seguro de realizar esta accion', 'Confirmacion', function(r) {
        if(r==true){

            $.ajax({
                type: "GET",
                url: "classDirectorioFunciones.php",
                data:
                "cedula="+cedula
                +"&obtenerFuncion=eliminarusuario_f5",
                beforeSend:function(datos){
                    $("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
                },
                success:function(datos){
                    aparecer("#form");
                    $("#form").html(datos);
                },
                complete:function(datos){
                    $("#loadiing").fadeOut("slow");
                }
            });

        }else{
            return false;
        }
    });

}

function eliminartraje(id_tipo_traje){

    jConfirm('Esta usted seguro de realizar esta accion', 'Confirmacion', function(r) {
        if(r==true){

            $.ajax({
                type: "GET",
                url: "classDirectorioFunciones.php",
                data:
                "id_tipo_traje="+id_tipo_traje
                +"&obtenerFuncion=eliminartraje",
                beforeSend:function(datos){
                    $("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
                },
                success:function(datos){
                    aparecer("#form");
                    $("#form").html(datos);
                },
                complete:function(datos){
                    $("#loadiing").fadeOut("slow");
                }
            });

        }else{
            return false;
        }
    });

}

function eliminarprograma(id_programa){

    jConfirm('Esta usted seguro de realizar esta accion', 'Confirmacion', function(r) {
        if(r==true){

            $.ajax({
                type: "GET",
                url: "classDirectorioFunciones.php",
                data:
                "id_programa="+id_programa
                +"&obtenerFuncion=eliminarprograma",
                beforeSend:function(datos){
                    $("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
                },
                success:function(datos){
                    aparecer("#form");
                    $("#form").html(datos);
                },
                complete:function(datos){
                    $("#loadiing").fadeOut("slow");
                }
            });

        }else{
            return false;
        }
    });

}

/////////////////////////////////////////Divisiones////////////////////////////////////////////

function combodivision1_f5()
{

    if($("#gerencia1").val()==0){
        jAlert('Disculpe debe seleccionar una gerencia');
        $("#gerencia1").focus();
        return false;
    }



    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "gerenciaaux="+$("#gerencia1").val()
        +"&obtenerFuncion=combodivision1_f5",
        beforeSend:function(datos){
            $("#cargando1").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#div_division1");
            $("#div_division1").html(datos);
        },
        complete:function(datos){
            $("#cargando1").fadeOut("slow");
        }
    });

}




function combodivision_f5()
{

    if($("#gerencia2").val()==0){
        jAlert('Disculpe debe seleccionar una gerencia');
        $("#gerencia2").focus();
        return false;
    }



    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "gerenciaaux="+$("#gerencia2").val()
        +"&obtenerFuncion=combodivision_f5",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#div_division");
            $("#div_division").html(datos);
        },
        complete:function(datos){
            $("#cargando").fadeOut("slow");
        }
    });
}


function combodivision3_f5()
{

    if($("#gerencia3").val()==0){
        jAlert('Disculpe debe seleccionar una gerencia');
        $("#gerencia3").focus();
        return false;
    }



    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "gerenciaaux="+$("#gerencia3").val()
        +"&obtenerFuncion=combodivision3_f5",
        beforeSend:function(datos){
            $("#cargando3").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#div_division3");
            $("#div_division3").html(datos);
        },
        complete:function(datos){
            $("#cargando3").fadeOut("slow");
        }
    });
}


function get_combodivision(obj){
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "idgerencia="+$(obj).val()
        +"&obtenerFuncion=combodivisionf5_hb",
        beforeSend:function(datos){
        //$("#cargando3").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
    },
    success:function(datos){
            //aparecer("#div_division1");
            $("#div_division1").html(datos);
        },
        complete:function(datos){
        //$("#cargando3").fadeOut("slow");
    }
});
}



///////////////////////////////////////////combos///////////////////////////////////////////////////////////
function combotipousuario()
{

    if($("#tipousuario").val()==0){
        jAlert('Disculpe debe seleccionar un tipo de usuario');
        $("#tipousuario").focus();
        return false;
    }



    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "tipousuario="+$("#tipousuario").val()
        +"&obtenerFuncion=combotipousuario",
        beforeSend:function(datos){
            $("#cargando").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datosp){
            aparecer("#div_tipousuario");
            $("#div_tipousuario").html(datos);
        },
        complete:function(datosp){
            $("#cargando").fadeOut("slow");
        }
    });
}



function combotipousuario()
{

    if($("#tipousuario").val()==0){
        jAlert('Disculpe debe seleccionar un tipo de usuario');
        $("#tipousuario").focus();
        return false;
    }



/*$.ajax({
			type: "GET",
			url: "classDirectorioFunciones.php",
			data:
			"tipousuarioaux="+$("#tipousuario").val()
			 +"&obtenerFuncion=combotipousuario",
			 beforeSend:function(datos){
			$("#cargando3").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
			},
			success:function(datos){
			aparecer("#codigoval_1");
			$("#codigoval_1").html(datos);
			},
			complete:function(datos){
			$("#cargando3").fadeOut("slow");
			}
       });*/

}





function combopauta_f5()
{

    if($("#pauta").val()==0){
        jAlert('Disculpe debe seleccionar un tipo de pauta');
        $("#pauta").focus();
        return false;
    }



    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "pautaaux="+$("#pauta").val()
        +"&obtenerFuncion=combopauta_f5",
        beforeSend:function(datos){
            $("#cargando4").html('<img src="../estilos/imagenes/estatus/cargando3.gif" >');
        },
        success:function(datos){
            aparecer("#id_pauta");
            $("#div_tipopauta").html(datos);
        },
        complete:function(datos){
            $("#cargando4").fadeOut("slow");
        }
    });

}

function combolocacion_f5()
{

    if($("#locacion").val()==0){
        jAlert('Disculpe debe seleccionar la locacion');
        $("#locacion").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "locacion="+$("#locacion").val()
        +"&obtenerFuncion=combolocacion_f5",
        beforeSend:function(datos){
            $("#cargando_locac").html('<img src="../estilos/imagenes/estatus/cargando3.gif" >');
        },
        success:function(datos){
            aparecer("#id_pauta");
            $("#div_locacpauta").html(datos);
        },
        complete:function(datos){
            $("#cargando_locac").fadeOut("slow");
        }
    });
}


////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

function cambioclave(){
    //alert(correo+' '+pin);


    if($("#claveactual").val()==""){
        jAlert('Disculpe debe escribir la clave actual');
        $("#claveactual").focus();
        return false;
    }
    if($("#clavenueva").val()==""){
        jAlert('Disculpe debe escribir la nueva clave');
        $("#clavenueva").focus();
        return false;
    }

    if($("#claveconfirmacion").val()==""){
        jAlert('Disculpe debe escribir la confirmacion de la clave');
        $("#claveconfirmacion").focus();
        return false;
    }

    if($("#claveconfirmacion").val()!=$("#clavenueva").val()){
        jAlert('Disculpe la nueva clave no es igual a la confirmacion');
        $("#claveconfirmacion").focus();
        return false;
    }


    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "claveactual="+$("#claveactual").val()
        +"&clavenueva="+$("#clavenueva").val()
        +"&obtenerFuncion=cambioclave",
        beforeSend:function(datos){
            $("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loadiing").fadeOut("slow");
        }
    });
}


//se agrego el tope a los correos...
function enviarcorreonuevamente(pin,correo,tope){
    //alert(correo+' '+pin);
    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "pin="+pin
        +"&correo="+correo
        +"&obtenerFuncion=correoenviado"
        +"&tope="+tope,
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function guardaradjunto()
{

    extensiones_permitidas = new Array(".txt",".TXT",".Txt");
    extension = (document.frmblob.archivo_adjunto.value.substring(document.frmblob.archivo_adjunto.value.lastIndexOf("."))).toLowerCase();

    if($("#archivo_adjunto").val()==""){
        jAlert('Disculpe debe adjuntar un archivo','Alerta');
        $("#archivo_adjunto").focus();
        return false;
    }else{
        permitida = false;
        for (var i = 0; i < extensiones_permitidas.length; i++) {
            if (extensiones_permitidas[i] == extension) {
                permitida = true;
                break;
            }
        }
        if (!permitida) {
            mierror = "Compruebe la extensión del archivos a subir. \nSólo se pueden subir archivos con extensiones: " + extensiones_permitidas.join();
            jAlert(mierror);
            $("#archivo_adjunto").focus();
            return false;
        }
    }
    $('#frmblob').submit();
}

function buscarpersona()
{

    if($("#cedula_buscar").val()==""){
        jAlert('Disculpe debe escribir la cedula de identidad del usuario');
        $("#cedula_buscar").focus();
        return false;
    }


    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "cedula_buscar="+$("#cedula_buscar").val()
        +"&obtenerFuncion=buscarpersona",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function combodivision()
{
    //var activacion=activacion;
    //jAlert (activacion);

    if($("#gerencia").val()==0){
        jAlert('Disculpe debe seleccionar la gerencia');
        $("#gerencia").focus();
        return false;
    }


    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "gerencias="+$('#gerencias').val()
        +"&obtenerFuncion=combodivision",
        beforeSend: function(datos){
            $("#div_division").html('<img src="../estilos/imagenes/estatus/cargando3.gif" />');
        },
        success: function(datos){
            $("#div_division").fadeIn("slow");
            $("#div_division").html(datos);
            $("#div_division").fadeOut("slow");
            $("#div_division").fadeIn("slow");

        //combodelegado();
    }
});

}

function chequearadio_pimtope()
{
    if($("#form input:radio:checked").val()!="M" && $("#form input:radio:checked").val()!="S"){
        $("#monto").attr("disabled","true");
    }


    //jAlert($("#monto").attr("disabled"));

    //monto
    $("input:radio").click(function(){
        if($(this).val()=="M" || $(this).val()=="S"){
            $("#monto").removeAttr("disabled");
        }else{
            $("#monto").attr("disabled","true");			//$("#monto").remove()
        }
    //alert($(this).val());
})
}
function guardardatospersona()
{

    if($("#nombres").val()==""){
        jAlert('Disculpe debe escribir los nombres del usuario');
        $("#nombres").focus();
        return false;
    }

    if($("#apellidos").val()==""){
        jAlert('Disculpe debe escribir los apellidos del usuario');
        $("#apellidos").focus();
        return false;
    }

    if($("#gerencias").val()==0){
        jAlert('Disculpe debe seleccionar la gerencia a la que pertenece el usuario');
        $("#gerencias").focus();
        return false;
    }

    if($("#cargo").val()==0){
        jAlert('Disculpe debe seleccionar el cargo del usuario');
        $("#cargo").focus();
        return false;
    }

    if($("#ext").val()==""){
        jAlert('Disculpe debe escribir la extension del usuario');
        $("#ext").focus();
        return false;
    }

    if($("#correo").val()!=""){
        re=/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/
        if(!re.exec($("#correo").val())){
            jAlert('Disculpe direccion de correo electronico invalido');
            return false;
        }
    }

    if($("#monto").attr("disabled")){
        //jAlert("manda un cero");
        var monto=0;
    }else{
        //jAlert("guardando el monto");
        if($("#monto").val()!=""){
            re=/^[0-9]*,[0-9]{2}$/
            if(!re.exec($("#monto").val())){
                jAlert('Disculpe monto invalido el formato correcto es 0000,00');
                return false;
            }else{
                var monto=$("#monto").val();
            }
        }else{
            jAlert('Disculpe debe colocar el monto');
            return false;
        }

    }



    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "cedula="+$("#cedula").val()
        +"&nombres="+$("#nombres").val()
        +"&apellidos="+$("#apellidos").val()
        +"&gerencia="+$("#gerencias").val()
        +"&division="+$("#division").val()
        +"&cargo="+$("#cargo").val()
        +"&pim="+$("#form input:radio:checked").val()//#form es el div para acederlo
        +"&ext="+$("#ext").val()
        +"&correo="+$("#correo").val()
        +"&monto="+monto
        +"&obtenerFuncion=guardardatospersona",
        beforeSend:function(datos){
            $("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#form");
            $("#form").html(datos);
        },
        complete:function(datos){
            $("#loadiing").fadeOut("slow");
        }
    });
}

function cancelarocultardiv(div){
    $("#"+div).hide();
}

function buscarpersonalista()
{

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listapersona",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function buscarpersonalistapinesviejos()
{

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listapineseliminados",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function buscarserviciolista()
{

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listadeservicio",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function buscarauditoria()
{

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listaauditoria",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function accionespim(accion,id_persona){
    jConfirm('Esta usted seguro de realizar esta accion', 'Confirmación', function(r) {
        if(r==true){

            $.ajax({
                type: "GET",
                url: "classDirectorioFunciones.php",
                data:
                "accion="+accion
                +"&id_persona="+id_persona
                +"&obtenerFuncion=accionespim",
                beforeSend:function(datos){
                    $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
                },
                success:function(datos){
                    aparecer("#datosp");
                    $("#datosp").html(datos);
                },
                complete:function(datos){
                    $("#loading").fadeOut("slow");
                }
            });
        }else{
            return false;
        }
    });
}

function accionesservicio(accion,id_grupo){
    jConfirm('Esta usted seguro de realizar esta accion', 'Confirmación', function(r) {
        if(r==true){

            $.ajax({
                type: "GET",
                url: "classDirectorioFunciones.php",
                data:
                "accion="+accion
                +"&id_grupo="+id_grupo
                +"&obtenerFuncion=accionesservicio",
                beforeSend:function(datos){
                    $("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
                },
                success:function(datos){
                    aparecer("#form");
                    $("#form").html(datos);
                },
                complete:function(datos){
                    $("#loadiing").fadeOut("slow");
                }
            });
        }else{
            return false;
        }
    });
}

function buscarpersonalistaactivacion()
{

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listapersonaactivacion",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function buscarsigainotindirecctorio()
{

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "busqueda="+$("#busqueda").val()
        +"&obtenerFuncion=listasigainotindirectorio",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function buscarsigai2directorio(cedula)
{
    /*alert("cedula"+cedula);
	return false;

	if($("#cedula_buscar").val()==""){
		jAlert('Disculpe debe escribir la cedula de identidad del usuario');
		$("#cedula_buscar").focus();
		return false;
	}*/


    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "cedula_buscar="+cedula
        +"&obtenerFuncion=buscarpersona",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function traducir()
{

    if($("#tipo").val()==0){
        jAlert('Disculpe debe seleccionar el tipo');
        $("#tipo").focus();
        return false;
    }


    if($("#atraducir").val()==""){
        jAlert('Disculpe debe escribir el texto a traducir');
        $("#atraducir").focus();
        return false;
    }

    $('#frmblob').submit();
}

function datosreporte()
{

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "gerencias="+$("#gerencias").val()
        +"&mes="+$("#mes").val()
        +"&year="+$("#year").val()
        +"&obtenerFuncion=datosreporte",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function obtenergerente()
{

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "gerencias="+$("#gerencias").val()
        +"&obtenerFuncion=obtenergerente",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando3.gif" >');
        },
        success:function(datos){
            aparecer("#divreceptor");
            $("#divreceptor").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}


function datosabreviadosmonto()
{
    if($("#gerencias").val()==0){
        jAlert('Disculpe debe seleccionar la gerencia');
        $("#gerencias").focus();
        return false;
    }

    if($("#monto").val()!=""){
        re=/^[0-9]*,[0-9]{2}$/
        if(!re.exec($("#monto").val())){
            jAlert('Disculpe monto invalido el formato correcto es 0000,00');
            return false;
        }
    }else{
        jAlert('Disculpe debe colocar el monto');
        return false;
    }


    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "gerencias="+$("#gerencias").val()
        +"&mes="+$("#mes").val()
        +"&year="+$("#year").val()
        +"&monto="+$("#monto").val()
        +"&obtenerFuncion=datosabreviadosmonto",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function guardarservicio()
{

    if($("#tipo_grupo").val()==0){
        jAlert('Disculpe debe seleccionar el Grupo');
        $("#tipo_grupo").focus();
        return false;
    }

    if($("#nombre").val()==""){
        jAlert('Disculpe debe escribir el nombre');
        $("#nombre").focus();
        return false;
    }

    if($("#extension").val()==""){
        jAlert('Disculpe debe escribir la extension ');
        $("#extension").focus();
        return false;
    }

    if($("#correo").val()!=""){
        re=/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/
        if(!re.exec($("#correo").val())){
            jAlert('Disculpe direccion de correo electronico invalido');
            return false;
        }
    }

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "tipo_grupo="+$("#tipo_grupo").val()
        +"&nombre="+$("#nombre").val()
        +"&extension="+$("#extension").val()
        +"&correo="+$("#correo").val()
        +"&obtenerFuncion=guardarservicio",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function modificarservicio()
{
    if($("#tipo_grupo").val()==0){
        jAlert('Disculpe debe seleccionar el Grupo');
        $("#tipo_grupo").focus();
        return false;
    }

    if($("#nombre").val()==""){
        jAlert('Disculpe debe escribir el nombre');
        $("#nombre").focus();
        return false;
    }

    if($("#extension").val()==""){
        jAlert('Disculpe debe escribir la extension ');
        $("#extension").focus();
        return false;
    }

    if($("#correo").val()!=""){
        re=/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/
        if(!re.exec($("#correo").val())){
            jAlert('Disculpe direccion de correo electronico invalido');
            return false;
        }
    }

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "tipo_grupo="+$("#tipo_grupo").val()
        +"&nombre="+$("#nombre").val()
        +"&extension="+$("#extension").val()
        +"&correo="+$("#correo").val()
        +"&id_grupo="+$("#id_grupo").val()
        +"&obtenerFuncion=modificarservicio",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#datosp");
            $("#datosp").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function validarenviomemo(){

    if($("#gerencias").val()==0){
        jAlert('Disculpe debe seleccionar la gerencia');
        $("#gerencias").focus();
        return false;
    }

    if($("#nombre_receptor").val()==0){
        jAlert('Disculpe debe escribir el receptor');
        $("#nombre_receptor").focus();
        return false;
    }


    if($("#numero_memo").val()==""){
        jAlert('Disculpe debe escribir el numero de memo');
        $("#numero_memo").focus();
        return false;
    }
    if($("#asunto").val()==""){
        jAlert('Disculpe debe escribir el asunto');
        $("#asunto").focus();
        return false;
    }

    $('#frmblob').submit();
}



/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

/*
function aparecer(div){
	//div es el id del div que se le va a colocar le efecto
	$(div).animate({
	opacity: 0.2,
	height: 'toggle'
	}, 1000 );

	$(div).animate({
	opacity: 0.2,
	height: 'toggle'
	}, 1000);

	$(div).animate({
	opacity: 1,
	}, 1000);
}*/


function minizar(div){

    //jAlert(div);
    $("#"+div).fadeOut(2000);
    $("#"+div).hide();
}

function maximizar(div){

    //jAlert(div);
    $("#"+div).show();
    $("#"+div).fadeIn(2000);

}






function siguiente(pagina_ajax,fn)
{

    var pagina_ajax=pagina_ajax;
    var funcion=fn;

    var minimo=parseInt($("#minimo").val())+10;
    var count=parseInt($("#count").val())+1;


    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "minimo="+minimo
        +"&count="+count
        +"&parametrobusqueda="+$("#parametrobusqueda").val()
        //+"&obtenerFuncion=listavehiculos",
        +"&obtenerFuncion="+pagina_ajax,
        success:function(datos){
            $("#datosp").html(datos);
        /*initalizetooltip();
						$("#tabla_corres").find("tr").click(function(){
							//funcion($(this).attr("id"));
							//jAlert($(this).attr("id"));

						});*/
}
});
}

function anterior(pagina_ajax,fn)
{

    var pagina_ajax=pagina_ajax;
    var funcion=fn;

    var minimo=parseInt($("#minimo").val())-10;
    var count=parseInt($("#count").val())-1;

    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "minimo="+minimo
        +"&count="+count
        +"&parametrobusqueda="+$("#parametrobusqueda").val()
        +"&obtenerFuncion="+pagina_ajax,
        success:function(datos){
            $("#datosp").html(datos);
        }
    });
}

function cambiopagina(pagina_ajax,fn)
{
    var pagina_ajax=pagina_ajax;
    var funcion=fn;


    if ($("#cantidad_paginas").val()==0)
    {
        var minimo=$("#cantidad_paginas").val();
        var count=1;

    }
    else
    {
        var minimo=$("#cantidad_paginas").val()+0;
        var count=document.getElementById('cantidad_paginas')[document.getElementById('cantidad_paginas').selectedIndex].text;
    }


    $.ajax({
        type: "GET",
        url: "classDirectorioFunciones.php",
        data:
        "minimo="+minimo
        +"&count="+count
        +"&parametrobusqueda="+$("#parametrobusqueda").val()
        //+"&obtenerFuncion=listavehiculos",
        +"&obtenerFuncion="+pagina_ajax,
        success:function(datos){
            $("#datosp").html(datos);
        /*initalizetooltip();
						$("#tabla_corres").find("tr").click(function(){
							//funcion($(this).attr("id"));
							//jAlert($(this).attr("id"));

						});*/
}
});
}



function abrir_corres(id_corres)
{
    if (id_corres!="")
    {
        $.ajax({
            type: "GET",
            url: "classConstanciaFunciones.php",
            data:
            "id_corres="+id_corres
            +"&obtenerFuncion=abrir_corres",
            beforeSend:function(datos){
                $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
            },
            success:function(datos){
                aparecer("#form1");
                $("#form1").html(datos);
            },
            complete:function(datos){
                $("#loading").fadeOut("slow");
            }
        });
    }

}



function abrir_corres_recibidas(id_corres)
{
    if (id_corres!="")
    {
        $.ajax({
            type: "GET",
            url: "classConstanciaFunciones.php",
            data:
            "id_corres="+id_corres
            +"&obtenerFuncion=abrir_corres_recibidas",
            beforeSend:function(datos){
                $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
            },
            success:function(datos){
                aparecer("#form1");
                $("#form1").html(datos);
            },
            complete:function(datos){
                $("#loading").fadeOut("slow");
            }
        });
    }

}


function eliminaradjunto(id_adjunto,id_corres)
{
    //jAlert(id_corres+" vbvbvb");

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "id_adjunto="+id_adjunto
        +"&id_corres="+id_corres
        +"&obtenerFuncion=eliminaradjunto",
        beforeSend:function(datos){
            $("#div_adjuntos").html('<img src="../estilos/imagenes/estatus/cargando3.gif" >');
        },
        success:function(datos){
            aparecer("#div_adjuntos");
            $("#div_adjuntos").html(datos);
        }
    });

}




function lista(nombre_lista,fn)
{

    //jAlert(nombre_lista);
    //return false;

    var funcion=fn;

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "busqueda=@@@"
        +"&obtenerFuncion="+nombre_lista,


        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#form1");
            $("#form1").html(datos);
            initalizetooltip();
            $("#tabla_corres").find("tr").click(function(){
                funcion($(this).attr("id"));
            //jAlert($(this).attr("id"));

        });


        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function busqueda(pagina_ajax,fn)
{
    //jAlert(pagina_ajax);
    var funcion=fn;

    if($("#busqueda").val()==""){
        var busqueda='@@@';
    }
    else{
        var busqueda=$("#busqueda").val();
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "busqueda="+busqueda
        +"&obtenerFuncion="+pagina_ajax,
        success:function(datos){
            $("#form1").html(datos);
            initalizetooltip();
            $("#tabla_corres").find("tr").click(function(){
                funcion($(this).attr("id"));
            //jAlert($(this).attr("id"));

        });
        }
    });
    $("#busqueda").focus();
}


function busqueda2(e,pagina_ajax,fn) {

    tecla = (document.all) ? e.keyCode : e.which;
    te = String.fromCharCode(tecla);
    //jAlert(tecla);
    var funcion=fn;
    if (tecla==13)
    {

        if($("#busqueda").val()==""){
            var busqueda='@@@';
        }
        else{
            var busqueda=$("#busqueda").val();
        }

        $.ajax({
            type: "GET",
            url: "classConstanciaFunciones.php",
            data:
            "busqueda="+busqueda
            +"&obtenerFuncion="+pagina_ajax,
            success:function(datos){
                $("#form1").html(datos);
                initalizetooltip();
                $("#tabla_corres").find("tr").click(function(){
                    funcion($(this).attr("id"));
                //jAlert($(this).attr("id"));
            });
            }
        });

        $("#busqueda").focus();
    }
}


function inicio()
{


    //$('#form').hide();

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "obtenerFuncion=inicio",
        beforeSend:function(datos){
            $("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success:function(datos){
            aparecer("#form");
            $("#form").html(datos);
        },
        complete:function(datos){
            $("#loadiing").fadeOut('slow');
        }
    });
}



function asignarcorres(id_corres)
{
    //jAlert(pagina_ajax);
    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "id_corres="+id_corres
        +"&obtenerFuncion=asignarcorres",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#form1");
            $("#form1").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });

}


function aceptarrespuesta(id_respuesta)
{

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "id_respuesta="+id_respuesta
        +"&obtenerFuncion=aceptarrespuesta",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#form1");
            $("#form1").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function aceptarymodificarrespuesta(id_respuesta)
{

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "id_respuesta="+id_respuesta
        +"&obtenerFuncion=aceptarymodificarrespuesta",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#form1");
            $("#form1").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}

function rechazarrespuesta(id_respuesta)
{

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "id_respuesta="+id_respuesta
        +"&obtenerFuncion=rechazarrespuesta",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#form1");
            $("#form1").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}





function respondercorres(id_corres)
{
    //jAlert(pagina_ajax);
    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "id_corres="+id_corres
        +"&obtenerFuncion=respondercorres",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#form1");
            $("#form1").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}



function cambiarprioridadcorres(id_corres)
{
    //jAlert(pagina_ajax);
    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "id_corres="+id_corres
        +"&obtenerFuncion=cambiarprioridadcorres",
        beforeSend:function(datos){
            $("#div_prioridad").html('<img src="../estilos/imagenes/estatus/cargando3.gif" />');
        },
        success:function(datos){
            aparecer("#div_prioridad");
            $("#div_prioridad").html(datos);
        }
    });

}

function guardarcambioprioridad(id_corres)
{
    //jAlert(pagina_ajax);

    if($("#prioridad").val()==0){
        jAlert('Disculpe debe seleccionar la prioridad');
        $("#prioridad").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "id_corres="+id_corres
        +"&id_prioridad="+$("#prioridad").val()
        +"&obtenerFuncion=guardarcambioprioridad",
        beforeSend:function(datos){
            $("#div_prioridad").html('<img src="../estilos/imagenes/estatus/cargando3.gif" />');
        },
        success:function(datos){
            aparecer("#div_prioridad");
            $("#div_prioridad").html(datos);
        }
    });

}



function alerta(variable)
{
    jAlert(variable,"Mensaje de Alerta");
}




function guardarasignacion(id_corres)
{


    if($("#instruccion").val()==0){
        jAlert('Disculpe debe seleccionar la instruccion');
        $("#instruccion").focus();
        return false;
    }

    if($("#fecha_muletilla").val()==''){
        jAlert('Disculpe debe seleccionar la fecha de culminacion');
        $("#fecha_muletilla").focus();
        return false;
    }

    var elementos_chequeados="";
    var usuarios_asignados=$("input[name*='usuario_asignado']").each(function(){
        if($(this).is(':checked')){
            if(elementos_chequeados!=""){
                elementos_chequeados=elementos_chequeados+";";
            }
            elementos_chequeados=elementos_chequeados+$(this).val();
        }
    });

    if(elementos_chequeados==''){
        jAlert('Disculpe debe seleccionar al menos un usuario para la asignacion');
        return false;
    }



    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "id_corres="+id_corres
        +"&instruccion="+$("#instruccion").val()
        +"&fecha_culminacion="+$("#fecha_muletilla").val()
        +"&observaciones="+$("#observaciones").val()
        +"&usuarios_asignados="+elementos_chequeados
        +"&obtenerFuncion=guardarasignacion",
        beforeSend:function(datos){
            $("#loading").html('<img src="../estilos/imagenes/estatus/cargando2.gif" >');
        },
        success:function(datos){
            aparecer("#form1");
            $("#form1").html(datos);
        },
        complete:function(datos){
            $("#loading").fadeOut("slow");
        }
    });
}
function guardarrespondercorres()
{
    contenido = FCKeditorAPI.__Instances['txt_html'].GetHTML();
    if( contenido==""){
        jAlert('Disculpe debe escribir el contenido de la respuesta');
        return false;
    //la variable contenido posse el contenido html de fckeditor
}
$('#frm').submit();

}

function guardarregistarcorres()
{
    if($("#organismo").val()==0){
        jAlert('Disculpe debe seleccionar el organismo emisor');
        $("#organismo").focus();
        return false;
    }

    if($("#dependencia").val()==0){
        jAlert('Disculpe debe seleccionar la dependencia emisora');
        $("#dependencia").focus();
        return false;
    }

    if($("#usuarioemisor").val()==0){
        jAlert('Disculpe debe seleccionar el usuario emisor');
        $("#usuarioemisor").focus();
        return false;
    }

    if($("#tipocorres").val()==0){
        jAlert('Disculpe debe seleccionar el tipo de correspondencia');
        $("#tipocorres").focus();
        return false;
    }

    if($("#nrocontrol").val()==""){
        jAlert('Disculpe debe escribir el numero de control');
        $("#nrocontrol").focus();
        return false;
    }

    if($("#fecha").val()==''){
        jAlert('Disculpe debe seleccionar la fecha de la correspondencia');
        $("#fecha").focus();
        return false;
    }

    if($("#asunto").val()==""){
        jAlert('Disculpe debe escribir el asunto');
        $("#asunto").focus();
        return false;
    }

    contenido = FCKeditorAPI.__Instances['txt_html'].GetHTML();
    if( contenido==""){
        jAlert('Disculpe debe escribir el contenido de la correspondencia');
        return false;
    }


    $('#frm').submit();
}

function guardarmodificacioncorres()
{
    if($("#organismo").val()==0){
        jAlert('Disculpe debe seleccionar el organismo emisor');
        $("#organismo").focus();
        return false;
    }

    if($("#dependencia").val()==0){
        jAlert('Disculpe debe seleccionar la dependencia emisora');
        $("#dependencia").focus();
        return false;
    }

    if($("#usuarioemisor").val()==0){
        jAlert('Disculpe debe seleccionar el usuario emisor');
        $("#usuarioemisor").focus();
        return false;
    }

    if($("#tipocorres").val()==0){
        jAlert('Disculpe debe seleccionar el tipo de correspondencia');
        $("#tipocorres").focus();
        return false;
    }

    if($("#nrocontrol").val()==""){
        jAlert('Disculpe debe escribir el numero de control');
        $("#nrocontrol").focus();
        return false;
    }

    if($("#fecha").val()==''){
        jAlert('Disculpe debe seleccionar la fecha de la correspondencia');
        $("#fecha").focus();
        return false;
    }

    if($("#asunto").val()==""){
        jAlert('Disculpe debe escribir el asunto');
        $("#asunto").focus();
        return false;
    }

    $('#frm').submit();
}



function enviaragregar()
{
    var elements="";

    $.each($("input[name='contador[]']"), function() {
        elements=elements+$(this).val()+",";
    });


    jAlert(elements);


/*//var variable='';
for (i=0;i<$("input[name='contador']").length;i++)
{
var variable=variable+"|"+$("input[name='contador']").val();
}


				$.ajax({
						type: "GET",
						url: "classConstanciaFunciones.php",
						data:
						"contador="+$("input[name='contador[]']").val()
				 		 +"&obtenerFuncion=recibir",
						beforeSend: function(datos){
						$("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
                		},

						success: function(datos){
						$("#form").fadeIn("slow");
						$("#form").html(datos);
						$("#form").fadeOut("slow");
						$("#form").fadeIn("slow");
						}
			});
*/}


function agregardatosprueba(){
    //jAlert("entar");

    /*var contador=$("#contador").serializeArray();
    contador=contador.length;*/
    //

    $('#tabladatos').append(


        $("<tr>").attr("id",$("input[name='contador[]']").length).append(

            $("<td>").append($("<input type='button'>").attr("id",$("input[name='contador[]']").length).click(function(){
                //jAlert($("tr").attr("id"));
                //$("tr[id='"+$("input[name='contador']").length+"']").remove()
                $("tr[id='"+$(this).attr("id")+"']").remove()

            }),
            $("#desc_cargo").val()
            ),
            $("<td>").append(
                $("<input>").attr("id","contador").attr("name","contador[]").val($("input[name='contador[]']").length)
                )

            )

        )

//$("input[name='contador']").
/*var contador=$("#contador").serializeArray();
jAlert("contador"+contador.length);*/
}


function aceptargeneralventana(nombre_funcion)
{
    var id = $("input[name='radiobutton']:checked").val();
    var name = $("input[name='radiobutton']:checked").attr('texto');
    //self.parent.agregaridnombre(name,id);
    eval("self.parent."+nombre_funcion+"(name,id);");
    self.parent.tb_remove();
}


function busquedaventana(consulta,conector,titulo,jqfuntion) {


    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "busqueda="+$('#busqueda').val()
        +"&consulta="+consulta
        +"&conector="+conector
        +"&titulo="+titulo
        +"&jqfuntion="+jqfuntion
        +"&obtenerFuncion=listargeneralventana",
        /*beforeSend: function(datos){
						$("#loading2").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
                  },*/

                  success: function(datos){
                    $("#form2").fadeIn("slow");
                    $("#form2").html(datos);
        //$("#form2").fadeOut("slow");
        //$("#form2").fadeIn("slow");

    },
    complete:     function(datos){
        $("#loading2").fadeOut('slow');
    }
});


}

function pruebaprueba()
{
    //jAlert("entre");
    if($("#nombre").val()==''){
        jAlert('Disculpe debe escribir el nombre');
        $("#nombre").focus();
        return false;
    }

    var id = $("input[name='radiobutton']:checked").val();
    var name = $("input[name='radiobutton']:checked").attr('texto');
    //jAlert(name+'name');
    /*
				$.ajax({
						type: "GET",
						url: "classConstanciaFunciones.php",
						data:
						"nombre="+$('#nombre').val()
				 		 +"&obtenerFuncion=pruebaprueba2",
						beforeSend: function(datos){
						$("#loading2").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
                		},

						success: function(datos){
						$("#form2").fadeIn("slow");
						$("#form2").html(datos);
						$("#form2").fadeOut("slow");
						$("#form2").fadeIn("slow");

						},
						complete:     function(datos){
						$("#loading2").fadeOut('slow');
						}
                 });*/
    //$('#cargo').val($('#nombre').val());
    //self.parent.document.formulario.cargo.value = $("#nombre").val();


    self.parent.nueva(name,id);
    self.parent.tb_remove();
//self.parent.nueva2(id);


//   opener.document.formRedactarCorrespondecia.id_cc.value = $("#id_ocultocc").val();
//self.parent.tb_remove();
//self.parent.tb_remove();

}


function abriradjuntar()
{

    //jAlert("entre");
    //var id_cc =$("#id_cc").val();

    window.open("classadjuntar.php","ventana1","width=400,height=600,scrollbars=yes,toolbar=no,location=no,directories=no,menubar=no")
}

function abrirventanacc()
{

    //jAlert("entre");
    var id_cc =$("#id_cc").val();

    window.open("classConCopia.php?id_cc="+id_cc,"ventana1","width=370,height=600,scrollbars=yes,toolbar=no,location=no,directories=no,menubar=no")
}



function marcartodos()
{
    var inputss=$("input:checkbox").attr('checked','checked');
    var  contedidoconcatenado='';
    var  oculto='';
    var  id_oculto='';
    var contenidocc=$("input:checkbox[checked]").serializeArray();

    for(i=0;i<contenidocc.length;i++)
    {
        var con_contenidocc=contenidocc[i].name;
        contedidoconcatenado=contedidoconcatenado+"<div style='float:left'>"+contenidocc[i].value+';&nbsp;</div>';
        var oculto=oculto+contenidocc[i].value+';';
        var id_oculto=id_oculto+contenidocc[i].name+';';
    }
    $('#contedorcc').html(contedidoconcatenado);
    $("#ocultocc").val(oculto);
    $("#id_ocultocc").val(id_oculto);
}

function desmarcartodos()
{
    var inputss=$("input:checkbox").removeAttr('checked');
    var  contedidoconcatenado='';
    var  oculto='';
    var  id_oculto='';
    var contenidocc=$("input:checkbox[checked]").serializeArray();

    for(i=0;i<contenidocc.length;i++)
    {
        var con_contenidocc=contenidocc[i].name;
        contedidoconcatenado=contedidoconcatenado+"<div style='float:left'>"+contenidocc[i].value+';&nbsp;</div>';
        var oculto=oculto+contenidocc[i].value+';';
        var id_oculto=id_oculto+contenidocc[i].name+';';
    }
    $('#contedorcc').html(contedidoconcatenado);
    $("#ocultocc").val(oculto);
    $("#id_ocultocc").val(id_oculto);
}




function checkcampos()
{
    //jAlert("ejecute "+$("input:checkbox").length );
    //jAlert("id_get_ocultocc "+$("#id_get_ocultocc").val());

    var cadena =$("#id_get_ocultocc").val();
    cadena = cadena.split(";");
    var  contedidoconcatenado='';
    var  oculto='';
    var  id_oculto='';
    var contenidocc=$("input:checkbox[checked]").serializeArray();

    for(i=0;i<contenidocc.length;i++)
    {
        var con_contenidocc=contenidocc[i].name;
        for (var ii=0;ii<cadena.length;ii++)
        {//document.write(cadena +'<br>')
            //jAlert("chekeados "+cadena[i]);
            if(cadena[ii]==con_contenidocc)
            {
                contedidoconcatenado=contedidoconcatenado+"<div style='float:left'>"+contenidocc[i].value+';&nbsp;</div>';
                var oculto=oculto+contenidocc[i].value+';';
                var id_oculto=id_oculto+contenidocc[i].name+';';
            }
        }
    }
    $('#contedorcc').html(contedidoconcatenado);
    $("#ocultocc").val(oculto);
    $("#id_ocultocc").val(id_oculto);


    $("input:checkbox").click(
        function()
        {
            //jAlert( $(this).val());
            //$(this).check
            var  oculto='';
            var  id_oculto='';

            if($(this).is(':checked')){
                $('#contedorcc').append("<div style='float:left'>"+$(this).val()+';&nbsp;</div>')//todos ckeaados
                var contenidocc=$("input:checkbox[checked]").serializeArray();//los chekeados
                for(i=0;i<contenidocc.length;i++)
                {
                    var oculto=oculto+contenidocc[i].value+';';
                    var id_oculto=id_oculto+contenidocc[i].name+';';
                }

            }else{
                //jAlert($("input:checkbox[checked]").serialize());
                //serializeArray
                var contenidocc=$("input:checkbox[checked]").serializeArray();//los chekeados
                var  contedidoconcatenado='';
                var  oculto='';
                var  id_oculto='';

                for(i=0;i<contenidocc.length;i++)
                {
                    contedidoconcatenado=contedidoconcatenado+"<div style='float:left'>"+contenidocc[i].value+';&nbsp;</div>';
                    var oculto=oculto+contenidocc[i].value+';';
                    var id_oculto=id_oculto+contenidocc[i].name+';';

                }
                //jAlert(contedidoconcatenado);
                $('#contedorcc').html(contedidoconcatenado);

            } //else
            $("#ocultocc").val(oculto);
            $("#id_ocultocc").val(id_oculto);
        }//function

        )//click




}

function enviarcc()
{
    //jAlert("oculto Cc==> "+$("#ocultocc").val());
    //jAlert(foto);
    opener.document.formRedactarCorrespondecia.cc.value = $("#ocultocc").val();
    opener.document.formRedactarCorrespondecia.id_cc.value = $("#id_ocultocc").val();
    window.close();
}

function cancelarcc()
{
    window.close();
}


function validatipousuario()
{

    if($("#tipo_usuario").val()==''){
        jAlert('Disculpe debe escribir el tipo de usuario');
        $("#tipo_usuario").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "tipo_usuario="+$('#tipo_usuario').val()
        +"&obtenerFuncion=guardartipousuario",
        beforeSend: function(datos){
            $("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },

        success: function(datos){
            $("#form").fadeIn("slow");
            $("#form").html(datos);
            $("#form").fadeOut("slow");
            $("#form").fadeIn("slow");
        }
    });
}



function validatipoinstruccion()
{
    if($("#tipo_instruccion").val()==''){
        jAlert('Disculpe debe escribir el tipo de instruccion');
        $("#tipo_instruccion").focus();
        return false;
    }
    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "tipo_instruccion="+$('#tipo_instruccion').val()
        +"&obtenerFuncion=guardartipoinstruccion",
        beforeSend: function(datos){
            $("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success: function(datos){
            $("#form").fadeIn("slow");
            $("#form").html(datos);
            $("#form").fadeOut("slow");
            $("#form").fadeIn("slow");
        }
    });
}

function validainstruccion()
{
    if($("#tipo_instruccion").val()==0){
        jAlert('Disculpe debe seleccionar el tipo de instruccion');
        $("#tipo_organismo").focus();
        return false;
    }

    if($("#instruccion").val()==''){
        jAlert('Disculpe debe escribir la instruccion');
        $("#instruccion").focus();
        return false;
    }
    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "tipo_instruccion="+$('#tipo_instruccion').val()
        +"&instruccion="+$('#instruccion').val()
        +"&obtenerFuncion=guardarinstruccion",
        beforeSend: function(datos){
            $("#form").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success: function(datos){
            $("#form").fadeIn("slow");
            $("#form").html(datos);
            $("#form").fadeOut("slow");
            $("#form").fadeIn("slow");
        }
    });
}




function validatipoorganismo()
{
    if($("#tipo_organismo").val()==''){
        jAlert('Disculpe debe escribir el tipo de organismo');
        $("#tipo_organismo").focus();
        return false;
    }
    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "tipo_organismo="+$('#tipo_organismo').val()
        +"&obtenerFuncion=guardartipoorganismo",
        beforeSend: function(datos){
            $("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success: function(datos){
            $("#form").fadeIn("slow");
            $("#form").html(datos);
            $("#form").fadeOut("slow");
            $("#form").fadeIn("slow");
        }
    });
}

function validaorganismo()
{
    if($("#tipo_organismo").val()==0){
        jAlert('Disculpe debe seleccionar el tipo de organismo');
        $("#tipo_organismo").focus();
        return false;
    }

    if($("#organismo").val()==''){
        jAlert('Disculpe debe escribir el nombre del organismo');
        $("#organismo").focus();
        return false;
    }
    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "tipo_organismo="+$('#tipo_organismo').val()
        +"&organismo="+$('#organismo').val()
        +"&obtenerFuncion=guardarorganismo",
        beforeSend: function(datos){
            $("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success: function(datos){
            $("#form").fadeIn("slow");
            $("#form").html(datos);
            $("#form").fadeOut("slow");
            $("#form").fadeIn("slow");
        }
    });
}

function validadependencia()
{

    if($("#organismo").val()==0){
        jAlert('Disculpe debe seleccionar el organismo');
        $("#organismo").focus();
        return false;
    }

    if($("#dependencia").val()==''){
        jAlert('Disculpe debe escribir la dependencia');
        $("#dependencia").focus();
        return false;
    }

    if($("#abr_dependencia").val()==''){
        jAlert('Disculpe debe escribir la abreviatura de la dependencia');
        $("#abr_dependencia").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "organismo="+$('#organismo').val()
        +"&dependencia="+$('#dependencia').val()
        +"&abr_dependencia="+$('#abr_dependencia').val()
        +"&obtenerFuncion=guardardependencia",
        beforeSend: function(datos){
            $("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success: function(datos){
            $("#form").fadeIn("slow");
            $("#form").html(datos);
            $("#form").fadeOut("slow");
            $("#form").fadeIn("slow");
        }
    });
}

function validasubdependencia()
{

    if($("#organismo").val()==0){
        jAlert('Disculpe debe seleccionar el organismo');
        $("#organismo").focus();
        return false;
    }

    if($("#dependencia").val()==0){
        jAlert('Disculpe debe seleccionar la dependencia');
        $("#dependencia").focus();
        return false;
    }

    if($("#sub_dependencia").val()==''){
        jAlert('Disculpe debe escribir la sub dependencia');
        $("#sub_dependencia").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "organismo="+$('#organismo').val()
        +"&dependencia="+$('#dependencia').val()
        +"&sub_dependencia="+$('#sub_dependencia').val()
        +"&obtenerFuncion=guardarsubdependencia",
        beforeSend: function(datos){
            $("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success: function(datos){
            $("#form").fadeIn("slow");
            $("#form").html(datos);
            $("#form").fadeOut("slow");
            $("#form").fadeIn("slow");
        }
    });
}

function validadivision()
{


    if($("#organismo").val()==0){
        jAlert('Disculpe debe seleccionar el organismo');
        $("#organismo").focus();
        return false;
    }

    if($("#dependencia").val()==0){
        jAlert('Disculpe debe seleccionar la dependencia');
        $("#dependencia").focus();
        return false;
    }

    if($("#subdependencia").val()==0){
        jAlert('Disculpe debe seleccionar la sub dependencia');
        $("#subdependencia").focus();
        return false;
    }

    if($("#division").val()==''){
        jAlert('Disculpe debe escribir la division');
        $("#division").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "subdependencia="+$('#subdependencia').val()
        +"&division="+$('#division').val()
        +"&obtenerFuncion=guardardivision",
        beforeSend: function(datos){
            $("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success: function(datos){
            $("#form").fadeIn("slow");
            $("#form").html(datos);
            $("#form").fadeOut("slow");
            $("#form").fadeIn("slow");
        }
    });
}


function validacargo()
{
    if($("#nivelcargo").val()==0){
        jAlert('Disculpe debe seleccionar el nivel del cargo');
        $("#nivelcargo").focus();
        return false;
    }

    if($("#cargo").val()==''){
        jAlert('Disculpe debe escribir el cargo');
        $("#cargo").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "division="+$('#division').val()
        +"&cargo="+$('#cargo').val()
        +"&desc_cargo="+$('#desc_cargo').val()
        +"&nivelcargo="+$('#nivelcargo').val()
        +"&obtenerFuncion=guardarcargo",
        beforeSend: function(datos){
            $("#loadiing").html('<img src="../estilos/imagenes/estatus/cargando2.gif">');
        },
        success: function(datos){
            $("#form").fadeIn("slow");
            $("#form").html(datos);
            $("#form").fadeOut("slow");
            $("#form").fadeIn("slow");
        }
    });
}


function validadependencia_div()
{

    if($("#organismo").val()==0){
        jAlert('Disculpe debe seleccionar el organismo');
        $("#organismo").focus();
        return false;
    }

    if($("#dependencia").val()==''){
        jAlert('Disculpe debe escribir la dependencia');
        $("#dependencia").focus();
        return false;
    }

    if($("#abr_dependencia").val()==''){
        jAlert('Disculpe debe escribir la abreviatura de la dependencia');
        $("#abr_dependencia").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "organismo="+$('#organismo').val()
        +"&dependencia="+$('#dependencia').val()
        +"&abr_dependencia="+$('#abr_dependencia').val()
        +"&obtenerFuncion=guardardependencia_div",
        beforeSend: function(datos){
            $("#div_dependencia").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success: function(datos){
            $("#div_dependencia").fadeIn("slow");
            $("#div_dependencia").html(datos);
            $("#div_dependencia").fadeOut("slow");
            $("#div_dependencia").fadeIn("slow");
        }
    });
}


function validasubdependencia_div()
{


    if($("#dependencia").val()==0){
        jAlert('Disculpe debe seleccionar la dependencia');
        $("#dependencia").focus();
        return false;
    }

    if($("#subdependencia").val()==''){
        jAlert('Disculpe debe escribir la sub dependencia');
        $("#subdependencia").focus();
        return false;
    }


    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "subdependencia="+$('#subdependencia').val()
        +"&dependencia="+$('#dependencia').val()
        +"&obtenerFuncion=guardarsubdependencia_div",
        beforeSend: function(datos){
            $("#div_sub_dependencia").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success: function(datos){
            $("#div_sub_dependencia").fadeIn("slow");
            $("#div_sub_dependencia").html(datos);
            $("#div_sub_dependencia").fadeOut("slow");
            $("#div_sub_dependencia").fadeIn("slow");
        }
    });
}


function validadivision_div()
{

    if($("#subdependencia").val()==0){
        jAlert('Disculpe debe seleccionar la sub dependencia');
        $("#subdependencia").focus();
        return false;
    }

    if($("#division").val()==''){
        jAlert('Disculpe debe escribir la division');
        $("#division").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "subdependencia="+$('#subdependencia').val()
        +"&division="+$('#division').val()
        +"&obtenerFuncion=guardardivision_div",
        beforeSend: function(datos){
            $("#div_division").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success: function(datos){
            $("#div_division").fadeIn("slow");
            $("#div_division").html(datos);
            $("#div_division").fadeOut("slow");
            $("#div_division").fadeIn("slow");
        }
    });
}

function validacargo_div()
{

    /*	if($("#division").val()==0){
		jAlert('Disculpe debe seleccionar la sub dependencia');
		$("#division").focus();
		return false;
	}*/

    if($("#nivelcargo").val()==0){
        jAlert('Disculpe debe seleccionar el nivel del cargo');
        $("#nivelcargo").focus();
        return false;
    }

    if($("#cargo").val()==''){
        jAlert('Disculpe debe escribir el cargo');
        $("#cargo").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "division="+$('#division').val()
        +"&nivelcargo="+$('#nivelcargo').val()
        +"&cargo="+$('#cargo').val()
        +"&desc_cargo="+$('#desc_cargo').val()
        +"&obtenerFuncion=guardarcargo_div",
        beforeSend: function(datos){
            $("#div_cargo").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success: function(datos){
            $("#div_cargo").fadeIn("slow");
            $("#div_cargo").html(datos);
            $("#div_cargo").fadeOut("slow");
            $("#div_cargo").fadeIn("slow");
        }
    });
}

function combodependencia()
{
    //var activacion=activacion;
    //jAlert (activacion);

    if($("#organismo").val()==0){
        jAlert('Disculpe debe seleccionar el organismo');
        $("#organismo").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "organismo="+$('#organismo').val()
        +"&obtenerFuncion=combodependencia",
        beforeSend: function(datos){
            $("#div_dependencia").html('<img src="../estilos/imagenes/estatus/cargando3.gif" />');
        },
        success: function(datos){
            $("#div_dependencia").fadeIn("slow");
            $("#div_dependencia").html(datos);
            $("#div_dependencia").fadeOut("slow");
            $("#div_dependencia").fadeIn("slow");

        //combodelegado();
    }
});
}

function combogerencia()
{
    //var activacion=activacion;
    //jAlert (activacion);

    if($("#organismo").val()==0){
        jAlert('Disculpe debe seleccionar el organismo');
        $("#organismo").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "organismo="+$('#organismo').val()
        +"&obtenerFuncion=combogerencia",
        beforeSend: function(datos){
            $("#div_dependencia").html('<img src="../estilos/imagenes/estatus/cargando3.gif" />');
        },
        success: function(datos){
            $("#div_dependencia").fadeIn("slow");
            $("#div_dependencia").html(datos);
            $("#div_dependencia").fadeOut("slow");
            $("#div_dependencia").fadeIn("slow");

        //combodelegado();
    }
});
}

function combousuarioemisor()
{
    //var activacion=activacion;
    //jAlert (activacion);

    if($("#organismo").val()==0){
        jAlert('Disculpe debe seleccionar el organismo');
        $("#organismo").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "id_sub_dependencia="+$('#dependencia').val()
        +"&obtenerFuncion=combousuarioemisor",
        beforeSend: function(datos){
            $("#div_usuario_emisor").html('<img src="../estilos/imagenes/estatus/cargando3.gif" />');
        },
        success: function(datos){
            $("#div_usuario_emisor").fadeIn("slow");
            $("#div_usuario_emisor").html(datos);
            $("#div_usuario_emisor").fadeOut("slow");
            $("#div_usuario_emisor").fadeIn("slow");

        //combodelegado();
    }
});
}



function combosubdependencia()
{
    //var activacion=activacion;
    //jAlert (activacion);

    if($("#dependencia").val()==0){
        jAlert('Disculpe debe seleccionar la depencia');
        $("#dependencia").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "dependencia="+$('#dependencia').val()
        +"&obtenerFuncion=combosubdependencia",
        beforeSend: function(datos){
            $("#div_sub_dependencia").html('<img src="../estilos/imagenes/estatus/cargando3.gif" />');
        },
        success: function(datos){
            $("#div_sub_dependencia").fadeIn("slow");
            $("#div_sub_dependencia").html(datos);
            $("#div_sub_dependencia").fadeOut("slow");
            $("#div_sub_dependencia").fadeIn("slow");

        //combodelegado();
    }
});
}



function combopertenece(id_sub_dependencia)
{
    //var activacion=activacion;
    //jAlert (activacion);

    if($("#subdependencia").val()==0){
        jAlert('Disculpe debe seleccionar la sub depencia');
        $("#subdependencia").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "id_sub_dependencia="+id_sub_dependencia
        +"&obtenerFuncion=combopertenece",
        beforeSend: function(datos){
            $("#div_pertenece").html('<img src="../estilos/imagenes/estatus/cargando3.gif" />');
        },
        success: function(datos){
            $("#div_pertenece").fadeIn("slow");
            $("#div_pertenece").html(datos);
            $("#div_pertenece").fadeOut("slow");
            $("#div_pertenece").fadeIn("slow");
        }
    });

}


function combocargo()
{
    //var activacion=activacion;
    //jAlert (activacion);

    /*if($("#division").val()==0){
		jAlert('Disculpe debe seleccionar la division');
		$("#division").focus();
		return false;
	}*/
    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "division="+$('#division').val()
        +"&obtenerFuncion=combocargo",
        beforeSend: function(datos){
            $("#div_cargo").html('<img src="../estilos/imagenes/estatus/cargando3.gif" />');
        },
        success: function(datos){
            $("#div_cargo").fadeIn("slow");
            $("#div_cargo").html(datos);
            $("#div_cargo").fadeOut("slow");
            $("#div_cargo").fadeIn("slow");
        }
    });
}
/*
function combodelegado()
{
	//var activacion=activacion;
	//jAlert (activacion);

				$.ajax({
						type: "GET",
						url: "classConstanciaFunciones.php",
						data:
						"organismo="+$('#organismo').val()
						+"&dependencia="+$('#dependencia').val()
						+"&dependencia="+$('#dependencia').val()
						+"&subdependencia="+$('#subdependencia').val()
						+"&division="+$('#division').val()
				 		+"&obtenerFuncion=combodelegado",
						beforeSend: function(datos){
						$("#div_delegado").html('<img src="../estilos/imagenes/estatus/cargando3.gif" />');
                		},
						success: function(datos){
						$("#div_delegado").fadeIn("slow");
						$("#div_delegado").html(datos);
						$("#div_delegado").fadeOut("slow");
						$("#div_delegado").fadeIn("slow");
						}
			});
}*/


function interfazdependencia()
{

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "organismo="+$('#organismo').val()
        +"&obtenerFuncion=interfaz_agregar_dependencia",
        beforeSend: function(datos){
            $("#div_dependencia").html('<img src="../estilos/imagenes/estatus/cargando3.gif" />');
        },
        success: function(datos){
            $("#div_dependencia").fadeIn("slow");
            $("#div_dependencia").html(datos);
            $("#div_dependencia").fadeOut("slow");
            $("#div_dependencia").fadeIn("slow");
        }
    });
}


function interfazsubdependencia()
{

    if($("#dependencia").val()==0){
        jAlert('Disculpe debe seleccionar la depencia');
        $("#dependencia").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "dependencia="+$('#dependencia').val()
        +"&obtenerFuncion=interfaz_agregar_subdependencia",
        beforeSend: function(datos){
            $("#div_sub_dependencia").html('<img src="../estilos/imagenes/estatus/cargando3.gif" />');
        },
        success: function(datos){
            $("#div_sub_dependencia").fadeIn("slow");
            $("#div_sub_dependencia").html(datos);
            $("#div_sub_dependencia").fadeOut("slow");
            $("#div_sub_dependencia").fadeIn("slow");
        }
    });
}

function interfazdivision()
{

    if($("#subdependencia").val()==0){
        jAlert('Disculpe debe seleccionar la sub depencia');
        $("#subdependencia").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "subdependencia="+$('#subdependencia').val()
        +"&obtenerFuncion=interfaz_agregar_division",
        beforeSend: function(datos){
            $("#div_division").html('<img src="../estilos/imagenes/estatus/cargando3.gif" />');
        },
        success: function(datos){
            $("#div_division").fadeIn("slow");
            $("#div_division").html(datos);
            $("#div_division").fadeOut("slow");
            $("#div_division").fadeIn("slow");
        }
    });
}

function interfazcargo()
{

    /*if($("#division").val()==0){
		jAlert('Disculpe debe seleccionar la sub depencia');
		$("#division").focus();
		return false;
	}*/

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "division="+$('#division').val()
        +"&obtenerFuncion=interfaz_agregar_cargo",
        beforeSend: function(datos){
            $("#div_cargo").html('<img src="../estilos/imagenes/estatus/cargando3.gif" />');
        },
        success: function(datos){
            $("#div_cargo").fadeIn("slow");
            $("#div_cargo").html(datos);
            $("#div_cargo").fadeOut("slow");
            $("#div_cargo").fadeIn("slow");
        }
    });
}


function validalogin()
{

    if($("#login").val()==''){
        jAlert('Disculpe debe escribir el usuario');
        $("#login").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "login="+$('#login').val()
        +"&obtenerFuncion=validalogin",
        beforeSend: function(datos){
            $("#div_validacion").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success: function(datos){
            $("#div_validacion").fadeIn("slow");
            $("#div_validacion").html(datos);
            $("#div_validacion").fadeOut("slow");
            $("#div_validacion").fadeIn("slow");
        }
    });
}


function validalogin2()
{

    if($("#login").val()==''){
        jAlert('Disculpe debe escribir el usuario');
        $("#login").focus();
        return false;
    }

    //$('#validacion').val()='20';

    document.getElementById("validacion").value='20';

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "login="+$('#login').val()
        +"&validacion="+$('#validacion').val()
        +"&obtenerFuncion=validalogin",
        beforeSend: function(datos){
            $("#div_validacion").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success: function(datos){
            $("#div_validacion").fadeIn("slow");
            $("#div_validacion").html(datos);
            $("#div_validacion").fadeOut("slow");
            $("#div_validacion").fadeIn("slow");
        }
    });
}


function validausuariointerno()
{
    if($("#organismo").val()==0){
        jAlert('Disculpe debe seleccionar el organismo');
        $("#organismo").focus();
        return false;
    }
    if($("#dependencia").val()==0){
        jAlert('Disculpe debe seleccionar la dependencia');
        $("#dependencia").focus();
        return false;
    }

    if($("#subdependencia").val()==0){
        jAlert('Disculpe debe seleccionar la sub dependencia');
        $("#subdependencia").focus();
        return false;
    }

    if($("#cargo").val()==0){
        jAlert('Disculpe debe seleccionar el cargo');
        $("#cargo").focus();
        return false;
    }

    if($("#tipousuario").val()==0){
        jAlert('Disculpe debe seleccionar el tipo de usuario');
        $("#tipousuario").focus();
        return false;
    }

    if($("#estatususuario").val()==0){
        jAlert('Disculpe debe seleccionar el estatus del usuario');
        $("#estatususuario").focus();
        return false;
    }

    if($("#gradoinstrucion").val()==0){
        jAlert('Disculpe debe seleccionar el grado de instruccion');
        $("#gradoinstrucion").focus();
        return false;
    }

    if($("#cedula").val()==""){
        jAlert('Disculpe debe escribir la cedula');
        $("#gradoinstruccion").focus();
        return false;
    }

    if($("#nombres").val()==""){
        jAlert('Disculpe debe escribir los nombres');
        $("#nombres").focus();
        return false;
    }

    if($("#apellidos").val()==""){
        jAlert('Disculpe debe escribir los apellidos');
        $("#apellidos").focus();
        return false;
    }

    if($("#tipousuario").val()==1){
        //si usuario es interno
        if($("#validacion").val()!=1){
            jAlert('Disculpe debe validar el usuario');
            $("#login").focus();
            return false;
        }

        if($("#clave").val()==""){
            jAlert('Disculpe debe escribir la clave');
            $("#clave").focus();
            return false;
        }

        if($("#verificacion_clave").val()==""){
            jAlert('Disculpe debe escribir la verificacion de la clave');
            $("#verificacion_clave").focus();
            return false;
        }

        if($("#clave").val()!=$("#verificacion_clave").val()){
            jAlert('Disculpe la clave y la verificacion deben ser iguales');
            $("#clave").focus();
            return false;
        }

        if($("#pertenece").val()==0){
            jAlert('Disculpe debe escribir la clave');
            $("#clave").focus();
            return false;
        }


    }


    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "organismo="+$('#organismo').val()
        +"&dependencia="+$('#dependencia').val()
        +"&subdependencia="+$('#subdependencia').val()
        +"&division="+$('#division').val()
        +"&cargo="+$('#cargo').val()
        +"&tipousuario="+$('#tipousuario').val()
        +"&estatususuario="+$('#estatususuario').val()
        +"&gradoinstrucion="+$('#gradoinstrucion').val()
        +"&cedula="+$('#cedula').val()
        +"&nombres="+$('#nombres').val()
        +"&apellidos="+$('#apellidos').val()
        +"&login="+$('#login').val()
        +"&clave="+$('#clave').val()
        +"&pertenece="+$('#pertenece').val()
        +"&obtenerFuncion=guardarusuariointerno",
        beforeSend: function(datos){
            $("#form").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success: function(datos){
            $("#form").fadeIn("slow");
            $("#form").html(datos);
            $("#form").fadeOut("slow");
            $("#form").fadeIn("slow");
        }
    });
}



function validausuarioexterno()
{
    if($("#organismo").val()==0){
        jAlert('Disculpe debe seleccionar el organismo');
        $("#organismo").focus();
        return false;
    }

    if($("#cargo").val()==0){
        jAlert('Disculpe debe seleccionar el cargo');
        $("#cargo").focus();
        return false;
    }

    if($("#estatususuario").val()==0){
        jAlert('Disculpe debe seleccionar el estatus del usuario');
        $("#estatususuario").focus();
        return false;
    }

    if($("#gradoinstrucion").val()==0){
        jAlert('Disculpe debe seleccionar el grado de instruccion');
        $("#gradoinstrucion").focus();
        return false;
    }

    if($("#cedula").val()==""){
        jAlert('Disculpe debe escribir la cedula');
        $("#gradoinstruccion").focus();
        return false;
    }

    if($("#nombres").val()==""){
        jAlert('Disculpe debe escribir los nombres');
        $("#nombres").focus();
        return false;
    }

    if($("#apellidos").val()==""){
        jAlert('Disculpe debe escribir los apellidos');
        $("#apellidos").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "organismo="+$('#organismo').val()
        +"&cargo="+$('#cargo').val()
        +"&estatususuario="+$('#estatususuario').val()
        +"&gradoinstrucion="+$('#gradoinstrucion').val()
        +"&cedula="+$('#cedula').val()
        +"&nombres="+$('#nombres').val()
        +"&apellidos="+$('#apellidos').val()
        +"&obtenerFuncion=guardarusuarioexterno",
        beforeSend: function(datos){
            $("#form").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success: function(datos){
            $("#form").fadeIn("slow");
            $("#form").html(datos);
            $("#form").fadeOut("slow");
            $("#form").fadeIn("slow");
        }
    });
}


function fechaMuletilla()
{
    Calendar.setup({
        inputField     :    'fecha_muletilla',
        button         :    'calendario-muletilla'
    });
}

function fecha_()
{
    Calendar.setup({
        inputField     :    'fecha',
        button         :    'calendario'
    });
}


function validamuletilla()
{
    if($("#muletilla").val()==''){
        jAlert('Disculpe debe escribir la descripcion de la muletilla');
        $("#muletilla").focus();
        return false;
    }

    if($("#autor").val()==''){
        jAlert('Disculpe debe escribir el autor de la muletilla');
        $("#autor").focus();
        return false;
    }
    if($("#fecha_muletilla").val()==''){
        jAlert('Disculpe debe seleccionar la facha de la muletilla');
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "muletilla="+$('#muletilla').val()
        +"&autor="+$('#autor').val()
        +"&fecha_muletilla="+$('#fecha_muletilla').val()
        +"&obtenerFuncion=guardarmuletilla",
        beforeSend: function(datos){
            $("#form").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
        },
        success: function(datos){
            $("#form").fadeIn("slow");
            $("#form").html(datos);
            $("#form").fadeOut("slow");
            $("#form").fadeIn("slow");
        }
    });
}


function busquedausuariocc()
{
    //jAlert("entre"+$("#busqueda").val());


    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "busqueda="+$('#busqueda').val()
        +"&id_cc="+$('#id_get_ocultocc').val()
        +"&obtenerFuncion=listacc",
        /*beforeSend: function(datos){
						$("#form").html('<img src="../estilos/imagenes/estatus/cargando2.gif" />');
                  }, */
                  success: function(datos){
            //$("#form").fadeIn("slow");
            $("#div_busqueda").html(datos);
        //checkcampos();
        //$("#form").fadeOut("slow");
        //$("#form").fadeIn("slow");
    }
});

}
////////////////////////////////////////////////////////////////////////////////
function listaconstancia()
{

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "cedula="+0
        +"&tipo_estatus="+1
        +"&nro_solicitud="+2
        +"&obtenerFuncion=listaconstancia",
        success: function(datos){
            $("div[class='contenedor2']").html(datos);

            $('#styling-cutom-striping').tableSorter({
                sortClassAsc: 'headerSortUp', 		// class name for ascending sorting action to header
                sortClassDesc: 'headerSortDown', 	// class name for descending sorting action to header
                headerClass: 'header', 				// class name for headers (th's)
                stripingRowClass: ['even','odd'],	// class names for striping supplyed as a array.
                stripeRowsOnStartUp: true
            });


        }
    });
}
/*
function siguiente()
{

var minimo=parseInt($("#minimo").val())+10;
var count=parseInt($("#count").val())+1;


				$.ajax({
						type: "GET",
						url: "classConstanciaFunciones.php",
						data:
						"minimo="+minimo
						 +"&count="+count
				 		 +"&obtenerFuncion=listaconstancia",
						success: function(datos){
						$("div[class='contenedor2']").html(datos);

							$('#styling-cutom-striping').tableSorter({
							sortColumn: 'name',					// Integer or String of the name of the column to sort by.
							sortClassAsc: 'headerSortUp', 		// class name for ascending sorting action to header
							sortClassDesc: 'headerSortDown', 	// class name for descending sorting action to header
							headerClass: 'header', 				// class name for headers (th's)
							stripingRowClass: ['even','odd'],	// class names for striping supplyed as a array.
							stripeRowsOnStartUp: true
						});

						}
			});
}


function anterior()
{

var minimo=parseInt($("#minimo").val())-10;
var count=parseInt($("#count").val())-1;

				$.ajax({
						type: "GET",
						url: "classConstanciaFunciones.php",
						data:
						"minimo="+minimo
						 +"&count="+count
				 		 +"&obtenerFuncion=listaconstancia",
						success: function(datos){
						$("div[class='contenedor2']").html(datos);

							$('#styling-cutom-striping').tableSorter({
							sortColumn: 'name',					// Integer or String of the name of the column to sort by.
							sortClassAsc: 'headerSortUp', 		// class name for ascending sorting action to header
							sortClassDesc: 'headerSortDown', 	// class name for descending sorting action to header
							headerClass: 'header', 				// class name for headers (th's)
							stripingRowClass: ['even','odd'],	// class names for striping supplyed as a array.
							stripeRowsOnStartUp: true
						});

						}
			});
}

function cambiopagina()
{

	if ($("#cantidad_paginas").val()==0)
	{
	var minimo=$("#cantidad_paginas").val();
	var count=1;

	}
	else
	{
	//parseInt($("#cantidad_paginas").val())+10;
	var minimo=$("#cantidad_paginas").val()+0;
	//var count=$("#cantidad_paginas").text();
	var count=document.getElementById('cantidad_paginas')[document.getElementById('cantidad_paginas').selectedIndex].text;
	}


				$.ajax({
						type: "GET",
						url: "classConstanciaFunciones.php",
						data:
						"minimo="+minimo
						 +"&count="+count
				 		 +"&obtenerFuncion=listaconstancia",
						success: function(datos){
						$("div[class='contenedor2']").html(datos);

							$('#styling-cutom-striping').tableSorter({
							sortColumn: 'name',					// Integer or String of the name of the column to sort by.
							sortClassAsc: 'headerSortUp', 		// class name for ascending sorting action to header
							sortClassDesc: 'headerSortDown', 	// class name for descending sorting action to header
							headerClass: 'header', 				// class name for headers (th's)
							stripingRowClass: ['even','odd'],	// class names for striping supplyed as a array.
							stripeRowsOnStartUp: true
						});

						}
			});
}*/












////////////////////////////////////////////////////////////////////////////////////////

function fechaDesde()
{
    Calendar.setup({
        inputField     :    'fecha_desde',
        button         :    'calendario-desde'
    });
}

function fechaHasta()
{
    Calendar.setup({
        inputField     :    'fecha_hasta',
        button         :    'calendario-hasta'
    });
}




function guardarConstancia()
{
    //jAlert('Entre en generar');

    /*if($("#cedula").val()==""){
		jAlert('Disculpe debe iniciar session');
		$("#cedula").focus();
		return false;
	}*/

    /*	if($("#tipo_constancia").val()==0){
		jAlert('Disculpe debe seleccionar el tipo de constancia');
		$("#tipo_constancia").focus();
		return false;
	}
	if($("#tipo_constancia").val()<4){

		if($("#clasificacion_constancia").val()==0){
			jAlert('Disculpe debe seleccionar la clasificacion de la constancia');
			$("#clasificacion_constancia").focus();
			return false;
		}
	}
			$.ajax({
						type: "GET",
						url: "classConstanciaFunciones.php",
						data:
						"cedula="+$('#cedula').val()
						 +"&tipo_constancia="+$('#tipo_constancia').val()
						 +"&dirigido="+$('#dirigido').val()
						 +"&clasificacion_constancia="+$('#clasificacion_constancia').val()
				 		 +"&obtenerFuncion=guardarConstancia",
						success: function(datos){
						$("#form1").html(datos);
						}
                 });*/

$('#formRedactarCorrespondecia').submit();
}

function CambiarEstatus()
{

    if($("#tipo_estatus").val()=='S'){
        jAlert('Disculpe debe seleccionar el estatus');
        $("#tipo_estatus").focus();
        return false;
    }

    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "cedula="+$('#cedula').val()
        +"&tipo_estatus="+$('#tipo_estatus').val()
        +"&nro_solicitud="+$('#nro_solicitud').val()
        +"&obtenerFuncion=cambiarEstatus",
        success: function(datos){
            $("#form1").html(datos);
        }
    });
}

function CambiarEstatusMasivo()
{


    if($("#desde").val()==''){
        jAlert('Disculpe debe escribir el numero de solicitud desde');
        $("#desde").focus();
        return false;
    }

    if($("#hasta").val()==''){
        jAlert('Disculpe debe escribir el numero de solicitud hasta');
        $("#hasta").focus();
        return false;
    }

    if($("#desde").val()>=$("#hasta").val()){
        jAlert('Disculpe el numero de solicitud desde debe ser menor al numero de solicitud hasta');
        $("#desde").focus();
        return false;
    }

    if($("#tipo_estatus").val()=='S'){
        jAlert('Disculpe debe seleccionar el estatus');
        $("#tipo_estatus").focus();
        return false;
    }





    $.ajax({
        type: "GET",
        url: "classConstanciaFunciones.php",
        data:
        "desde="+$('#desde').val()
        +"&hasta="+$('#hasta').val()
        +"&tipo_estatus="+$('#tipo_estatus').val()
        +"&obtenerFuncion=cambiarEstatusMasivo",
        success: function(datos){
            $("#form1").html(datos);
        }
    });
}

function guardarobservaciones(callbacks){
    //preguntar por el componente de tobservaciones_all...
    if($.find("#tobservaciones_all").length==1){
        $.ajax({
            type: "GET",
            url: "classDirectorioFunciones.php",
            data:
            "id_pauta="+$('#tobservaciones_all').attr("pauta")
            +"&mensaje="+$("#tobservaciones_all textarea").val()
            +"&obtenerFuncion=comentarios_comp_tablacomentario",
            success: function(datos){
                callbacks();
            }
        });
    }else{
        callbacks();
    }
}

function CancelarRegresar(pagina, obj){
    if($(obj).val()=="Vista Previa" || $(obj).val()=="Guardar para futuros Cambios"){
        guardarobservaciones(function(){
            location.href=pagina;
        });
    }else{
        location.href=pagina;
    }
}

function Cancelar()
{
    location.href='classSolicitudConstancia.php';
}

function CancelarCambioEstatusMasivo()
{
    location.href='classCambioEstatusConstanciaMasivo.php';
}

function CancelarLista()
{
    location.href='classBusquedaListaConstancia.php';
}

function Enviar(numero)
{
    //jAlert(numero);
    //$('#divGenerarComprobante').hide();
    $('#constancia'+numero).submit();
}

function BusquedaConstancia()
{
    //jAlert(numero);
    //$('#divGenerarComprobante').hide();
    $('#formBuscarConstancia').submit();
}


function ValidacionImpresionMasiva()
{
    if($("#tipo_constancia").val()==0){
        jAlert('Disculpe debe seleccionar el tipo de constancia');
        $("#tipo_constancia").focus();
        return false;
    }

    if($("#fecha_desde").val()==''){
        jAlert('Disculpe debe seleccionar la fecha desde');
        $("#fecha_desde").focus();
        return false;
    }

    if($("#fecha_hasta").val()==''){
        jAlert('Disculpe debe seleccionar la fecha hasta');
        $("#fecha_hasta").focus();
        return false;
    }

    $('#formImpresionMasiva').submit();
}



function BusquedaOtraSolicitud()
{
    if($("#cedula_busqueda").val()==''){
        jAlert('Disculpe debe escribir la cedula a buscar');
        $("#cedula_busqueda").focus();
        return false;
    }

    $('#formBuscarConstancia').submit();
}


function validaN(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    //jAlert(tecla);
    if (tecla==0) return true;
    if (tecla==8) return true; //Tecla de retroceso (para poder borrar)
    if (tecla==9) return true;
    //if (tecla==32) return true;
    if (tecla==37) return true;
    if (tecla==38) return true;
    if (tecla==39) return true;
    if (tecla==40) return true;
    //if (tecla==46) return true;

    //patron =/[A-Za-z]/; // Solo acepta letras
    patron = /\d/;
    //patron = /\w/;
    //patron = /\D/;
    //
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

function validaL(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    //jAlert(tecla);
    if (tecla==0) return true;
    if (tecla==8) return true; //Tecla de retroceso (para poder borrar)
    if (tecla==9) return true;
    if (tecla==32) return true;
    if (tecla==37) return true;
    if (tecla==38) return true;
    if (tecla==39) return true;
    if (tecla==40) return true;
    if (tecla==241) return true;
    if (tecla==46) return true;

    patron =/[A-Za-z]/; // Solo acepta letras
    //patron = /\d/;
    //patron = /\w/;
    //patron = /\D/;
    //
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

function validaBsf(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    //jAlert(tecla);
    if (tecla==0) return true;
    if (tecla==8) return true; //Tecla de retroceso (para poder borrar)
    if (tecla==9) return true;
    //if (tecla==32) return true;
    if (tecla==37) return true;
    if (tecla==38) return true;
    if (tecla==39) return true;
    if (tecla==40) return true;
    if (tecla==44) return true;

    //if (tecla==46) return true;

    //patron =/[A-Za-z]/; // Solo acepta letras
    patron = /\d/;
    //patron = /\w/;
    //patron = /\D/;
    //
    te = String.fromCharCode(tecla);
    return patron.test(te);
}



function enter(e,funcion) {
    /*alert("hhhh"+funcion);
    return false;*/
    tecla = (document.all) ? e.keyCode : e.which;
    //alert(tecla);
    if (tecla==0) return true;
    if (tecla==8) return true; //Tecla de retroceso (para poder borrar)
    if (tecla==9) return true;
    //if (tecla==32) return true;
    if (tecla==37) return true;
    if (tecla==38) return true;
    if (tecla==39) return true;
    if (tecla==40) return true;
    if (tecla==32) return true;
    if (tecla==61) return true;
    if (tecla==241) return true;
    if (tecla==209) return true;
    if (tecla==13){
        funcion();
    }
    patron = /\w/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

function xyx(){
    alert("hola");
}
