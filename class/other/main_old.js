$(document).ready(function(){
	$("div#search_algorithm_result").html("");
	$("table").tablesorter();
});


//codigo que hace las busquedas de un string
function encontrarEnPajar(dato){

	//borrando datos anteriores si existe.
	ajaxContent = $.ajax({
		url: "Json.php",
		global: false,
		type: "POST",
		data: ({JSONDATA : dato}),
		success: function(msg){
			$("div#search_algorithm_result").html("");

			jsondata=eval( "(" +msg+ ")" );

			var cabezales = new Array("Foto" , "Datos Personales" , "Correo", "Extension" );
			var suplementoInfo= new Array(Array('Foto'),Array('nombre','gerencia','division','cargo'),Array('email'),Array('extension'));
			if (jsondata.length > 0){
				var tabla = '' ;



				tabla += '<table id="tablabusqueda" class="tabla">' ;
				tabla +=  "<thead>" ;
				tabla +=   "<tr>";
				for (var i = 0; i < cabezales.length; i++) {
					tabla +=    "<th>"    + cabezales[i] +       "</th>";
				}
				tabla +=   "</tr>" ;
				tabla +=  "</thead>" ;
				tabla +=  "<tbody>" ;
				for(i=0;i<jsondata.length;i++){
					tabla +=    '<tr>';

					for (var cab = 0; cab < cabezales.length; cab++) {
						tabla += '<td>';
						if(suplementoInfo[cab][0]=="Foto"){
							tabla+="<img height=\"60\" width=\"60\" src=\"http://vtvsoporte/_Carnetizacion/fotoscisigai/"+jsondata[i][suplementoInfo[cab][0]]+".jpeg\" />";
						}else if (suplementoInfo[cab][0]=="email"){
							if(jsondata[i][suplementoInfo[cab][0]]!=" -- no disponible -- "){
								tabla+="<a href=\"mailto:"+jsondata[i]['emailalias']+"\">"+jsondata[i][suplementoInfo[cab][0]]+"</a>";
							}else{
								tabla+=jsondata[i][suplementoInfo[cab][0]];
							}
						}else{
							for(sub=0;sub<suplementoInfo[cab].length;sub++){

								tabla+=jsondata[i][suplementoInfo[cab][sub]]+"<br>";

							}
						}
						tabla += "</td>";
					}
					tabla +=    "</tr>";
				}
				tabla +=  "</tbody>" ;
				tabla += "</table>" ;
				$("div#search_algorithm_result").append(tabla) ;
			}
		}
	}
	).responseText;
}

function initgerencias(){
	//dom

	document.getElementById("text_buscarenDirectorio").value="";
	ajaxContent = $.ajax({
		url: "Json.php",
		global: false,
		type: "POST",
		data: ({TDEGEREBCIAS : 'Gerencias'}),
		success: function(msg){
			$("#gui_gerencias").html(msg);
		}
	}
	).responseText;

}

function ejecutarbusquedadegerencia(objeto){
	initdivisiones(objeto);
	encontrarEnPajar(objeto[objeto.selectedIndex].innerHTML);
}

function borrarlistados(){
	
	try{
		document.getElementById("gerencias").value="";
		document.getElementById("divisiones").value="";
	}catch (objError){
	}
	document.getElementById('mostrargerencias').style.display='none';
	document.getElementById('mostrardependencias').style.display='none';
	document.getElementById('gui_gerencias').style.display='none';
	document.getElementById('gui_divisiones').style.display='none';
	document.getElementById('clickdegerencia').style.display='block';
}

function initdivisiones(objeto){
	document.getElementById("text_buscarenDirectorio").value="";
	ajaxContent = $.ajax({
		url: "Json.php",
		global: false,
		type: "POST",
		data: "TDEGEREBCIAS=Division&idgerencia="+objeto.value ,
		success: function(msg){
			$("#gui_divisiones").html(msg);
		}
	}
	).responseText;
	document.getElementById("gui_divisiones").style.display="block";
}