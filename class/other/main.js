$(document).ready(function(){
	$("#example0").treeview({animated: "fast", collapsed: true, unique: true});
	$("#example1").treeview({animated: "fast", collapsed: true, unique: true});
	$("div#search_algorithm_result").html("");
	$("table").tablesorter();
});

var Base64 = {

	// private property
	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

	// public method for encoding
	encode : function (input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;

		input = Base64._utf8_encode(input);

		while (i < input.length) {

			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);

			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;

			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}

			output = output +
			this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
			this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

		}

		return output;
	},

	// public method for decoding
	decode : function (input) {
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;

		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

		while (i < input.length) {

			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));

			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;

			output = output + String.fromCharCode(chr1);

			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}

		}

		output = Base64._utf8_decode(output);

		return output;

	},

	// private method for UTF-8 encoding
	_utf8_encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	},

	// private method for UTF-8 decoding
	_utf8_decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;

		while ( i < utftext.length ) {

			c = utftext.charCodeAt(i);

			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}

		}

		return string;
	}
}


function error(){
	document.getElementById("search_algorithm_result").innerHTML="<table><tr><th>No existen Datos para su B&uacute;squeda</th></tr></table>";
}
//codigo que hace las busquedas de un string
//version 2 dom HTML
function encontrarEnPajar(dato){
	//borrando datos anteriores si existe.

	ajaxContent = $.ajax({
		url: "../sistemas/directorio/paginas/classajaxserv.php",
		type: "POST",
		data: (  "JSONDATA="+dato+"&intfunc=cargarPagina"),
		success: function(msg){
			$("div#search_algorithm_result").html("");


                       // $("div#search_algorithm_result").html(msg);

			jsondata=eval( "(" +msg+ ")" );

			var cabezales = new Array("Foto" , "Datos Personales" , "Correo", "Extension" );
			var suplementoInfo= new Array(Array('Foto'),Array('nombre','gerencia','cargo'),Array('email'),Array('extension'));

			if(jsondata.length>0){
                                $("#resultadosdirectorio").fadeIn();
				// Crear <table> y sus dos atributos
				var tabla = document.createElement('table');
				tabla.setAttribute('id', 'tablaResultado');


				//tabla.setAttribute('summary', 'Descripción de la tabla y su contenido');
				// Crear <caption> y añadirlo a la <table>
				//var caption = document.createElement('caption');
				//var titulo = document.createTextNode('Título de la tabla');
				//caption.appendChild(titulo);
				//tabla.appendChild(caption);
				// Crear sección <thead>


				var thead = document.createElement('thead');
				tabla.appendChild(thead);
				// Añadir una fila a la sección <thead>
				thead.insertRow(0);
				for(i=0;i<cabezales.length;i++){
					// Añadir las tres columnas de la fila de <thead>
					var cabecera = document.createElement('th');
					if(i==0){
						cabecera.setAttribute('width', '60');
					}
					cabecera.innerHTML = cabezales[i];

					tabla.rows[0].appendChild(cabecera);
				}


				// La sección <tfoot> se crearía de forma similar a <thead>
				// Crear sección <tbody>
				var tbody = document.createElement('tbody');
				tabla.appendChild(tbody);

				for(i=0;i<jsondata.length;i++){
					// Añadir una fila a la sección <tbody>
					tbody.insertRow(i);
					//cabecera = document.createElement("th");
					//cabecera.setAttribute('scope', 'row');
					//cabecera.innerHTML = 'Cabecera fila 1';
					//tabla.tBodies[0].rows[0].appendChild(cabecera);

					// También se podría hacer:
					// tbody.rows[0].cells[0].appendChild(document.createTextNode('Celda 1 - 1'));

					for(var cab=0;cab<cabezales.length;cab++){
						tbody.rows[i].insertCell(cab);

						if(suplementoInfo[cab][0]=="Foto"){
						try{
							tbody.rows[i].cells[cab].setAttribute('align', 'center');
							//tbody.rows[i].cells[cab].innerHTML ="<img src=\"http://vtvsoporte/_Carnetizacion/fotoscisigai/imagen.php?ancho=60&alto=60&ruta="+jsondata[i][suplementoInfo[cab][0]]+".jpeg\" />";
							tbody.rows[i].cells[cab].innerHTML ="<img height=\"60\" width=\"60\" src=\"download_foto.php?id="+Base64.encode(jsondata[i][suplementoInfo[cab][0]])+"\" />";
							}catch(err){
							}

						}else if (suplementoInfo[cab][0]=="email"){

						try{
							if(jsondata[i][suplementoInfo[cab][0]]!=" -- no disponible -- "){
								tbody.rows[i].cells[cab].innerHTML ="<a href=\"mailto:"+jsondata[i]['emailalias']+"\">"+jsondata[i][suplementoInfo[cab][0]]+"</a>";
							}else{
								tbody.rows[i].cells[cab].innerHTML =jsondata[i][suplementoInfo[cab][0]];
							}
						}catch(err){
						}

						}else{
							for(sub=0;sub<suplementoInfo[cab].length;sub++){
								try{
									tbody.rows[i].cells[cab].innerHTML=(tbody.rows[i].cells[cab].innerHTML)+jsondata[i][suplementoInfo[cab][sub]]+"<br>";
								}catch(err){}
							}
						}
					}
				}
				// El resto de filas del <tbody> se crearía de la misma forma
				// Añadir la tabla creada al final de la página
				$("#search_algorithm_result").html(tabla);


				//query efects!
				$("#tablaResultado").css('display','none');
				//agregando el directorio.


                                //margin:0px;padding:0px;width:800px
				$("#tablaResultado").addClass('tabla');

				$("#tablaResultado").css('margin','0px');
				$("#tablaResultado").css('padding','0px');
				$("#tablaResultado").css('width','800px');

				$("#tablaResultado").fadeIn("slow");

                                //tbody {height:270px;overflow-x:hidden;overflow-y:auto}
                                //$("#tablaResultado").find("tbody").css("max-height","400px").css("overflow-x","hidden").css("overflow-y","auto");
                                $("#tablaResultado").find("tbody").find("tr").mouseover(function(){
                                    $(this).css("background-color","#F0F0F6");
                                }).mouseout(function(){
                                    $(this).css("background-color","#FFFFFF");
                                });
                                $("#search_algorithm_result").css("max-height","400px").css("overflow-x","hidden").css("overflow-y","auto");



                                //$("#tablaResultado").tableSorter();

			}else{
                             $("#resultadosdirectorio").fadeOut();
                        }
		}
	}
	).responseText;
}




function initgerencias(){
	//dom

	document.getElementById("text_buscarenDirectorio").value="";
	ajaxContent = $.ajax({
		url: "../sistemas/directorio/paginas/classajaxserv.php",
		global: false,
		type: "POST",
		data: ("intfunc=vergerencias"),
		success: function(msg){
			$("#gui_gerencias").html(msg);
		}
	}
	).responseText;

}

function ejecutarbusquedadegerencia(objeto){
	initdivisiones(objeto);
	encontrarEnPajar(objeto[objeto.selectedIndex].innerHTML);
}

function borrarlistados(){
	try{
		document.getElementById("gerencias").value="";
		document.getElementById("divisiones").value="";
	}catch (objError){
	}
	document.getElementById('mostrargerencias').style.display='none';
	document.getElementById('mostrardependencias').style.display='none';
	document.getElementById('gui_gerencias').style.display='none';
	document.getElementById('gui_divisiones').style.display='none';
	document.getElementById('clickdegerencia').style.display='block';
}

function initdivisiones(objeto){
	document.getElementById("text_buscarenDirectorio").value="";
	ajaxContent = $.ajax({
		url: "../sistemas/directorio/paginas/classajaxserv.php",
		global: false,
		type: "POST",
		data: "intfunc=verdiv&idgerencia="+objeto.value ,
		success: function(msg){
			$("#gui_divisiones").html(msg);
		}
	}
	).responseText;
	document.getElementById("gui_divisiones").style.display="block";
}
