<?php
// Clase Principal.
class classbdConsultas 
{
	var $ObjDb="";
	var $orderby, $groupby,$limite,$camporeturn;
	function classbdConsultas($ruta)
	{
		require_once($ruta);
		$this->orderby=array();
		$this->groupby=array();
		$this->limite=array();
		$this->condicion=array();
        }
	/**
	 * funcion que se encarga de ejecutar las funciones de base de datos 
	 * para obtener resultados. (hace solo selects)
	 *
	 * @param string de ruta de conexion a usar $conect
	 * @param array de configuracion de la tabla a usar $tabla
	 * @param array de configuracion de campos a regresar $campos
	 * @param string de la clase a llamar $clase
	 * @param string de la pa propiedad respectiva de la clase a llamar $propiedadclase
	 * @param array de configuracion de la condicion a usar $condicion
	 * @return array query result
	 */
	function query_classdb($conect,$tabla,$campos,$clase,$propiedadclase,$condicion=""){
		if($condicion==""){
                    $condicion=array();
		}
		
                
                $campo_return=$this->camporeturn;
                $this->camporeturn=array();

		$ordenarpor=$this->orderby;
		$this->orderby=array();//dejar de usar.
		
		$agruparpor=$this->groupby;
		$this->groupby=array();
           
                $limite=$this->limite;
                $this->limite=array();

		
		$this->ObjDb = new $clase($conect);
		$this->ObjDb->fdbConectar();


               //hub de recursos.. classdbconsulta
               switch ($propiedadclase){
               
                   case "fbdSelect";
                   return $this->ObjDb->$propiedadclase($tabla,$campos,$condicion,$ordenarpor,$agruparpor,$limite);
                   break;
               
                   case "fdbInsert";
                   return $this->ObjDb->$propiedadclase($tabla,$campos);
                   break;

                   case "fdbInsertRn";
                   return $this->ObjDb->$propiedadclase($tabla,$campos,$campo_return);
                   break;

                   case "fdbdUpdate";
                   return $this->ObjDb->$propiedadclase($tabla,$campos,$condicion);
                   break;
               }
		
	}

        /**
         *convierte query a json utf8 ideal para querys con sqlserver.
         * @param array $data
         */
        function dbcore_toutf8($data){
            if(count($data)>0){
                foreach ($data[1] as $key => $value){
                    $valoresajson[$key]=utf8_encode($value);
                }
            }
           echo json_encode($valoresajson);
        }
	
	/**
	 * funcion que genera los datos actuales de las gerencias registradas en sigai
	 *
	 * @param string $conect
	 * @return sql_select_resultado
	 */
	function select_sigai_gerencias($conect){
		// Tabla para hacer la consulta
		$nameTabla['GRLGERENCIA']="GRLGERENCIA";
		// Campos para seleccionar
		$campos['ID_GERENCIA']="ID_GERENCIA";
		$campos['DE_GERENCIA']="DE_GERENCIA";
		
		$this->orderby=array("DE_GERENCIA"=>"DE_GERENCIA ASC");
		return $this->query_classdb($conect,$nameTabla,$campos,"classdbsqlserver","fbdSelect");
	}
	
	/**
	 * Funcion que genera los datos actuales de las divisiones registradas en sigai
	 * dependiendo de su gerencia.
	 *
	 * @param string de ruta de conexion a usar $conect
	 * @param numero $id_gerencia
	 * @return array
	 */
	function select_sigai_division($conect,$id_gerencia){
		// Tabla para hacer la consulta
		$nameTabla['GRLDIVISION']="GRLDIVISION";
		// Campos para seleccionar
		$campos['ID_DIVISION']="ID_DIVISION";
		$campos['DE_DIVISION']="DE_DIVISION";
		//Condicion
		$condicion['ID_GERENCIA']=$id_gerencia;
		return $this->query_classdb($conect,$nameTabla,$campos,"classdbsqlserver","fbdSelect",$condicion);
	}
        /*
         * funcion que regresa los datos de un EMPLEADO de sigai
         */
        function sigai_dtrabajador($conect)
	{
		// Tabla para hacer la consulta
		$nameTabla['VISTA_VTV']="VISTA_VTV";
		// Campos para seleccionar
		$campos['Cedula']="Cedula";//1
		$campos['Nombre']="Nombre";//2
		$campos['Tipo_Trabajador']="Tipo_Trabajador";//3
		$campos['Cargo']="Cargo";//4
		$campos['Gerencia']="Gerencia";//5
		$campos['Codigo_Empleado']="Codigo_Empleado";//6
		$campos['Ingreso']="Ingreso";//7


		$condicion['Cedula']=getpost("cedula");


		$this->groupby['Cedula']="Cedula";
		$this->groupby['Nombre']="Nombre";
		$this->groupby['Tipo_Trabajador']="Tipo_Trabajador";
		$this->groupby['Cargo']="Cargo";
		$this->groupby['Gerencia']="Gerencia";
		$this->groupby['Codigo_Empleado']="Codigo_Empleado";
		$this->groupby['Ingreso']="Ingreso";

                if(getpost("tojson")){
                    $this->dbcore_toutf8($this->query_classdb($conect,$nameTabla,$campos,"classdbsqlserver","fbdSelect",$condicion));
                }else{
                    return $this->query_classdb($conect,$nameTabla,$campos,"classdbsqlserver","fbdSelect",$condicion);
                }
	}


        /**
         * FUNCION eSTANDAR PARA LOS LISTADOS DE USUARIOS DE SISTEMAS
         */

        function select_coreusers_sys($conect){
            $nameTabla['usuario.main_usuariossys']="usuario.main_usuariossys";
            $campos['id']="id";
            $campos['nombre_usuario']="nombre_usuario";
            $campos['apellido_usuario']="apellido_usuario";
            $campos['perfil']="perfil";


            $condicion['id_aplicacion']=getpost('idprograma');
            $this->orderby[getpost('sortname')]=getpost('sortname')." ".getpost('sortorder');

            $this->limite[INICIO]=((getpost("page")-1) * getpost("rp"));
            $this->limite[FIN]=getpost("rp");

            return $this->query_classdb($conect,$nameTabla,$campos,"classdb","fbdSelect",$condicion);

        }

        function nreg_select_coreusers_sys($conect){
            session_start();
            //$this->limite=array();
            $nameTabla['usuario.main_usuariossys']="usuario.main_usuariossys";
            $campos['id']="count(id) as numreg";
            //$condicion['id_aplicacion']=$_SESSION['idprograma'];
            $condicion['id_aplicacion']=getpost('idprograma');

            $data=$this->query_classdb($conect,$nameTabla,$campos,"classdb","fbdSelect",$condicion);
            return $data[1][1];
        }


        /**
         * FUNCION QUE REGRESA LOS MODULOS ACTIVOS DE LOS SISTEMAS
         */

        function select_coremod_sys($conect){


            $nameTabla['usuario.t_modulo']="usuario.t_modulo";
            $campos['id']="id_modulo as id";
            $campos['nombre']="nombre";
            $condicion['id_aplicacion']=getpost('idprograma');
            $condicion['fecha_exp']="'2222-12-31'";
            
            if(getpost('lst')==false){
                $this->orderby[getpost('sortname')]=getpost('sortname')." ".getpost('sortorder');
                $this->limite[INICIO]=((getpost("page")-1) * getpost("rp"));
                $this->limite[FIN]=getpost("rp");
            }
            return $this->query_classdb($conect,$nameTabla,$campos,"classdb","fbdSelect",$condicion);
        }

        function nreg_select_coremod_sys($conect){
            session_start();
            //$this->limite=array();
            $nameTabla['usuario.t_modulo']="usuario.t_modulo";
            $campos['id']="count(id_modulo) as numreg";
            $condicion['fecha_exp']="'2222-12-31'";
            //$condicion['id_aplicacion']=$_SESSION['idprograma'];
            $condicion['id_aplicacion']=getpost('idprograma');

            $data=$this->query_classdb($conect,$nameTabla,$campos,"classdb","fbdSelect",$condicion);
            return $data[1][1];
        }

        /**
         *
         * @param <type> $conect
         * @return <type> pensando en los submodulo
         */

        function select_coresubmod_sys($conect){
            $nameTabla['usuario.t_submodulo']="usuario.main_modulosys";
            $campos['id']="id_submodulo as id";
            $campos['modulo']="modulo";
            $campos['sub_modulo']="sub_modulo";
            $condicion['id_aplicacion']=getpost('idprograma');
            if(getpost('lst')==false){
                $this->orderby[getpost('sortname')]=getpost('sortname')." ".getpost('sortorder');
                $this->limite[INICIO]=((getpost("page")-1) * getpost("rp"));
                $this->limite[FIN]=getpost("rp");
            }
            return $this->query_classdb($conect,$nameTabla,$campos,"classdb","fbdSelect",$condicion);
        }

        function nreg_select_coresubmod_sys($conect){
            session_start();
            //$this->limite=array();
            $nameTabla['usuario.t_submodulo']="usuario.main_modulosys";
            $campos['id']="count(id_submodulo) as numreg";
            $condicion['id_aplicacion']=getpost('idprograma');
            $data=$this->query_classdb($conect,$nameTabla,$campos,"classdb","fbdSelect",$condicion);
            return $data[1][1];
        }


        /**
         * desactivacion de modulos
         */
        function del_admmod($conect){
            session_start();
            //el string de los dias por veces....
            $nameTabla="usuario.t_modulo";
            $campos['fecha_exp']=date("Y-m-d");
            $condicion['id_aplicacion']=$_SESSION['idprograma'];
            $condicion['id_modulo']=getpost('id');

            ob_start();
            if($data=$this->query_classdb($conect,$nameTabla,$campos,"classdb","fdbdUpdate",$condicion)){
                print "ok";
            }
            $data=ob_get_contents();
            ob_end_clean();
            if($data=="" || $data=="ok"){
                $data="Culminado con exito"; //alerta.
            }
            return $data;
        }


        


        /**
         * Busca un usuario, si esta activo me da el valor de 1 o mayores a 1
         * si no esta activo da un valor a cero.
         */
        function serch_usrcoreprogram($conect){
            $nameTabla['usuario.main_usuariossys']="usuario.main_usuariossys";
            $campos['id']="count(id) as numreg";
            $condicion['id_aplicacion']=getpost('idprograma');
            $condicion['id']="'".getpost('ci')."'";
            return json_encode($this->query_classdb($conect,$nameTabla,$campos,"classdb","fbdSelect",$condicion));
        }




        /**
         * funccion de class core para eliminar usuarios de un sistema
         *
         * A DELUSER TENGO QUE PASARLE LOS PARAMETROS PARA QUE BORRE PERFECTAMENTE
         */
        function del_admuser($conect){
            session_start();
            //el string de los dias por veces....
            $nameTabla="usuario.t_acceso";
            $campos['fecha_exp']=date("Y-m-d");

            $condicion["cedula"]=getpost('id');
            $condicion['id_aplicacion']=$_SESSION['idprograma'];
            $condicion['id_modulo']=$_SESSION['idmodulo'];

            ob_start();
            if($data=$this->query_classdb($conect,$nameTabla,$campos,"classdb","fdbdUpdate",$condicion)){
                print "ok";
            }
            $data=ob_get_contents();
            ob_end_clean();
            if($data=="" || $data=="ok"){
                $data="Culminado con exito"; //alerta.
            }
            return $data;
        }


        function add_admuser($conect){
            session_start();
            //seleccionar el usuario en la tabla de usuarios
            //si no existe
            ////agregar sus caracteristicas y regresar su id
            ////insertar en la tabla de t_acceso
            //si existe
            ////obtener el id usuario
            ////insertar en la tabla de tacceso.

            $nameTabla['usuario.t_datos_personales']="usuario.t_datos_personales";
            $campos['id_datos_personales']="id_datos_personales";
            $condicion['cedula']=getpost('ci');

            $data=$this->query_classdb($conect,$nameTabla,$campos,"classdb","fbdSelect",$condicion);
            if(count($data)>0){
                $id=$data[1][1];
                unset($data);
                $data=array('id'=>$id);
            }else{
                unset($data);
                $data=array('id'=>"0");
            }

            //unsetear los campos

            unset ($nameTabla);
            unset ($campos);
            unset ($condicion);

            //existe como un usuario del sistema de el sistema de los usuarios.
            if($data['id']=="0"){
               //para futuras implementaciones estas sion las lineas a modificar para agregar usuarios.
               $nameTabla="usuario.t_datos_personales";

               list($apellidos,$nombres)=explode(",", getpost('nyp'));

               $campos['nombre_usuario']=trim($apellidos);
               $campos['apellido_usuario']=trim($nombres);
               $campos['cedula']=getpost('ci'); 

               $this->camporeturn="id_datos_personales";
               $presonal_id=json_encode($this->query_classdb($conect,$nameTabla,$campos,"classdb","fdbInsertRn"));
            }

            unset ($nameTabla);
            unset ($campos);
            unset ($condicion);


                //agregar nuevo acceso...
                //el string de los dias por veces....
               $nameTabla="usuario.t_acceso";
               $campos['cedula']=getpost('ci');
               $campos['clave']="123456";
               
               //hay dos formas de recibir este parametro
               //forma 1 mediante ddmodule (modulo_dinamico) getpost
               //la otra forma mediante el modulo del logueado.
               
               if(getpost("idmodulo")!=""){
                   $campos['id_modulo']=getpost("idmodulo");
               }else{
                   $campos['id_modulo']=$_SESSION['idmodulo']; //modulo de la persona que esta logueada.
               }

               if(getpost("idsubmodulo")!=""){
                   $campos['id_submodulo']=getpost("idsubmodulo"); // el submodulo
               }
               
               $campos['id_aplicacion']=$_SESSION['idprograma']; //el id del programa
               $campos['niv_con']=getpost("idperfil");

                //para futuras implementaciones
               if(getpost('niv_eli')==""){
                   $niv_eli="1";
               }else{
                   $niv_eli=getpost('niv_eli');
               }
               if(getpost("niv_inc")==""){
                   $niv_inc="1";
               }else{
                   $niv_inc=getpost('niv_inc');
               }
               if(getpost("niv_mod")==""){
                   $niv_mod="1";
               }else{
                   $niv_mod=getpost("niv_mod");
               }

               
               $campos['niv_eli']=$niv_eli;
               $campos['niv_inc']=$niv_inc;
               $campos['niv_mod']=$niv_mod;
               $campos['ced_trans']=$_SESSION["ci_sys"]; //cedula del administrador
               $campos['fecha_trans']=date("Y-m-d");
               $campos['fecha_exp']="2222-12-31";
               $campos['id_tipo_usuario']=getpost("idperfil");
               $this->camporeturn="id_acceso";
            ob_start();
            if($data=$this->query_classdb($conect,$nameTabla,$campos,"classdb","fdbInsertRn")){
                print "ok";
            }
            $data=ob_get_contents();
            ob_end_clean();

            if($data=="" || $data=="ok"){
                $data="se ha creado un nuevo usuario con la contraseña 123456"; //alerta.
            }

            return $data;
        }

        function coreaplic_perfiles($conect){
            //$this->limite=array();
            $nameTabla['usuario.t_tipo_usuario']="usuario.t_tipo_usuario";
            $campos['id']="id_tipo_usuario";
            $campos['desc_usuario']="desc_usuario";
            $condicion['id_aplicacion']=getpost('idprograma');
            return $this->query_classdb($conect,$nameTabla,$campos,"classdb","fbdSelect",$condicion);
        }

        //funcion para actualizar la contraseña de los usuarios una vez registrados.
        function core_update_pwd($conect){
            session_start();
            $nameTabla="usuario.t_acceso";
            $campos['clave']=getpost('pwdchg');
            $condicion['cedula']=$_SESSION['ci_sys'];
            $condicion['id_modulo']=$_SESSION['idmodulo']; //el modulo del administrador
            $condicion['id_aplicacion']=$_SESSION['idprograma']; //el modulo del programa
            $condicion['fecha_exp']="2222-12-31";

            ob_start();
            if($data=$this->query_classdb($conect,$nameTabla,$campos,"classdb","fdbdUpdate",$condicion)){
                print "ok";
            }
            $data=ob_get_contents();
            ob_end_clean();
            if($data=="" || $data=="ok"){
                $data="se cambio su contraseña a ".getpost('pwdchg').", por seguridad debe volver a loguearse con su nueva contraseña"; //alerta.
            }
            return $data;
        }
}


//instalando los drivers de proyecto apolo para classcore.
//require(rptpath."../sistemas/apolo/class_db_aplic_apolo.php");

//instalando drivers de proyecto pautascomerciales para classcore.
require(rptpath."../sistemas/pautascomerciales/class_db_aplic_pautascomerciales.php");

//instalando drivers de proyecto PIZARRA F5 para classcore.
//require(rptpath."../sistemas/pizarraf5/class_db_aplic_pizarraf5.php");

?>
