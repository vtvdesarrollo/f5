<?php
/*
    Caracas; 25/05/2009
    Clase Libreria Cabecera y Pie de Pagina (classlibCabPie)
    Intranet VTV Version 1.0

*/
	class classlibCabPie
	{
		// Variables globales de la clase
		var $titulo="", $nombrePagina;
		/*
			Constructor de la Clase classlibCabPie()
			Tiene como parmetro:
			$tituloPa: Es el titulo enviado por parmetro
			$nombrePaginaPa: Es el nombre de la Pagina que se esta trabajando
		*/
		function classlibCabPie($tituloPa, $nombrePaginaPa)
		{
			$this->titulo=$tituloPa;
			$this->nombrePagina=$nombrePaginaPa;
			require_once("classlibFecHor.php");
			$this->ObjFecHor=new classlibFecHor();
		}
		/*
			Funcin de Librera HTML (flibHtm())
			Contiene todo lo referente al cdigo HTML usado al inicio. Parametros enviados:
			$activarRefresh: Es para indicar si la pagina se necesita autorefrescarse.
			$ficheroJavaScript: Se debe agregar la ruta y el nombre del archivo .js que se va hacer referencia.
			$validaJavaScript: Son las validacion correspondientes a cada formulario independiente de la aplicacin.
			$css: es el que te indica si es menu normal = 0 o el principal de la intranet = 1
		*/
		function flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript, $css="")
		{
            //echo "esta es mi variable => ".$css." => ".strcmp($css,2)."<br>";
				$htmCab="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN'
    'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' lang='en' xml:lang='en'>
				<head>";
				$htmCab.="<title>".$this->titulo."</title>
				<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />
				<link rel=\"shortcut icon\" href=\"../estilos/imagenes/vtv.ico\">
				<link href=\"../estilos/menu_superior/menu_superior.css\" rel=\"stylesheet\" type=\"text/css\" />
				<link href=\"../estilos/intranet_new.css\" rel=\"stylesheet\" type=\"text/css\" />
				<link href=\"../estilos/tabcontent.css\" rel=\"stylesheet\" type=\"text/css\" />
				<link href=\"../estilos/IntranetAplicaciones.css\" rel=\"stylesheet\" type=\"text/css\" />
				<link href=\"../estilos/s3Slider.css\" rel=\"stylesheet\" type=\"text/css\" />
				<!--<link rel=\"stylesheet\" type=\"text/css\" href=\"../intranet/galeria/css/e2.css\" title=\"default\" />-->
				<script type=\"text/javascript\" src=\"../estilos/menu_superior/menu_superior.js\"></script>
				<script type=\"text/javascript\" src=\"../librerias/ajaxtabs.js\"></script>
				<script src='../librerias/jquery-1.6.2.js' type='text/javascript'></script>
				<script type=\"text/javascript\" src=\"../librerias/ddaccordion.js\"></script>
				<script type=\"text/javascript\" src=\"../librerias/s3Slider.js\"></script>
				<script type=\"text/javascript\" src=\"../librerias/jquery.json.js\"></script>


				<script src=\"../librerias/jquery.ui.draggable.js\" type=\"text/javascript\"></script>
				<script src=\"../librerias/jquery.alerts.js\" type=\"text/javascript\"></script>
				<link href=\"../estilos/jquery.alerts.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\" />
				<script type=\"text/javascript\" src=\"../estilos/thickbox/thickbox.js\"></script>
				<link href=\"../estilos/thickbox/thickbox.css\" rel=\"stylesheet\" type=\"text/css\" />
				<script type=\"text/javascript\" src=\"	../librerias/jquery.tablePagination.0.2.min.js\"></script>
				";
				$htmCab.="\n".$ficheroJavaScript;
				$htmCab.="\n<script type=\"text/javascript\">".$validaJavaScript."</script>";
				$htmCab.="</head>";
			return $htmCab;
		}
		/*
			Funcin de Librera HTML Cabecera (flibHtmCab())
			Contiene todo lo referente al cdigo HTML usado al inicio al invocar la
			Funcin de Librera HTML (flibHtm()), luego se aade una cadena de caracteres que
			tiene la Union de las Imagenes de Cabeceras. Parametros enviados:
			$activarRefresh: Es para indicar si la pagina se necesita autorefrescarse.
			$ficheroJavaScript: Se debe agregar la ruta y el nombre del archivo .js que se va hacer referencia.
			$validaJavaScript: Son las validacion correspondientes a cada formulario independiente de la aplicacin.
			$menu: Es la cadena de caracteres que tiene el men correspondiente de la aplicacin
		*/

				/*

				/// Sin imagen en movimiento
				function flibHtmCabIndex($activarRefresh, $ficheroJavaScript, $validaJavaScript, $menu="", $Css="0",$fecha)
		{
			require_once("../intranet/principal/paginas/classIndexFunciones.php");
			$this->classIndexFunciones=new classIndexFunciones();
		//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCabLis=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body >
				<div class='contenedor_principal'>
					<div class='logo_top'>
					<div class='cont_feha'>".$this->ObjFecHor->flibdiamesano()."<br>Caracas - Venezuela</div>
					".$menu."
				</div>
				<div class=\"marquesina_atn\">&nbsp;El canal de todos los venezolanos</div><div id=\"marquesina\">".$this->classIndexFunciones->marquesina()."</div>
				<div class=\"contenedor\">";
			return $htmCabLis;
		}*/

						function flibHtmCabIndex($activarRefresh, $ficheroJavaScript, $validaJavaScript, $menu="", $Css="0",$fecha)
		{
			require_once("../intranet/principal/paginas/classIndexFunciones.php");
			$this->classIndexFunciones=new classIndexFunciones();
		//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCabLis=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body >
			<script type=\"text/javascript\" src=\"../librerias/wz_tooltip.js\"></script>
				<div class='contenedor_principal'>
				<div class='logo_aniversario'>

			<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0\" width=\"133\" height=\"90\" title=\"vtv\">

  <param name=\"movie\" value=\"../estilos/imagenes/vtv1.swf\" />

  <param name=\"quality\" value=\"high\" />
<param name=\"wmode\" value=\"opaque\">

  <embed wmode=\"opaque\" src=\"../estilos/imagenes/vtv1.swf\" quality=\"high\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" type=\"application/x-shockwave-flash\" width=\"133\" height=\"90\"></embed>

</object>

				</div>
					<div class='logo_top'>
					<div class='cont_feha'>".$this->ObjFecHor->flibdiamesano()."</div>
					".$menu."
				</div>
				<div class=\"marquesina_atn\">&nbsp;Todos somos necesarios. Todos somos VTV.</div><div id=\"marquesina\">".$this->classIndexFunciones->marquesina()."</div>
				<div class=\"contenedor\">";
			return $htmCabLis;
		}

					function flibHtmCabBitacora($activarRefresh, $ficheroJavaScript, $validaJavaScript, $menu="", $Css="0",$fecha)
		{
		//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCabLis=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body >
				<div class='contenedor_principal'>
					<div class='logo_top_bit'>
					<div class='cont_feha'><br><br></div>
					".$menu."
				</div>
				<div class=\"marquesina_atn\">&nbsp;El canal de todos los venezolanos</div>
				<div class=\"contenedor2\">";
			return $htmCabLis;
		}

			function flibHtmCabIndexSWF($activarRefresh, $ficheroJavaScript, $validaJavaScript, $menu="", $Css="0",$fecha)
		{
			require_once("../intranet/principal/paginas/classIndexFunciones.php");
			$this->classIndexFunciones=new classIndexFunciones();
		//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCabLis=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body >
			<script type=\"text/javascript\" src=\"../librerias/wz_tooltip.js\"></script>
				<div class='contenedor_principal'>
				<div class='logo_top_swf'>

					<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0\" width=\"780\" height=\"90\" title=\"vtv\">
  <param name=\"movie\" value=\"../estilos/imagenes/intranet_swf.swf\" />
  <param name=\"quality\" value=\"high\" />
  <embed src=\"../estilos/imagenes/intranet_swf.swf\" quality=\"high\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" type=\"application/x-shockwave-flash\" width=\"780\" height=\"90\"></embed>

</object>

				</div>
				<div class=\"marquesina_atn\">&nbsp;Todos somos necesarios. Todos somos VTV.</div><div id=\"marquesina\">".$this->classIndexFunciones->marquesina()."</div>
				<div class=\"contenedor\">";
			return $htmCabLis;
		}



			function flibHtmCabIntranetvieja($activarRefresh, $ficheroJavaScript, $validaJavaScript, $menu="", $Css="0",$fecha)
		{
		//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCabLis=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body >
				<div class='contenedor_principal'>
				<div class='logo_aniversario'>

				<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0\" width=\"133\" height=\"90\" title=\"vtv\">

  <param name=\"movie\" value=\"../estilos/imagenes/vtv1.swf\" />

  <param name=\"quality\" value=\"high\" />

  <embed src=\"../estilos/imagenes/vtv1.swf\" quality=\"high\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" type=\"application/x-shockwave-flash\" width=\"133\" height=\"90\"></embed>

</object>
				</div>
					<div class='logo_top'>
					<div class='cont_feha'>".$this->ObjFecHor->flibdiamesano()."</div>
					".$menu."
				</div>
				<div class=\"marquesina_atn\">&nbsp;Todos somos necesarios. Todos somos VTV.</div>
				<div class=\"contenedor2\">";
			return $htmCabLis;
		}

		function flibHtmCab2($activarRefresh, $ficheroJavaScript, $validaJavaScript, $menu="", $Css="0",$fecha)
		{
		//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCabLis=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body>
			<div id='div_principal' >
			<div id='div_sombra' align='center'>
				<div id='div_segundario'>
					<div id='logo-minci'></div>
					<div id='logo-vtv-menu'>
						<div id='logo-vtv' align='left'></div>
						<div id='top-menu-intranet' align='center'>
						<ul class='solidblockmenu'>
						<li><a href='classIndex.php'>inicio</a></li>
						<li><a href='classelcanal.php'>el canal</a></li>
						<li><a href='classmultimedia.php'>multimedia</a></li>
						<li><a href='classservicios.php'>servicios</a></li>
						<li><a href='classcontactenos.php'>contactenos</a></li>
						<li><a onclick=\"window.open(this.href, this.target, 'width=500,height=410'); return false;\" target=\"_blank\" href=\"http://www.vtv.gob.ve/envivo/Index.html\">se&ntilde;al en vivo</a></li>
						</ul>
						</div>
						<div id='div-cont-feha' class='fecha'>".$this->ObjFecHor->flibdiamesano()."&nbsp;</div>
						<div id='countdounn' style='width:220px;overflow:hidden;'>

							<!--<div id='encountdown' style='font-size:12px;float:left;height:12px;width:220px;margin-top:3px;' align='left'><b>Trabajamos:<b></b></b></div><b><b>
							<div style='margin-top:2px;overflow:hidden;width:190px;height:28px;float:left;' id='defaultCountdown' class='hasCountdown'><span class='countdown_row countdown_show4'><span class='countdown_section'><span class='countdown_amount'>137</span><br>Dias</span><span class='countdown_section'><span class='countdown_amount'>6</span><br>Horas</span><span class='countdown_section'><span class='countdown_amount'>2</span><br>Minutos</span><span class='countdown_section'><span class='countdown_amount'>8</span><br>Segundos</span></span></div>
							<div id='textcountdown' style='font-size:9.4px;float:left;height:12px;width:220px;' align='left'><b>Ya tenemos nuevos ascensores<b></b></b></div><b><b>
						</b></b></b></b>-->
					</div>
					</div>";
			return $htmCabLis;
		}


		function flibHtmCab($activarRefresh, $ficheroJavaScript, $validaJavaScript, $menu="", $Css="0",$fecha)
		{
		//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCab=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body>
			<div id='div_principal'>
			<div id='div_sombra' align='center'>
				<div id='div_segundario'>
					<div id='logo-minci'></div>
					<div id='logo-vtv-menu'>
						<div id='logo-vtv' align='left'></div>
						<div id='top-menu-intranet' align='center'>
						</div>
						<div id='div-cont-feha' class='fecha'>".$this->ObjFecHor->flibdiamesano()."&nbsp;</div>
					</div>
					<div id='menu-sistemas'>".$menu."</div>
					<div id='contenedor-sistemas' align='center'>
					<div id='contenido-system' align='center'>";
			return $htmCab;
		}

		function flibCerrarHtmIndex(){
		$CerrarHtm="
				<div id='div-pie-pagina-sistemas'  align='center'>
					Desarrollado: Gerencia de Tecnolog&iacute;a de la Informaci&oacute;n y Comunicaci&oacute;n<br>
					Navegador Recomendado Mozilla Firefox
				</div>
				</div>
				</div>
				<div id='sombra-bottom' align='center'></div>
			</div>
		</body>
		</html>
		";
		return $CerrarHtm;
		}

		function flibCerrarHtm(){
		$CerrarHtm="
				</div></div>
				<div id='div-pie-pagina-sistemas' align='center'>
				Desarrollado: Gerencia de Tecnolog&iacute;a de la Informaci&oacute;n y Comunicaci&oacute;n<br>
				Navegador Recomendado Mozilla Firefox
				</div>
				</div>
				</div>
				<div id='sombra-bottom' align='center'></div>
				</div>
			</div>
		</body>
		</html>
		";
		return $CerrarHtm;
		}

		function flibCerrarHtmPaginas(){
		$CerrarHtm="

				<div id='div-pie-pagina-sistemas' align='center'>
				Desarrollado: Gerencia de Tecnolog&iacute;a de la Informaci&oacute;n y Comunicaci&oacute;n<br>
				Navegador Recomendado Mozilla Firefox
				</div>
				</div>
				</div>
				<div id='sombra-bottom' align='center'></div>
			</div>
		</body>
		</html>
		";
		return $CerrarHtm;
		}


		/*function flibHtmCab($activarRefresh, $ficheroJavaScript, $validaJavaScript, $menu="", $Css="0",$fecha)
		{
		//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCabLis=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body >
				<div class='contenedor_principal'>
					<div class='logo_top'>
					<div class='cont_feha'>".$this->ObjFecHor->flibdiamesano()."<br>Caracas - Venezuela</div>
					".$menu."
				</div>
				<div class=\"marquesina_atn\">&nbsp;El canal de todos los venezolanos</div>
				<div class=\"contenedor2\">";
			return $htmCabLis;
		}
		*/

		function flibHtmCabCarnet($activarRefresh, $ficheroJavaScript, $validaJavaScript, $menu="", $Css="0",$fecha)
		{
		//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCab=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body>
			<div id='div_principal'>
			<div id='div_sombra' align='center'>
				<div id='div_segundario'>
					<div id='logo-minci'></div>
					<div id='logo-vtv-menu'>
						<div id='logo-vtv' align='left'></div>
						<div id='top-menu-intranet' align='center'>
						</div>
						<div id='div-cont-feha' class='fecha'>".$this->ObjFecHor->flibdiamesano()."&nbsp;</div>
					</div>
					<div id='menu-sistemas'>".$menu."</div>
					<div id='contenedor-sistemas' align='center'>
					<div id='contenido-system' align='center'>";
			return $htmCab;
		}

		function flibHtmCabCarnet__($activarRefresh, $ficheroJavaScript, $validaJavaScript, $menu="", $Css="0",$fecha)
		{
		//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCabLis=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body >
				<div class='contenedor_principal'>
					<div class='logo_top_carnet'>
					<div class='cont_feha'><br><br></div>
					".$menu."
				</div>
				<div class=\"marquesina_atn\">&nbsp;Todos somos necesarios. Todos somos VTV.</div>
				<div class=\"contenedor2\">";
			return $htmCabLis;
		}

			function flibHtmCabRSS($activarRefresh, $ficheroJavaScript, $validaJavaScript, $menu="", $Css="0",$fecha)
		{
		//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCabLis=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body >
				<div class='contenedor_principal'>
					<div class='logo_top_rss'>
					<div class='cont_feha' ><br><br></div>
					".$menu."
				</div>
				<div class=\"marquesina_atn\">&nbsp;Todos somos necesarios. Todos somos VTV.</div>
				<div class=\"contenedor2\">";
			return $htmCabLis;
		}


		function flibHtmCabSIGAI($activarRefresh, $ficheroJavaScript, $validaJavaScript, $menu="", $Css="0",$fecha)
		{
		//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCabLis=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body >
				<div class='contenedor_principal'>
					<div class='logo_top_sigai'>
					<div class='cont_feha'><br><br></div>
					".$menu."
				</div>
				<div class=\"marquesina_atn\">&nbsp;Todos somos necesarios. Todos somos VTV.</div>
				<div class=\"contenedor2\">";
			return $htmCabLis;
		}


		function flibHtmCabAgenda($activarRefresh, $ficheroJavaScript, $validaJavaScript, $menu="", $Css="0",$fecha)
		{
		//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCabLis=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body >
				<div class='contenedor_principal'>
					<div class='logo_top_agenda'>
					<div class='cont_feha'><br><br></div>
					".$menu."
				</div>
				<div class=\"marquesina_atn\">&nbsp;Todos somos necesarios. Todos somos VTV.</div>
				<div class=\"contenedor2\">";
			return $htmCabLis;
		}


		function flibHtmCabVideo($activarRefresh, $ficheroJavaScript, $validaJavaScript, $menu="", $Css="0")
		{
			//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCabLis=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body onLoad=\"createplayer('video5.flv', false)\">
			<div class=\"contenedor_sombra\">
	<div class=\"contenedor\">
		<div class=\"logo-top\"></div>
		<div class=\"logo\">


		<script type=\"text/javascript\">

var trans_width='790px' //slideshow width
var trans_height='96px' //slideshow height
var pause=5000 //SET PAUSE BETWEEN SLIDE (3000=3 seconds)
var degree=20 //animation speed. Greater is faster.

var slideshowcontent=new Array()
//Define slideshow contents: [image URL, OPTIONAL LINK, OPTIONAL LINK TARGET]
slideshowcontent[0]=[\"../Estilos/imagenes/BannerPC.jpg\", \"\", \"\"]
slideshowcontent[1]=[\"../Estilos/imagenes/BannerPC2.jpg\", \"\", \"\"]




////NO need to edit beyond here/////////////

var bgcolor='white'

var imageholder=new Array()
for (i=0;i<slideshowcontent.length;i++){
imageholder[i]=new Image()
imageholder[i].src=slideshowcontent[i][0]
}

var ie4=document.all
var dom=document.getElementById&&navigator.userAgent.indexOf(\"Opera\")==-1

if (ie4||dom)
document.write('<div style=\"position:relative;width:'+trans_width+';height:'+trans_height+';overflow:hidden\"><div id=\"canvas0\" style=\"position:absolute;background-color:'+bgcolor+';width:'+trans_width+';height:'+trans_height+';left:-'+trans_width+';filter:alpha(opacity=20);-moz-opacity:0.2;\"></div><div id=\"canvas1\" style=\"position:absolute;background-color:'+bgcolor+';width:'+trans_width+';height:'+trans_height+';left:-'+trans_width+';filter:alpha(opacity=20);-moz-opacity:0.2;\"></div></div>')
else if (document.layers){
document.write('<ilayer id=tickernsmain visibility=hide width='+trans_width+' height='+trans_height+' bgColor='+bgcolor+'><layer id=tickernssub width='+trans_width+' height='+trans_height+' left=0 top=0>'+'<img src=\"'+slideshowcontent[0][0]+'\"></layer></ilayer>')
}

var curpos=trans_width*(-1)
var curcanvas=\"canvas0\"
var curindex=0
var nextindex=1

function getslidehtml(theslide){
var slidehtml=\"\"
if (theslide[1]!=\"\")
slidehtml='<a href=\"'+theslide[1]+'\" target=\"'+theslide[2]+'\">'
slidehtml+='<img src=\"'+theslide[0]+'\" border=\"0\">'
if (theslide[1]!=\"\")
slidehtml+='</a>'
return slidehtml
}

function moveslide(){
if (curpos<0){
curpos=Math.min(curpos+degree,0)
tempobj.style.left=curpos+\"px\"
}
else{
clearInterval(dropslide)
if (crossobj.filters)
crossobj.filters.alpha.opacity=100
else if (crossobj.style.MozOpacity)
crossobj.style.MozOpacity=1
nextcanvas=(curcanvas==\"canvas0\")? \"canvas0\" : \"canvas1\"
tempobj=ie4? eval(\"document.all.\"+nextcanvas) : document.getElementById(nextcanvas)
tempobj.innerHTML=getslidehtml(slideshowcontent[curindex])
nextindex=(nextindex<slideshowcontent.length-1)? nextindex+1 : 0
setTimeout(\"rotateslide()\",pause)
}
}

function rotateslide(){
if (ie4||dom){
resetit(curcanvas)
crossobj=tempobj=ie4? eval(\"document.all.\"+curcanvas) : document.getElementById(curcanvas)
crossobj.style.zIndex++
if (crossobj.filters)
document.all.canvas0.filters.alpha.opacity=document.all.canvas1.filters.alpha.opacity=20
else if (crossobj.style.MozOpacity)
document.getElementById(\"canvas0\").style.MozOpacity=document.getElementById(\"canvas1\").style.MozOpacity=0.2
var temp='setInterval(\"moveslide()\",50)'
dropslide=eval(temp)
curcanvas=(curcanvas==\"canvas0\")? \"canvas1\" : \"canvas0\"
}
else if (document.layers){
crossobj.document.write(getslidehtml(slideshowcontent[curindex]))
crossobj.document.close()
}
curindex=(curindex<slideshowcontent.length-1)? curindex+1 : 0
}

function jumptoslide(which){
curindex=which
rotateslide()
}

function resetit(what){
curpos=parseInt(trans_width)*(-1)
var crossobj=ie4? eval(\"document.all.\"+what) : document.getElementById(what)
crossobj.style.left=curpos+\"px\"
}

function startit(){
crossobj=ie4? eval(\"document.all.\"+curcanvas) : dom? document.getElementById(curcanvas) : document.tickernsmain.document.tickernssub
if (ie4||dom){
crossobj.innerHTML=getslidehtml(slideshowcontent[curindex])
rotateslide()
}
else{
document.tickernsmain.visibility='show'
curindex++
setInterval(\"rotateslide()\",pause)
}
}

if (window.addEventListener)
window.addEventListener(\"load\", startit, false)
else if (window.attachEvent)

window.attachEvent(\"onload\", startit)
else if (ie4||dom||document.layers)
window.onload=startit

</script>




		</div>
	  <div id=\"contenido\">
	  <div class='menu'>".$menu."</div>
						";
			return $htmCabLis;
		}


		function flibHtmCabSinTop()
		{
			//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCabLis=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body>";
			return $htmCabLis;
		}


		///////////////////////////////////////////////////////////////////////////////////////////////////

		/*
			Funcin de Librera Cerrar HTML (flibCerrarHtm())
			Contiene todo lo referente al cierre del cdigo HTML usado al inicio,
			se puede encontrar en la Funcin de Librera HTML (flibHtm()), este a
			su vez contiene la imagen que esta al pie de cada presentacion de la
			Intranet VTV.
		*/

		function flibCerrarHtm_intranetvieja()
		{
			$CerrarHtm="
			</div>
			<div class=\"cont_bottom\"></div>
			</div>
			<div class=\"footer\">Desarrollado: Gerencia de Tecnología de la Información y Comunicación<br>
			Contenido e Ilustración: Gerencia de Relaciones P&uacute;blicas<br>Navegador Recomendado Mozilla Firefox Resoluci&oacute;n 1024x768</div>
			</body>
			</html>
			";
			return $CerrarHtm;
		}

		function flibCerrarHtmSinTop()
		{
			$CerrarHtm="
			</body>
			</html>";
			return $CerrarHtm;
		}

		function flibHtmPoup($activarRefresh, $validaJavaScript)
		{
		    $htmCab="";
			$htmCab="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
				<head>";
				if($activarRefresh)
				{
					$htmCab.="<meta http-equiv=\"refresh\" content=\"60;".$this->$nombrePagina."\">";
				}
				$htmCab.="<title>".$this->titulo."</title>
				<link href=\"../Estilos/pop-up.css\" rel=\"stylesheet\" type=\"text/css\">
				<link href=\"../Estilos/estilosIntranetAplicaciones.css\" rel=\"stylesheet\" type=\"text/css\">
				<script language=\"javascript\">".$validaJavaScript."</script>
				";
				if($validaJavaScript==14868322){
				$htmCab.="
				<script type='text/javascript' src='../calendario/calendar.js'></script>
				<script type='text/javascript' src='../calendario/lang/calendar-es.js'></script>
				<script type='text/javascript' src='../calendario/calendar-setup.js'></script>
				<link href='../css/calendario.css' rel='stylesheet' type='text/css' media='all'  />
				";
				}
				$htmCab.="</head><body>";
			return $htmCab;
		}
		/**/
		function flibCerrarHtmPoup()
		{
			$CerrarHtm="
			</body>
			</html>";
			return $CerrarHtm;
		}


		function flibHtmCab_noestandar($menu,$fecha,$class_logo_name_top="menu_sistemas",$classLogo_logo="logo-vtv-menu")
		{
		//version modificada en la cabecera de la libreria de classlibCabPie
		//no se necesita la etiqueta de apertura body ya que simplemente se encarga de imprimir unicamente el diseño
		//del encabezado solo necesario para los sistemas no estandarizados a la libreria classlibCabPie.
			$htmCabLis="
				<div id='logo-minci'></div>
				<div id='$classLogo_logo'>
					 <div id='logo-vtv' align='left'></div>
					 <div id='top-menu-intranet' align='center'></div>
					 <div id='div-cont-feha' class='fecha'>".$fecha."</div>
				</div>
				<div class='$class_logo_name_top'>
					".$menu."
				</div>";
			return $htmCabLis;
		}
}
?>
