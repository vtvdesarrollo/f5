<?php
/*
    Caracas; 25/05/2009
    Clase Libreria Cabecera y Pie de Pagina (classlibCabPie)
    Intranet VTV Version 1.0

*/
	class classlibCabPie
	{
		// Variables globales de la clase
		var $titulo="", $nombrePagina;
		/*
			Constructor de la Clase classlibCabPie()
			Tiene como parmetro:
			$tituloPa: Es el titulo enviado por parmetro
			$nombrePaginaPa: Es el nombre de la Pagina que se esta trabajando
		*/
		function classlibCabPie($tituloPa, $nombrePaginaPa)
		{
			$this->titulo=$tituloPa;
			$this->nombrePagina=$nombrePaginaPa;
			require_once("classlibFecHor.php");
			$this->ObjFecHor=new classlibFecHor();
		}
		/*
			Funcin de Librera HTML (flibHtm())
			Contiene todo lo referente al cdigo HTML usado al inicio. Parametros enviados: 
			$activarRefresh: Es para indicar si la pagina se necesita autorefrescarse.
			$ficheroJavaScript: Se debe agregar la ruta y el nombre del archivo .js que se va hacer referencia.
			$validaJavaScript: Son las validacion correspondientes a cada formulario independiente de la aplicacin.
			$css: es el que te indica si es menu normal = 0 o el principal de la intranet = 1
		*/
		function flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript, $css="")
		{
            //echo "esta es mi variable => ".$css." => ".strcmp($css,2)."<br>";
				$htmCab="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN'
    'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' lang='en' xml:lang='en'>
				<head>";
						
				$htmCab.="<title>".$this->titulo."</title>
				<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />
				<link rel=\"shortcut icon\" href=\"../../../estilos/imagenes/vtv.ico\">
				<link href=\"../../../estilos/menu_superior/menu_superior.css\" rel=\"stylesheet\" type=\"text/css\" />
				<link href=\"../../../estilos/intranet.css\" rel=\"stylesheet\" type=\"text/css\" />
				<link href=\"../../../estilos/tabcontent.css\" rel=\"stylesheet\" type=\"text/css\" />
				<link href=\"../../../estilos/IntranetAplicaciones.css\" rel=\"stylesheet\" type=\"text/css\" />
				<link href=\"../../../estilos/s3Slider.css\" rel=\"stylesheet\" type=\"text/css\" />
				<link rel=\"stylesheet\" type=\"text/css\" href=\"../../../intranet/galeria/css/e2.css\" title=\"default\" />
				<link rel=\"alternate stylesheet\" type=\"text/css\" href=\"../../../intranet/galeria/css/e2photo.css\" title=\"none\">
				
				<script type=\"text/javascript\" src=\"../../../estilos/menu_superior/menu_superior.js\"></script>
				<script type=\"text/javascript\" src=\"../../../librerias/ajaxtabs.js\"></script>
				<script type=\"text/javascript\" src=\"../../../librerias/jquery.js\"></script>
				<script type=\"text/javascript\" src=\"../../../librerias/ddaccordion.js\"></script>
				<script type=\"text/javascript\" src=\"../../../librerias/s3Slider.js\"></script>
				<script type=\"text/javascript\" src=\"../../../librerias/jquery.json.js\"></script>
				";
				$htmCab.="\n".$ficheroJavaScript;
				$htmCab.="\n<script type=\"text/javascript\">".$validaJavaScript."</script>";
				$htmCab.="</head>";
			return $htmCab;
		}
		/*
			Funcin de Librera HTML Cabecera (flibHtmCab())
			Contiene todo lo referente al cdigo HTML usado al inicio al invocar la 
			Funcin de Librera HTML (flibHtm()), luego se aade una cadena de caracteres que
			tiene la Union de las Imagenes de Cabeceras. Parametros enviados: 
			$activarRefresh: Es para indicar si la pagina se necesita autorefrescarse.
			$ficheroJavaScript: Se debe agregar la ruta y el nombre del archivo .js que se va hacer referencia.
			$validaJavaScript: Son las validacion correspondientes a cada formulario independiente de la aplicacin.
			$menu: Es la cadena de caracteres que tiene el men correspondiente de la aplicacin
		*/
		function flibHtmCab($activarRefresh, $ficheroJavaScript, $validaJavaScript, $menu="", $Css="0",$fecha)
		{
		//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCabLis=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body >
				<div class='contenedor_principal'>
					<div class='logo_top'>
					<div class='cont_feha'>".$this->ObjFecHor->flibdiamesano()."<br>Caracas - Venezuela</div>
					".$menu."
				</div>
				<div class=\"marquesina\">&nbsp;El canal de todos los venezolanos</div>
				<div class=\"contenedor\">";
			return $htmCabLis;	
		}
		
		function flibHtmCabVideo($activarRefresh, $ficheroJavaScript, $validaJavaScript, $menu="", $Css="0")
		{
			//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCabLis=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body onLoad=\"createplayer('video5.flv', false)\">
			<div class=\"contenedor_sombra\">
	<div class=\"contenedor\">
		<div class=\"logo-top\"></div>
		<div class=\"logo\">
		
		
		<script type=\"text/javascript\">

var trans_width='790px' //slideshow width
var trans_height='96px' //slideshow height
var pause=5000 //SET PAUSE BETWEEN SLIDE (3000=3 seconds)
var degree=20 //animation speed. Greater is faster.

var slideshowcontent=new Array()
//Define slideshow contents: [image URL, OPTIONAL LINK, OPTIONAL LINK TARGET]
slideshowcontent[0]=[\"../../../Estilos/imagenes/BannerPC.jpg\", \"\", \"\"]
slideshowcontent[1]=[\"../../../Estilos/imagenes/BannerPC2.jpg\", \"\", \"\"]




////NO need to edit beyond here/////////////

var bgcolor='white'

var imageholder=new Array()
for (i=0;i<slideshowcontent.length;i++){
imageholder[i]=new Image()
imageholder[i].src=slideshowcontent[i][0]
}

var ie4=document.all
var dom=document.getElementById&&navigator.userAgent.indexOf(\"Opera\")==-1

if (ie4||dom)
document.write('<div style=\"position:relative;width:'+trans_width+';height:'+trans_height+';overflow:hidden\"><div id=\"canvas0\" style=\"position:absolute;background-color:'+bgcolor+';width:'+trans_width+';height:'+trans_height+';left:-'+trans_width+';filter:alpha(opacity=20);-moz-opacity:0.2;\"></div><div id=\"canvas1\" style=\"position:absolute;background-color:'+bgcolor+';width:'+trans_width+';height:'+trans_height+';left:-'+trans_width+';filter:alpha(opacity=20);-moz-opacity:0.2;\"></div></div>')
else if (document.layers){
document.write('<ilayer id=tickernsmain visibility=hide width='+trans_width+' height='+trans_height+' bgColor='+bgcolor+'><layer id=tickernssub width='+trans_width+' height='+trans_height+' left=0 top=0>'+'<img src=\"'+slideshowcontent[0][0]+'\"></layer></ilayer>')
}

var curpos=trans_width*(-1)
var curcanvas=\"canvas0\"
var curindex=0
var nextindex=1

function getslidehtml(theslide){
var slidehtml=\"\"
if (theslide[1]!=\"\")
slidehtml='<a href=\"'+theslide[1]+'\" target=\"'+theslide[2]+'\">'
slidehtml+='<img src=\"'+theslide[0]+'\" border=\"0\">'
if (theslide[1]!=\"\")
slidehtml+='</a>'
return slidehtml
}

function moveslide(){
if (curpos<0){
curpos=Math.min(curpos+degree,0)
tempobj.style.left=curpos+\"px\"
}
else{
clearInterval(dropslide)
if (crossobj.filters)
crossobj.filters.alpha.opacity=100
else if (crossobj.style.MozOpacity)
crossobj.style.MozOpacity=1
nextcanvas=(curcanvas==\"canvas0\")? \"canvas0\" : \"canvas1\"
tempobj=ie4? eval(\"document.all.\"+nextcanvas) : document.getElementById(nextcanvas)
tempobj.innerHTML=getslidehtml(slideshowcontent[curindex])
nextindex=(nextindex<slideshowcontent.length-1)? nextindex+1 : 0
setTimeout(\"rotateslide()\",pause)
}
}

function rotateslide(){
if (ie4||dom){
resetit(curcanvas)
crossobj=tempobj=ie4? eval(\"document.all.\"+curcanvas) : document.getElementById(curcanvas)
crossobj.style.zIndex++
if (crossobj.filters)
document.all.canvas0.filters.alpha.opacity=document.all.canvas1.filters.alpha.opacity=20
else if (crossobj.style.MozOpacity)
document.getElementById(\"canvas0\").style.MozOpacity=document.getElementById(\"canvas1\").style.MozOpacity=0.2
var temp='setInterval(\"moveslide()\",50)'
dropslide=eval(temp)
curcanvas=(curcanvas==\"canvas0\")? \"canvas1\" : \"canvas0\"
}
else if (document.layers){
crossobj.document.write(getslidehtml(slideshowcontent[curindex]))
crossobj.document.close()
}
curindex=(curindex<slideshowcontent.length-1)? curindex+1 : 0
}

function jumptoslide(which){
curindex=which
rotateslide()
}

function resetit(what){
curpos=parseInt(trans_width)*(-1)
var crossobj=ie4? eval(\"document.all.\"+what) : document.getElementById(what)
crossobj.style.left=curpos+\"px\"
}

function startit(){
crossobj=ie4? eval(\"document.all.\"+curcanvas) : dom? document.getElementById(curcanvas) : document.tickernsmain.document.tickernssub
if (ie4||dom){
crossobj.innerHTML=getslidehtml(slideshowcontent[curindex])
rotateslide()
}
else{
document.tickernsmain.visibility='show'
curindex++
setInterval(\"rotateslide()\",pause)
}
}

if (window.addEventListener)
window.addEventListener(\"load\", startit, false)
else if (window.attachEvent)

window.attachEvent(\"onload\", startit)
else if (ie4||dom||document.layers)
window.onload=startit

</script>

		
		
		
		</div>
	  <div id=\"contenido\">
	  <div class='menu'>".$menu."</div>
						";
			return $htmCabLis;	
		}
		
		
		function flibHtmCabSinTop()
		{
			//Se llama a la Funcin de Librera HTML (flibHtm()) y concatenamos la cadena de caracteres
			$htmCabLis=$this->flibHtm($activarRefresh, $ficheroJavaScript, $validaJavaScript,$Css)."
			<body>";
			return $htmCabLis;	
		}

				
		///////////////////////////////////////////////////////////////////////////////////////////////////
		
		/*
			Funcin de Librera Cerrar HTML (flibCerrarHtm())
			Contiene todo lo referente al cierre del cdigo HTML usado al inicio,
			se puede encontrar en la Funcin de Librera HTML (flibHtm()), este a 
			su vez contiene la imagen que esta al pie de cada presentacion de la
			Intranet VTV.
		*/
		
		function flibCerrarHtm()
		{
			$CerrarHtm="
			</div>
			<div class=\"cont_bottom\"></div>
			</div>
			<div class=\"footer\">Desarrollado por la Gerencia de Tecnología de la Información y Comunicación en colaboraci&oacute;n con la Gerencia de Relaciones P&uacute;blicas<br>Navegador Recomendado Mozilla Firefox<br>Resoluci&oacute;n 1024x768</div>
			</body>
			</html>
			";
			return $CerrarHtm;
		}
		
		function flibCerrarHtmSinTop()
		{
			$CerrarHtm="
			</body>
			</html>";
			return $CerrarHtm;
		}
		
		function flibHtmPoup($activarRefresh, $validaJavaScript)
		{
		    $htmCab="";
			$htmCab="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
				<head>";
				if($activarRefresh)
				{
					$htmCab.="<meta http-equiv=\"refresh\" content=\"60;".$this->$nombrePagina."\">";
				}
				$htmCab.="<title>".$this->titulo."</title>
				<link href=\"../../../Estilos/pop-up.css\" rel=\"stylesheet\" type=\"text/css\">
				<link href=\"../../../Estilos/estilosIntranetAplicaciones.css\" rel=\"stylesheet\" type=\"text/css\">
				<script language=\"javascript\">".$validaJavaScript."</script>
				";
				if($validaJavaScript==14868322){
				$htmCab.="
				<script type='text/javascript' src='../calendario/calendar.js'></script>
				<script type='text/javascript' src='../calendario/lang/calendar-es.js'></script>
				<script type='text/javascript' src='../calendario/calendar-setup.js'></script>
				<link href='../css/calendario.css' rel='stylesheet' type='text/css' media='all'  />	
				";
				}
				$htmCab.="</head><body>";
			return $htmCab;
		}
		/**/
		function flibCerrarHtmPoup()
		{
			$CerrarHtm="
			</body>
			</html>";
			return $CerrarHtm;
		}
		
		
}				
?>
