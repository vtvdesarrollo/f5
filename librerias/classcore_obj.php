<?php
/**
 * CLASSCORE_OBJ
 *
 *
 * ESTE ARCHIVO CONTIENE TODAS LAS INSTRUCCIONES POSIBLES PARA LA CONSTRUCCION DE OBJETOS Y CONSULTAS A UNA BASE DE DATOS
 * DEPENDE DE CLASSCORE_DBCONSULTAS QUE ESTE A SU VEZ DEPENDE DE LAS CONEXIONES Y CLASES DE jFLORES lFLOREZ
 *
 *
 * VERSION 2.0
 *      SOPORTA PARAMETRIZACION DE VARIABLES Y ACCESO DIRECTO PARA USO EN REPORTES PDF
 */

/**
 * optiene dato de post o get
 *
 * @param string $nombre
 * @return string
 */


function getpost($nombre){
	if ($_GET[$nombre]!="") {
		$dato=$_GET[$nombre];
	}elseif ($_POST[$nombre]!="") {
		$dato=$_POST[$nombre];
	}else{
		return false;
	}

	return  utf8_decode($dato);
}


/*
 * fix path para reportes y consultas directas a clase sin necesidad de apache.
 */
if(!defined(rptpath)){
    define("rptpath", "");
}


require_once("classcore_dbconsulta.php");
class obj{
	private $conect_sistemas;
	private $conect_sigai;
	private $conect_intranet;
	private $conect_otros;
	private $conexion_postgree;
	private $conexion_sigai;


        //para saber que la clase esta siendo accedida de forma directa.
        private $parametrizadaclase;

	function __construct($clasemadre=""){
		$this->conect_sistemas=rptpath."../database/archi_conex/sistemas_vtv";
		$this->conect_sistemas84=rptpath."../database/archi_conex/sistemas_vtv_5431";

		$this->conect_intranet=rptpath."../database/archi_conex/sistemas_vtv_5431";
		$this->conect_sigai=rptpath."../database/archi_conex/sqlserver_sigai";
		//esta es una variable que se usa para migraciones.
		$this->conect_otros=rptpath."../database/archi_conex/sistemas_vtv_5431";

		//para consultas de postgree con las clases de conexion de los muchachos.

		if($clasemadre==""){
                    $clasemadre=getpost("class");
                    $this->parametrizadaclase=FALSE;
                }else{
                    $this->parametrizadaclase=TRUE;
                }

                $this->conexion_postgree=new $clasemadre(rptpath."../database/classdb.php");
		//para consultas de sigai con las clases de conexion de los muchachos.
		$this->conexion_sigai=new classbdConsultas(rptpath."../database/classdbsqlserver.php");
	}


	/**
	 * function que sirve para traducir de cualquier codigo de caracteres al estandar
	 * de codigo de caracteres optimizado para ajax UTF_8 combos listas
	 *
	 * @param array $data
	 * @return array
	 */
	private function traduce_combolista($data){
		for ($i=1;$i<=count($data);$i++){
			$data[$i]=array_map("utf8_encode",$data[$i]);
			$datos[]["id"]=$data[$i][1];
			$datos[count($datos)-1]["valor"]=$data[$i][2];
		}

		//borrando el arreglo
		unset($data);
		return $datos;
	}

	//function estandar de listados.
	private function traduce_a_listado($datainput,$conexion="",$dbconsulta="",$archconx=""){
                if($conexion==""){$conexion=getpost("cx");}
                if($dbconsulta==""){$dbconsulta=getpost("dbconsulta");}
		if($archconx==""){$archconx=getpost("archconx");}


		$dataoutput[page]=getpost("page");

                //bug para la resolucion de numero de datos en lista
                if(method_exists($this->$conexion, "nreg_".$dbconsulta)){
                    $nregmeth="nreg_".$dbconsulta;
                    $dataoutput[total]=$this->$conexion->$nregmeth($this->$archconx);
                }else{
                    $dataoutput[total]=count($datainput);
                }

		for ($i=1;$i<=count($datainput);$i++){
			$dataoutput["rows"][$pc=count($dataoutput["rows"])]['id']=$datainput[$i][1];
                        unset ($datainput[$i][1]);
			$dataoutput["rows"][$pc]['cell']=explode(",",implode(",",array_map("utf8_encode",$datainput[$i])));
		}

                $dataoutput=json_encode($dataoutput);

                if($this->parametrizadaclase==TRUE){
                    return $dataoutput;
                }else{
                    echo $dataoutput;
                }
	}

	/**
	 * function que conecta con cualquier base de datos, esquema y consulta
	 * para luego enviar la data en json para crear selects listas.
	 */
	function selectlibre($conexion="",$archconx="",$dbconsulta="",$tipor="",$valor=""){

		if($conexion==""){$conexion=getpost("cx");}
                if($dbconsulta==""){$dbconsulta=getpost("dbconsulta");}
		if($archconx==""){$archconx=getpost("archconx");}

                if($tipor==""){ $tipor=getpost("tipo"); }

		if($valor==""){
                    $valor=str_replace("\\","",getpost("valor")); //'"arg1","arg2","arg3","argnx"'
                    if($valor!=false){
                            $valor=",".$valor;
                    }
                }else{
                    $valor=str_replace("\\","",$valor); //'"arg1","arg2","arg3","argnx"'
                    if($valor!=false){
                            $valor=",".$valor;
                    }
                }


		//truco para crear funciones dinamicas con infinitos argumentos.
		$evalua='$data=$this->'.$conexion.'->'.$dbconsulta.'("'.$this->$archconx.'"'.$valor.');';
		//echo $evalua;
                eval($evalua);
		//traducciones a los distintos tipos de muestra
		switch ($tipor){
			//se crea el estandar de envio de datos a ser usado en los selects
			case "combolista":
				$data=$this->traduce_combolista($data);
                                if($this->parametrizadaclase==TRUE){
                                    return $this->responderjson($data);
                                }else{
                                    $this->responderjson($data);
                                }
				break;
			//estandar para los listados...
                        case "listados":
                                if($this->parametrizadaclase==TRUE){
                                    return $this->traduce_a_listado($data,$conexion,$dbconsulta,$archconx);
                                }else{
                                    $data=$this->traduce_a_listado($data,$conexion,$dbconsulta,$archconx);
                                }
                                break;

			default: //en caso que solo responda con el dato que la class_db_aplication
                                if($this->parametrizadaclase==TRUE){
                                    return $data;
                                }else{
                                    echo $data;
                                }
		}
	}

	private function responderjson($json){
            if($this->parametrizadaclase==TRUE){
		return @json_encode($json);
            }else{
                echo @json_encode($json);
            }
	}

}


if(rptpath==""){
    /**
     * servidor de datos a responder.
     */
    if (getpost("s")!="" or getpost("s")!="") {
            $servidor=getpost("s");
            $objetoquery=getpost("q");
            if (class_exists($servidor)) {
                    $init_serv=new $servidor();
                    if (method_exists($init_serv,$objetoquery)){
                            echo $init_serv->$objetoquery();
                    }else{

                            echo "null";

                    }
            }else {
                        echo "null";
            }
    }else{
            echo "null";
    }
}
?>
