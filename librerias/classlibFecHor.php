<?php
/*
    Caracas; 09/05/2008
    Clase Libreria de Fechas y Horas (classLibFecHor)
    Intranet VTV Version 1.0
*/
	class classlibFecHor
	{
		function classlibFecHor()
		{
		}
		/*
		    Funcion de libreria, Mes en String (flibMesString($numMes))
		    $numMes: un numero entre [0,12] y 
		    retorna el mes asociado a ese numero.
		*/
		function flibMesString($numMes)
        {
            if($numMes>=1 && $numMes<=12)
            {
            	$nombreMes= array(
            		"01"	=>"Enero",
            		"02"	=>"Febrero",
            		"03"	=>"Marzo",
            		"04"	=>"Abril",
            		"05"	=>"Mayo",
            		"06"	=>"Junio",
            		"07"	=>"Julio",
            		"08"	=>"Agosto",
            		"09"	=>"Septiembre",
            		"10"=>"Octubre",
            		"11"=>"Noviembre",
            		"12"=>"Diciembre");
            	return $nombreMes[$numMes];
            }
            else
            {
                return "Error 0001: Valor del par�metro incorrecto. [01,12]";
            }
        }
		/*
    		Funcion de libreria, Invertir de Espa�ol a Ingles flibInvertirEsIn($fechaEs)
    		Formato Espa�ol: dd/mm/yyyy
    		Formato Ingles: yyyy/mm/dd 
		*/
		function flibInvertirEsIn($fechaEs)
        {
        	$dia=substr($fechaEs, 0, 2);
        	$mes=substr($fechaEs, 3, 2);
        	$ano=substr($fechaEs, 6, 4);
        	$fechaIn = ($ano."/".$mes."/".$dia);
        	return $fechaIn;
        }
        /*
    		Funcion de libreria, Invertir de Ingles a Espa�ol flibInvertirInEs($fechaIn)
    		Formato Ingles: yyyy/mm/dd 
    		Formato Espa�ol: dd/mm/yyyy
		*/
		function flibInvertirInEs($fechaIn)
        {
        	$dia=substr($fechaIn, 8, 2);
        	$mes=substr($fechaIn, 5, 2);
        	$ano=substr($fechaIn, 0, 4);
        	$fechaEs = ($dia."/".$mes."/".$ano);
        	return $fechaEs;
        }
		/*
            Funcion libreria, Suma N dias a una fecha Dada flibSumNdiaFecha($nDia, $fecha, $formato)
            Uso de los par�metros:
                $nDia: Numeros de d�as que se desean Sumar
                $fecha: Fecha que se le desea sumar los d�as
                $formato:
                            0 => Indica si es Espa�ol. (dd/mm/yyyy)
                            1 => Indica si es Ingles. (yyyy/mm/dd)
        */
        function flibSumNdiaFecha($nDia, $fecha, $formato)
        {
            if($formato>=0 && $Formato<=1)
            {
                if($nDia>=0)
                {
                    if($formato==0)
                        $fechaFun=$this->flibInvertirEsIn($fecha);//Convertimos al formato ingles
                    else
                        $fechaFun=$fecha;
                    //Extraemos las sub cadenas
                    $dia=substr($fechaFun, 8, 2);
                	$mes=substr($fechaFun, 5, 2);
                	$ano=substr($fechaFun, 0, 4);
                    $fechaFunAux= mktime (0, 00, 00, $mes, $dia, $ano );
                    $sum=$fechaFunAux + 86400*$nDia;
                    $fechaFun= getdate($sum);
					// Para poder hacer la conversion de datos
                    if($fechaFun['mday']<10)
                    {
                    	$dia="0".$fechaFun['mday'];
                    }
                    else
                    {
                    	$dia=$fechaFun['mday'];
                    }
                    // Para poder hacer la conversion de datos
                    if($fechaFun['mon']<10)
                    {
                    	$mes="0".$fechaFun['mon'];
                    }
                    else
                    {
                    	$mes=$fechaFun['mon'];
                    }
                    $fechaFun=$fechaFun['year']."/".$mes."/".$dia;
                    if($formato==0)
                        return $this->flibInvertirInEs($fechaFun);
                    else
                        return $fechaFun;
                }
                else
                {
                    return "Error 0001: Valor del par�metro 'nDia' incorrecto. [nDia >= 0]";
                }
            }
            else
            {
                return "Error 0001: Valor del par�metro 'Formato' incorrecto. [0,1]";
            }
        }
        /*
            Funcion libreria, Resta N dias a una fecha Dada flibResNdiaFecha($nDia, $fecha, $formato)
            Uso de los par�metros:
                $nDia: Numeros de d�as que se desean Sumar
                $fecha: Fecha que se le desea restar los d�as
                $formato:
                            0 => Indica si es Espa�ol. (dd/mm/yyyy)
                            1 => Indica si es Ingles. (yyyy/mm/dd)
        */
        function flibResNdiaFecha($nDia, $fecha, $formato)
        {
            if($formato>=0 && $Formato<=1)
            {
                if($nDia>=0)
                {
                    if($formato==0)
                        $fechaFun=$this->flibInvertirEsIn($fecha);//Convertimos al formato ingles
                    else
                        $fechaFun=$fecha;
                    //Extraemos las sub cadenas
                    $dia=substr($fechaFun, 8, 2);
                	$mes=substr($fechaFun, 5, 2);
                	$ano=substr($fechaFun, 0, 4);
                    $fechaFunAux= mktime (0, 00, 00, $mes, $dia, $ano );
                    $resta=$fechaFunAux - 86400*$nDia;
                    $fechaFun= getdate($resta);
                    if($fechaFun['mday']<10)
                    {
                    	$dia="0".$fechaFun['mday'];
                    }
                    else
                    {
                    	$dia=$fechaFun['mday'];
                    }
                    // Para poder hacer la conversion de datos
                    if($fechaFun['mon']<10)
                    {
                    	$mes="0".$fechaFun['mon'];
                    }
                    else
                    {
                    	$mes=$fechaFun['mon'];
                    }
                    $fechaFun=$fechaFun['year']."/".$mes."/".$dia;
                    if($formato==0)
                        return $this->flibInvertirInEs($fechaFun);
                    else
                        return $fechaFun;
                }
                else
                {
                    return "Error 0001: Valor del par�metro 'nDia' incorrecto. [nDia >= 0]";
                }
            }
            else
            {
                return "Error 0001: Valor del par�metro 'Formato' incorrecto. [0,1]";
            }
        }
         /*
            Funcion de libreria, Data Time (flibDataTime())
            Retorna el dato: Dia y Hora actual, unidos.
   		*/
   		function flibDataTime()
   		{
   			return $this->flibDia()." ".$this->flibHora();
   		}
   		/*
            Funcion de libreria, Dia Actual del Servidor (flibDia())
            Retorna el d�a actual en formato Ingles.
        */
        function flibDia()
        {
        	return date("Y-m-d");
        }
        /*
            Funcion de libreria, Hora Actual del servidor (flibHora())
            retorna la hora actual en formato: Hora:Minutos:Segundos
        */
        function flibHora()
        {
        	return date("H:i:s");
        }
        /*
            Funcion de libreria,
        */
        function flibAnoActual()
        {
        	return date("Y");
        }
        /*
            Funcion de libreria,
        */
        function flibMesActual()
        {
        	return date("n");
        }
		
		function flibdiamesano(){
				$dias = array(" Domingo "," Lunes "," Martes "," Miercoles "," Jueves "," Viernes "," S�bado ");
				$dia =date('d');
				$mes=date('n');
				$ano=date('Y');
				$meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
				$fecha=$dias[date('w')].$dia.' de '.$meses[$mes].' de '.$ano;
		
		return $fecha;
		
		}
		
		function num2letras($num, $fem = true, $dec = true) { 
//if (strlen($num) > 14) die("El n?mero introducido es demasiado grande"); 
   $matuni[2]  = "dos"; 
   $matuni[3]  = "tres"; 
   $matuni[4]  = "cuatro"; 
   $matuni[5]  = "cinco"; 
   $matuni[6]  = "seis"; 
   $matuni[7]  = "siete"; 
   $matuni[8]  = "ocho"; 
   $matuni[9]  = "nueve"; 
   $matuni[10] = "diez"; 
   $matuni[11] = "once"; 
   $matuni[12] = "doce"; 
   $matuni[13] = "trece"; 
   $matuni[14] = "catorce"; 
   $matuni[15] = "quince"; 
   $matuni[16] = "dieciseis"; 
   $matuni[17] = "diecisiete"; 
   $matuni[18] = "dieciocho"; 
   $matuni[19] = "diecinueve"; 
   $matuni[20] = "veinte"; 
   $matunisub[2] = "dos"; 
   $matunisub[3] = "tres"; 
   $matunisub[4] = "cuatro"; 
   $matunisub[5] = "quin"; 
   $matunisub[6] = "seis"; 
   $matunisub[7] = "sete"; 
   $matunisub[8] = "ocho"; 
   $matunisub[9] = "nove"; 

   $matdec[2] = "veint"; 
   $matdec[3] = "treinta"; 
   $matdec[4] = "cuarenta"; 
   $matdec[5] = "cincuenta"; 
   $matdec[6] = "sesenta"; 
   $matdec[7] = "setenta"; 
   $matdec[8] = "ochenta"; 
   $matdec[9] = "noventa"; 
   $matsub[3]  = 'mill'; 
   $matsub[5]  = 'bill'; 
   $matsub[7]  = 'mill'; 
   $matsub[9]  = 'trill'; 
   $matsub[11] = 'mill'; 
   $matsub[13] = 'bill'; 
   $matsub[15] = 'mill'; 
   $matmil[4]  = 'millones'; 
   $matmil[6]  = 'billones'; 
   $matmil[7]  = 'de billones'; 
   $matmil[8]  = 'millones de billones'; 
   $matmil[10] = 'trillones'; 
   $matmil[11] = 'de trillones'; 
   $matmil[12] = 'millones de trillones'; 
   $matmil[13] = 'de trillones'; 
   $matmil[14] = 'billones de trillones'; 
   $matmil[15] = 'de billones de trillones'; 
   $matmil[16] = 'millones de billones de trillones'; 

   $num = trim((string)@$num); 
   if ($num[0] == '-') { 
      $neg = 'menos '; 
      $num = substr($num, 1); 
   }else 
      $neg = ''; 
   while ($num[0] == '0') $num = substr($num, 1); 
   if ($num[0] < '1' or $num[0] > 9) $num = '0' . $num; 
   $zeros = true; 
   $punt = false; 
   $ent = ''; 
   $fra = ''; 
   for ($c = 0; $c < strlen($num); $c++) { 
      $n = $num[$c]; 
      if (! (strpos(".,'''", $n) === false)) { 
         if ($punt) break; 
         else{ 
            $punt = true; 
            continue; 
         } 

      }elseif (! (strpos('0123456789', $n) === false)) { 
         if ($punt) { 
            if ($n != '0') $zeros = false; 
            $fra .= $n; 
         }else 

            $ent .= $n; 
      }else 

         break; 

   } 
   $ent = '     ' . $ent; 
   if ($dec and $fra and ! $zeros) { 
      $fin = ' coma'; 
      for ($n = 0; $n < strlen($fra); $n++) { 
         if (($s = $fra[$n]) == '0') 
            $fin .= ' cero'; 
         elseif ($s == '1') 
            $fin .= $fem ? ' una' : ' un'; 
         else 
            $fin .= ' ' . $matuni[$s]; 
      } 
   }else 
      $fin = ''; 
   if ((int)$ent === 0) return 'Cero ' . $fin; 
   $tex = ''; 
   $sub = 0; 
   $mils = 0; 
   $neutro = false; 
   while ( ($num = substr($ent, -3)) != '   ') { 
      $ent = substr($ent, 0, -3); 
      if (++$sub < 3 and $fem) { 
         $matuni[1] = 'una'; 
         $subcent = 'as'; 
      }else{ 
         $matuni[1] = $neutro ? 'un' : 'uno'; 
         $subcent = 'os'; 
      } 
      $t = ''; 
      $n2 = substr($num, 1); 
      if ($n2 == '00') { 
      }elseif ($n2 < 21) 
         $t = ' ' . $matuni[(int)$n2]; 
      elseif ($n2 < 30) { 
         $n3 = $num[2]; 
         if ($n3 != 0) $t = 'i' . $matuni[$n3]; 
         $n2 = $num[1]; 
         $t = ' ' . $matdec[$n2] . $t; 
      }else{ 
         $n3 = $num[2]; 
         if ($n3 != 0) $t = ' y ' . $matuni[$n3]; 
         $n2 = $num[1]; 
         $t = ' ' . $matdec[$n2] . $t; 
      } 
      $n = $num[0]; 
      if ($n == 1) { 
         $t = ' ciento' . $t; 
      }elseif ($n == 5){ 
         $t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t; 
      }elseif ($n != 0){ 
         $t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t; 
      } 
      if ($sub == 1) { 
      }elseif (! isset($matsub[$sub])) { 
         if ($num == 1) { 
            $t = ' mil'; 
         }elseif ($num > 1){ 
            $t .= ' mil'; 
         } 
      }elseif ($num == 1) { 
         $t .= ' ' . $matsub[$sub] . '?n'; 
      }elseif ($num > 1){ 
         $t .= ' ' . $matsub[$sub] . 'ones'; 
      }   
      if ($num == '000') $mils ++; 
      elseif ($mils != 0) { 
         if (isset($matmil[$sub])) $t .= ' ' . $matmil[$sub]; 
         $mils = 0; 
      } 
      $neutro = true; 
      $tex = $t . $tex; 
   } 
   $tex = $neg . substr($tex, 1) . $fin; 
   return ucfirst($tex); 
} 
		
    }
?>