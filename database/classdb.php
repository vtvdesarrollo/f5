<?php
/*
    Caracas; 09/05/2008
    Clase Base de Datos (classdb)
    Intranet VTV Version 1.0
*/
	require_once("class.phpmailer.php");
	class classdb
	{
		//Variables Globales de la Clase
		var $dbName="", $dbLogin="", $dbPassword="", $dbServidor="", $dbPuerto="";
		var $conElem="";
        var $imprimesql=false;
		/*
			Constructor de la aplicacin, se le envia por parametro la ruta del archivo
			que contiene la informacion necesaria para establecer la conexiond de la base
			de datos, Este archivo debe tener:
			$archivoName: Tiene el nombre de la Base de Datos
			$archivoLogin: Tiene el login de la Base de Datos
			$archivoPas: Tiene el password de la Base de Datos
			$archivoServidor: Contiene el Root de la Base de Datos
			$archivoPuerto: Contiene el nmero del puerto
		*/

        function fdberror($funciondb, $sql){
            echo "se reporta un error en la tratando hacer un ".$funciondb. " con el query ".$sql;
        }

		function classdb($archivoInfoDb)
		{
			require($archivoInfoDb);
			$this->dbName=$archivoName;
			$this->dbLogin=$archivoLogin;
			$this->dbPassword=$archivoPas;
			$this->dbServidor=$archivoServidor;
			$this->dbPuerto=$archivoPuerto;

            $this->imprimesql=false;
		}
		/*
			Funcion de Data Base Conectar ( fdbConectar )
			Es la encarga de hacer la conexion a la Base de Datos,
			Retorna el Id de Conexion
		*/
		function fdbConectar()
		{
	    	$conexion="";
			$conexion = pg_connect("host=".$this->dbServidor." port=".$this->dbPuerto." dbname='".$this->dbName."' user=".$this->dbLogin." password=".$this->dbPassword);
	    	//$conexion = pg_connect("host=172.16.8.42 port=5432 dbname=sigefirrhh user=postgres password=sigefirrhhvenezuela");
			if($conexion)
	    	{
				return $conexion;
	    	}
	    	else
	    	{
	    		//echo "Error 0500: Error en la Funcion fdbConectar,<br>No se logro la conexion";
	    	//	echo $conexion;
				//die;
				echo "<script>
				 location.href='classMantenimiento.php';
	 			</script>";

			$mail = new PHPMailer();
			$mail->From     = "intranet@vtv.gob.ve";
			$mail->FromName =   "INTRANET";
			$mail->Host     = '172.16.8.8';
			$mail->Subject =  "ERROR EN CONEXION CON BASE DE DATOS POSTGRES";
			$mail->Mailer   = "smtp";
			// HTML body
			$htmltable.='
			Fecha: '.date('d/m/Y').'<br>
			Hora: '.date('h:i:s').'<br><br>
			<b>Error 0500: Error en la Funci�n fdbConectar,<br>No se logro la conexi�n con PostgreSQL.</b><br>';

			$mail->Body    = $htmltable;
			$mail->AltBody = "INTRANET VTV";
			$mail->AddAddress("desarrollo@vtv.gov.ve", "");

			if(!$mail->Send())
			//echo $mail->ErrorInfo;
			 // Clear all addresses and attachments for next loop
			$mail->ClearAddresses();
			$mail->ClearAttachments();

	    	}
		}
		/*
			Funcion de Data Base Desconexion ( fdbDesConexion )
			Es la encarga de hacer la desconexion a la Base de Datos.
		*/
		function fdbDesConexion($conexion="")
		{
			pg_close($conexion);
		}
		/*
			Funcion Data Base Insertar (fdbInsert)
			Esta funcion permite insertar datos dentro de una base de datos, acorde a los parametros
			enviados, los cuale son:
			$nombreTabla: Nombre de la tabla en donde se va hacer el insert.
			$arregloDatos: Es el arreglo asociativo de los datos que se desean insertar. Un arreglo
			asociativo es de la siguiente manera:

			$arregloDatos['campoTabla']=$infoGuardar;

			Donde:
			$arregloDatos: Nombre de la variable arrays.
			'campoTabla': Nombbre del campo de la tabla que se desea hacer el insert, debe estar
			entre comillas simples.
			$infoGuardar: Tiene la informacion que va hacer insertada en la base de datos.
		*/
		function fdbInsert($nombreTabla,$arregloDatos)
		{
			//$sql: Variable que construye el SQl.
            $sql = "insert into $nombreTabla (";
            $datos="";
            //Para llevar al arreglo a su primer valor
            reset($arregloDatos);
			// Moverse dentro del Arreglo asociativo
            foreach($arregloDatos as  $campo => $valor )
            {
            	$sql .=  $campo . ",";
            	$datos .= "'". $valor . "',";
            }
            $sql=substr($sql,0,strlen($sql)-1);
            $datos=substr($datos,0,strlen($datos)-1);
            $sql.= ") values (". $datos.")";
            //echo "<br>SQL=> ".$sql."<br>";
			if (!pg_query($sql))
            {
            	echo "Error 0501: Error en la Funcion fdbInsert,<br>Al momento de realizar la inserccin, No se logro con Exito ". $sql;
            	exit;
            }
		}

		function fdbInsertRn($nombreTabla,$arregloDatos,$campo_return)
		{
			//$sql: Variable que construye el SQl.
            $sql = "insert into $nombreTabla (";
            $datos="";
            //Para llevar al arreglo a su primer valor
            reset($arregloDatos);
			// Moverse dentro del Arreglo asociativo
            foreach($arregloDatos as  $campo => $valor )
            {
            	$sql .=  $campo . ",";
            	$datos .= "'". $valor . "',";
            }
            $sql=substr($sql,0,strlen($sql)-1);
            $datos=substr($datos,0,strlen($datos)-1);
            $sql.= ") values (". $datos.")";
			$sql.= "RETURNING ".$campo_return;

            //echo "<br>SQL=> ".$sql."<br>";
			if (!$q=@pg_query($sql))
            {
			$this->fdberror("fdbInsertRn", $sql);
            	//echo "Error 0501: Error en la Funcion fdbInsert,<br>Al momento de realizar la inserccin, No se logro con Exito";
            	exit;
            }
			else
			{
				return pg_fetch_array($q);
			}
		}



		/*
			Funcion Data Base Update (fdbUpdate)
			Esta funcion permite modificar datos dentro de una base de datos, acorde a los parametros
			enviados, los cuale son:
			$nombreTabla: Nombre de la tabla en donde se va hacer el insert.
			$arregloDatos: Es el arreglo asociativo de los datos que se desean modificar. Un arreglo
			asociativo es de la siguiente manera:

			$arregloDatos['campoTabla']=$infoGuardar;

			Donde:
			$arregloDatos: Nombre de la variable arrays.
			'campoTabla': Nombbre del campo de la tabla que se desea hacer el insert, debe estar
			entre comillas simples.
			$infoGuardar: Tiene la informacion que va hacer insertada en la base de datos.

			$criterio: Es el arreglo asociativo que contiene los criterios para realizar el update.
			NOTA: El "Criterio" se forma solo con AND.
		*/
		function fdbdUpdate($nombreTabla, $arregloDatos, $criterio)
		{
			//$sql: Variable que construye el SQL
			$sql = "update $nombreTabla set ";
            $datos = '';
            // Para llevar el arreglo de a su primer valor
            reset($arregloDatos);
            // Para moverme dentro del arrays
            foreach($arregloDatos as  $campo => $valor )
            {
            	$sql .= ' ' . $campo . ' = \'' . $valor . '\',';
            }
            $sql=substr($sql,0,strlen($sql)-1);
            // Se verifica que existe un criterio
            if($criterio)
            {
            	$datos .= ' where ';
            	reset($criterio);
            	foreach($criterio as  $campo => $valor )
            	{
                    if($valor!=null){
                        $datos .= ' ' . $campo. ' = \'' . $valor . '\' and ';
                    }else{
                        $datos .= ' ' . $campo. ' and ';
                    }

            	}
            	$datos=substr($datos,0,strlen($datos)-4);
            }
            $sql.= $datos;
            //echo "<br>SQL=> ".$sql."<br>";
            if (!@pg_query($sql))
            {
            	echo "Error 0502: Error en la Funcion fdbUpdate,<br>Al momento de realizar la modificacion, No se logro con Exito<br>".$sql;
            	exit;
            }
		}
		function fdbdUpdateLibre($nombreTabla, $arregloDatos, $criterio)
		{
			//$sql: Variable que construye el SQL
			$sql = "update $nombreTabla set ";
            $datos = '';
            // Para llevar el arreglo de a su primer valor
            reset($arregloDatos);
            // Para moverme dentro del arrays
            foreach($arregloDatos as  $campo => $valor )
            {
                if($valor=="NULL"){
                    $sql .= ' ' . $campo . ' = '. $valor .",";
                }else{
                    $sql .= ' ' . $campo . ' = \'' . $valor . '\',';
                }

            }
            $sql=substr($sql,0,strlen($sql)-1);
            // Se verifica que existe un criterio
            if($criterio)
            {
				$sql.=' where ';
	   			$sql .= $criterio;

            }
            $sql.= $datos;
            //echo "<br>SQL=> ".$sql."<br>";
            if (!@pg_query($sql))
            {
            	echo "Error 0502: Error en la Funcion fdbUpdate,<br>Al momento de realizar la modificacion, No se logro con Exito -> ".$sql;
            	exit;
            }
		}

		/**/
		function fbdSelect($arregloTable, $arregloCampos, $arregloCondicion, $arregloOrder, $arregloGrup, $arreglolimite)
		{
			$x=1;// VARIABLE QUE CONTROLA LAS FILAS DE LA MATRIZ
			// $sql: Variable que construye el sql.
			$sql="SELECT ";
			foreach ($arregloCampos as $index => $valor)
			{
				$sql.=$valor.",";
			}
			$sql=substr($sql,0,strlen($sql)-1);
			$sql.=" FROM ";
			foreach ($arregloTable as $index => $valor)
			{
				$sql.=$valor.",";
			}
			$sql=substr($sql,0,strlen($sql)-1);
			$i=count($arregloCondicion);
			if($i > 0)
			{
	   			$sql.=" WHERE ";
	   			foreach($arregloCondicion as  $campo => $valor )
	           	{
	           		$sql .= $campo."=" . $valor . " and ";
	           	}
	           	$sql=substr($sql,0,strlen($sql)-4);
	         }
	         $i=count($arregloGrup);
			if($i > 0)
			{
	   			$sql.=" GROUP BY ";
	   			foreach($arregloGrup as  $campo => $valor )
	           	{
	           		$sql .= $valor . ",";
	           	}
	           	$sql=substr($sql,0,strlen($sql)-1);
	         }
	         $i=count($arregloOrder);
			if($i > 0)
			{
	   			$sql.=" ORDER BY ";
	   			foreach($arregloOrder as  $campo => $valor )
	           	{
	           		$sql .= $valor . ",";
	           	}
	           	$sql=substr($sql,0,strlen($sql)-1);
	         }

	         $i=count($arreglolimite);
			if($i > 0)
			{
	   			$sql.=" OFFSET  ".$arreglolimite['INICIO']." LIMIT ".$arreglolimite['FIN'];
	         }

            if($this->imprimesql){
        	   echo "<br>SQL=> ".$sql."<br>";
            }
        	$arrayResultado=pg_query($sql);
        	if (!@$arrayResultado)
	         {
	         	echo "Error 0503: Error en la Funcion fbdSelect,<br>Al momento de realizar la consulta, No se logro con Exito <br>".$sql;
	         	exit;
	         }
	         else
	         {
         		$matrizDatos=array();
         		while($array_resul=pg_fetch_array($arrayResultado))
			   {
					$j=1;//VARIABLE QUE CONTROLA LAS COLUMNAS
					$tam=count($array_resul,COUNT_RECURSIVE);
					//echo "Tamao del arrays: ".$tam."<br>";
					for ($y = 0; $y < ($tam/2); $y++)
					{
						$matrizDatos[$x][$j] = $array_resul[$y];
						//echo $j.") MatrizDatos[".$x."][".$j."]".$matrizDatos[$x][$j]." [y]".$array_resul[$y]."<br>";
						$j++;
					}
					$x++;
				}
			    return $matrizDatos;
			}
		}
			function fbdSelectLibre($arregloTable, $arregloCampos, $arregloCondicion, $arregloOrder, $arregloGrup, $arreglolimite)
		{
			$x=1;// VARIABLE QUE CONTROLA LAS FILAS DE LA MATRIZ
			// $sql: Variable que construye el sql.
			$sql="SELECT ";
			foreach ($arregloCampos as $index => $valor)
			{
				$sql.=$valor.",";
			}
			$sql=substr($sql,0,strlen($sql)-1);

			$i=count($arregloTable);
			if($i > 0)
			{
				$sql.=" FROM ";
				foreach ($arregloTable as $index => $valor)
				{
					$sql.=$valor.",";
				}
				$sql=substr($sql,0,strlen($sql)-1);
			}
			//$i=count($arregloCondicion);
			if($arregloCondicion !="")
			{
	   			$sql.=" WHERE ";
	   			$sql .= $arregloCondicion;
	           	//$sql=substr($sql,0,strlen($sql)-1);
	         }

			 $i=count($arregloGrup);
			if($i > 0)
			{
	   			$sql.=" GROUP BY ";
	   			foreach($arregloGrup as  $campo => $valor )
	           	{
	           		$sql .= $valor . ",";
	           	}
	           	$sql=substr($sql,0,strlen($sql)-1);
	         }

			 $i=count($arregloOrder);
			if($i > 0)
			{
	   			$sql.=" ORDER BY ";
	   			foreach($arregloOrder as  $campo => $valor )
	           	{
	           		$sql .= $valor . ",";
	           	}
	           	$sql=substr($sql,0,strlen($sql)-1);
	         }

	         $i=count($arreglolimite);
			if($i > 0)
			{
	   			$sql.=" OFFSET  ".$arreglolimite['INICIO']." LIMIT ".$arreglolimite['FIN'];
	         }

             if($this->imprimesql){
                echo "<br>SQL=> ".$sql."<br>";
             }
        	$arrayResultado=pg_query($sql);

            if (!@$arrayResultado)
	         {
	         	echo "Error 0503: Error en la Funcion fbdSelect,<br>Al momento de realizar la consulta, No se logro con Exito".$sql;
	         	exit;
	         }
	         else
	         {
         		$matrizDatos=array();
         		while($array_resul=pg_fetch_array($arrayResultado))
			   {
					$j=1;//VARIABLE QUE CONTROLA LAS COLUMNAS
					$tam=count($array_resul,COUNT_RECURSIVE);
					//echo "Tamao del arrays: ".$tam."<br>";
					for ($y = 0; $y < ($tam/2); $y++)
					{
						$matrizDatos[$x][$j] = $array_resul[$y];
						//echo $j.") MatrizDatos[".$x."][".$j."]".$matrizDatos[$x][$j]." [y]".$array_resul[$y]."<br>";
						$j++;
					}
					$x++;
				}
			    return $matrizDatos;
			}
		}

    function fdbdDelete($nombreTabla, $criterio)
  {
   //$sql: Variable que construye el SQL
   $sql = "delete from $nombreTabla ";
            $datos = '';
            // Se verifica que existe un criterio
            if($criterio)
            {
             $datos .= ' where ';
             reset($criterio);
             foreach($criterio as  $campo => $valor )
             {
              $datos .= ' ' . $campo. ' = \'' . $valor . '\' and ';
             }
             $datos=substr($datos,0,strlen($datos)-4);
            }
            $sql.= $datos;
           //echo "<br>SQL=> ".$sql."<br>";
            if (!@pg_query($sql))
            {
             echo "Error 0502: Error en la Funcion fdbdDelete,<br>Al momento de realizar la modificacion, No se logro con Exito";
             exit;
            }
  }


    }
?>
