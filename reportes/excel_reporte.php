<?php

require 'Classes/PHPExcel.php';

class reporte extends PHPExcel {

    private $tab = 0;
    private $nombrearchivo;
    //manejo de filas
    private $col = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private $fila = 1;
    private $titulo;

    function __construct() {
        parent::__construct();
    }

    function seteartitulos($titulo, $sujeto, $descripcion, $nombrearchivo=false) {
        $this->titulo = $titulo;
        $this->getProperties()->setCreator("sistema de reportes de xls");
        $this->getProperties()->setLastModifiedBy("sistema de reportes xls");
        $this->getProperties()->setTitle($this->titulo);
        $this->getProperties()->setSubject($sujeto);
        $this->getProperties()->setDescription($descripcion);
        if ($nombrearchivo == false) {
            $this->nombrearchivo = str_replace('.php', '.xls', __FILE__);
        } else {
            $this->nombrearchivo = $nombrearchivo;
        }
    }

    function __destruct() {
        $objWriter = PHPExcel_IOFactory::createWriter($this, 'Excel5');
        if ($this->nombrearchivo != str_replace('.php', '.xls', __FILE__)) {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $this->nombrearchivo . '.xls"');
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
        } else {
            $objWriter->save($this->nombrearchivo);
        }
    }

    function activaNuevaPestaña($nombre) {
        if ($this->tab > 0) {
            $this->createSheet($this->tab);
            $this->setdesdefila(1);
        }
        $activo = $this->setActiveSheetIndex($this->tab);
        $activo->setTitle($nombre);
        $this->tab++;
    }

    function irapestaña($num) {
        $this->getActiveSheetIndex($num);
    }

    function setdesdefila($fila) {
        $this->fila = $fila;
    }

    function configuraranchocolmnas($data=array()) {
        if (count($data) != 0) {
            do {
                $this->getActiveSheet()->getColumnDimension($this->col[key($data)])->setWidth(current($data));
            } while (next($data));
        }
    }

    function esribirfila($data=array(), $fila=false, $alto=false) {
        if ($fila == false) {
            $fila = $this->fila;
        }
        $filadesde = $this->col[key($data)] . $fila;
        if (count($data) != 0) {
            do {
                $this->getActiveSheet()->setCellValue($this->col[key($data)] . $fila, current($data));

                if($alto!=false) {
                    $this->getActiveSheet()->getRowDimension($fila)->setRowHeight($alto);
                }
                
                $filaasta = $this->col[key($data)] . $fila;
            } while (next($data));
        }
        $this->fila++;

        return "$filadesde:$filaasta";
    }

    function escribirfilaformateada($data=array(), $fila=false, $color="FFFFFF") {
        $filas = $this->esribirfila($data, $fila);
        $this->getActiveSheet()->getStyle($filas)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($color);
    }

    function insertaimagen($nombre, $ruta, $celda) {
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName($nombre);
        $objDrawing->setDescription($nombre);
        $objDrawing->setPath(__DIR__ . "/" . $ruta);
        $objDrawing->setCoordinates($celda);
        $objDrawing->setWorksheet($this->getActiveSheet());
    }

    function cabezal() {
        //$this->insertaimagen("logo", "../imagenes/logo_vtv.jpg", "A1");
        $this->getActiveSheet()->setCellValue("B2", $this->titulo);
        $this->getActiveSheet()->setCellValue("B3", "Reporte generado a la fecha " . date("d/m/Y"));
        $this->getActiveSheet()->setCellValue("B4", "REPORTE INVENTARIO");
        $this->fila = 7;
    }

}

?>
