<?php

header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
ini_set('memory_limit', '9999999999999999999M');
require_once('tcpdf/tcpdf.php');


require_once("../class/bd/classbdConsultas.php");
require_once("../librerias/classlibFecHor.php");

class reporte extends TCPDF {

    public $conect_sistemas_vtv;
    public $ObjConsulta;
    public $Objfechahora;
    public $registros;
    public $almacenista;
    public $receptor;

    function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
        $this->conect_sistemas_vtv = "../database/archi_conex/sistemas_vtv_5431";
        $this->ObjConsulta = new classbdConsultas();
        $this->Objfechahora = new classlibFecHor();
    }

    function header() {
        if ($this->registros[0][1] == "") {
            $this->registros[0][1] = "NO EMPLEADO";
        }
        $this->SetFont('', '', 10);
        $this->almacenista = utf8_encode($this->registros[0][1]);
        $this->receptor = utf8_encode($this->registros[0][2]);
        $titulo1 = "REMOTO ";
        $htmltable = '<table border="0" width="650px" cellspacing="4">
	  <tr>

		<td width="160px" rowspan="3"><img src="imagenes/logo_vtv.jpg" style="width: 173px; height: 72px;" alt="logo"/></td>
		<td ><br /><br /><div align="center"><font size="10"><b>' . $titulo1 . '</b></font></div></td>
	 </tr>
	 </table>';
        $fechaimp = date("d/m/Y H:i:s");
        $this->writeHTML($htmltable);
    }

    function footer() {
        $this->SetFont('', '', 6);
        $this->Ln(3);
        $this->Cell(0, 0, 'FECHA:' . date("d/m/Y H:i:s") . '', 0, 0, 'L');
    }

    function renderizarimagetofile($url, $name, $path="imagenes/") {
        if (($f = fopen($url, 'r')) != false) {
            fclose($f);
            $res = join(file($url));
            if (($f = fopen($path . $name . ".png", "w")) != false) {
                fwrite($f, $res);
                fclose($f);
            }
        }
    }

}

$pdf = new reporte(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(15, 38, 20);
$pdf->SetHeaderMargin(15);
$pdf->SetFooterMargin(20);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);
$pdf->AddPage('P');
require_once('tcpdf/htmlcolors.php');
$pdf->Ln(2);
$cedula = $_GET['cedula'];
$pauta = $_GET['id_pauta'];
$idspauta = $pdf->ObjConsulta->selectidspauta($pdf->conect_sistemas_vtv, $pauta);
$id_pauta = $idspauta[1][1];
$id_tipo_pauta = $idspauta[1][2];
$id_locacion = $idspauta[1][3];
$id_tipo_traje = $idspauta[1][4];
$id_program = $idspauta[1][5];
$id_citacion = $idspauta[1][6];
$id_montaje = $idspauta[1][7];
$id_emision_grabacion = $idspauta[1][8];
$id_retorno = $idspauta[1][9];
$id_tipo_evento = $idspauta[1][10];
$user_reg = $idspauta[1][11];
$lugar_pauta = $idspauta[1][12];
$descripciontevenbd = $idspauta[1][13];

$descprograma = $pdf->ObjConsulta->selectdescprograma($pdf->conect_sistemas_vtv, $id_program);
$idprograma = $descprograma[1][1];
$descripcionprog = ucwords($descprograma[1][2]);

$desclocacion = $pdf->ObjConsulta->selectdesclocacion($pdf->conect_sistemas_vtv, $id_locacion);
$idlocacion = $desclocacion[1][1];
$descripcionloc = ucwords($desclocacion[1][2]);

$desctipotraje = $pdf->ObjConsulta->selectdesctipotraje($pdf->conect_sistemas_vtv, $id_tipo_traje);
$descripciontraje = ucwords($desctipotraje[1][1]);

$descproductor = $pdf->ObjConsulta->selectusuariof5($pdf->conect_sistemas_vtv, $user_reg);
$nombre = ucwords(strtoupper($descproductor[1][1]));
$apellido = ucwords(strtoupper($descproductor[1][2]));
$descripcionprod = $nombre . " " . $apellido;

$desctipoevento = $pdf->ObjConsulta->selectdesctipoevento($pdf->conect_sistemas_vtv, $id_tipo_evento);
$idtipoevento = $desctipoevento[1][1];
$descripcionteven = ucwords($desctipoevento[1][2]);

$datoscitacion = $pdf->ObjConsulta->selectcitacion($pdf->conect_sistemas_vtv, $id_citacion);
$idcitacion = $datoscitacion[1][1];
$fechacitacion = $datoscitacion[1][2];
$horacitacion = $datoscitacion[1][3];
$id_lugar_citacion = $datoscitacion[1][4];
$fechacitacion = $pdf->Objfechahora->flibInvertirInEs($fechacitacion);

$lugarcitacion = $pdf->ObjConsulta->selectlugar($pdf->conect_sistemas_vtv, $id_lugar_citacion);
$lugarcitacion = $lugarcitacion[1][2];
$descripcitacion = ucwords($lugarcitacion);

$datosmontaje = $pdf->ObjConsulta->selectmontaje($pdf->conect_sistemas_vtv, $id_montaje);
$idmontaje = $datosmontaje[1][1];
$fechamontaje = $datosmontaje[1][2];
$horamontaje = $datosmontaje[1][3];
$id_lugar_montaje = $datosmontaje[1][4];
$fechamontaje = $pdf->Objfechahora->flibInvertirInEs($fechamontaje);
$descripmontaje=ucwords ($lugar_pauta);

$datosemision = $pdf->ObjConsulta->selectemision($pdf->conect_sistemas_vtv, $id_emision_grabacion);
$idemision = $datosemision[1][1];
$fechaemision = $datosemision[1][2];
$horaemision = $datosemision[1][3];
$id_lugar_emision = $datosemision[1][4];
$fechaemision = $pdf->Objfechahora->flibInvertirInEs($fechaemision);
$descripemision=ucwords ($lugar_pauta);

$datosretorno = $pdf->ObjConsulta->selectretorno($pdf->conect_sistemas_vtv, $id_retorno);
$idretorno = $datosretorno[1][1];
$fecharetorno = $datosretorno[1][2];
$horaretorno = $datosretorno[1][3];
$id_lugar_retorno = $datosretorno[1][4];
$fecharetorno = $pdf->Objfechahora->flibInvertirInEs($fecharetorno);
$descripretorno=ucwords ($lugar_pauta);

$estatuspauta = $pdf->ObjConsulta->selectestatuspautas($pdf->conect_sistemas_vtv, $pauta);
$estatus = $estatuspauta[1][1];

if ($estatus == '9') {
    $obsrech = $pdf->ObjConsulta->selectobsrech($pdf->conect_sistemas_vtv, $pauta);
    $observaciones = $obsrech[1][1];
    $obs = "<table class='tabla'><tr><th>Observaciones:</th><td>" . $observaciones . "</td></tr></table>";
} else {
    $obs = "";
}
if ($idlocacion == '2') {
    $descripretorno = "";
} else {
    $descripretorno = '
		<tr nobr="true" >
		<th align="left"><font size="10">&nbsp;Retorno:&nbsp;</font><font size="8">' . $descripretorno . '</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Fecha:&nbsp;</font><font size="8">' . $fecharetorno . '</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Hora:&nbsp;</font><font size="8">' . $horaretorno . '</font></th>
		</tr>';
}

if($id_program==0){
	$evento='<th align="left"><font size="10">&nbsp;&nbsp;Evento:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;'.$descripciontevenbd.'</font></td>';
}else if(
	$id_program>=1){$evento='<th align="left"><font size="10">&nbsp;&nbsp;Programa:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;'.$descripcionprog.'</font></td>';
}
$verificarestatuspauta = $pdf->ObjConsulta->verificarestatuspauta($pdf->conect_sistemas_vtv, $id_pauta);
$estatusactual = $verificarestatuspauta[1][1];
$descestatus = $pdf->ObjConsulta->selectdescestatus($pdf->conect_sistemas_vtv, $estatusactual);
$descripestatus = ucwords($descestatus[1][1]);
$id_estatus = 21;
$aprobadopor = $pdf->ObjConsulta->selectquienaprobo($pdf->conect_sistemas_vtv, $id_pauta, $id_estatus);
$aprobado = $aprobadopor[1][1];
if ($aprobado == 0) {
    $asignados2 = '<tr nobr="true"><td  align="center" colspan="3"><div align="center"><font size="8">No se ha sido aprobada</font></div></td></tr>';
}else{
    $desc_aprobado=$pdf->ObjConsulta->selectnombreyapellidof5($pdf->conect_sistemas_vtv,$aprobado);
    $nomb=strtoupper($desc_aprobado[1][1]);
    $apellido=strtoupper($desc_aprobado[1][2]);
    $aprobadopor2 = $pdf->ObjConsulta->selectcargoquienaprobo($pdf->conect_sistemas_vtv, $aprobado);
    $cargo = strtolower($aprobadopor2[1][1]);
    $cargo = ucwords($cargo);

    $datoslista = $pdf->ObjConsulta->selectlistmatasig($pdf->conect_sistemas_vtv, $pauta);
    $contador = count($datoslista);
    foreach ($datoslista as $llave3 => $valor3) {
        $id_recurso_asignado = $valor3[1];
        $id_recurso = $valor3[2];
        $datosmaterial = $pdf->ObjConsulta->selectlistmatdesc($pdf->conect_sistemas_vtv, $id_recurso);
        $desc_id_recurso = strtoupper($datosmaterial[1][1]);
        $datoscaract = $pdf->ObjConsulta->selectdatoscedula($pdf->conect_sistemas_vtv, $id_recurso_asignado);
        $primero = $datoscaract[1][1];

        if ($primero == "") {
            $datoscaract4 = $pdf->ObjConsulta->selectdatosserial($pdf->conect_sistemas_vtv, $id_recurso_asignado);
            $primero = $datoscaract4[1][1];
            $datoscaract5 = $pdf->ObjConsulta->selectdatosmarca($pdf->conect_sistemas_vtv, $id_recurso_asignado);
            $segundo = $datoscaract5[1][1];
            $datoscaract6 = $pdf->ObjConsulta->selectdatosagregados($pdf->conect_sistemas_vtv, $id_recurso_asignado);
            $tercero = $datoscaract6[1][1];
            if ($primero == "") {
                $datoscaract7 = $pdf->ObjConsulta->selectdatosbiennac($pdf->conect_sistemas_vtv, $id_recurso_asignado);
                $primero = $datoscaract7[1][1];
            }
            if ($primero != "") {
                $primero = "Serial: " . $primero . "";
            }
            if ($segundo != "") {
                $segundo = "Marca: " . $segundo . "";
            }
            if ($tercero != "") {
                $tercero = "Agregado: " . $tercero . "";
            }
            $desc_caracteristicas = "" . $primero . "&nbsp;" . $segundo . "&nbsp;" . $tercero . " ";
        } else {
            $datoscaract2 = $pdf->ObjConsulta->selectdatospnombre($pdf->conect_sistemas_vtv, $id_recurso_asignado);
            $segundo = $datoscaract2[1][1];
            $datoscaract3 = $pdf->ObjConsulta->selectdatospapellido($pdf->conect_sistemas_vtv, $id_recurso_asignado);
            $tercero = $datoscaract3[1][1];
            $primero = "Cedula: " . ucwords($primero) . "";
            $segundo = "Nombre: " . ucwords($segundo) . "";
            $tercero = "Apellido: " . ucwords($tercero) . "";
            $desc_caracteristicas = "" . $primero . "<br />" . $segundo . "<br />" . $tercero . " ";
        }
        $asignados2.='<tr nobr="true"><td align="left"><font size="8">' . $desc_id_recurso . '</font></td><td align="left"><font size="8">' . $desc_caracteristicas . '</font></td></tr>';
    }
    if ($contador == 0){
        $asignados2 = '<tr nobr="true"><td  align="center" colspan="3"><div align="center"><font size="8">No se asign&oacute; ningun recurso</font></div></td></tr>';
    }
    $asignados = '
		<table align="center" border="1">
			<tr nobr="true"><th colspan="2"  bgcolor="#8B0000"><font size="10" color="white"><b>LISTA DE RECURSOS ASIGNADOS</b></font></th></tr>
			<tr nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Detalle</font></div></th></tr>
			' . $asignados2 . '
		</table>';

    $datoslista2=$pdf->ObjConsulta->selectrecsinasig($pdf->conect_sistemas_vtv,$pauta);
    $contna = count($datoslista2);
        foreach($datoslista2 as $llave2 => $valor2){
            $id_recurso=$valor2[1];
            $cantidad=$valor2[2];
            $datosmaterial=$pdf->ObjConsulta->selectlistmatdesc($pdf->conect_sistemas_vtv,$id_recurso);
            $desc_id_recurso=$datosmaterial[1][1];
            $materiales2.='<tr nobr="true"><td align="left"><font size="8">'.ucwords (strtoupper($desc_id_recurso)).'</font></td><td align="center"><font size="8">'.$cantidad.'</font></td></tr>';
            }

        $materiales='
            <table align="center" border="1">
            <tr nobr="true"><th colspan="2"  bgcolor="#8B0000"><font size="10" color="white"><b>LISTA DE RECURSOS SIN ASIGNAR</b></font></th></tr>
            <tr nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Cantidad</font></div></th></tr>
            ' . $materiales2 . '
            </table>';

        if($contna == 0 ){
            $materiales = " ";
        }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $body = '
        <table align="center" border="1">
		<tr nobr="true"><th colspan="2"  bgcolor="#8B0000"><font size="10" color="white"><b>DATOS GENERALES</b></font></th></tr>
		<tr nobr="true"><th align="left" width="100px"><font size="10">&nbsp;&nbsp;N&deg; de pauta:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;' . $id_pauta . '</font></td></tr>
		<tr nobr="true">' . $evento . '</tr>
		<tr nobr="true"><th align="left"><font size="10">&nbsp;&nbsp;Productor:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;' . $descripcionprod . '</font></td></tr>
		<tr nobr="true"><th align="left"><font size="10">&nbsp;&nbsp;Locaci&oacute;n:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;' . $descripcionloc . '</font></td></tr>
		<tr nobr="true"><th align="left"><font size="10">&nbsp;&nbsp;Tipo evento:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;' . $descripcionteven . '</font></td></tr>
		<tr nobr="true"><th align="left"><font size="10">&nbsp;&nbsp;Tipo traje:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;' . $descripciontraje . '</font></td></tr>
		<tr nobr="true"><th align="left"><font size="10">&nbsp;&nbsp;Estatus:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;' . $descripestatus . '</font></td></tr>

	    </table>
	    <table  align="center" border="1" >
		<tr nobr="true" >
		<th align="left"><font size="10">&nbsp;Citaci&oacute;n:&nbsp;</font><font size="8">' . $descripcitacion . '</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Fecha:&nbsp;</font><font size="8">' . $fechacitacion . '</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Hora:&nbsp;</font><font size="8">' . $horacitacion . '</font></th>
		</tr>

		<tr nobr="true" >
		<th align="left"><font size="10">&nbsp;Montaje:&nbsp;</font><font size="8">' . $descripmontaje . '</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Fecha:&nbsp;</font><font size="8">' . $fechamontaje . '</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Hora:&nbsp;</font><font size="8">' . $horamontaje . '</font></th>
		</tr>

		<tr nobr="true" >
		<th align="left"><font size="10">&nbsp;Emisi&oacute;n:&nbsp;</font><font size="8">' . $descripemision . '</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Fecha:&nbsp;</font><font size="8">' . $fechaemision . '</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Hora:&nbsp;</font><font size="8">' . $horaemision . '</font></th>
		</tr>
		' . $descripretorno . '
	    </table>
        <table  align="center" border="0" ><tr nobr="true" ><td colspan="3"></td></tr></table>
		' . $asignados . '
        <table  align="center" border="0" ><tr nobr="true" ><td colspan="3"></td></tr></table>
        ' . $materiales . '
		<br>
		<br>
		<br>
		<br>
        <table  align="center" border="0" >
		<tr nobr="true" ><td style="width: 250px;">Aprobado por:</td></tr>
		</table>
		<br>
        <table  align="center" border="0" >
        <tr nobr="true" ><td align="center" >__________________</td></tr>
        <tr nobr="true" ><td align="center">'.$nomb.' '.$apellido.'</td></tr>
        <tr nobr="true" ><td align="center">C.I:'.$aprobado.'</td></tr>
        <tr nobr="true" ><td align="center">'.$cargo.'</td></tr>
        </table>
        ';
    $pdf->Cell(10);
    $pdf->writeHTML(utf8_encode($body), true, 0, true, 0);
    $pdf->Output("Reporte_de_asignacion_pauta'.$id_pauta.'.pdf", 'I');

?>