<?php

header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
ini_set('memory_limit', '9999999999999999999M');
require_once('tcpdf/tcpdf.php');


require_once("../class/bd/classbdConsultas.php");
require_once("../librerias/classlibFecHor.php");

class reporte extends TCPDF {

    public $conect_sistemas_vtv;
    public $ObjConsulta;
    public $Objfechahora;
    public $registros;
    public $almacenista;
    public $receptor;

    function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
        $this->conect_sistemas_vtv = "../database/archi_conex/sistemas_vtv_5431";
        $this->ObjConsulta = new classbdConsultas();
        $this->Objfechahora = new classlibFecHor();
    }

    function header() {

        // $this->registros = $this->ObjConsulta->select_data_asignacionplani($this->conect_sistemas_vtv, $_GET['idasignacion']);
        //fix array
        //  $this->registros = array_merge($this->registros);
        //$this->registros = array_map("array_merge", $this->registros);

        if ($this->registros[0][1] == "") {
            $this->registros[0][1] = "NO EMPLEADO";
        }


        $this->SetFont('', '', 10);


        $this->almacenista = utf8_encode($this->registros[0][1]);
        $this->receptor = utf8_encode($this->registros[0][2]);

        $titulo1 = "MODIFICACIONES";

        $htmltable = '<table border="0" width="650px" cellspacing="4">
	  <tr>

		<td width="160px" rowspan="3"><img src="imagenes/logo_vtv.jpg" style="width: 173px; height: 72px;" alt="logo"/></td>
		<!--<div align="center"><font size="10">FECHA: <b>' . date("d/m/Y H:i:s") . '</b></font></div>-->
		<td ><br /><br /><div align="center"><font size="10"><b>' . $titulo1 . '</b></font></div></td>
		<!--<td width="180px"><div align="left"><font size="10">Nº. <b>' . str_pad($_GET['idasignacion'], 10, 0, STR_PAD_LEFT) . '</b></font></div></td>-->
	 </tr>
	 </table>

';
        $fechaimp = date("d/m/Y H:i:s");

        // echo $htmltable;
        $this->writeHTML($htmltable);

        //$this->Image('../imagenes/bandera.jpg','', $this->GetY()-3, 168);
    }

    function footer() {
        $this->SetFont('', '', 6);
        $this->Ln(3);
        $this->Cell(0, 0, 'FECHA:' . date("d/m/Y H:i:s") . '', 0, 0, 'L');
    }

    function renderizarimagetofile($url, $name, $path="imagenes/") {
        if (($f = fopen($url, 'r')) != false) {
            fclose($f);
            $res = join(file($url));
            if (($f = fopen($path . $name . ".png", "w")) != false) {
                fwrite($f, $res);
                fclose($f);
            }
        }
    }

}

//$pdf2=new MEM_IMAGE();
$pdf = new reporte(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', PDF_HEADER_STRING);//PDF_HEADER_TITLE
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'LISTADO CONSTANCIAS DE TRABAJO DEL '.$desde.' AL '.$hasta, PDF_HEADER_STRING);//PDF_HEADER_TITLE
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(15, 38, 20);
$pdf->SetHeaderMargin(15);
$pdf->SetFooterMargin(20);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

$pdf->AddPage('P');
require_once('tcpdf/htmlcolors.php');
$pdf->Ln(2);

//print_r($pdf->registros);
$cedula = $_GET['cedula'];
$pauta = $_GET['id_pauta'];

$idspauta = $pdf->ObjConsulta->selectidspauta($pdf->conect_sistemas_vtv, $pauta);
$id_pauta = $idspauta[1][1];
$id_tipo_pauta = $idspauta[1][2];
$id_locacion = $idspauta[1][3];
$id_tipo_traje = $idspauta[1][4];
$id_program = $idspauta[1][5];
$id_citacion = $idspauta[1][6];
$id_montaje = $idspauta[1][7];
$id_emision_grabacion = $idspauta[1][8];
$id_retorno = $idspauta[1][9];
$id_tipo_evento = $idspauta[1][10];
//$id_evento=$idspauta[1][11];
$user_reg = $idspauta[1][11];
$lugar_pauta = $idspauta[1][12];
$descripciontevenbd = $idspauta[1][13];


$descprograma = $pdf->ObjConsulta->selectdescprograma($pdf->conect_sistemas_vtv, $id_program);
$idprograma = $descprograma[1][1];
$descripcionprog = ucwords($descprograma[1][2]);

$desctipoevento=$pdf->ObjConsulta-> selectdesctipoevento($pdf->conect_sistemas_vtv,$id_tipo_evento);
$idtipoevento=$desctipoevento[1][1];
$descripcionteven=ucwords ($desctipoevento[1][2]);

$desclocacion = $pdf->ObjConsulta->selectdesclocacion($pdf->conect_sistemas_vtv, $id_locacion);
$idlocacion = $desclocacion[1][1];
$descripcionloc = ucwords($desclocacion[1][2]);

$desctipotraje = $pdf->ObjConsulta->selectdesctipotraje($pdf->conect_sistemas_vtv, $id_tipo_traje);
$descripciontraje = strtoupper($desctipotraje[1][1]);

$descproductor = $pdf->ObjConsulta->selectusuariof5($pdf->conect_sistemas_vtv, $user_reg);
$nombre = ucwords(strtoupper($descproductor[1][1]));
$apellido = ucwords(strtoupper($descproductor[1][2]));
$descripcionprod = $nombre . " " . $apellido;

$datoscitacion = $pdf->ObjConsulta->selectcitacion($pdf->conect_sistemas_vtv, $id_citacion);
$idcitacion = $datoscitacion[1][1];
$fechacitacion = $datoscitacion[1][2];
$horacitacion = $datoscitacion[1][3];
$id_lugar_citacion = $datoscitacion[1][4];
$fechacitacion = $pdf->Objfechahora->flibInvertirInEs($fechacitacion);

$lugarcitacion = $pdf->ObjConsulta->selectlugar($pdf->conect_sistemas_vtv, $id_lugar_citacion);
$lugarcitacion = $lugarcitacion[1][2];
$descripcitacion = ucwords($lugarcitacion);

$datosmontaje = $pdf->ObjConsulta->selectmontaje($pdf->conect_sistemas_vtv, $id_montaje);
$idmontaje = $datosmontaje[1][1];
$fechamontaje = $datosmontaje[1][2];
$horamontaje = $datosmontaje[1][3];
$id_lugar_montaje = $datosmontaje[1][4];
$fechamontaje = $pdf->Objfechahora->flibInvertirInEs($fechamontaje);
$descripmontaje=ucwords ($lugar_pauta);

$datosemision = $pdf->ObjConsulta->selectemision($pdf->conect_sistemas_vtv, $id_emision_grabacion);
$idemision = $datosemision[1][1];
$fechaemision = $datosemision[1][2];
$horaemision = $datosemision[1][3];
$id_lugar_emision = $datosemision[1][4];
$fechaemision = $pdf->Objfechahora->flibInvertirInEs($fechaemision);
$descripemision=ucwords ($lugar_pauta);

$datosretorno = $pdf->ObjConsulta->selectretorno($pdf->conect_sistemas_vtv, $id_retorno);
$idretorno = $datosretorno[1][1];
$fecharetorno = $datosretorno[1][2];
$horaretorno = $datosretorno[1][3];
$id_lugar_retorno = $datosretorno[1][4];
$fecharetorno = $pdf->Objfechahora->flibInvertirInEs($fecharetorno);
$descripretorno=ucwords ($lugar_pauta);

///////////////////////////////////////////////////////////////////////////////////////////////

if ($idlocacion == '2') {
    $descripretorno = "";
} else {
    $descripretorno = '
		<tr nobr="true" >
		<th align="left"><font size="10">&nbsp;Retorno:&nbsp;</font><font size="8">' . $descripretorno . '</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Fecha:&nbsp;</font><font size="8">' . $fecharetorno . '</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Hora:&nbsp;</font><font size="8">' . $horaretorno . '</font></th>
		</tr>
		';
}


/////////////////////////////////////////////////////////////////////////////////////////////////
if($id_program==0)
{$evento='<th align="left"><font size="10">&nbsp;&nbsp;Evento:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;'.$descripciontevenbd.'</font></td>';}else if($id_program>=1)
{$evento='<th align="left"><font size="10">&nbsp;&nbsp;Programa:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;'.$descripcionprog.'</font></td>';}

$verificarestatuspauta = $pdf->ObjConsulta->verificarestatuspauta($pdf->conect_sistemas_vtv, $id_pauta);
$estatusactual = $verificarestatuspauta[1][1];

$descestatus = $pdf->ObjConsulta->selectdescestatus($pdf->conect_sistemas_vtv, $estatusactual);
$descripestatus = ucwords($descestatus[1][1]);


$est_sol = 13;
$est_sol2 = 20;
$est_elim = 15;
$est_agre_ual = 17;
$est_asig_ual = 14;

////////////////////////////LISTA DE RECURSOS SOLICITADOS //////////////////////////////
$datoslista = $pdf->ObjConsulta->selectlistmatsolicitados($pdf->conect_sistemas_vtv, $pauta);
$contsol= count($datoslista);
if ($contsol != 0){
    foreach ($datoslista as $llave => $valor) {
        $id_recurso = $valor[1];
        $cantidad = $valor[2];
        $id_tipo_rec = $valor[3];
        if($id_tipo_rec==1){
            $datosmaterial=$pdf->ObjConsulta->selectlistmatdescnuevo($pdf->conect_sistemas_vtv,$id_recurso);
            $desc_id_recurso=$datosmaterial[1][1];
        }else{
            $datosmaterial = $pdf->ObjConsulta->select_cargos($pdf->conect_sistemas_vtv, $id_recurso);
            $desc_id_recurso=$datosmaterial[1][2];
        }
        $solprod2.='<tr nobr="true"><td align="center"><font size="8">' . $cantidad . '</font></td><td align="left"><font size="8">' . $desc_id_recurso . '</font></td></tr>';
    }
}else{
    $solprod2.='<tr nobr="true"><td  align="center" colspan="2"><div align="center"><font size="8">No se a solicitado nig&uacute;n recurso</font></div></td></tr>';
}
$solprod = '
        <table align="center" border="1" >
            <tr nobr="true"><th colspan="2"  bgcolor="#8B0000"><font size="10"color="white"><b>RECURSOS SOLICITADOS</b></font></th></tr>
            <tr  nobr="true"><th><div align="center"><font size="10">Cantidad</font></div></th><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th></tr>
            ' . $solprod2 . '
        </table>';
//////////////////////////LISTA DE RECURSOS ELIMINADOS POR EL PROD//////////////////////////////

$datoslista3 = $pdf->ObjConsulta->selectlistmatprod($pdf->conect_sistemas_vtv, $pauta, $est_elim);
$countelim = count($datoslista3);
foreach ($datoslista3 as $llave3 => $valor3) {
    $id_detalle_servicio = $valor3[1];
    $fecha = $valor3[2];
    $fecha = $pdf->Objfechahora->flibInvertirInEs($fecha);
    $user_exp= $valor3[3];

    $datosuser2 = $pdf->ObjConsulta->selectnombreyapellidof5($pdf->conect_sistemas_vtv, $user_exp);
    $nombre = $datosuser2[1][1];
    $apellido = $datosuser2[1][2];

    $idsmatelimprod = $pdf->ObjConsulta->selectidsmatelim($pdf->conect_sistemas_vtv, $id_detalle_servicio);
    $id_recurso = $idsmatelimprod[1][1];
    $id_recurso_asignado = $idsmatelimprod[1][2];

    /*
    $datosmaterial = $pdf->ObjConsulta->selectlistmatdesc($pdf->conect_sistemas_vtv, $id_recurso);
    $desc_id_recurso = strtoupper($datosmaterial[1][1]);
    */
    $id_tipo_rec = $idsmatelimprod[3];
    if($id_tipo_rec==1){
        $datosmaterial=$pdf->ObjConsulta->selectlistmatdescnuevo($pdf->conect_sistemas_vtv,$id_recurso);
        $desc_id_recurso=$datosmaterial[1][1];
    }else{
        $datosmaterial = $pdf->ObjConsulta->select_cargos($pdf->conect_sistemas_vtv, $id_recurso);
        $desc_id_recurso=$datosmaterial[1][2];
    }


    $desc_caracteristicas = "Eliminado";
    $elimprod2.='<tr nobr="true"><td align="left"><font size="8">' . $desc_id_recurso . '</font></td><td align="center"><font size="8">' . $desc_caracteristicas . '</font></td><td align="center"><font size="8">' . $nombre . ' '. $apellido .'</font></td><td align="center"><font size="8">' . $fecha . '</font></td></tr>';
}
if ($countelim == 0) {
    $elimprod2 = '<tr nobr="true"><td  align="center" colspan="4"><div align="center"><font size="8">No elimin&oacute; ning&uacute;n recurso</font></div></td></tr>';
}
$titulo3 = "RECURSOS ELIMINADOS ";
$elimprod = '
		<table align="center" border="1" >
			<tr nobr="true"><th colspan="4"  bgcolor="#8B0000"><font size="10"color="white"><b>' . $titulo3 . '</b></font></th></tr>
            <tr  nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Detalle</font></div></th><th><div align="center"><font size="10">Por</font></div></th><th><div align="center"><font size="10">Fecha</font></div></th></tr>
			' . $elimprod2 . '
		</table>';
////////////////////////////LISTA DE RECURSOS AGREGADOS POR LA UAL //////////////////////////////


$datoslista4 = $pdf->ObjConsulta->selectlistmatprod($pdf->conect_sistemas_vtv, $pauta, $est_agre_ual);
$countagreual = count($datoslista4);
foreach ($datoslista4 as $llave4 => $valor4) {
    $id_detalle_servicio = $valor4[1];
    $fecha = $valor4[2];
    $fecha = $pdf->Objfechahora->flibInvertirInEs($fecha);
    $user_exp= $valor4[3];

    $datosuser3 = $pdf->ObjConsulta->selectnombreyapellidof5($pdf->conect_sistemas_vtv, $user_exp);
    $nombre = $datosuser3[1][1];
    $apellido = $datosuser3[1][2];

    $idsmatelimprod = $pdf->ObjConsulta->selectidsmat($pdf->conect_sistemas_vtv, $id_detalle_servicio);
    $id_recurso = $idsmatelimprod[1][1];
    $id_recurso_asignado = $idsmatelimprod[1][2];
    $id_tipo_rec = $idsmatelimprod[3];
    if($id_tipo_rec==1){
        $datosmaterial=$pdf->ObjConsulta->selectlistmatdescnuevo($pdf->conect_sistemas_vtv,$id_recurso);
        $desc_id_recurso=$datosmaterial[1][1];
    }else{
        $datosmaterial = $pdf->ObjConsulta->select_cargos($pdf->conect_sistemas_vtv, $id_recurso);
        $desc_id_recurso=$datosmaterial[1][2];
    }

    $desc_caracteristicas = "Agregado";

    $agreual2.='<tr nobr="true"><td align="left"><font size="8">' . $desc_id_recurso . '</font></td><td align="center"><font size="8">' . $desc_caracteristicas . '</font></td><td align="center"><font size="8">' . $nombre . ' '. $apellido .'</font></td><td align="center"><font size="8">' . $fecha . '</font></td></tr>';
}

if ($countagreual == 0) {
    $agreual2 = '<tr nobr="true"><td  align="center" colspan="4"><div align="center"><font size="8">No agreg&oacute; ning&uacute;n recurso</font></div></td></tr>';
}

$titulo4 = "RECURSOS AGREGADOS EN LA UNIDAD DE APOYO LOGISTICO";
$agreual = '
		<table align="center" border="1" >
			<tr nobr="true"><th colspan="4"  bgcolor="#8B0000"><font size="10"color="white"><b>' . $titulo4 . '</b></font></th></tr>
			<tr  nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Detalle</font></div></th><th><div align="center"><font size="10">Por</font></div></th><th><div align="center"><font size="10">Fecha</font></div></th></tr>
			' . $agreual2 . '
		</table>';

/////////////////////////////LISTA DE RECURSOS ELIMINADOS UAL///////////////////////////////////////
$datosideliminados = $pdf->ObjConsulta->selectlistdeeliminados($pdf->conect_sistemas_vtv, $pauta);
$countelimual = count($datosideliminados);
foreach ($datosideliminados as $llave6 => $valor6) {
    $id_detalle_servicio = $valor6[1];
    $fecha = $valor6[2];
    $fecha = $pdf->Objfechahora->flibInvertirInEs($fecha);
    $user_exp= $valor6[3];

    $datosuser5 = $pdf->ObjConsulta->selectnombreyapellidof5($pdf->conect_sistemas_vtv, $user_exp);
    $nombre = $datosuser5[1][1];
    $apellido = $datosuser5[1][2];

    $datosreceliminado = $pdf->ObjConsulta->selectlistaeliminados($pdf->conect_sistemas_vtv, $id_detalle_servicio);
    $id_recursos = $datosreceliminado[1][1];
    $id_tipo_rec = $idsmatelimprod[1][2];
    if($id_tipo_rec==1){
        $datosmaterial=$pdf->ObjConsulta->selectlistmatdescnuevo($pdf->conect_sistemas_vtv,$id_recurso);
        $desc_id_recurso=$datosmaterial[1][1];
    }else{
        $datosmaterial = $pdf->ObjConsulta->select_cargos($pdf->conect_sistemas_vtv, $id_recurso);
        $desc_id_recurso=$datosmaterial[1][2];
    }


    /*$datosmatelim = $pdf->ObjConsulta->selectlistmatdesc($pdf->conect_sistemas_vtv, $id_recursos);
    $desc_id_recursos = strtoupper($datosmatelim[1][1]);*/

    $desc_caracteristicas = "Eliminado";
    $eliminados2.='<tr nobr="true"><td  align="left"><font size="8">' . strtoupper($desc_id_recursos) . '</font></td><td align="center"><font size="8">' . $desc_caracteristicas . '</font></td><td align="center"><font size="8">' . $nombre . ' '. $apellido .'</font></td><td  align="center"><font size="8">' . $fecha . '</font></td></tr>';
}

if ($countelimual == 0) {
    $eliminados2 = '<tr nobr="true"><td  align="center" colspan="4"><div align="center"><font size="8">No elimin&oacute; ning&uacute;n recurso</font></div></td></tr>';
}
$titulo6 = "RECURSOS ELIMINADOS EN LA UNIDAD DE APOYO LOGISTICO";
$eliminados = '
        <table align="center" border="1">
            <tr nobr="true"><th colspan="4"  bgcolor="#8B0000"><font size="10"color="white"><b>' . $titulo6 . '</b></font></th></tr>
            <tr  nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Detalle</font></div></th><th><div align="center"><font size="10">Por</font></div></th><th><div align="center"><font size="10">Fecha</font></div></th></tr>
            ' . $eliminados2 . '
        </table>
        ';
//////////////////////////LISTA DE RECURSOS ASIGNADOS/////////////////////////
$datoslista5 = $pdf->ObjConsulta->selectlistdeasig($pdf->conect_sistemas_vtv, $pauta, $est_asig_ual);
$countasig = count($datoslista5);
//echo "$datoslista5";
foreach ($datoslista5 as $llave5 => $valor5) {
    $id_detalle_servicio = $valor5[1];
    $id_recurso_asignado = $valor5[2];
    $fecha = $valor5[3];
    $fecha = $pdf->Objfechahora->flibInvertirInEs($fecha);
    $user_exp= $valor5[4];

    $datosuser4 = $pdf->ObjConsulta->selectnombreyapellidof5($pdf->conect_sistemas_vtv, $user_exp);
    $nombre = $datosuser4[1][1];
    $apellido = $datosuser4[1][2];

    $primero = "";

    $idsmatelimprod = $pdf->ObjConsulta->selectidsmat($pdf->conect_sistemas_vtv, $id_detalle_servicio);
    $id_recurso = $idsmatelimprod[1][1];

    $degerencia= $pdf->ObjConsulta->selectidgerencia($pdf->conect_sistemas_vtv, $id_recurso);
    $gerencia_rec = $degerencia[1][1];


    /*$datosmaterial = $pdf->ObjConsulta->selectlistmatdesc($pdf->conect_sistemas_vtv, $id_recurso);
    $desc_id_recurso = strtoupper($datosmaterial[1][1]);*/

    $datoscaract = $pdf->ObjConsulta->selectdatoscedula($pdf->conect_sistemas_vtv, $id_recurso_asignado);
    $primero = $datoscaract[1][1];

    if ($primero == "") {
        $datoscaract4 = $pdf->ObjConsulta->selectdatosserial($pdf->conect_sistemas_vtv, $id_recurso_asignado);
        $primero = $datoscaract4[1][1];
        $datoscaract5 = $pdf->ObjConsulta->selectdatosmarca($pdf->conect_sistemas_vtv, $id_recurso_asignado);
        $segundo = $datoscaract5[1][1];
        $datoscaract6 = $pdf->ObjConsulta->selectdatosagregados($pdf->conect_sistemas_vtv, $id_recurso_asignado);
        $tercero = $datoscaract6[1][1];

        if ($primero == "") {
            $datoscaract7 = $pdf->ObjConsulta->selectdatosbiennac($pdf->conect_sistemas_vtv, $id_recurso_asignado);
            $primero = $datoscaract7[1][1];
        }

        if ($primero != "") {
            $primero = "Serial: " . $primero . "";
        }
        if ($segundo != "") {
            $segundo = "Marca: " . $segundo . "";
        }
        if ($tercero != "") {
            $tercero = "Agregado: " . $tercero . "";
        }

        $desc_caracteristicas = "" . $primero . "&nbsp;" . $segundo . "&nbsp;" . $tercero . " ";
    } else {

        $datoscaract2 = $pdf->ObjConsulta->selectdatospnombre($pdf->conect_sistemas_vtv, $id_recurso_asignado);
        $segundo = $datoscaract2[1][1];

        $datoscaract3 = $pdf->ObjConsulta->selectdatospapellido($pdf->conect_sistemas_vtv, $id_recurso_asignado);
        $tercero = $datoscaract3[1][1];

        $primero = "Cedula: " . $primero . "";
        $segundo = "Nombre: " . $segundo . "";
        $tercero = "Apellido: " . $tercero . "";

        $desc_caracteristicas = "" . $primero . "<br />" . $segundo . "<br />" . $tercero . " ";
    }

    $asignados2.='<tr nobr="true"><td><font size="8">' . $desc_id_recurso . '</font></td><td align="left"><font size="8">' . $desc_caracteristicas . '</font></td><td align="center"><font size="8">' . $nombre . ' ' . $apellido . '</font></td><td><font size="8">' . $fecha . '</font></td><td><font size="8">' . $gerencia_rec . '</font></td></tr>';
}
if ($countasig == 0) {
    $asignados2 = '<tr nobr="true"><td  align="center" colspan="5"><div align="center"><font size="8">No asign&oacute; ning&uacute;n recurso</font></div></td></tr>';
}
$titulo5 = "RECURSOS ASIGNADOS EN LA UNIDAD DE APOYO LOGISTICO";
$asignados = '
		<table align="center" border="1" >
			<tr nobr="true"><th colspan="5"  bgcolor="#8B0000"><font size="10"color="white"><b>' . $titulo5 . '</b></font></th></tr>
			<tr  nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Detalle</font></div></th><th><div align="center"><font size="10">Por</font></div></th><th><div align="center"><font size="10">Fecha</font></div></th><th><div align="center"><font size="10">Gerencia</font></div></th></tr>
			' . $asignados2 . '
		</table>';


////////////////////////////////LISTA DE RECURSOS NO ASIGNADOS//////////////////////////////////////////

        $datoslista7 = $pdf->ObjConsulta->selectrecnoasig($pdf->conect_sistemas_vtv,$pauta);
        $countnoasig= count ($datoslista7);
        foreach($datoslista7 as $llave7 => $valor7){
            $id_recurso=$valor7[1];
            $cantidad=$valor7[2];

            /*$datosmaterial=$pdf->ObjConsulta->selectlistmatdesc($pdf->conect_sistemas_vtv,$id_recurso);
            $desc_id_recurso=$datosmaterial[1][1];*/

            $noasignados2.='<tr nobr="true"><td align="left"><font size="8">'.$desc_id_recurso.'</font></td><td  align="center"><font size="8">'.$cantidad.'</font></td></tr>';

            }

            if ($countnoasig == 0) {
            $noasignados2 = '<tr nobr="true"><td  align="center" colspan="2"><div align="center"><font size="8">Todos los recursos fueron asignados</font></div></td></tr>';
            }

            $tablanoasignados = '<table align="center" border="1" >
                        <tr nobr="true"><th colspan="2"  bgcolor="#8B0000"><font size="10"color="white"><b>RECURSOS SIN ASIGNAR</b></font></th></tr>
                        <tr nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Cantidad</font></div></th></tr>
                        '.$noasignados2.'
                        </table>';


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$body = '
<table align="center" border="1">
		<tr nobr="true"><th colspan="2"  bgcolor="#8B0000"><font size="10" color="white"><b>DATOS GENERALES</b></font></th></tr>
		<tr nobr="true"><th align="left" width="100px"><font size="10">&nbsp;&nbsp;N&deg; de pauta:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;' . $id_pauta . '</font></td></tr>
		<tr nobr="true">' . $evento . '</tr>
		<tr nobr="true"><th align="left"><font size="10">&nbsp;&nbsp;Productor:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;' . $descripcionprod . '</font></td></tr>
		<tr nobr="true"><th align="left"><font size="10">&nbsp;&nbsp;Locaci&oacute;n:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;' . $descripcionloc . '</font></td></tr>
		<tr nobr="true"><th align="left"><font size="10">&nbsp;&nbsp;Tipo evento:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;' . $descripcionteven . '</font></td></tr>
		<tr nobr="true"><th align="left"><font size="10">&nbsp;&nbsp;Tipo traje:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;' . $descripciontraje . '</font></td></tr>
		<tr nobr="true"><th align="left"><font size="10">&nbsp;&nbsp;Estatus:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;' . $descripestatus . '</font></td></tr>

	</table>
	<table  align="center" border="1" >
		<tr nobr="true" >
		<th align="left"><font size="10">&nbsp;Citaci&oacute;n:&nbsp;</font><font size="8">' . $descripcitacion . '</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Fecha:&nbsp;</font><font size="8">' . $fechacitacion . '</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Hora:&nbsp;</font><font size="8">' . $horacitacion . '</font></th>
		</tr>

		<tr nobr="true" >
		<th align="left"><font size="10">&nbsp;Montaje:&nbsp;</font><font size="8">' . $descripmontaje . '</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Fecha:&nbsp;</font><font size="8">' . $fechamontaje . '</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Hora:&nbsp;</font><font size="8">' . $horamontaje . '</font></th>
		</tr>

		<tr nobr="true" >
		<th align="left"><font size="10">&nbsp;Emisi&oacute;n:&nbsp;</font><font size="8">' . $descripemision . '</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Fecha:&nbsp;</font><font size="8">' . $fechaemision . '</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Hora:&nbsp;</font><font size="8">' . $horaemision . '</font></th>
		</tr>
		' . $descripretorno . '

	</table>

<table  align="center" border="0" ><tr nobr="true" ><td colspan="3"></td></tr></table>
		' . $solprod . '
<table  align="center" border="0" ><tr nobr="true" ><td colspan="3"></td></tr></table>

		' . $elimprod . '
<table  align="center" border="0" ><tr nobr="true" ><td colspan="3"></td></tr></table>

		' . $agreual . '
<table  align="center" border="0" ><tr nobr="true" ><td colspan="3"></td></tr></table>

        ' . $eliminados . '
<table  align="center" border="0" ><tr nobr="true" ><td colspan="3"></td></tr></table>

		' . $asignados . '
<table  align="center" border="0" ><tr nobr="true" ><td colspan="2"></td></tr></table>

'.$tablanoasignados.'



';
//echo $_GET['idasignacion'];
//$regmat = $pdf->ObjConsulta->select_equiposasignacion($pdf->conect_sistemas_vtv, $_GET['idasignacion']);
//$regmat = array_merge($regmat);
//$regmat = array_map(array_merge, $regmat);
//echo $body;

$pdf->Cell(10);
$pdf->writeHTML(utf8_encode($body), true, 0, true, 0);
$pdf->Output("Reporte_de_modificaciones_pauta'.$id_pauta.'.pdf", 'I');
?>