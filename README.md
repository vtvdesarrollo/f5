Sistema de Facilidades Tecnicas 2.0b 
============

![procesof5](https://bytebucket.org/vtvdesarrollo/f5/raw/22ee8550d16c9d4c72c7b64df39a2aa1183230c7/docs/ProcesoF5-VTV.png "proceso F5-VTV")



DATOS DE LA VERSIÓN
-------------
* Es una versión Beta
* Esta versión cuenta con un inventario propio de recursos humanos
* Posee una integración con el sistema de inventario de equipos (sistema almacén técnico)

REQUISITOS
-------------
* APACHE O NGINX a su ultima version estable
* PHP >= 5.3.27 o <= 5.5.9
* POSTGRES >= 8.4 o <= 9.3.5

CONTRIBUYE
-------------
ayudarnos es muy facil:

> Si no sabes programar: 
>> puedes comunicarte con nosotros y nos ayudas a documentarlo.

> Si sabes programar:
>> Solo tienes que hacer un fork del proyecto y enviarnos tus pull request, nosotros lo evaluamos y lo ponemos en la proxima version...

> Si detectas errores en el sistema te invitamos a que registres una incidencia [Aquí](https://bitbucket.org/vtvdesarrollo/f5/issues/new "Aquí") 

Versiones
-------------
* Actualmente se están realizando mejoras, temporalmente se actualiza la versión cada 2 dias.


##### Para información adicional comunicarse a través de [desarrollotic@vtv.gob.ve](mailto:desarrollotic@vtv.gob.ve) #####