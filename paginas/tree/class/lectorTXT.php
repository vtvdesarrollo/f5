<?php
class leerDatosTXT{
	var $LeerDatosTXT;
	
	function __construct(){
	}
	
	function leerTxT($archivo,$separador){
		$gestor = @fopen($archivo, "r");
		if ($gestor) {
			while (!feof($gestor)) {
				$bufer = fgets($gestor, 4096);
				$this->LeerDatosTXT[]=explode($separador,$bufer);
			}
			fclose ($gestor);
		}
		return $this->LeerDatosTXT;
	}
	
	function __destruct(){
		//liberar memoria
		unset($this->LeerDatosTXT);
	}
}
?>