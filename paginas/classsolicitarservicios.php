<?php
//ini_set("error_reporting","E_ALL & ~E_NOTICE");
require("classDirectorioFunciones.php");
class classasignarrecursos{
    public $html;
    function __construct(){
        $this->ObjclasslibSession = new classlibSession();
        $this->ObjCabPie=new classlibCabPie("Solicitar servicios","");
        $this->ObjOther=new classOtherMenu();
        $this->ObjMensaje=new classMensaje("","mostrar");
        $this->classDirectorioFunciones = new classDirectorioFunciones(true);
        $ficherosjs = "
            <script type='text/javascript' src='../class/other/classjavascript.js'></script>
            <script type='text/javascript' src='../librerias/datepick/jquery.datepick.pack.js'></script>
        <script type='text/javascript' src='../librerias/datepick/jquery.datepick-es.js'></script>
        <link rel='stylesheet' href='../librerias/datepick/jquery.datepick.css' type='text/css' media='screen' charset='utf-8' />
        <link rel='stylesheet' href='../css/f5.css' type='text/css' media='screen' charset='utf-8' />
                <script type='text/javascript'>
                $(document).ready(function(){
                    reload_serviciosrequeridos(".$_GET['id_pauta'].");
                })
                </script>";
        $administrador=$_SESSION['id_tipo_usuario'];
        if(isset($_SESSION['cedula'])){
            $this->htm = $this->ObjCabPie->flibHtmCab(0, $ficherosjs, '', $this->ObjOther->fomArregloAsocia2($administrador), 0, "");
        }else{
            echo"<script>var pagina='classRegistro.php';
            alert('Disculpa la session ha expirado, debe iniciar sesion nuevamente.');
            function redireccionar() {
            location.href=pagina;
            }
            setTimeout ('redireccionar()', 0);
            </script>
            ";
        }
    }

    function modulo($modulo, $arg){
        if ($_SESSION['id_tipo_usuario'] == 26 or $_SESSION['id_tipo_usuario'] == 21 or $_SESSION['id_tipo_usuario'] == 22) {
            $this->htm.= $this->classDirectorioFunciones->$modulo();
        }
    }

    function __destruct(){
        if(isset($_SESSION['cedula'])){
            $this->htm.=$this->ObjCabPie->flibCerrarHtm("");
            echo $this->htm;
        }
    }
}

$classasignarrecursos = new classasignarrecursos();
$classasignarrecursos->modulo("pag_solicitarservicios", $arg);
?>