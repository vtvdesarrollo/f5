<?php
$ObjBienvenida = new classRegistro();
class classRegistro {
	function classRegistro(){
		// Librerias Comunes
		require("../librerias/classlibCabPie.php");
		// Libreria de bd
		require("../class/bd/classbdConsultas.php");
		// Clase Other
		require("../class/other/classOtherMenu.php");
		// Clase Interfaz
		require("../librerias/classlibSession.php");
		require("../class/interfaz/classMensaje.php");
		$this->ObjCabPie = new classlibCabPie("INGRESO DE USUARIO","classRegistro.php");
		$this->ObjclassMensaje = new classMensaje();
		$this->ObjConsulta = new classbdConsultas();
		$this->ObjSesion = new classlibSession();
		$this->cargarpagina();
	}

	function cargarpagina(){
		$ficherosjs.="<script type='text/javascript' src='../class/other/classjavascript.js'></script>";
		$cedula=$_SESSION['cedula'];
		$id_tipo_usuario=$_SESSION['id_tipo_usuario'];
		$nombres=$_SESSION['nombres'];
		$apellidos=$_SESSION['apellidos'];
		unset($cedula);
		unset($nombres);
		unset($apellidos);
		unset($id_tipo_usuario);
		session_destroy();
		$parametros_cookies = session_get_cookie_params();
	    setcookie(session_name(),0,1,$parametros_cookies["path"]);

		if(isset($_GET['error'])){
			$error= "<table align='center' width='100%'><tr><td align='center'>
			<div style='color: #ff0000;font-weight: bold;' align='center' id='div_error'><br>Usuario o Clave incorrecta<div></td></tr></table>";
		}else{
			$error="";
		}

		$htm =$this->ObjCabPie->flibHtmCab(0,$ficherosjs,'',"",0,"");
		$htm.= $this->ObjclassMensaje->interfazRegistro($error);
		$htm.=$this->ObjCabPie->flibCerrarHtm("");
		echo $htm;
	}
}
?>