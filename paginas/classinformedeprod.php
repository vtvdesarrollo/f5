<?php
//ini_set("error_reporting","E_ALL & ~E_NOTICE");
require("classDirectorioFunciones.php");
class mostrar{
    public $htm;
    function __construct(){
        $this->ObjclasslibSession = new classlibSession();
        $this->ObjCabPie=new classlibCabPie("INFORME DE PRODUCCI&Oacute;N","");
        $this->ObjOther=new classOtherMenu();
        $this->ObjMensaje=new classMensaje("","mostrar");
        $this->classDirectorioFunciones = new classDirectorioFunciones(true);
        $ficherosjs = "
        <script type='text/javascript' src='../class/other/ms-Dropdown/msdropdown/js/uncompressed.jquery.dd.js'></script>
        <script type='text/javascript' src='../class/other/classjavascript.js'></script>
        <script type='text/javascript' src='../class/other/asignacionesdraganddrop.js'></script>
        <script src='../class/other/jquery.tools.min.js' type='text/javascript'></script>
        <script type='text/javascript' src='../librerias/datepick/jquery.datepick.pack.js'></script>
        <script type='text/javascript' src='../librerias/datepick/jquery.datepick-es.js'></script>
        <link rel='stylesheet' type='text/css' href='../class/other/ms-Dropdown/msdropdown/dd.css' />
        <link rel='stylesheet' href='../librerias/datepick/jquery.datepick.css' type='text/css' media='screen' charset='utf-8' />
        <!-- <link href='../css/overlay-apple.css' rel='stylesheet' type='text/css' /> -->
        <link href='../css/vtvtheme/jquery-ui.css' rel='stylesheet' type='text/css' />
        <link href='../css/cuadrodrag.css' rel='stylesheet' type='text/css' />
        <link rel='stylesheet' href='../css/f5.css' type='text/css' media='screen' charset='utf-8' />
        <script src=\"../librerias/jquery-ui.js\" type=\"text/javascript\"></script>
        <script type=\"text/javascript\">

        $(document).ready(function() {
            try {
                //oHandler = $(\"select [name*=\"caracteristicas\"]\").msDropDown({mainCSS:'dd2'}).data(\"dd\");
                //alert($.msDropDown.version);
                $.msDropDown.create(\"body select\");
                //$(\"#ver\").html($.msDropDown.version);
            } catch(e) {
                alert(\"Error: \"+e.message);
            }
        })

        </script>";

        $administrador=$_SESSION['id_tipo_usuario'];
        if(isset($_SESSION['cedula'])){
            $this->htm = $this->ObjCabPie->flibHtmCab(0, $ficherosjs, '', $this->ObjOther->fomArregloAsocia2($administrador), 0, "");
        }else{
            echo"<script>var pagina='classRegistro.php';
            alert('Disculpa la session ha expirado, debe iniciar sesion nuevamente.');
            function redireccionar() {
                location.href=pagina;
            }
            setTimeout ('redireccionar()', 0);
            </script>";
        }
    }

    function modulo($modulo){
        $this->htm.= $this->classDirectorioFunciones->$modulo();
    }

    function modulo_param($modulo){
        $this->htm.= $this->classDirectorioFunciones->$modulo($_GET['id_pauta']);
    }



    function __destruct(){
        if(isset($_SESSION['cedula'])){
            $botonA = "<input type=\"button\" class='boton' value=\"Enviar\" OnClick=enviarsolicitud3(".$_GET['id_pauta'].");>";
            $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadeinformes');>";
            $this->htm.="<table class='tabla'>
            <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;" . $botonC . "</div></tr>
            </table>";

            $this->htm.=$this->ObjCabPie->flibCerrarHtm("");
            echo $this->htm;
        }
    }
}
$pauta = new mostrar();
$pauta->modulo("datos_pauta");
$pauta->modulo_param("materiales_no_justificados");
$pauta->modulo_param("materiales_justificados");



?>