<?php
//ini_set("error_reporting","E_ALL & ~E_NOTICE");
$classagregar = new classagregar();


function select_operadora($n_object){
    /*return "<select id='".$n_object."' name='".$n_object."'>
            <option value='+58416' selected='selected'>0416</opcion>
            <option value='+58412'>0412</opcion>
            <option value='+58414'>0414</opcion>
            </select>";*/
    return "<input type='text' name='".$n_object."' id='".$n_object."' class='campo' size='5' maxlength='4'/>";
}


class classagregar {

    function classagregar() {
        //Librerias comunes
        require("../librerias/classlibCabPie.php");
        // Libreria de bd
        require("../class/bd/classbdConsultas.php");
        // Clase Other
        require("../class/other/classOtherMenu.php");
        // Clase Interfaz
        require("../librerias/classlibSession.php");
        require("../class/interfaz/classMensaje.php");
        ////////////////////////////////////////////////////
        $this->ObjclasslibSession = new classlibSession();
        $this->conect_sistemas_vtv = "../database/archi_conex/sistemas_vtv_5431";


        if (isset($_SESSION['cedula'])) {
            $this->cargarPagina();
        } else {
            echo"<script>var pagina='classRegistro.php';
			alert('Disculpa la session ha expirado, debe iniciar sesion nuevamente.');
			function redireccionar() {
			location.href=pagina;
			}
			setTimeout ('redireccionar()', 0);
			</script>
			";
        }
    }

    function cargarPagina() {

        $ficherosjs = "
			<script type='text/javascript' src='../class/other/classjavascript.js'></script>";

        $this->ObjCabPie = new classlibCabPie("AGREGAR USUARIO", "");
        $this->ObjOther = new classOtherMenu();
        $this->ObjMensaje = new classMensaje("", "mostrar");
        $this->ObjclasslibSession = new classlibSession();
        $this->ObjConsulta = new classbdConsultas();
        $cedula = $_SESSION['cedula'];
        $administrador = $_SESSION['id_tipo_usuario'];
        $nombres = $_SESSION['nombres'];
        $apellidos = $_SESSION['apellidos'];
        $gerencia = $_SESSION['gerencia'];
        $division = $_SESSION['division'];

        if ($administrador == 20 or $administrador == 27) {

            if ($administrador == 20) {

                if ($division == 132) {
                    $tipousuario = "<select id='tipousuario' onChange='combotipousuario()'>
                        <option value=0 selected='selected' >Seleccione &nbsp;</option>
                        <option value='24'>Anal&iacute;sta UAL</option>";
                } else{
                $tipousuario = "<select id='tipousuario' onChange='combotipousuario()'>
		           <option value='0' selected='selected' >Seleccione &nbsp;</option>
                   <option value='20'>Administrador</option>
			       <option value='22'>Jefe de &aacute;rea</option>
                   <option value='25'>Anal&iacute;sta AO</option>
                   <option value='26'>Productor</option></select>";
               }
            } else {

                $tipousuario = "<select id='tipousuario' onChange='combotipousuario()'>
			             <option value=0 selected='selected' >Seleccione &nbsp;</option>
                        <option value='27'>Super Administrador</option>
                        <option value='20'>Administrador</option>
			             <option value='22'>Jefe de &aacute;rea</option>
                        <option value='24'>Anal&iacute;sta UAL</option>
                        <option value='25'>Anal&iacute;sta AO</option>
                        <option value='26'>Productor</option></select>";

            }

            $nombre = "<input type='text' name='nombre' id='nombre' class='campo' size='25'/>";
            $apellido = "<input type='text' name='apellido' id='apellido' class='campo' size='25'/>";
            $cedula = "<input type='text' name='cedula' id='cedula' class='campo' size='8' maxlength='8'/>";
            $gerencia = "<select id='idgerencia' name='idgerencia' onchange='get_combodivision(this);'>
                <option value='0' selected='selected' >Seleccione &nbsp;</option>";
            $data_gerencias = $this->ObjConsulta->selectgerencia($this->conect_sistemas_vtv);
            foreach ($data_gerencias as $llave => $valor) {
                $gerencia.="<option value='{$valor[1]}'>{$valor[2]}</opcion>";
            }
            $gerencia.="</select>";

            $telefono1 = select_operadora('op_telefono1')."
            <input type='text' name='telefono1' id='telefono1' class='campo' onkeypress='return validaN(event)' size='11' maxlength='11'/>";
            $telefono2 = select_operadora('op_telefono2')."<input type='text' name='telefono2' id='telefono2' class='campo' onkeypress='return validaN(event)' size='11' maxlength='11'/>";
            $correo = "<input type='text' name='correo' id='correo' class='campo' size='40' maxlength='40'/>";
            $clave1 = "<input type='password' name='clave1' id='clave1' class='campo' maxlength='6'/>";
            $clave2 = "<input type='password' name='clave2' id='clave2' class='campo' maxlength='6'/>";
            $botonA = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick=guardarusuario_f5();>";
            $pag=$_GET['pag'];
            if ($pag==1){
                $lista = 'classlistadeusuarios.php';
            }
            else{
                $lista = 'classbienvenida.php';
            }
            $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('".$lista."');>";
            $botonB = "<input type=\"button\" class='boton' value=\"Buscar\" OnClick=buscarpersona();>";

            $titulo = "AGREGAR USUARIO";
            $htm = $this->ObjCabPie->flibHtmCab(0, $ficherosjs, '', $this->ObjOther->fomArregloAsocia2($administrador), 0, "");
            if ($administrador == 27) {
                $htm.=$this->ObjMensaje->interfazadmagregar($tipousuario, $nombre, $apellido, $cedula, $telefono1, $telefono2, $correo, $clave1, $clave2, $titulo, $botonA, $botonC, $firma, $gerencia);
            } else {
                $htm.=$this->ObjMensaje->interfazadmagregar($tipousuario, $nombre, $apellido, $cedula, $telefono1, $telefono2, $correo, $clave1, $clave2, $titulo, $botonA, $botonC, $firma);
            }
            $htm.=$this->ObjCabPie->flibCerrarHtm("");
            echo $htm;
        } else {
            echo"<script>var pagina='classRegistro.php';
			alert('Disculpa no tiene permitido el acceso a esta pagina.');
			function redireccionar() {
			location.href=pagina;
			}
			setTimeout ('redireccionar()', 0);
			</script>
			";
        }
    }
}

?>