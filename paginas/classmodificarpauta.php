<?php
//ini_set("error_reporting","E_ALL & ~E_NOTICE");
require("classDirectorioFunciones.php");
$classmodificarpauta = new classmodificarpauta();

class classmodificarpauta {

    function classmodificarpauta() {
        $this->ObjclasslibSession = new classlibSession();
        $this->conect_sistemas_vtv = "../database/archi_conex/sistemas_vtv_5431";


        if (isset($_SESSION['cedula'])) {
            $this->cargarPagina();
        } else {
            echo"<script>var pagina='classRegistro.php';
			alert('Disculpa la session ha expirado, debe iniciar sesion nuevamente.');
			function redireccionar() {
			location.href=pagina;
			}
			setTimeout ('redireccionar()', 0);
			</script>
			";
        }
    }

    function cargarPagina() {
        $ficherosjs = "
			<script type='text/javascript' src='../class/other/classjavascript.js'></script>
			<script type='text/javascript' src='../librerias/datepick/jquery.datepick.pack.js'></script>
		<script type='text/javascript' src='../librerias/datepick/jquery.datepick-es.js'></script>
		<link rel='stylesheet' href='../librerias/datepick/jquery.datepick.css' type='text/css' media='screen' charset='utf-8' />
         <link rel='stylesheet' href='../css/f5.css' type='text/css' media='screen' charset='utf-8' />
			";

        $this->ObjCabPie = new classlibCabPie("SOLICITUD DE FACILIDADES T&Eacute;CNICAS", "");
        $this->ObjOther = new classOtherMenu();
        $this->ObjMensaje = new classMensaje("", "mostrar");
        $this->ObjclasslibSession = new classlibSession();
        $this->ObjConsulta = new classbdConsultas();
		//$this->ObjConsultaSqlServer = new classbdConsultasSqlServer();
		$this->Objfechahora = new classlibFecHor();
        $this->classDirectorioFunciones = new classDirectorioFunciones(true);

        $cedula = $_SESSION['cedula'];
        $administrador = $_SESSION['id_tipo_usuario'];
        $nombres = $_SESSION['nombres'];
        $apellidos = $_SESSION['apellidos'];
		$id_pauta=$_GET['id_pauta'];
        $datosbdpauta = $this->ObjConsulta->selectdatospauta($this->conect_sistemas_vtv,$id_pauta);

	    $bd_tipo_pauta=$datosbdpauta[1][1];
		$bd_nombre_programa=$datosbdpauta[1][2];
		$bd_nombre_evento=$datosbdpauta[1][3];
		$bd_productor=$datosbdpauta[1][4];
		$bd_pauta_locacion=$datosbdpauta[1][5];
		$bd_tipo_evento=$datosbdpauta[1][6];
		$bd_pauta_traje=$datosbdpauta[1][7];
		$bd_citacion_lugar=$datosbdpauta[1][8];
		$bd_fecha_citacion=$datosbdpauta[1][9];
		$bd_fecha_citacion = $this->Objfechahora->flibInvertirInEs($bd_fecha_citacion);
		$bd_hora_citacion=$datosbdpauta[1][10];
	list($hora_citacion, $minuto_citacion, $seg_citacion) = explode(":", $bd_hora_citacion);
		$bd_fecha_montaje=$datosbdpauta[1][11];
		$bd_fecha_montaje = $this->Objfechahora->flibInvertirInEs($bd_fecha_montaje);
		$bd_hora_montaje=$datosbdpauta[1][12];
	list($hora_montaje, $minuto_montaje, $seg_montaje) = explode(":", $bd_hora_montaje);
		$bd_fecha_emision=$datosbdpauta[1][13];
		$bd_fecha_emision = $this->Objfechahora->flibInvertirInEs($bd_fecha_emision);
		$bd_hora_emision=$datosbdpauta[1][14];
	list($hora_emision, $minuto_emision, $seg_emision) = explode(":", $bd_hora_emision);
		$bd_fecha_retorno=$datosbdpauta[1][15];
		$bd_fecha_retorno = $this->Objfechahora->flibInvertirInEs($bd_fecha_retorno);
		$bd_hora_retorno=$datosbdpauta[1][16];
	list($hora_retorno, $minuto_retorno, $seg_retorno) = explode(":", $bd_hora_retorno);
		$bd_lugar_pauta=$datosbdpauta[1][17];


        if (($administrador == 26) ||($administrador == 22)){
			$uno=1;
            $datospauta = $this->ObjConsulta->selectpauta($this->conect_sistemas_vtv);
            $pauta = "<select id='pauta' name='pauta' style='width:150px;' onChange='combopauta_f5()' size='".(count($datospauta)+$uno)."'>";
            $pauta.="<option value='0' selected >Seleccione </option>";
            foreach ($datospauta as $llave => $valor) {
				if ($bd_tipo_pauta==$valor[2]){
				$selected='selected';
				}else{$selected='';}

                $pauta.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
            }
            $pauta.="</SELECT> ";

		if ($bd_tipo_pauta == 'PROGRAMA') {
            $datosprograma = $this->ObjConsulta->selectprograma($this->conect_sistemas_vtv);
            $evento_programa_ = "<select id='programa' name='programa' style='width:350px;'>";
            $evento_programa_.="<option value='0' selected >Seleccione </option>";
            foreach ($datosprograma as $llave => $valor) {
                if ($valor[1] > 0) {
					if ($bd_nombre_programa==$valor[2]){
					$selected='selected';
					}else{$selected='';}
                    $evento_programa_.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
                }
            }
            $evento_programa_.="</SELECT> ";
        }
        if ($bd_tipo_pauta == 'EVENTO') {
            $evento_programa_= "<input type='text' name='evento' id='evento' size='30'  maxlength='40' class='campo' style='width:350px; height:10px;margin:0px; padding:0;' value='".$bd_nombre_evento."'>";
        }



            $datoslocacion = $this->ObjConsulta->selectlocacion($this->conect_sistemas_vtv);
            $locacion = "<select id='locacion' name='locacion' style='width:150px;' size='".(count($datoslocacion)+$uno)."' onChange='combolocacion_f5()'>";
            $locacion.="<option value='0' selected >Seleccione </option>";
            foreach ($datoslocacion as $llave => $valor) {
				if ($bd_pauta_locacion==$valor[2]){
				$selected='selected';
				}else{$selected='';}
                $locacion.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
            }
            $locacion.="</SELECT> ";

            $datotevento = $this->ObjConsulta->selecttipoevento($this->conect_sistemas_vtv);
            $tipoevento = "<select id='tipoevento' name='tipoevento' style='width:150px;' onChange='cambianomenclatura(this);' size='".(count($datotevento)+$uno)."'>";
            $tipoevento.="<option value='0' selected >Seleccione </option>";
            foreach ($datotevento as $llave => $valor) {
				if ($bd_tipo_evento==$valor[2]){
				$selected='selected';
				}else{$selected='';}
                $tipoevento.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
            }
            $tipoevento.="</SELECT> ";

            $datostraje = $this->ObjConsulta->selecttipotraje($this->conect_sistemas_vtv);
            $tipotraje = "<select id='tipotraje' name='tipotraje' style='width:150px;' size='".(count($datostraje)+$uno)."'>";
            $tipotraje.="<option value='0' selected >Seleccione </option>";
            foreach ($datostraje as $llave => $valor) {
				if ($bd_pauta_traje==$valor[2]){
				$selected='selected';
				}else{$selected='';}
                $tipotraje.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
            }
            $tipotraje.="</SELECT> ";
			/////////ojoooooo////////////////////////
			/////////aqui verificar biennnn////////////
			if($bd_pauta_locacion=='REMOTO'){
		   $lugar_pt_lef = "<input type='text' name='lugar_pt' id='lugar_pt' class='campo' size='20'  maxlength='30' style='width:250px;' value='".$bd_lugar_pauta."'>";
			}
			if($bd_pauta_locacion=='ESTUDIO'){
			$datoslugar2 = $this->ObjConsulta->selectlugar2($this->conect_sistemas_vtv,"");
            $lugar_lef = "<select id='lugar_lef' name='lugar_lef' style='width:250px;'>";
            $lugar_lef.="<option value='0' selected >Seleccione </option>";
            foreach ($datoslugar2 as $llave => $valor) {
				if ($bd_lugar_pauta==$valor[2]){
				$selected='selected';
				$va=1;
				}else{$selected='';}
                $lugar_lef.="<option value='" . $valor[2] . "' $selected >" . $valor[2] . "</option>";
            }
            $lugar_lef.="</SELECT> ";
			}




			$datoslugarcitacionlef = $this->ObjConsulta->selectlugar2($this->conect_sistemas_vtv,2);
            $lugar_citacion_lef = "<select id='lugar_citacion_lef' name='lugar_citacion_lef' style='width:150px;'>";
            $lugar_citacion_lef.="<option value='0' selected >Seleccione </option>";
            foreach ($datoslugarcitacionlef as $llave => $valor) {
				if ($bd_citacion_lugar==$valor[2]){
				$selected='selected';
				$va=1;
				}else{$selected='';}
                $lugar_citacion_lef.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
            }
            $lugar_citacion_lef.="</SELECT> ";

            $fecha1 = "<input id='fecha1' name='fecha1'  readonly='readonly' size='10' class='campo_vl' value='" .$bd_fecha_citacion. "'>";

            $hora1 = "<select name='hora1' id='hora1' onChange='validarhoras(this);' class='campo_vl'>";
            $hora1.="<option value='24' selected='selected'>Hora&nbsp;</option>";
            for ($e = 0; $e <= 23; $e++) {
                if ($e <= 9) {$cero = 0;} else {$cero = "";}
				if ($hora_citacion==$e){
				$selected='selected';
				}else{$selected='';}
                $hora1.="<option value='$e' $selected>$cero$e&nbsp;</option>\n";
            }
            $hora1.="</select>";

            $min1 = "<select name='min1' id='min1' onChange='validarhoras(this);' class='campo_vl'>";
            $min1.="<option value='60' selected='selected'>Min&nbsp;</option>";
            for ($e = 00; $e <= 59; $e++) {
                if ($e <= 9) {$cero = 0;} else {$cero = "";}
				if ($minuto_citacion==$e){
				$selected='selected';
				}else{$selected='';}
	            $min1.="<option value='$e' $selected>$cero$e&nbsp;</option>\n";
            }
            $min1.="</select>";

           $fecha2 = "<input id='fecha2' name='fecha2'  readonly='readonly' size='10' class='campo_vl' value='" .$bd_fecha_montaje. "'>";

            $hora2 = "<select name='hora2' id='hora2' onChange='validarhoras(this);' class='campo_vl'>";
            $hora2.="<option value='24' selected='selected'>Hora&nbsp;</option>";
            for ($e = 0; $e <= 23; $e++) {
                if ($e <= 9) {$cero = 0;} else {$cero = "";}
				if ($hora_montaje==$e){
				$selected='selected';
				}else{$selected='';}
                $hora2.="<option value='$e' $selected>$cero$e&nbsp;</option>\n";
            }
            $hora2.="</select>";

            $min2 = "<select name='min2' id='min2' onChange='validarhoras(this);' class='campo_vl'>";
            $min2.="<option value='60' selected='selected'>Min&nbsp;</option>";
            for ($e = 00; $e <= 59; $e++) {
                if ($e <= 9) {$cero = 0;} else {$cero = "";}
				if ($minuto_montaje==$e){
				$selected='selected';
				}else{$selected='';}
                $min2.="<option value='$e' $selected>$cero$e&nbsp;</option>\n";
            }
            $min2.="</select>";


            $fecha3 = "<input id='fecha3' name='fecha3'  readonly='readonly' size='10' class='campo_vl' value='".$bd_fecha_emision."'>";

            $hora3 = "<select name='hora3' id='hora3' onChange='validarhoras(this);' class='campo_vl'>";
            $hora3.="<option value='24' selected='selected'>Hora&nbsp;</option>";
            for ($e = 0; $e <= 23; $e++) {
                if ($e <= 9) {$cero = 0;} else {$cero = "";}
				if ($hora_emision==$e){
				$selected='selected';
				}else{$selected='';}
                $hora3.="<option value='$e' $selected>$cero$e&nbsp;</option>\n";
            }
            $hora3.="</select>";

            $min3 = "<select name='min3' id='min3' onChange='validarhoras(this);' class='campo_vl'>";
            $min3.="<option value='60' selected='selected'>Min&nbsp;</option>";
            for ($e = 00; $e <= 59; $e++) {
                if ($e <= 9) {$cero = 0;} else {$cero = "";}
				if ($minuto_emision==$e){
				$selected='selected';
				}else{$selected='';}
                $min3.="<option value='$e' $selected>$cero$e&nbsp;</option>\n";
            }
            $min3.="</select>";

            $fecha4 = "<input id='fecha4' name='fecha4'  readonly='readonly' size='10' class='campo_vl' value='" .$bd_fecha_retorno. "'>";

            $hora4 = "<select name='hora4' id='hora4' onChange='validarhoras(this);' class='campo_vl'>";
            $hora4.="<option value='24' selected='selected'>Hora&nbsp;</option>";
            for ($e = 0; $e <= 23; $e++) {
                if ($e <= 9) {$cero = 0;} else {$cero = "";}
				if ($hora_retorno==$e){
				$selected='selected';
				}else{$selected='';}
                $hora4.="<option value='$e' $selected>$cero$e&nbsp;</option>\n";
            }
            $hora4.="</select>";

            $min4 = "<select name='min4' id='min4' onChange='validarhoras(this);' class='campo_vl'>";
            $min4.="<option value='60' selected='selected'>Min&nbsp;</option>";
            for ($e = 00; $e <= 59; $e++) {
                if ($e <= 9) {$cero = 0;} else {$cero = "";}
				if ($minuto_retorno==$e){
				$selected='selected';
				}else{$selected='';}
                $min4.="<option value='$e' $selected>$cero$e&nbsp;</option>\n";
            }
            $min4.="</select>";

            $pagina = '4';
            $pasos = $this->classDirectorioFunciones->pasosdelproceso($pagina);

            $botonA = "<input type=\"button\" class='boton' value=\"Modificar\" OnClick='modificarpauta_f5()';><input name='id_pauta' id='id_pauta' type='hidden' value=".$_GET['id_pauta'].">";
            $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick='ir_a(\"{$_GET['lista']}\")'>";
            $botonB = "<input type=\"button\" class='boton' value=\"Buscar\" OnClick=buscarpersona();>";
            $titulo = "MODIFICACI&Oacute;N DE FACILIDADES T&Eacute;CNICAS";


            $htm = $this->ObjCabPie->flibHtmCab(0, $ficherosjs, '', $this->ObjOther->fomArregloAsocia2($administrador), 0, "");
            $htm.=$this->ObjMensaje->interfazpauta_f5_moficar($pauta, $locacion, $tipotraje, $tipoevento, $hora1, $min1, $hora2, $min2, $hora3, $min3, $hora4, $min4, $fecha1, $fecha2, $fecha3, $fecha4,$lugar_lef,$lugar_citacion_lef,$lugar_pt_lef ,$evento_programa_, $titulo, $botonA, $botonC, $pasos);
            $htm.=$this->ObjCabPie->flibCerrarHtm("");
            echo $htm;
        } else {
            echo"<script>var pagina='classRegistro.php';
			alert('Disculpa no tiene permitido el acceso a esta pagina.');
			function redireccionar() {
			location.href=pagina;
			}
			setTimeout ('redireccionar()', 0);
			</script>
			";
        }
    }

}
?>
<script type="text/javascript">
    /*$(document).ready(function(){
        //configure the date format to match mysql date
        //$('#date').datepick({dateFormat: 'yy-mm-dd'},$.datepick.regional['es']);
        $('#fecha').datepick($.extend({showStatus: true,
    showOn: 'both', buttonImageOnly: true, buttonImage: '../estilos/imagenes/estatus/calendar.png',
    altField: '#fecha'},
    $.datepick.regional['es']));

});*/
    $('#fecha1').datepick({beforeShow: customRange,
        showOn: 'both', buttonImageOnly: true, buttonImage: '../estilos/imagenes/estatus/calendar.png', onSelect:validadorfechas});
    $('#fecha2').datepick({beforeShow: customRange_2,
        showOn: 'both', buttonImageOnly: true, buttonImage: '../estilos/imagenes/estatus/calendar.png', onSelect:validadorfechas});
    $('#fecha3').datepick({beforeShow: customRange_3,
        showOn: 'both', buttonImageOnly: true, buttonImage: '../estilos/imagenes/estatus/calendar.png', onSelect:validadorfechas});
    $('#fecha4').datepick({beforeShow: customRange_4,
        showOn: 'both', buttonImageOnly: true, buttonImage: '../estilos/imagenes/estatus/calendar.png' , onSelect:validadorfechas});

    function customRange(input) {
        //fecha actual..
        // alert ($("#fecha1").datepick("getDate"));
        return {minDate: "today"};
    }

    function customRange_2(input) {
        return {minDate: $("#fecha1").datepick("getDate")};
    }

    function customRange_3(input) {
        return {minDate: $("#fecha2").datepick("getDate")};
    }

    function customRange_4(input) {
        return {minDate: $("#fecha3").datepick("getDate")};
    }



    function validadorfechas(dateText, inst){
        //alert($(this).attr('id'));
        if($(this).attr('id')=="fecha1"){
            var fechaIni=$("#fecha1").val().split("/");
            var fechaFin1=$("#fecha2").val().split("/");
            var fechaFin2=$("#fecha3").val().split("/");
            var fechaFin3=$("#fecha4").val().split("/");
            var fechainicial=fechaIni[2]+fechaIni[1]+fechaIni[0];
            var fechafinal1=fechaFin1[2]+fechaFin1[1]+fechaFin1[0];
            var fechafinal2=fechaFin2[2]+fechaFin2[1]+fechaFin2[0];
            var fechafinal3=fechaFin3[2]+fechaFin3[1]+fechaFin3[0];
            if(parseInt(fechainicial)>parseInt(fechafinal1)){

                $("#fecha2").val($("#fecha1").val());
                if(parseInt(fechainicial)>parseInt(fechafinal2)){
                    $("#fecha3").val($("#fecha1").val());
                }
                if(parseInt(fechainicial)>parseInt(fechafinal3)){
                    $("#fecha4").val($("#fecha1").val());
                }

            }else if(parseInt(fechainicial)>parseInt(fechafinal2)){
                $("#fecha3").val($("#fecha1").val());
            }else if(parseInt(fechainicial)>parseInt(fechafinal3)){
                $("#fecha4").val($("#fecha1").val());
            }
        }

        if($(this).attr('id')=="fecha2"){
            var fechaIni=$("#fecha2").val().split("/");
            var fechaFin1=$("#fecha3").val().split("/");
            var fechaFin2=$("#fecha4").val().split("/");
            var fechainicial=fechaIni[2]+fechaIni[1]+fechaIni[0];
            var fechafinal1=fechaFin1[2]+fechaFin1[1]+fechaFin1[0];
            var fechafinal2=fechaFin2[2]+fechaFin2[1]+fechaFin2[0];
            if(parseInt(fechainicial)>parseInt(fechafinal1)){
                $("#fecha3").val($("#fecha2").val());
                if(parseInt(fechainicial)>parseInt(fechafinal2)){
                    $("#fecha4").val($("#fecha2").val());
                }

            }else if(parseInt(fechainicial)>parseInt(fechafinal2)){
                $("#fecha4").val($("#fecha2").val());
            }
        }

        if($(this).attr('id')=="fecha3"){
            var fechaIni=$("#fecha3").val().split("/");
            var fechaFin=$("#fecha4").val().split("/");
            var fechainicial=fechaIni[2]+fechaIni[1]+fechaIni[0];
            var fechafinal=fechaFin[2]+fechaFin[1]+fechaFin[0];
            if(parseInt(fechainicial)>parseInt(fechafinal)){
                $("#fecha4").val($("#fecha3").val());
            }
        }

        validarhoras($(this));

    }

    function validarhoras(obj){
        var fila=new Array();
        var fecha=new Array();
        var c=0;
        //$(obj).parents("tbody").find(".tr_fecha").each(function(){
		  $(obj).parents("tbody").find("tr").each(function(){

            fila[c]=new Array();
            fila[c]['hora']="";
            $(this).find("select[class='campo_vl']").each(function(){
                if($(this).val()!="60" && $(this).val()!="24"){
                    if(parseInt($(this).val())<10){
                        fila[c]['hora']=fila[c]['hora']+"0"+$(this).val()
                    }else{
                        fila[c]['hora']=fila[c]['hora']+$(this).val()
                    }
                }else{
                    fila[c]['hora']=fila[c]['hora']+"00"
                }
            })
            var fechaexplod=undefined;
            fechaexplod=$(this).find("input[name*='fecha']").val().split("/");
            fecha[c]=fechaexplod[2]+fechaexplod[1]+fechaexplod[0];
            c++;
        })

        if(parseInt(fecha[0]+fila[0]['hora'])>parseInt(fecha[1]+fila[1]['hora'])){
            $("#hora2").val($("#hora1").val());
            $("#min2").val($("#min1").val());
            if(parseInt(fecha[0]+fila[0]['hora'])>parseInt(fecha[2]+fila[2]['hora'])){
                $("#hora3").val($("#hora1").val());
                $("#min3").val($("#min1").val());
            }

            if(parseInt(fecha[0]+fila[0]['hora'])>parseInt(fecha[3]+fila[3]['hora'])){
                $("#hora4").val($("#hora1").val());
                $("#min4").val($("#min1").val());
            }
        }else if(parseInt(fecha[1]+fila[1]['hora'])>parseInt(fecha[2]+fila[2]['hora'])){
            $("#hora3").val($("#hora2").val());
            $("#min3").val($("#min2").val());
            if(parseInt(fecha[1]+fila[1]['hora'])>parseInt(fecha[3]+fila[3]['hora'])){
                $("#hora4").val($("#hora2").val());
                $("#min4").val($("#min2").val());
            }
        }else if(parseInt(fecha[2]+fila[2]['hora'])>parseInt(fecha[3]+fila[3]['hora'])){
            $("#hora4").val($("#hora3").val());
            $("#min4").val($("#min3").val());
        }
    }


    function cambianomenclatura(obj){
        if($(obj).val()=="1"){

            $("#grabado_div").fadeOut(function(){
                $("#envivo_div").fadeIn();
            })
        }else{
            $("#envivo_div").fadeOut(function(){
                $("#grabado_div").fadeIn();
            })
        }

    }

    function ir_a(url){
        window.top.location.href = url
    }
</script>
