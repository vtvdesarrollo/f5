<?php
/**
* librerias comunes...
*/

include("../conf.php");
include("../librerias/classlibSession.php");
include("../class/bd/classbdConsultas.php");
include("../class/interfaz/classMensaje.php");

//include("class.phpmailer.php"); //esta inluido en classdecolsultas.php

include("../librerias/classlibCabPie.php");
include("../librerias/classlibFecHor.php");
include("../class/other/classOtherMenu.php");

function modhb_implode($relleno, $array) {
	while ($data = current($array)) {
		$string[] = str_replace("_", " ", (key($array))) . ': ' . $data;
		next($array);
	}
	return (implode($relleno, $string));
}

class classDirectorioFunciones {
	var $prefix_table = "f5."; //esquerma DB
	function __construct($nocab=false){
		$this->ObjConsulta = new classbdConsultas();
		$this->ObjMensaje = new classMensaje("", "");
		$this->enviomail = new PHPMailer();
		$this->Objfechahora = new classlibFecHor();
		$this->conect_sistemas_vtv = "../database/archi_conex/sistemas_vtv_5431";
		/**
		*configuracion de correo..
		**/

		$this->enviomail->From = "f5@vtv.gob.ve";
		$this->enviomail->Host = '172.16.8.8';
		$this->enviomail->Mailer = "smtp";
		$this->enviomail->FromName = "Sistema F5";
		$this->enviomail->AltBody = "EL SERVICIO DE MENSAJERIA REQUERE UN MANEJADOR DE CORREO MODERNO";
		if($nocab==false){
			$this->ObjCabPie = new classlibCabPie("", "");
			$this->ObjSesion = new classlibSession();
			$this->Objfechahora = new classlibFecHor();
			if (isset($_GET['obtenerFuncion'])) {
				$this->$_GET['obtenerFuncion']();
			}else if (isset($_POST['obtenerFuncion'])) {
				$this->$_POST['obtenerFuncion']();
			}
		}
	}

	/**
	* Esta funcion se encarga de enviar correo de forma general.
	*/
	function enviarcorreo($correoaenviar, $datospauta, $asunto) {
		$this->enviomail->Subject = $asunto;
		$htmltable = $datospauta;
		$this->enviomail->Body = $htmltable;
		$this->enviomail->AddAddress($correoaenviar, "");
		if ($this->enviomail->Send()) {
			$this->enviomail->ClearAddresses();
		}
		$this->enviomail->ClearAttachments();
	}

	/**
	*esta funcion se encarga de manejar las peticiones de correo en ajax
	*/
	function correoenviado(){
		$pin = $_GET['pin'];
		$correo = $_GET['correo'];
		$tope = $_GET['tope'];
		$this->enviarcorreo($correo, $pin, $tope);
		$mensaje = "<div style='color: #009900;font-weight: bold;' align='center'><br>Correo Enviado Satisfactoriamente<div><br>";
		$tabla = "
		<table class='tabla'>
		<tr><td>" . $mensaje . "</td></tr>
		</table>";
		echo $tabla;
	}

	/**
	*esta funcion permite a los usuarios recordar la clave al correo electronico...
	*/
	function olvidocontrasena($cedula=null, $imprimemensaje=null, $noboton=null) {
		if($cedula==null){
			$cedula = $_GET['cedula'];
		}

		$claveusuario = $this->ObjConsulta->selectclave($this->conect_sistemas_vtv, $cedula);
		$clave = $claveusuario[1][1];

		if ($clave == "") {
			$javascript="<script>var pagina='classRegistro.php';
			alert('Disculpe la cedula ingresada no tiene acceso al sistema.');
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 0);
			</script>
			";
			if($imprimemensaje==null){
				echo $javascript;
			}else{
				return $javascript;
			}
		} else {
			$datocorreo = $this->ObjConsulta->selectdatocorreo($this->conect_sistemas_vtv, $cedula);
			$correo = $datocorreo[1][1];
			if ($correo == "") {
				$javascript="<script>var pagina='classRegistro.php';
				alert('Disculpe no tiene asociada una direccion de correo.');
				function redireccionar() {
					location.href=pagina;
				}
				setTimeout ('redireccionar()', 0);
				</script>
				";
				if($imprimemensaje==null){
					echo $javascript;
				}else{
					return $javascript;
				}
			} else {
				$datoscorreo = "Su clave usuario es: " . $clave . " , se le recomienda por seguridad cambiarla al ingresar al sistema.<br /> <br />Gerencia de Tecnolog&iacute;a de la Informaci&oacute;n y la Comunicaci&oacute;n.<br />
				Solicitudes de facilidades t&eacute;cnicas";
				$this->enviarcorreo($correo, $datoscorreo, "SU CLAVE AL CORREO");
				if($noboton==null){
					$botonA = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=CancelarRegresar('classRegistro.php');>";
				}
				$mensaje = "<div style='color: #009900;font-weight: bold;'><br>Su clave usuario se a enviado con exito a su e_mail: " . $correo . "<div><br>" . $botonA . "<br>";

				if($imprimemensaje==null){
					echo $this->ObjMensaje->InterfazExitosamente($mensaje);
				}else{
					return $this->ObjMensaje->InterfazExitosamente($mensaje);
				}
			}
		}
	}

	/**
	* esta funcion permite reiniciar la clave a los usuarios y a su vez envia un correo usando
	* los metodos de olvido de contraseña.
	*/
	function reiniciar_clave() {
		$cedula = $_GET['cedula'];
		$datosusuario = $this->ObjConsulta->selectusuariof5($this->conect_sistemas_vtv, $cedula);
		$nombre = $datosusuario[1][1];
		$apellido = $datosusuario[1][2];
		$reiniciarclave = $this->ObjConsulta->updateclavef5($this->conect_sistemas_vtv, $cedula);
		$olvidoclave=$this->olvidocontrasena($cedula, $imprimemensaje="imprime","sinboton");
		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La clave del usuario " . $nombre . "&nbsp;" . $apellido . " fue reiniciada con exito<div><br>".$olvidoclave;
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
		echo "<script>var pagina='classlista.php?modulo=listadeusuarios';
		function redireccionar() {
			location.href=pagina;
		}
		setTimeout ('redireccionar()', 3500);
		</script>";
	}

	function guardarusuario_f5() {
		$tipousuario = $_GET['tipousuario'];
		$nombre = $_GET['nombre'];
		$apellido = $_GET['apellido'];
		$cedula = $_GET['cedula'];
		$idgerencia = $_GET['idgerencia'];
		$iddivision = $_GET['iddivision'];
		$telefono1 = $_GET['telefono1'];
		$telefono2 = $_GET['telefono2'];
		$clave1 = $_GET['clave1'];
		$correo = $_GET['correo'];
		$ced_sesion = $_SESSION['cedula'];		
		$t_datospersonales = $this->ObjConsulta->selectexiste($this->conect_sistemas_vtv, $cedula);
		if ((count($t_datospersonales) > 0)) {
			if ($correo != "") {
				$updatecorreo = $this->ObjConsulta->updatecorreo($this->conect_sistemas_vtv, $cedula, $correo);
			}
			if ($telefono1 != "") {
				$updatetlf1bd = $this->ObjConsulta->updatetlf1($this->conect_sistemas_vtv, $cedula, $telefono1);
			}

			if ($telefono2 != "" and $telefono2 != "undefined") {
				$updatetlf2bd = $this->ObjConsulta->updatetlf2($this->conect_sistemas_vtv, $cedula, $telefono2);
			}
			$t_acceso = $this->ObjConsulta->selectexisteaccesof5($this->conect_sistemas_vtv, $cedula, id_aplicacion);
			if ((count($t_acceso) == 0)) {
				$insertclaveusuarios1 = $this->ObjConsulta->insertclaveusuarios($this->conect_sistemas_vtv, $cedula, $clave1, id_modulo, $ced_sesion, $tipousuario, $idgerencia, $iddivision);
			}

			$olvidoclave=$this->olvidocontrasena($cedula, $imprimemensaje="imprime","sinboton");
			$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El usuario " . $nombre . "&nbsp;" . $apellido . " fue actualizado con exito<div><br>".$olvidoclave;
			echo $this->ObjMensaje->InterfazExitosamente($mensaje);
			echo"<script>var pagina='classbienvenida.php';
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 3000);
			</script>";
		} else {
			//ingresar datos personales nuevos y agregar acceso.
			$insertusuarios1 = $this->ObjConsulta->insertusuarios($this->conect_sistemas_vtv, $nombre, $apellido, $cedula, $correo, $telefono1, $telefono2);
			//revisar acceso..
			$t_acceso = $this->ObjConsulta->selectexisteaccesof5($this->conect_sistemas_vtv, $cedula, id_aplicacion);
			if ((count($t_acceso) == 0)) {
				$insertclaveusuarios1 = $this->ObjConsulta->insertclaveusuarios($this->conect_sistemas_vtv, $cedula, $clave1, id_modulo, $ced_sesion, $tipousuario, $idgerencia, $iddivision);
			}

			$olvidoclave=$this->olvidocontrasena($cedula, $imprimemensaje="imprime","sinboton");
			$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El usuario " . $nombre . "&nbsp;" . $apellido . " fue ingresado con exito<div><br>".$olvidoclave;

			echo $this->ObjMensaje->InterfazExitosamente($mensaje);
			echo"<script>var pagina='classbienvenida.php';
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 3000);
			</script>";
		}
	}

	function eliminarusuario_f5() {
		$cedula = $_GET['cedula'];
		$datosusuario = $this->ObjConsulta->selectusuariof5($this->conect_sistemas_vtv, $cedula);
		$nombre = $datosusuario[1][1];
		$apellido = $datosusuario[1][2];
		$correo = $datosusuario[1][3];
		$elimusuario = $this->ObjConsulta->updateusuariof5($this->conect_sistemas_vtv, $cedula, id_aplicacion);
		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El usuario " . $nombre . "&nbsp;" . $apellido . " fue eliminado con exito<div><br>";
		$DatosCorreo = "Aviso de eliminacion de usuario del sistema de facilidades tecnicas, comuniquese con su administrador local.";
		$this->enviarcorreo($correo, $DatosCorreo, "Su usuario ha sido eliminado del sistema");
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);

		echo"<script>var pagina='classlista.php?modulo=listadeusuarios';
		function redireccionar() {
			location.href=pagina;
		}
		setTimeout ('redireccionar()', 3000);
		</script>";
	}

	function eliminartraje() {
		$id_tipo_traje = $_GET['id_tipo_traje'];
		$elimtraje = $this->ObjConsulta->updatedelistatraje($this->conect_sistemas_vtv, $id_tipo_traje);
		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>Traje eliminado<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);

		echo"<script>var pagina='classlista.php?modulo=pag_listatipotra';
		function redireccionar() {
			location.href=pagina;
		}
		setTimeout ('redireccionar()', 3000);
		</script>";
	}

	function eliminarprograma() {
		$id_programa= $_GET['id_programa'];
		$elimprog = $this->ObjConsulta->updatedelistaprog($this->conect_sistemas_vtv, $id_programa);
		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>Programa eliminado<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);

		echo"<script>var pagina='classlista.php?modulo=pag_listaprogra';
		function redireccionar() {
			location.href=pagina;
		}
		setTimeout ('redireccionar()', 3000);
		</script>";
	}

	function cambioperfil_f5() {
		$cedula = $_GET['cedula'];
		$perfilmod = $_GET['perfilmod'];
		$perfilact = $_GET['perfilact'];
		$datosusuario = $this->ObjConsulta->selectusuariof5($this->conect_sistemas_vtv, $cedula);
		$nombre = utf8_encode($datosusuario[1][1]);
		$apellido = utf8_encode($datosusuario[1][2]);
		$correo = $datosusuario[1][3];
		$cambiarperfil = $this->ObjConsulta->updatecambiarperfil($this->conect_sistemas_vtv, $cedula, $perfilact, $perfilmod);
		$descperfil = $this->ObjConsulta->selectdescperfil($this->conect_sistemas_vtv, $perfilmod);
		$desc = $descperfil[1][1];

		$DatosCorreo = "Aviso de cambio de perfil en el sistema, comuniquese con el administrador local y consulte el manual de sistema sobre las acciones que podra realizar sobre el perfil ". ucwords($desc);
		$this->enviarcorreo($correo, $DatosCorreo, "Aviso de cambio de perfil");
		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El nuevo perfil del usuario  " . $nombre . "&nbsp;" . $apellido . ", es: " . ucwords($desc) . "<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
		echo"<script>var pagina='classlista.php?modulo=listadeusuarios';
		function redireccionar() {
			location.href=pagina;
		}
		setTimeout ('redireccionar()', 3000);
		</script>
		";
	}

	function cambioclave() {
		if (isset($_SESSION['cedula'])) {
			$claveactual = $_GET['claveactual'];
			$clavenueva = $_GET['clavenueva'];
			$cedula = $_SESSION['cedula'];
			$datosusuario = $this->ObjConsulta->usuario($this->conect_sistemas_vtv, $cedula, $claveactual, id_aplicacion, id_modulo, "", "");
			if (count($datosusuario) > 0) {
				$id_acceso = $datosusuario[1][1];
				$cedula = $datosusuario[1][2];
				$clave = $datosusuario[1][3];
				$niv_con = $datosusuario[1][6];
				$niv_eli = $datosusuario[1][7];
				$niv_inc = $datosusuario[1][8];
				$niv_mod = $datosusuario[1][9];
				$ced_trans = $datosusuario[1][10];
				$fecha_trans = $datosusuario[1][11];
				$fecha_exp = $datosusuario[1][12];
				$id_tipo_usuario = $datosusuario[1][13];
				$id_submodulo = $datosusuario[1][14];
				$id_gerencia = $datosusuario[1][15];
				$id_division = $datosusuario[1][16];
				$updateexpirausuario = $this->ObjConsulta->updateexpirausuario($this->conect_sistemas_vtv, $cedula, $clave, id_aplicacion, id_modulo, $id_tipo_usuario, $id_submodulo);
				$insertusuario = $this->ObjConsulta->insertusuario($this->conect_sistemas_vtv, $cedula, $clavenueva, id_aplicacion, id_modulo, $id_tipo_usuario, "", $id_gerencia, $id_division);
				$olvidoclave=$this->olvidocontrasena($cedula, $imprimemensaje="imprime","sinboton");
				$mensaje = "<div style='color: #009900;font-weight: bold;'><br>Cambio de Clave Satisfactoria<div><br>".$olvidoclave;
				echo $this->ObjMensaje->InterfazExitosamente($mensaje);
				echo"<script>var pagina='classbienvenida.php';
				function redireccionar() {
					location.href=pagina;
				}
				setTimeout ('redireccionar()', 3000);
				</script>
				";
			} else {
				echo"<script>var pagina='classcambioclave.php';
				jAlert('Disculpa la clave actual es incorrecta');
				function redireccionar() {
					location.href=pagina;
				}
				setTimeout ('redireccionar()', 3000);
				</script>
				";
			}
		} else {
			echo"<script>var pagina='classRegistro.php';
			alert('Disculpa la session ha expirado, debe iniciar session nuevamente.');
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 0);
			</script>
			";
		}
	}

	function guardarpauta_f5() {
		$pauta = $_GET['pauta_aux'];
		$cedula_sesion = $_SESSION['cedula'];
		$programa = $_GET['programa_aux'];
		$id_evento = $_GET['evento_aux'];
		$locacion = $_GET['locacion_aux'];
		$lugar_pauta = $_GET['lugar_pauta'];
		$des_evento = $_GET['evento_aux'];
		$tipoevento = $_GET['tipoevento_aux'];
		$tipotraje = $_GET['tipotraje_aux'];
		$fecha1 = $_GET['fecha1_aux'];
		$fecha1 = $this->Objfechahora->flibInvertirEsIn($fecha1);
		$hora1 = $_GET['hora1_aux'];
		$min1 = $_GET['min1_aux'];
		$fecha2 = $_GET['fecha2_aux'];
		$fecha2 = $this->Objfechahora->flibInvertirEsIn($fecha2);
		$hora2 = $_GET['hora2_aux'];
		$min2 = $_GET['min2_aux'];
		$fecha3 = $_GET['fecha3_aux'];
		$fecha3 = $this->Objfechahora->flibInvertirEsIn($fecha3);
		$hora3 = $_GET['hora3_aux'];
		$min3 = $_GET['min3_aux'];
		$fecha4 = $_GET['fecha4_aux'];
		$fecha4 = $this->Objfechahora->flibInvertirEsIn($fecha4);
		$hora4 = $_GET['hora4_aux'];
		$min4 = $_GET['min4_aux'];
		if ($hora1 <= 9) {
			$cero = 0;
			$hora1 = $cero . $hora1;
		} else {
			$cero = "";
			$hora1 = $hora1;
		}
		if ($hora2 <= 9) {
			$cero = 0;
			$hora2 = $cero . $hora2;
		} else {
			$cero = "";
			$hora2 = $hora2;
		}
		if ($hora3 <= 9) {
			$cero = 0;
			$hora3 = $cero . $hora3;
		} else {
			$cero = "";
			$hora3 = $hora3;
		}
		if ($hora4 <= 9) {
			$cero = 0;
			$hora4 = $cero . $hora4;
		} else {
			$cero = "";
			$hora4 = $hora4;
		}
		if ($min1 <= 9) {
			$cero = 0;
			$min1 = $cero . $min1;
		} else {
			$cero = "";
			$min1 = $min1;
		}
		if ($min2 <= 9) {
			$cero = 0;
			$min2 = $cero . $min2;
		} else {
			$cero = "";
			$min2 = $min2;
		}
		if ($min3 <= 9) {
			$cero = 0;
			$min3 = $cero . $min3;
		} else {
			$cero = "";
			$min3 = $min3;
		}
		if ($min4 <= 9) {
			$cero = 0;
			$min4 = $cero . $min4;
		} else {
			$cero = "";
			$min4 = $min4;
		}
		$hora1 = $hora1 . ":" . $min1 . ":00";
		$hora2 = $hora2 . ":" . $min2 . ":00";
		$hora3 = $hora3 . ":" . $min3 . ":00";
		$hora4 = $hora4 . ":" . $min4 . ":00";
		if($_GET['cedulaproductor']!="undefined" and $_GET['cedulaproductor']!=""){
			$cedulaproductor = $_GET['cedulaproductor'];
		}else{
			$cedulaproductor = $_SESSION['cedula'];
		}
		$administrador = $_SESSION['id_tipo_usuario'];
		//bypassear el proceso envienao el nuevo f5 a la unidad de apoyo logistico.
		if ($administrador == 24) {
			$estatus = 6; //enviado a apoyo logistico..
		} else {
			if($_GET['cedulaproductor']!="undefined" and $_GET['cedulaproductor']!=""){
				$estatus = 3; //enviado al jefe de area...
			}else{
				$estatus = 1;
			}
		}
		$insertcitacion = $this->ObjConsulta->insertcitacion($this->conect_sistemas_vtv, $fecha1, $hora1, $_GET['lugar_citacion']);
		$id_citacion = $insertcitacion[0];
		$insertmontaje = $this->ObjConsulta->insertmontaje($this->conect_sistemas_vtv, $fecha2, $hora2, "");
		$id_montaje = $insertmontaje[0];
		$insertemision = $this->ObjConsulta->insertemision($this->conect_sistemas_vtv, $fecha3, $hora3, "");
		$id_emision = $insertemision[0];
		$insertretorno = $this->ObjConsulta->insertretorno($this->conect_sistemas_vtv, $fecha4, $hora4, "");
		$id_retorno = $insertretorno[0];
		$insertdatospauta = $this->ObjConsulta->insertdatospauta($this->conect_sistemas_vtv, $cedulaproductor, $pauta, $locacion, $tipotraje, $programa, $evento, $id_citacion, $id_montaje, $id_emision, $id_retorno, $tipoevento, $lugar_pauta, $des_evento);
		$id_pauta = $insertdatospauta[0];
		$insertestatus = $this->ObjConsulta->insertestatus($this->conect_sistemas_vtv, $id_pauta, $estatus, $cedula_sesion);
		if ($administrador == 24) {
			$mensaje = "classasignarrecursos.php?id_pauta=".$id_pauta."&lista=classlista.php?modulo=listadepautas";
		} elseif ($administrador == 22) {
			$mensaje = "classsolicitarservicios.php?id_pauta=".$id_pauta."&pagina=2&mod=2&lista=classlista.php?modulo=listadepautas";
		}else {
			$mensaje = "classsolicitarservicios.php?id_pauta=".$id_pauta."&pagina=2&mod=2&lista=classlista.php?modulo=listadepautas" ;
		}
		echo $mensaje;
	}

	function modificarpauta_f5() {
		$id_pauta = $_GET['id_pauta'];
		$pauta = $_GET['pauta_aux'];
		$cedula_sesion = $_SESSION['cedula'];
		$programa = $_GET['programa_aux'];
		$id_evento = $_GET['evento_aux'];
		$locacion = $_GET['locacion_aux'];
		$lugar_pauta = $_GET['lugar_pauta'];
		$des_evento = $_GET['evento_aux'];
		$tipoevento = $_GET['tipoevento_aux'];
		$tipotraje = $_GET['tipotraje_aux'];
		$fecha1 = $_GET['fecha1_aux'];
		$fecha1 = $this->Objfechahora->flibInvertirEsIn($fecha1);
		$hora1 = $_GET['hora1_aux'];
		$min1 = $_GET['min1_aux'];
		$fecha2 = $_GET['fecha2_aux'];
		$fecha2 = $this->Objfechahora->flibInvertirEsIn($fecha2);
		$hora2 = $_GET['hora2_aux'];
		$min2 = $_GET['min2_aux'];
		$fecha3 = $_GET['fecha3_aux'];
		$fecha3 = $this->Objfechahora->flibInvertirEsIn($fecha3);
		$hora3 = $_GET['hora3_aux'];
		$min3 = $_GET['min3_aux'];
		$fecha4 = $_GET['fecha4_aux'];
		$fecha4 = $this->Objfechahora->flibInvertirEsIn($fecha4);
		$hora4 = $_GET['hora4_aux'];
		$min4 = $_GET['min4_aux'];
		if ($hora1 <= 9) {
			$cero = 0;
			$hora1 = $cero . $hora1;
		} else {
			$cero = "";
			$hora1 = $hora1;
		}
		if ($hora2 <= 9) {
			$cero = 0;
			$hora2 = $cero . $hora2;
		} else {
			$cero = "";
			$hora2 = $hora2;
		}
		if ($hora3 <= 9) {
			$cero = 0;
			$hora3 = $cero . $hora3;
		} else {
			$cero = "";
			$hora3 = $hora3;
		}
		if ($hora4 <= 9) {
			$cero = 0;
			$hora4 = $cero . $hora4;
		} else {
			$cero = "";
			$hora4 = $hora4;
		}
		if ($min1 <= 9) {
			$cero = 0;
			$min1 = $cero . $min1;
		} else {
			$cero = "";
			$min1 = $min1;
		}
		if ($min2 <= 9) {
			$cero = 0;
			$min2 = $cero . $min2;
		} else {
			$cero = "";
			$min2 = $min2;
		}
		if ($min3 <= 9) {
			$cero = 0;
			$min3 = $cero . $min3;
		} else {
			$cero = "";
			$min3 = $min3;
		}
		if ($min4 <= 9) {
			$cero = 0;
			$min4 = $cero . $min4;
		} else {
			$cero = "";
			$min4 = $min4;
		}
		$hora1 = $hora1 . ":" . $min1 . ":00";
		$hora2 = $hora2 . ":" . $min2 . ":00";
		$hora3 = $hora3 . ":" . $min3 . ":00";
		$hora4 = $hora4 . ":" . $min4 . ":00";
		$datosbdpauta = $this->ObjConsulta->selectidspauta($this->conect_sistemas_vtv, $id_pauta);
		$id_pauta = $datosbdpauta[1][1];
		$id_tipo_pauta = $datosbdpauta[1][2];
		$id_locacion = $datosbdpauta[1][3];
		$id_tipo_traje = $datosbdpauta[1][4];
		$id_program = $datosbdpauta[1][5];
		$id_citacion = $datosbdpauta[1][6];
		$id_montaje = $datosbdpauta[1][7];
		$id_emision_grabacion = $datosbdpauta[1][8];
		$id_retorno = $datosbdpauta[1][9];
		$id_tipo_evento = $datosbdpauta[1][10];
		$lugar = $datosbdpauta[1][12];
		$descripcion = $datosbdpauta[1][13];
		$datosbdstatuspauta = $this->ObjConsulta->selectestatus($this->conect_sistemas_vtv, $id_pauta);
		$id_estatus = $datosbdstatuspauta[1][1];
		$datosobs=$this->ObjConsulta->selectobsrech($this->conect_sistemas_vtv, $id_pauta);
		$this->ObjConsulta->updatecitacion($this->conect_sistemas_vtv, $id_citacion, $fecha1, $hora1, $_GET['lugar_citacion']);
		$this->ObjConsulta->updatemontaje($this->conect_sistemas_vtv, $id_montaje, $fecha2, $hora2);
		$this->ObjConsulta->updateemision($this->conect_sistemas_vtv, $id_emision_grabacion, $fecha3, $hora3);
		$this->ObjConsulta->updateretorno($this->conect_sistemas_vtv, $id_retorno, $fecha4, $hora4);
		$this->ObjConsulta->updatepauta($this->conect_sistemas_vtv, $id_pauta, $pauta, $locacion, $tipotraje, $programa, $tipoevento, $lugar_pauta, $des_evento);
		$ced_sesion = $_SESSION['cedula'];
		$estatus = $id_estatus;     // estatus=1 es: nueva pauta
		$this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $id_pauta, $datosobs[1][1]);
		$this->ObjConsulta->insertestatusnuevo($this->conect_sistemas_vtv, $id_pauta, $estatus, $ced_sesion, $datosobs[1][1]);
		$mensaje = "classsolicitarservicios.php?id_pauta=" . $id_pauta."&pagina=5&mod=1" ;
		echo $mensaje;
	}

	function verificaREXP_pautaACC($id_pauta) {
		$datosobs=$this->ObjConsulta->selectobsrech($this->conect_sistemas_vtv, $id_pauta);
		$estatus_pauta = $this->ObjConsulta->selectestatus($this->conect_sistemas_vtv, $id_pauta);
		//si el estatus es enviado a jefe entonces....
		if ($estatus_pauta[1][1] == 3) {
			//expirar los estatus anteriores..
			$expirarpautas = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $id_pauta);
			//agregar estatus nuevo.. de modificado por jefe de area...
			$update_estatuspauta = $this->ObjConsulta->insertestatusnuevo($this->conect_sistemas_vtv, $id_pauta, '34', $_SESSION['cedula'], $datosobs[1][1]);
		}
	}

	function agregarenlista_f5() {
		$material = $_GET['servicios_aux']; //id_recurso
		$id_pauta = $_GET['id_pauta'];
		$cant = $_GET['cantidad_aux'];
		$cedula_sesion = $_SESSION['cedula'];
		$administrador = $_SESSION['id_tipo_usuario'];
		if($_GET['id_tipo_rec']!=""){
			$id_tipo_rec=$_GET['id_tipo_rec'];
		}else{
			//si no viene algo solo viene materiales.
			$id_tipo_rec=1;
		}

		if($id_tipo_rec==1){
			//id_recurso es el id de la descripcion del  material
			$id_gerencia=id_gerencia_inv;
			$id_division=id_division_inv;
		}else{
			//id_recurso es el cargo
			//$selectgerendiv = $this->ObjConsulta->selectgerendiv($this->conect_sistemas_vtv, $material);
			$id_gerencia=0;
			$id_division=0;
		}
		//FIX QUE PERMITE A UN JEFE O GERENTE AGREGAR RECURSOS..
		if ($administrador == 21 or $administrador == 22) {
			$id_estatus = 20; //JUSTIFICADO POR JEFE DE AREA..
		} else {
			$id_estatus = 13; //Solicitado
		}
		//si es productor o jefe de area...
		if ($administrador == 26 or $administrador == 22) {
			for ($i = 1; $i <= $cant; $i++) {
				$insertmaterial = $this->ObjConsulta->insertmaterial($this->conect_sistemas_vtv,1,$id_pauta,$cedula_sesion,$id_tipo_rec,$material,$id_gerencia,$id_division);
				$id_detalle_serv = $insertmaterial[0];
				$updateexpestatusrecursos = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $id_detalle_serv);
				$verrecursoasignado = $this->ObjConsulta->selectverrecursoasignado($this->conect_sistemas_vtv, $id_detalle_serv);
				$id_recurso_asignado = $verrecursoasignado[1][1];
				$insertestatusrecurso_f5 = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $id_pauta, $id_detalle_serv, $id_estatus, $cedula_sesion, $id_recurso_asignado); //agregado
				//cambiar el estatus de la pauta VERIFICANDO PRIMERO SU ESTATUS
				if ($administrador == 22) {
					$this->verificaREXP_pautaACC($id_pauta);
				} else {
					//aviso de nueva modificacion por parte del productor...
					//correo electronico para asignar recursos..
				}
			}
		}
	}

	function tablarecursos_solicitados($id_pauta=null) {
		if($id_pauta==null){
			$id_pauta=$_GET['id_pauta'];
		}
		$materiales="<table class='tabla' style='width:850px'; id='listarecursos'>
		<tr><th class='titulo' colspan='8'>LISTA DE MATERIALES</th></tr>
		<tr><th><div align='left'>Descripci&oacute;n</div></th></th></tr>";
		$datosrecsol = $this->ObjConsulta->selectrecsolporcant($this->conect_sistemas_vtv, $id_pauta);
		foreach ($datosrecsol as $llave => $valor) {
			$id_recurso = $valor[1];
			$cant = $valor[2];
			$tipo_rec = $this->ObjConsulta->selecttipo_rec($this->conect_sistemas_vtv, $id_pauta,$id_recurso);
			$id_tipo_rec = $tipo_rec[1][1];
			$botonE = "<input type=\"button\" class='boton' value='Eliminar' OnClick='eliminardelista_f5(this,$id_pauta,$id_recurso)';>";
			if($id_tipo_rec==1){//es material
				$botonaccion_mas = "<input type=\"button\" class='boton' value='+' OnClick='modcantidad($id_pauta,$id_recurso,1,1)';>";
				$botonaccion_menos = "<input type=\"button\" class='boton' value='-' OnClick='modcantidad($id_pauta,$id_recurso,0,1)';>";
				$desc_tipo = $this->ObjConsulta->selectdesctipo($this->conect_sistemas_vtv, $id_recurso);
				$descripcion=$desc_tipo[1][1];
			}else{
				$botonaccion_mas = "<input type=\"button\" class='boton' value='+' OnClick='modcantidad($id_pauta,$id_recurso,1,2)';>";
				$botonaccion_menos = "<input type=\"button\" class='boton' value='-' OnClick='modcantidad($id_pauta,$id_recurso,0,2)';>";
				$desc_tipo = $this->ObjConsulta->select_cargos($this->conect_sistemas_vtv, $id_recurso);
				$descripcion=$desc_tipo[1][2];
			}
			$materiales.="<tr id='$id_recurso'><td  align='left'>" . strtoupper(ucwords(utf8_encode($descripcion))) . "  </td><td>$botonaccion_mas<input type='text' readonly='readonly' size='4' value='{$cant}' />$botonaccion_menos</td><td  align='center'>" . $botonE . "</td></tr>";
		}

		$materiales.="</table>";
		if ($_GET['html'] == 'imprimir') {
			echo $materiales;
		} else {
			return $materiales;
		}
	}

	function modcantidad() {
		$id_tipo_rec=$_GET['tipo_recurso'];

		if($id_tipo_rec==1){
			//id_recurso es el id de la descripcion del  material
			$id_gerencia=id_gerencia_inv;
			$id_division=id_division_inv;
		}else{
			//id_recurso es el cargo
			$id_gerencia=0;
			$id_division=0;
		}

		if($_GET['accion']=="1"){
			//agregar un registro mas del mismo tipo de material
			if ($_SESSION['id_tipo_usuario'] == 21 or $_SESSION['id_tipo_usuario'] == 22) {
				$id_estatus = 20; //JUSTIFICADO POR JEFE DE AREA..
			} else {
				$id_estatus = 13; //Solicitado
			}

			$insertmaterial = $this->ObjConsulta->insertmaterial($this->conect_sistemas_vtv,1, $_GET['id_pauta'], $_SESSION['cedula'], $id_tipo_rec,$_GET['idtipomaterial'],$id_gerencia, $id_division);
			$id_detalle_serv = $insertmaterial[0];
			$updateexpestatusrecursosdata = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $id_detalle_serv);
			$verrecursoasignado = $this->ObjConsulta->selectverrecursoasignado($this->conect_sistemas_vtv, $id_detalle_serv);
			$id_recurso_asignado = $verrecursoasignado[1][1];
			$insertestatusrecurso_f5 = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $_GET['id_pauta'], $id_detalle_serv, $id_estatus , $_SESSION['cedula'], $id_recurso_asignado); //agregado
			/**
			*estatus de la pauta
			*/
			switch ($_SESSION['id_tipo_usuario']) {
				case '22':
				$this->verificaREXP_pautaACC($_GET['id_pauta']);
				break;
			}
		}else{

			if ($_SESSION['id_tipo_usuario'] == 26) {
				$id_estatus = 15; //Eliminado por el productor
			} else if ($_SESSION['id_tipo_usuario'] == 22) {
				$id_estatus = 33; //Eliminado por el jefe de area
			}

			//buscar iddetalles material de la pauta...
			$datoslista = $this->ObjConsulta->selectlistmat_group($this->conect_sistemas_vtv, $_GET['id_pauta'], $_GET['idtipomaterial']);

			if(count($datoslista)>0){
				$iddetalles=explode("|", (str_replace(" ", "", $datoslista[1][1])));
			}
			if($iddetalles[0]!=""){
				echo $iddetalles[0];
				//revisa que contenia el material antes de expirar.
				$recurso_asignado= $this->ObjConsulta->selectverrecursoasignado($this->conect_sistemas_vtv, $iddetalles[0]);
				//actualiza el material con la fecha de expiracion a ahora
				$updatedelista = $this->ObjConsulta->updatedelista($this->conect_sistemas_vtv, $iddetalles[0], $_SESSION['cedula']);
				//actualiza el estatus del material a expirado
				$updateexpestatusrecursos = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $iddetalles[0]);
				//crea un nuevo estatus del material
				$insertestatusrecursof5 = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $_GET['id_pauta'], $iddetalles[0], $id_estatus, $_SESSION['cedula'], $recurso_asignado[1][1]); //insertado
			}
			switch ($_SESSION['id_tipo_usuario']) {
				case '22':
				$this->verificaREXP_pautaACC($_GET['id_pauta']);
				break;
			}

		}
	}

	function eliminardelista_f5() {
		$id_pauta = $_GET['id_pauta'];
		$id_recurso = $_GET['id_rec'];

		//$id_detalle_servicio = $_GET['id_detalle_servicio'];
		//$id_detalle_servicio = explode("|",$id_detalle_servicio);
		$cedula_sesion = $_SESSION['cedula'];
		$administrador = $_SESSION['id_tipo_usuario'];
		if ($administrador == 26) {
			$id_estatus = 15; //Eliminado por el productor
		} else if ($administrador == 22) {
			$id_estatus = 33; //Eliminado por el jefe de area
		}


		$recursos = $this->ObjConsulta->selectrecsolind($this->conect_sistemas_vtv, $id_pauta,$id_recurso);
		$id_tipo_rec=$recursos[1][2];
		$id_detalle_servicio=$recursos[1][1];

		foreach ($recursos as $llave => $valor) {
			$id_detalle_servicio=$valor[3];
			$updateexpestatusrecursos = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $id_detalle_servicio);
			$verrecursoasignado = $this->ObjConsulta->selectverrecursoasignado($this->conect_sistemas_vtv, $id_detalle_servicio);
			$id_recurso_asignado = $verrecursoasignado[1][1];
			$insertestatusrecursof5 = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $id_pauta, $id_detalle_servicio, $id_estatus, $cedula_sesion, $id_recurso_asignado); //insertado
			$updatedelista = $this->ObjConsulta->updatedelista($this->conect_sistemas_vtv, $id_detalle_servicio, $cedula_sesion);

		}

 		if ($administrador == 22) {
			$this->verificaREXP_pautaACC($id_pauta); //modificando la pauta
		}

		/*if ($administrador == 26){
			for ($i=0; $i < count($id_detalle_servicio); $i++) {
				$this->get_materiales_count_solicitud($id_pauta, $id_detalle_servicio[$i], $id_estatus, $cedula_sesion);
			}
		} else if ($administrador == 22) {
			$this->verificaREXP_pautaACC($id_pauta); //modificando la pauta
			for ($i=0; $i < count($id_detalle_servicio); $i++) {
				$this->get_materiales_count_solicitud($id_pauta, $id_detalle_servicio[$i],$id_estatus, $cedula_sesion);
			}
		}*/
	}

	function get_materiales_count_solicitud($id_pauta, $id_detalle_servicio, $id_estatus, $cedula_sesion) {
		$updateexpestatusrecursos = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $id_detalle_servicio);
		$verrecursoasignado = $this->ObjConsulta->selectverrecursoasignado($this->conect_sistemas_vtv, $id_detalle_servicio);
		$id_recurso_asignado = $verrecursoasignado[1][1];
		$insertestatusrecursof5 = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $id_pauta, $id_detalle_servicio, $id_estatus, $cedula_sesion, $id_recurso_asignado); //insertado
		$updatedelista = $this->ObjConsulta->updatedelista($this->conect_sistemas_vtv, $id_detalle_servicio, $cedula_sesion);
	}
	//mundo
	function agregarenlalista() {
		$material = $_POST['servicios_aux']; //id_recurso
		$id_pauta = $_POST['id_pauta'];
		$cant = $_POST['cantidad_aux'];
		$cedula_sesion = $_SESSION['cedula'];
		$administrador = $_SESSION['id_tipo_usuario'];
		//$tipo_rec = $_POST['tm'];
		$titulo3 = "LISTA DE RECURSOS";
		$titulo4 = "LISTA DE RECURSOS ASIGNADOS";
		$id_estatus = 17; // agregado por la ual
		$id_tipo_rec=1;
		$id_gerencia=id_gerencia_inv;
		$id_division=id_division_inv;
		if ($administrador == 24) {


			$updateexpestatusmod = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $id_pauta);
			$insertestatusmod = $this->ObjConsulta->insertestatusmod($this->conect_sistemas_vtv, $id_pauta, $cedula_sesion);
			for ($i = 1; $i <= $cant; $i++) {
				$insertmaterial = $this->ObjConsulta->insertmaterial($this->conect_sistemas_vtv, 1, $id_pauta, $cedula_sesion,$id_tipo_rec,$material,$id_gerencia,$id_division);
				$id_detalle_serv = $insertmaterial[0];
				$verrecursoasignado = $this->ObjConsulta->selectverrecursoasignado($this->conect_sistemas_vtv, $id_detalle_serv);
				$id_recurso_asignado = $verrecursoasignado[1][1];
				$updateexpestatusrecursos = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $id_detalle_serv);
				$insertestatusrecurso = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $id_pauta, $id_detalle_serv, $id_estatus, $cedula_sesion, $id_recurso_asignado); //insertado
			}
			echo "<div id='materiales_porasignar'></div>
			<div id='recursos_hb_asignados'></div>";
		} else {
			echo"<script>var pagina='classRegistro.php';
			alert('Disculpa no tiene permitido el acceso a esta pagina.');
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 0);
			</script>
			";
		}
	}

	function get_selectsmateriales($id_pauta=null){
		$gtml = false;
		if ($id_pauta == null) {
			if ($_GET['id_pauta'] != null) {
				$id_pauta = $_GET['id_pauta'];
			} else {
				$id_pauta = $_POST['id_pauta'];
			}
			$html = true;
		}

		$materiales = "<script>
		$(document).ready(

		    function(){
		        $('.modalInput2').overlay({
		            mask: {color: '#ddd', loadSpeed: 200,opacity: 0.9},
		            closeOnClick: false,
		            oneInstance: false,
		            closeOnEsc: false
		        });
		        $('#agregar button').click(function(e){
		            clearInterval(intervalID);
		            window.location.reload();
		        });
		});
		</script>
		<table class='tabla' style='width:850px'; id='listarecursos'>
		<tr><th class='titulo' colspan='8'>" . $titulo3 . "</th></tr>
		<tr><th><div align='left'>Descripci&oacute;n</div></th></th></tr>";

		$datosrec = $this->ObjConsulta->selectdatosrec($this->conect_sistemas_vtv, $id_pauta);
		$rectotal = count($datosrec);//busca si hay recursos solicitados para la pauta
		//print_r($rectotal);

		if($rectotal==0){
			//no hay recursos solicitados, no se muestra el combo
		}else{
			//buscar los recursos solicitados y verificar que tipo de recurso son
			$datosrecsol = $this->ObjConsulta->selectrecsol($this->conect_sistemas_vtv, $id_pauta);
			foreach ($datosrecsol as $llave => $valor) {
				$id_rec=$valor[1];
				$id_tipo_rec=$valor[2];
				$id_detalle_servicio=$valor[3];

				if(($_POST['tm']=="rm" or $_POST['tm']=="1") and $id_tipo_rec=="1"){
					//con el $id_rec busco todas las caractieristicas disponible para los recursos con ese id
					$desc_tipo = $this->ObjConsulta->selectdesctipo($this->conect_sistemas_vtv, $id_rec);
					$descripcion=$desc_tipo[1][1];
					$caractrec = $this->ObjConsulta->selectcaractrec($this->conect_sistemas_vtv, $id_rec);
					$botonAs = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick=\"agregarenlista_asignar($id_pauta,$id_detalle_servicio,$id_tipo_rec);\" />";
					$botonE = "<input type=\"button\" class='boton' value='Eliminar' OnClick='eliminardelalista($id_pauta,$id_detalle_servicio,$id_tipo_rec);' />";

					$caracteristicas = "<select id='caracteristicas".$valor[3]."' name='caracteristicas' style='width:500px;'>";
					$caracteristicas.="<option value='0' selected >Seleccione </option>";
		       		foreach ($caractrec as $llave => $valor) {//MARCA MODELO BIEN NAC
		        		$caracteristicas.="<option value='" . $valor[1] . "' $selected >Marca:" . $valor[3] . "&nbsp;&nbsp;Modelo:" . $valor[4] . "&nbsp;&nbsp;Bien Nac.:" . $valor[5] . "</option>";
		        	}
		        	$caracteristicas.="</SELECT> ";

					$materiales.="<tr id='".$id_detalle_servicio."'><td align='left'>" . strtoupper(utf8_encode($descripcion)) . "</td>
					<td align='left'>" . utf8_encode($caracteristicas) . "&nbsp;" . $botonAs . "&nbsp;&nbsp;" . $botonE . "</td></tr>";
				}else if(($_POST['tm']=="" or $_POST['tm']=="undefined" or $_POST['tm']=="2") and $id_tipo_rec=="2"){
					//ES UN RECURSO HUMANO
					$botonPErs = "<button class='boton modalInput2' onclick=\"callback_select_personal(".$id_rec.",$id_pauta);\" rel=\"#agregar\" >Asignar</button>";
					$botonE = "<input type=\"button\" class='boton' value='Eliminar' OnClick='eliminardelalista($id_pauta,$id_detalle_servicio,$id_rec);' />";
					$datosmaterial = $this->ObjConsulta->select_cargos($this->conect_sistemas_vtv, $id_rec);
					$desc_id_recurso=$datosmaterial[1][2];
					$materiales.="<tr id='".$id_detalle_servicio."'><td align='left'>" . strtoupper(utf8_encode($desc_id_recurso))."</td><td align='left'>" . $botonPErs . $botonE . "</td></tr>";
				}
			}


		}

		$materiales.="</table>";
		if ($html == false) {
			return $materiales;
		} else {
			echo $materiales;
		}
	}
	function reload_asignados() {
		$titulo4 = "LISTA DE RECURSOS ASIGNADOS";
		$asignados.="<table class='tabla' style='width:850px'; id='listarecursosasignados'>
		<tr><th class='titulo' colspan='8'>" . $titulo4 . "</th></tr>
		<tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Detalle</div></th><th><div align='center'>opci&oacute;n</div></th></tr>";
		$datoslista2 = $this->ObjConsulta->selectlistmatasignuevo($this->conect_sistemas_vtv, $_GET['id_pauta']);

		$datosrec = $this->ObjConsulta->selectdatosrec($this->conect_sistemas_vtv, $_GET['id_pauta']);
		foreach ($datoslista2 as $llave3 => $valor3) {
			$id_recurso_asignado = $valor3[1];
			$id_recurso = $valor3[2];
			$id_tipo_rec = $valor3[4];
			if(($_GET['tm']=="rm" or $_GET['tm']=="1") and $id_tipo_rec==1){
				$datosmaterial = $this->ObjConsulta->selectlistmatdescnuevo($this->conect_sistemas_vtv, $id_recurso);
				$desc_id_recurso = $datosmaterial[1][1];
				$datoscaract = $this->ObjConsulta->selectdatosrecasig($this->conect_sistemas_vtv, $id_recurso_asignado);
				$marca=$datoscaract[1][1];
				$modelo=$datoscaract[1][2];
				$bien_nac=$datoscaract[1][3];

				$eliminar = "<a href='#recursos_hb_asignados' onclick='eliminarmaterialasignadohb($id_recurso_asignado, {$_GET['id_pauta']},$id_tipo_rec)'>eliminar</a>";
				$primero = "Marca: " . $marca . "";
				$segundo = "Modelo: " . $modelo . "";
				$tercero = "Bien Nac.: " . $bien_nac . "";
				$desc_caracteristicas = "" . $primero . "&nbsp;" . $segundo . "&nbsp;" . $tercero . "";

				$asignados.="<tr id='$id_recurso_asignado'><td  align='left'>" . strtoupper($desc_id_recurso) . "</td><td  align='left'>" . $desc_caracteristicas . "</td><td>$eliminar</td></tr>";

			}else if(($_GET['tm']=="" or $_GET['tm']=="undefined") and $id_tipo_rec==2){
				$eliminar = "<a href='#recursos_hb_asignados' onclick='eliminarmaterialasignadohb($id_recurso_asignado, {$_GET['id_pauta']},$id_tipo_rec)'>eliminar</a>";
				$datosmaterial = $this->ObjConsulta->select_cargos($this->conect_sistemas_vtv, $id_recurso);
				$desc_id_recurso=$datosmaterial[1][2];
				$recursos = $this->ObjConsulta->get_recursos_humanos($this->conect_sistemas_vtv,$id_recurso_asignado);
				$asignados.="<tr id='$id_recurso_asignado'><td  align='left'>" . strtoupper($desc_id_recurso) . "</td><td  align='left'>" . utf8_encode($recursos[1][6]) ." ".utf8_encode($recursos[1][7])."</td><td>$eliminar</td></tr>";
			}
		}
		$asignados.="<input name='datosrec'  id='datosrec' type='hidden' value=" . count($datosrec) . " /><input name='datosrecasig'  id='datosrecasig' type='hidden' value=" . count($datoslista2) . "></table>";
		echo utf8_decode($asignados);

	}

	//aqui
	function mostrar_lista_recursos_prodjefe($pauta, $idtablacontenedor="listarecursos",$trecurso="2", $soloestatus=null, $postitulo="", $gerencia=null){
		$datoslista_asignados_jefe=array();
		$datoslista_asignados_productor=array();


		if($soloestatus==null){
				$datoslista_asignados_jefe = $this->ObjConsulta->status_recurso($this->conect_sistemas_vtv, $pauta, $trecurso, "20"); //recursos con estatus 20..
				$datoslista_asignados = $this->ObjConsulta->status_recurso($this->conect_sistemas_vtv, $pauta, $trecurso, "13"); //recursos con estatus 13..
		}else{
				$datoslista_asignados = $this->ObjConsulta->status_recurso($this->conect_sistemas_vtv, $pauta, $trecurso, $soloestatus);
		}

			$datoslista = array_merge((array) $datoslista_asignados_jefe, (array) $datoslista_asignados);
			for ($i=0; $i < count($datoslista) ; $i++) {
				//preguntar por la gerencia de los recursos..
				if($gerencia!=null){

					$recursoesdegerencia=$this->ObjConsulta->recursotmaterial_gerencia($this->conect_sistemas_vtv, $datoslista[$i][1], $gerencia);

					if(count($recursoesdegerencia)>0){
						$data_recurso[$datoslista[$i][1]] = $data_recurso[$datoslista[$i][1]]+$datoslista[$i][2];
					}

				}else{
					$data_recurso[$datoslista[$i][1]] = $data_recurso[$datoslista[$i][1]]+$datoslista[$i][2];
				}
			}

			if(count($data_recurso)>0){
				foreach ($data_recurso as $llave => $valor) {
					$id = $llave;
					$cant = $valor;

					$datosmaterial = $this->ObjConsulta->selectlistmatdesc($this->conect_sistemas_vtv, $id);
					$desc_id_recurso = $datosmaterial[1][1];


					$resto = $llave % 2;
					if (($resto == 0) && ($llave != 0)) {
						$color = "style='background:#f1f1f1;'"; //par
					} else {
						$color = "style='background:#ffffff;'"; //impar f9f9f9
					}
					$materiales2.="<tr id='$id' $color><td  align='center'>" . ucwords(strtoupper($desc_id_recurso)) . "</td><td  align='center'>" . $cant . "</td>";
				}

				if($trecurso=="2"){
					$titulo3 = "TECNICOS";
				}else{
					$titulo3 = "HUMANOS";
				}

				if ($pagina == 1){
					$materiales = $materiales2;
				}else{
					$materiales = "<table class='tabla' align='center' style='width:850px'; id='$idtablacontenedor'>
					<tr><th class='titulo' colspan='8'>LISTA DE RECURSOS ".$titulo3.$postitulo."</th></tr>
					<tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Cantidad</div></th></tr>
					<tr><td>" . $materiales2 . "</td></tr>
					</table>";
				}
			}
			return $materiales;
	}

	function lista_rec_asig($pauta){
		$datoslista = $this->ObjConsulta->selectlistmatasig($this->conect_sistemas_vtv, $pauta);
		foreach ($datoslista as $llave3 => $valor3) {
			$id_recurso_asignado = $valor3[1];
			$id_recurso = $valor3[2];
			$id_tipo_rec = $valor3[4];//Si es material o humano
			if($id_tipo_rec==1){//es material
				$datosmaterial = $this->ObjConsulta->selectlistmatdescnuevo($this->conect_sistemas_vtv, $id_recurso);
				$desc_id_recurso = $datosmaterial[1][1];
				$datoscaract = $this->ObjConsulta->selectdatosrecasig($this->conect_sistemas_vtv, $id_recurso_asignado);
				$marca=$datoscaract[1][1];
				$modelo=$datoscaract[1][2];
				$bien_nac=$datoscaract[1][3];
				$primero = "Marca: " . $marca . "";
				$segundo = "Modelo: " . $modelo . "";
				$tercero = "Bien Nac.: " . $bien_nac . "";
				$desc_caracteristicas = "" . $primero . "&nbsp;" . $segundo . "&nbsp;" . $tercero . " ";

				$asignados2.="<tr id='$id'><td  align='left'>" . strtoupper($desc_id_recurso) . "</td><td  align='left'>" . $desc_caracteristicas . "</td></tr>";
			}
		}

		$titulo4 = "LISTA DE RECURSOS ASIGNADOS";
		$asignados .= "<table class='tabla' style='width:850px'; id='listarecursosasignados'>
		<tr><th class='titulo' colspan='8'>" . $titulo4 . "</th></tr>
		<tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Detalle</div></th></tr>
		<tr><td>" . $asignados2 . "</td></tr>
		</table>
		";
		return $asignados;
	}

	function agregarenlista_asignar() {
		$id_pauta = $_POST['id_pauta'];
		$caracteristicas = $_POST['caracteristicas'];
		$id_detalle_servicio = $_POST['id_detalle_servicio'];
		$ced_sesion = $_SESSION['cedula'];
		$administrador = $_SESSION['id_tipo_usuario'];
		$titulo3 = "LISTA DE RECURSOS";
		$titulo4 = "LISTA DE RECURSOS ASIGNADOS";
		$id_estatus = 14; // asignado por la ual
		$cedula_sesion = $_SESSION['cedula'];
		if ($administrador == 24) {

			$verificarasignacion = $this->ObjConsulta->verificarasignacion($this->conect_sistemas_vtv, $caracteristicas, $id_pauta);
			$asignacion = $verificarasignacion[1][1];
			if ($asignacion == $caracteristicas) {
				echo"<script>var pagina='classasignarrecursos.php?id_pauta=" . $id_pauta . "&lista=classlistadepautasual.php';
				alert('Disculpe otro recurso fue asignado con esas caracteristicas, seleccione otras semejantes');
				function redireccionar() {
					location.href=pagina;
				}
				setTimeout ('redireccionar()', 3000);
				</script>
				";
			} else {
				$verificarestatus = $this->ObjConsulta->verificarestatus($this->conect_sistemas_vtv, $id_detalle_servicio);
				$estatusactual = $verificarestatus[1][1];
				if ($estatusactual == 14) {
					$asignadopor = $this->ObjConsulta->asignadopor($this->conect_sistemas_vtv, $id_detalle_servicio);
					$cedula = $asignadopor[1][1];
					$datousuario = $this->ObjConsulta->selectdatousuario($this->conect_sistemas_vtv, $cedula);
					$nombre = $datousuario[1][1];
					$apellido = $datousuario[1][2];

					echo"<script>var pagina='classasignarrecursos.php?id_pauta=" . $id_pauta . "&lista=classlistadepautasual.php';
					alert('Disculpa el recurso ya fue asignado por: " . $nombre . " " . $apellido . "');
					function redireccionar() {
						location.href=pagina;
					}
					setTimeout ('redireccionar()', 1000);
					</script>
					";
				} else {

					//error de repetir el estatus de una pauta..
					$datosobs=$this->ObjConsulta->selectobsrech($this->conect_sistemas_vtv, $id_pauta);
					$updateexpestatusmod2 = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $id_pauta);
					$insertestatusmod2 = $this->ObjConsulta->insertestatusnuevo($this->conect_sistemas_vtv, $id_pauta, '7', $cedula_sesion, $datosobs[1][1]);
					//CAMBIO DE ESTATUS AL RECURSO
					$updateexpestatusrecursos = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $id_detalle_servicio);
					$act_estatus = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $id_pauta, $id_detalle_servicio, '14', $_SESSION['cedula'], $caracteristicas);
					$updatedelistaasignados = $this->ObjConsulta->updatedelistaasignados($this->conect_sistemas_vtv, $id_detalle_servicio, $caracteristicas, $_SESSION['cedula']);
					//ACTUALIZAR EL ESTATUS DEL RECURSO EN EL ALMACEN
					$updateestalmtec = $this->ObjConsulta->updateestalmtec($this->conect_sistemas_vtv, $caracteristicas,3);
					$insertpendiente = $this->ObjConsulta->insertpendiente($this->conect_sistemas_vtv, $id_pauta, $caracteristicas);
				}
			}
		} else {
			echo"<script>var pagina='classRegistro.php';
			alert('Disculpa no tiene permitido el acceso a esta pagina.');
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 0);
			</script>
			";
		}
	}

	function eliminardelalista() {
		$id_pauta = $_POST['id_pauta'];
		$id_detalle_servicio = $_POST['id_detalle_servicio'];
		$titulo3 = "LISTA DE RECURSOS";
		$titulo4 = "LISTA DE RECURSOS ASIGNADOS";
		$id_estatus = 16; // eliminado por la ual
		$cedula_sesion = $_SESSION['cedula'];
		$administrador = $_SESSION['id_tipo_usuario'];
		if ($administrador == 24) {
			$verificarestatus = $this->ObjConsulta->verificarestatus($this->conect_sistemas_vtv, $id_detalle_servicio);
			$estatusactual = $verificarestatus[1][1];
			if ($estatusactual == 16) {
				$eliminadopor = $this->ObjConsulta->eliminadopor($this->conect_sistemas_vtv, $id_detalle_servicio);
				$cedula = $eliminadopor[1][1];
				$datousuario = $this->ObjConsulta->selectdatousuario($this->conect_sistemas_vtv, $cedula);
				$nombre = $datousuario[1][1];
				$apellido = $datousuario[1][2];
				echo"<script>var pagina='classasignarrecursos.php?id_pauta=" . $id_pauta . "&lista=classlistadepautasual.php';
				alert('Disculpa el recurso ya fue eliminado por: " . $nombre . "" . $apellido . "');
				function redireccionar() {
					location.href=pagina;
				}
				setTimeout ('redireccionar()', 1000);
				</script>
				";
			} else {
				$updateexpestatusmod3 = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $id_pauta);
				$insertestatusmod3 = $this->ObjConsulta->insertestatusmod($this->conect_sistemas_vtv, $id_pauta, $cedula_sesion);
				$updateexpestatusrecursos = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $id_detalle_servicio);
				$verrecursoasignado = $this->ObjConsulta->selectverrecursoasignado($this->conect_sistemas_vtv, $id_detalle_servicio);
				$id_recurso_asignado = $verrecursoasignado[1][1];
				$insertestatusrecurso1 = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $id_pauta, $id_detalle_servicio, $id_estatus, $cedula_sesion, $id_recurso_asignado); //insertado
				$updatedelista = $this->ObjConsulta->updatedelista($this->conect_sistemas_vtv, $id_detalle_servicio, $cedula_sesion);

			}
		} else {
			echo"<script>var pagina='classRegistro.php';
			alert('Disculpa no tiene permitido el acceso a esta pagina.');
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 0);
			</script>
			";
		}
	}

	//tocar
	function agregarenlista_cambiado() {
		$id_pauta = $_GET['id_pauta'];
		$caracteristicas = $_GET['caracteristicas'];
		$id_detalle_servicio = $_GET['id_detalle_servicio'];
		$ced_sesion = $_SESSION['cedula'];
		$titulo3 = "LISTA DE RECURSOS";
		$titulo4 = "LISTA DE RECURSOS ASIGNADOS";
		$id_estatusrecurso = 28; // asignado por las ao
		$cedula_sesion = $_SESSION['cedula'];
		$id_estatus = 22; //modificado por las ao
		$gerencia = $_SESSION['gerencia'];
		$division = $_SESSION['division'];

		$administrador = $_SESSION['id_tipo_usuario'];
		if ($administrador == 25) {
			$verificarestatuspauta = $this->ObjConsulta->verificarestatuspauta($this->conect_sistemas_vtv, $id_pauta);
			$estatusactual = $verificarestatuspauta[1][1];
			if ($estatusactual == 21) {
				echo "<script>var pagina='classmostrar.php?mostrar=mostrar_pauta_con_asig_ao&id_pauta=".$id_pauta."&lista=classlista.php?modulo=listadepautas';
				jAlert('Disculpa la pauta ya fue aprobada por la Unidad de Apoyo Logistico', 'Alerta', function(){
					setTimeout ('redireccionar()', 0);
				});
				function redireccionar() {
					location.href=pagina;
				}
				</script>
				";
			} else {
				$verificarestatus = $this->ObjConsulta->verificarestatus($this->conect_sistemas_vtv, $id_detalle_servicio);
				$estatusactual = $verificarestatus[1][1];
				if ($estatusactual == 22) {
					echo"<script>var pagina='classasignarrecursos.php?id_pauta=" . $id_pauta . "&lista=classlistadepautasao.php';
					alert('Disculpa el recurso ya fue asignado');
					function redireccionar() {
						location.href=pagina;
					}
					setTimeout ('redireccionar()', 1000);
					</script>
					";
				} else {
					$updateexpestatusmod2 = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $id_pauta);
					$insertestatusmod2 = $this->ObjConsulta->insertestatusmodif($this->conect_sistemas_vtv, $id_pauta, $id_estatus, $cedula_sesion);
					$updateexpestatusrecursos = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $id_detalle_servicio);
					$updatedelistaasignados = $this->ObjConsulta->updatedelistaasignados($this->conect_sistemas_vtv, $id_detalle_servicio, $caracteristicas, $_SESSION['cedula']);
					$verrecursoasignado = $this->ObjConsulta->selectverrecursoasignado($this->conect_sistemas_vtv, $id_detalle_servicio);
					$id_recurso_asignado = $verrecursoasignado[1][1];
					$insertestatusrecurso2 = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $id_pauta, $id_detalle_servicio, $id_estatusrecurso, $cedula_sesion, $id_recurso_asignado);
					//ALMACEN TECNICO
					$updateestalmtec = $this->ObjConsulta->updateestalmtec($this->conect_sistemas_vtv, $id_recurso_asignado,3);
				    $insertpendiente = $this->ObjConsulta->insertpendiente($this->conect_sistemas_vtv, $id_pauta, $id_recurso_asignado);
					$materiales=$this->materiales_modificados_ao($id_pauta);
		    		$asignados=$this->materiales_con_caracteristicas($pauta,$_SESSION['gerencia'], $_SESSION['division']);
		    		$botonC = "<input type=\"button\" class='boton' value=\"Confirmar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
		    		$codHtml="";
		    		$codHtml="
		    		<div id='datosp2' align='center'>
		    		<div id='cargando2' align='center'></div>
		    		" . $reportes_assoc . "
		    		" . $materiales . "
		    		" . $asignados . "
		    		</div>
		    		<table class='tabla'>
		    		";
		    		echo $codHtml;


				}

			}
		}else{
				echo"<script>var pagina='classRegistro.php';
				alert('Disculpa no tiene permitido el acceso a esta pagina.');
				function redireccionar() {
					location.href=pagina;
				}
				setTimeout ('redireccionar()', 0);
				</script>
				";
		}
	}

	function enviarsolicitud() {
		$pauta = $_GET['id_pauta'];
		$estatus = $_GET['estatus_pt'];
		$cedula_sesion = $_SESSION['cedula'];
		$administrador = $_SESSION['id_tipo_usuario'];

		if ($administrador == 26) {
			//a el jefe de area de la session activa.
			$correo = $this->ObjConsulta->selectcorreousuario($this->conect_sistemas_vtv, $_SESSION['gerencia'], $_SESSION['division'], '22'); //jefe inmediato
			$datoscorreo= $this->datos_pauta($pauta);
			$datoscorreo.= $this->mostrar_pautas($pauta,"mostrar_pauta_generada", false);

		} else if ($administrador == 22) {
			//a todos el perfil de area operativa
			$correo = $this->ObjConsulta->selectcorreousuario($this->conect_sistemas_vtv, null, null, '24');
			$datoscorreo= $this->datos_pauta($pauta);
			$datoscorreo.= $this->mostrar_pautas($pauta,"mostrar_pauta_generada", false);

		} else if($administrador == 24){
			$idsgerencia = $this->ObjConsulta->selectidsgerencia_ao_sinasignacion($this->conect_sistemas_vtv, $pauta);
			foreach ($idsgerencia as $llave5 => $valor5) {
				$id_gerencia = $valor5[1];
				$id_division = $valor5[2];

				$datoscorreoAO[$id_gerencia]=$this->datos_pauta($pauta);
				$datoscorreoAO[$id_gerencia]=$datoscorreoAO[$id_gerencia].$this->mostrar_pautas($pauta,"mostrar_pauta_generada", false);

				$correo_analistasoperativos = $this->ObjConsulta->selectcorreousuario($this->conect_sistemas_vtv, $id_gerencia, $id_division, "25"); //selecciona el correo de los responsables respectivos
				$correo = array_merge((array) $correo, (array) $correo_analistasoperativos); //une todos los correos.
			}
		}

		$asunto = "Solicitud de facilidades tecnicas de la pauta " . $pauta . " ";
		//verificar el estatus de la pauta
		$datosobs=$this->ObjConsulta->selectobsrech($this->conect_sistemas_vtv, $pauta);
			//expirar la pauta
		$updateexpestatus = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $pauta);
			//nueva pauta
		$insertestatus = $this->ObjConsulta->insertestatus($this->conect_sistemas_vtv, $pauta, $estatus, $cedula_sesion, $datosobs[1][1]);
		if ($_GET['mod'] == 1){
			$pagina = '6';
		}elseif ($estatus == 10){
			$pagina = '9';
		}else {
			$pagina = '3';
		}

		foreach ($correo as $llavec => $valorc) {
			$correodef[$valorc[1]]=$valorc[2];
		}
		if (count($correodef)>0){
			foreach ($correodef as $llavec => $valorc) {

				if($administrador==24){
					$this->enviarcorreo($llavec, $datoscorreoAO[$valorc], $asunto);
				}else{
					$this->enviarcorreo($llavec, $datoscorreo, $asunto);
				}
			}
		}
		$mensaje="<div style='color: #009900;font-weight: bold;'><br>Su pauta se envio con exito</div>";
		$mensaje.="<script>var pagina='classlista.php?modulo=listadepautas';
		function redireccionar() { location.href=pagina; }
		setTimeout ('redireccionar()', 2000);
		</script>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje,($this->pasosdelproceso($pagina)));
	}

	function enviarsolicitud3() {
		$id_pauta = $_GET['id_pauta'];
		$estatus = 26; // estatus=26 es: informe generado
		$administrador = $_SESSION['id_tipo_usuario'];
		$cedula_sesion = $_SESSION['cedula'];
		if ($administrador == 26 or $administrador==22 or $administrador==24) {
			$verificarestatuspauta = $this->ObjConsulta->verificarestatuspauta($this->conect_sistemas_vtv, $id_pauta);
			$estatusactual = $verificarestatuspauta[1][1];
			if ($estatusactual == 26) {
				echo"<script>var pagina='classlista.php?modulo=listadepautas';
				alert('Disculpa el informe ya fue enviado');
				function redireccionar() {
					location.href=pagina;
				}
				setTimeout ('redireccionar()', 2000);
				</script>
				";
			} else {
				$updateexpestatus = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $id_pauta);
				$insertestatus = $this->ObjConsulta->insertestatus($this->conect_sistemas_vtv, $id_pauta, $estatus, $cedula_sesion);

				$datoscorreo= $this->datos_pauta($id_pauta); //datos generales de la pauta
				$datoscorreo.= $this->materiales_justificados($id_pauta); //los datos de los materiales que se justificaron

				$piedepagina = "Gerencia de Tecnolog&iacute;a de la Informaci&oacute;n y la Comunicaci&oacute;n. Solicitudes de facilidades t&eacute;cnicas";
				$datoscorreo = $datoscorreo . $piedepagina;
				$asunto = "Informe de produccion de la pauta " . $id_pauta . " ";
				$idsgerencia = $this->ObjConsulta->selectidsgerencia($this->conect_sistemas_vtv, $id_pauta);
				foreach ($idsgerencia as $llave5 => $valor5) {
					$id_gerencia = $valor5[1];
					if($id_gerencia != ""){
						$id_division = $valor5[2];
						$correoao = $this->ObjConsulta->selectcorreosger($this->conect_sistemas_vtv, $id_gerencia); //gerencia areas operativas
						$correodiv = $this->ObjConsulta->selectcorreosdiv($this->conect_sistemas_vtv, $id_gerencia, $id_division); //selecciona correo de la division respectiva.
						$correousuario = $this->ObjConsulta->selectcorreousuario($this->conect_sistemas_vtv, $id_gerencia, $id_division, "25"); //selecciona el correo de los responsables respectivos
						$correo = array_merge((array) $correoao, (array) $correodiv, (array) $correousuario); //une todos los correos.
						if ($correo == "") {
							$mensaje = "<div style='color: #FF3300;font-weight: bold;'><br>Disculpe la gerencia no tiene una direccion de correo asociada<div><br>";
							echo $this->ObjMensaje->InterfazExitosamente($mensaje);
							echo"<script>var pagina='classlista.php?modulo=listadepautas';
							function redireccionar() {
								location.href=pagina;
							}
							setTimeout ('redireccionar()', 2500);
							</script>
							";
						} else {
							foreach ($correo as $llave7 => $valor7) {
								$this->enviarcorreo($valor7[1], $datoscorreo, $asunto);
							}
						}
					}
				}
				$mensaje = "<div style='color: #009900;font-weight: bold;'><br>Su informe se a enviado con exito<div><br>";
				echo $this->ObjMensaje->InterfazExitosamente($mensaje);
				echo"<script>var pagina='classlista.php?modulo=listadepautas';
				function redireccionar() {
					location.href=pagina;
				}
				setTimeout ('redireccionar()', 3000);
				</script>
				";
			}
		} else {
			echo"<script>var pagina='classRegistro.php';
			alert('Disculpa no tiene permitido el acceso a esta pagina.');
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 0);
			</script>
			";
		}
	}

	function guardarcorreos() {
		$cedula = $_SESSION['cedula'];
		$gerencias = explode(",", $_GET['gerencias']);
		$divisiones = explode(",", $_GET['divisiones']);
		//revisar la insercion de gerencias..
		foreach ($gerencias as $llave => $valor) {
			if ($valor != "") {
				list ($idger, $ger) = explode(";", $valor);
				$verificarcorreos = $this->ObjConsulta->selectverificarcorreos($this->conect_sistemas_vtv, $idger);
				//verificar si al menos un solo correo hay de esta gerencia
				if (count($verificarcorreos) == 0) {
					//Si es igual a 0 es porque no hay ninguno y se inserta el nuevo correo
					$insertcorreos = $this->ObjConsulta->insertcorreos($this->conect_sistemas_vtv, $idger, $ger, $cedula);
				} else {
					$expcorreos = $this->ObjConsulta->updateexpcorreos($this->conect_sistemas_vtv, $idger); //donde este el idger exp los correos
					$insertcorreos = $this->ObjConsulta->insertcorreos($this->conect_sistemas_vtv, $idger, $ger, $cedula);
				}
			}
		}
		foreach ($divisiones as $llave2 => $valor2) {
			list ($iddiv, $div) = explode(";", $valor2);
			$datosdiv = $this->ObjConsulta->selectdivision($this->conect_sistemas_vtv, null, $iddiv);
			$idger = $datosdiv[1][3];
			$verificarcorreos_division = $this->ObjConsulta->selectverificarcorreos($this->conect_sistemas_vtv, $idger, $iddiv);
			if (count($verificarcorreos_division) == 0) {
				$insertcorreosdiv = $this->ObjConsulta->insertcorreosdiv($this->conect_sistemas_vtv, $idger, $iddiv, $div, $cedula);
			} else {
				$insertcorreosdiv = $this->ObjConsulta->insertcorreosdiv($this->conect_sistemas_vtv, $idger, $iddiv, $div, $cedula);
			}
		}
		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>Los correos se guardaron con exito<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
		echo"<script>var pagina='classbienvenida.php';
		function redireccionar() {
			location.href=pagina;
		}
		setTimeout ('redireccionar()', 3000);
		</script>
		";
	}

	function generarinforme() {
		$id_pauta = $_GET['id_pauta'];

		echo"<script>var pagina='classinformedeprod.php?id_pauta=$id_pauta&lista=classlistadeinformes.php';
		function redireccionar() {
			location.href=pagina;
		}
		setTimeout ('redireccionar()', 0);
		</script>
		";
	}

	function guardarinforme() {
		$asistio = $_GET['asistio'];
		$observaciones = $_GET['observaciones'];
		$id_pauta = $_GET['id_pauta'];
		$id_detalle_servicio = $_GET['id_detalle_servicio'];
		$ced_sesion = $_GET['ced_sesion'];
		$id_estatus = 35; //recurso justificado
		$administrador = $_SESSION['id_tipo_usuario'];
		if ($administrador == 26 or $administrador= 22) {
			$updateexpestatus = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $id_pauta);
			$insertestatusmod3 = $this->ObjConsulta->insertestatusmod3($this->conect_sistemas_vtv, $id_pauta, $ced_sesion);
			$verificarestatus = $this->ObjConsulta->verificarestatus($this->conect_sistemas_vtv, $id_detalle_servicio);
			$estatusactual = $verificarestatus[1][1];
			if ($estatusactual == 35) {
				echo"<script>var pagina='classinformedeprod.php?id_pauta=" . $id_pauta . "&lista=classlistadeinformes.php';
				alert('Disculpa el recurso ya fue justificado');
				function redireccionar() {
					location.href=pagina;
				}
				setTimeout ('redireccionar()', 500);
				</script>
				";
			} else {
				$updateexpestatusrecursos = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $id_detalle_servicio);
				$verrecursoasignado = $this->ObjConsulta->selectverrecursoasignado($this->conect_sistemas_vtv, $id_detalle_servicio);
				$id_recurso_asignado = $verrecursoasignado[1][1];
				$insertestatusjustificado = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $id_pauta, $id_detalle_servicio, $id_estatus, $ced_sesion, $id_recurso_asignado); //insertado
				$guardarinf = $this->ObjConsulta->insertguardar($this->conect_sistemas_vtv, $id_pauta, $id_detalle_servicio, $asistio, $observaciones, $ced_sesion);
				$datoslista = $this->ObjConsulta->selectlistmatasig2($this->conect_sistemas_vtv, $id_pauta);
				foreach ($datoslista as $llave3 => $valor3) {
					$id_detalle_servicio = $valor3[1];
					$id_recurso_asignado = $valor3[2];
					$id_recurso = $valor3[3];
					$datosnojust = $this->ObjConsulta->selectlistnojust($this->conect_sistemas_vtv, $id_detalle_servicio, $id_pauta);
					$id_estatus_actual = $datosnojust[1][1];
					if ($id_estatus_actual == 14 or $id_estatus_actual == 23 or $id_estatus_actual == 28) {
						$datosmaterial = $this->ObjConsulta->selectdetalles($this->conect_sistemas_vtv, $id_recurso);
						$desc_id_recurso = $datosmaterial[1][1];
						$idtipoinventario = $datosmaterial[1][2];
						$datoscaracteristicas = $this->ObjConsulta->selectcaracteristicas2($this->conect_sistemas_vtv, $id_recurso_asignado);
						$desc_caracteristicas = $datoscaracteristicas[1][2];
						if ($idtipoinventario == 1) {
							$asistio = "<select id='asistio" . $llave3 . "' style='width:110px';>
							<option value='0' selected>Seleccione</option>
							<option value='1'>Si asistio</option>
							<option value='2'>No asistio</option>
							</select>";
						} else {
							$asistio = "<select id='asistio" . $llave3 . "'>
							<option value='0' selected>Seleccione</option>
							<option value='3'>Bueno</option>
							<option value='4'>Regular</option>
							<option value='5'>Malo</option>
							<option value='6'>No entregado</option>
							</select>";
						}
						$observaciones = "<textarea name='observaciones" . $llave3 . "' id='observaciones" . $llave3 . "' rows='2' cols='30'></textarea>";
						$botonG = "<input type=\"button\" class='boton' value=\"Guardar\" OnClick=guardarinforme($id_pauta,$id_detalle_servicio,$llave3,$ced_sesion);>";
						$asignados2.="<tr id='$id'><td  align='left'>" . strtoupper(utf8_encode($desc_id_recurso)) . "</td><td  align='left'>" . strtoupper(utf8_encode(str_replace("|", "<BR>", $desc_caracteristicas))) . "</td><td  align='left'>" . $asistio . "</td><td  align='left'>" . $observaciones . "</td><td  align='left'>" . $botonG . "</td></tr>";
					}
					$titulo2 = "LISTA DE RECURSOS";
					$asignados = "<table class='tabla' style='width:850px'; id='listarecursosasignados'>
					<tr><th class='titulo' colspan='8'>" . $titulo2 . "</th></tr>
					<tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Detalle</div></th><th><div align='center'>Asistencia&frasl;Estado</div></th><th><div align='center'>Observaciones</div></th></tr>
					<tr><td>" . $asignados2 . "</td></tr>
					</table>
					";
				}
				echo $asignados;
				$datoslistajust = $this->ObjConsulta->selectlistmatjust($this->conect_sistemas_vtv, $id_pauta);
				foreach ($datoslistajust as $llave4 => $valor4) {//todos los recursos justificados con fecha_exp 2222-12-31
					$id_detalle_servicio = $valor4[1];
					$id_informe = $valor4[2];
					$id_estado_informe = $valor4[3];
					$observaciones = $valor4[4];
					$datosnojust = $this->ObjConsulta->selectlistnojust($this->conect_sistemas_vtv, $id_detalle_servicio, $id_pauta);
					$id_estatus_actual = $datosnojust[1][1];
					if ($id_estatus_actual == 35) {
						$datos_estado_inf = $this->ObjConsulta->selectestadoinf($this->conect_sistemas_vtv, $id_estado_informe);
						$desc_estado = $datos_estado_inf[1][1];
						$datosidrecurso = $this->ObjConsulta->selectidrecurso($this->conect_sistemas_vtv, $id_detalle_servicio);
						$id_recurso_asignado = $datosidrecurso[1][1];
						$id_recurso = $datosidrecurso[1][2]; //consulta en detalle servicio el id del recurso y el id recurso asignado para la descripcion
						$datosdesc2 = $this->ObjConsulta->selectdetalles($this->conect_sistemas_vtv, $id_recurso);
						$desc_id_recurso = $datosdesc2[1][1]; // nombre del material o cargo
						$idtipoinventario = $datosdesc2[1][2]; // tipo de recurso
						$datoscaractasignado = $this->ObjConsulta->selectcaracteristicas2($this->conect_sistemas_vtv, $id_recurso_asignado);
						$desc_caracteristicas = $datoscaractasignado[1][2]; // caracteristicas del material o del cargo
						$botonM = "<input type=\"button\" class='boton' value=\"Modificar\" OnClick=modificarinforme($id_pauta,$id_informe,$id_detalle_servicio,$llave4,$ced_sesion);>"; // un update
						$justificados2.="<tr id='$id'><td  align='left'>" . strtoupper(utf8_encode($desc_id_recurso)) . "</td><td  align='left'>" . strtoupper(utf8_encode(str_replace("|", "<BR>", $desc_caracteristicas))) . "</td><td  align='center'>" . utf8_encode($desc_estado) . "</td><td  align='left'>" . utf8_encode($observaciones) . "</td><td  align='left'>" . $botonM . "</td></tr>";
					}
					$titulo3 = "LISTA DE RECURSOS JUSTIFICADOS";
					$justificados = "<table class='tabla' style='width:850px'; id='listarecursosasignados'>
					<tr><th class='titulo' colspan='8'>" . $titulo3 . "</th></tr>
					<tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Detalle</div></th><th><div align='center'>Asistencia&frasl;Estado</div></th><th><div align='center'>Observaciones</div></th></tr>
					<tr><td>" . $justificados2 . "</td></tr>
					</table>
					<input name='canttotal'  id='canttotal' type='hidden' value=" . count($datoslista) . " />
					<input name='cantjust'  id='cantjust' type='hidden' value=" . count($datoslistajust) . " />
					";
				}
				echo $justificados;
			}
		} else {
			echo"<script>var pagina='classRegistro.php';
			alert('Disculpa no tiene permitido el acceso a esta pagina.');
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 0);
			</script>
			";
		}
	}

	function modificarinforme() {
		$id_informe = $_GET['id_informe'];
		$id_pauta = $pauta = $_GET['id_pauta'];
		$id_detalle_servicio = $_GET['id_detalle_servicio'];
		$ced_sesion = $_GET['ced_sesion'];
		$id_estatus = 23; //modificado en el informe
		$administrador = $_SESSION['id_tipo_usuario'];
		if ($administrador == 26) {
			$verificarestatus = $this->ObjConsulta->verificarestatus($this->conect_sistemas_vtv, $id_detalle_servicio);
			$estatusactual = $verificarestatus[1][1];
			if ($estatusactual == 23) {
				echo"<script>var pagina='classinformedeprod.php?id_pauta=" . $id_pauta . "&lista=classlistadeinformes.php';
				alert('Disculpa el recurso ya fue modificado');
				function redireccionar() {
					location.href=pagina;
				}
				setTimeout ('redireccionar()', 1000);
				</script>
				";
			} else {
				$cambiarinf = $this->ObjConsulta->updateexpestatusinf($this->conect_sistemas_vtv, $id_informe, $ced_sesion);
				$updateexpestatusrecursos = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $id_detalle_servicio);
				$verrecursoasignado = $this->ObjConsulta->selectverrecursoasignado($this->conect_sistemas_vtv, $id_detalle_servicio);
				$id_recurso_asignado = $verrecursoasignado[1][1];
				$insertestatusjust = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $id_pauta, $id_detalle_servicio, $id_estatus, $ced_sesion, $id_recurso_asignado);



				$datoslista = $this->ObjConsulta->selectlistmatasig2($this->conect_sistemas_vtv, $id_pauta);
            foreach ($datoslista as $llave3 => $valor3) {
                $id_detalle_servicio = $valor3[1];
                $id_recurso_asignado = $valor3[2];
                $id_recurso = $valor3[3];
                $id_tipo_recurso = $valor3[4];


                $datosnojust = $this->ObjConsulta->selectlistnojust($this->conect_sistemas_vtv, $id_detalle_servicio, $id_pauta);
                $id_estatus_actual = $datosnojust[1][1];


                if ($id_estatus_actual == 13 or $id_estatus_actual == 14 or  $id_estatus_actual == 20 or $id_estatus_actual == 23 or $id_estatus_actual == 28) {
                if($id_tipo_recurso==1){
                    $datosmaterial = $this->ObjConsulta->selectlistmatdescnuevo($this->conect_sistemas_vtv, $id_recurso);
                    $desc_id_recurso = $datosmaterial[1][1];
                    $datoscaract = $this->ObjConsulta->selectdatosrecasig($this->conect_sistemas_vtv, $id_recurso_asignado);
                    $marca=$datoscaract[1][1];
                    $modelo=$datoscaract[1][2];
                    $bien_nac=$datoscaract[1][3];
                        $primero = "Marca: " . $marca . "";
                        $segundo = "Modelo: " . $modelo . "";
                        $tercero = "Bien Nac.:: " . $bien_nac . "";
                        $desc_caracteristicas = "" . $primero . "&nbsp;" . $segundo . "&nbsp;" . $tercero . " ";
                }else{
                    $datosmaterial = $this->ObjConsulta->select_cargos($this->conect_sistemas_vtv, $id_recurso);
                    $desc_id_recurso=$datosmaterial[1][2];
                    $recursos = $this->ObjConsulta->get_recursos_humanos($this->conect_sistemas_vtv,$id_recurso_asignado);
                    $desc_caracteristicas= utf8_encode($recursos[1][6]) ." ".utf8_encode($recursos[1][7]);
                }


                    if ($id_tipo_recurso == 2) {
                        $asistio = "<select id='asistio" . $llave3 . "' style='width:110px';>
                        <option value='0' selected>Seleccione</option>
                        <option value='1'>Si asistio</option>
                        <option value='2'>No asistio</option>
                        </select>";
                    } else {
                        $asistio = "<select id='asistio" . $llave3 . "'>
                        <option value='0' selected>Seleccione</option>
                        <option value='3'>Bueno</option>
                        <option value='4'>Regular</option>
                        <option value='5'>Malo</option>
                        <option value='6'>No entregado</option>
                        </select>";
                    }
                    $observaciones = "<textarea name='observaciones" . $llave3 . "' id='observaciones" . $llave3 . "' rows='2' cols='30'></textarea>";
                    $botonG = "<input type=\"button\" class='boton' value=\"Guardar\" OnClick=guardarinforme($pauta,$id_detalle_servicio,$llave3,$ced_sesion);>";
                    $asignados2.="<tr id='$id'><td  align='left'>" . strtoupper($desc_id_recurso) . "</td><td  align='left'>".$desc_caracteristicas."</td><td  align='left'>" . $asistio . "</td><td  align='left'>" . $observaciones . "</td><td  align='left'>" . $botonG . "</td></tr>";
                }



                $titulo2 = "LISTA DE RECURSOS";
                $asignados = "<table class='tabla' style='width:850px'; id='listarecursosasignados'>
                <tr><th class='titulo' colspan='8'>" . $titulo2 . "</th></tr>
                <tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Detalle</div></th><th><div align='center'>Asistencia&frasl;Estado</div></th><th><div align='center'>Observaciones</div></th></tr>
                <tr><td>" . $asignados2 . "</td></tr>
                </table>
                ";
            }

				echo $asignados;
				$justificados=$this->materiales_justificados($_GET['id_pauta']);
				echo $justificados;
			}
		} else {
			echo"<script>var pagina='classRegistro.php';
			alert('Disculpa no tiene permitido el acceso a esta pagina.');
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 0);
			</script>
			";
		}
	}

	function modificarasignado() {
		$id_pauta = $_GET['id_pauta'];
		$id_detalle_servicio = $_GET['id_detalle_servicio'];
		$ced_sesion = $_SESSION['cedula'];
		$id_estatus = 11; //modificado por las areas operativas
		$gerencia = $_SESSION['gerencia'];
		$division = $_SESSION['division'];
		$administrador = $_SESSION['id_tipo_usuario'];


		if ($administrador == 25) {
			$verificarestatuspauta = $this->ObjConsulta->verificarestatuspauta($this->conect_sistemas_vtv, $id_pauta);
			$estatusactual = $verificarestatuspauta[1][1];
			if ($estatusactual == 21) {
				echo"<script>var pagina='classlistadepautasao.php';
				alert('Disculpa la pauta ya fue aprobada por la Unidad de Apoyo Logistico');
				function redireccionar() {
					location.href=pagina;
				}
				setTimeout ('redireccionar()', 0);
				</script>
				";
			} else {
				$updateexpestatusrecursos = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $id_detalle_servicio); // expira el recurso que se asigno
				$verrecursoasignado = $this->ObjConsulta->selectverrecursoasignado($this->conect_sistemas_vtv, $id_detalle_servicio); // id_recurso_asignado en el
				$id_recurso_asignado = $verrecursoasignado[1][1];
				$id_gerencia = $verrecursoasignado[1][2];
				$id_division = $verrecursoasignado[1][3];
				$updatedelistaasignados = $this->ObjConsulta->updatedelistaasignados($this->conect_sistemas_vtv, $id_detalle_servicio, "1", $_SESSION['cedula'], $id_gerencia,$id_division);
				$insertestatusjust = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $id_pauta, $id_detalle_servicio, $id_estatus, $ced_sesion, $id_recurso_asignado);

				if($_POST['id_tipo_rec']=="1"){
					//almacen tecnico.
					$updateestalmtec = $this->ObjConsulta->updateestalmtec($this->conect_sistemas_vtv, $id_recurso_asignado,1);
					$updatesolicitudes = $this->ObjConsulta->updatesolicitudes($this->conect_sistemas_vtv, $id_pauta, $id_recurso_asignado);
				}
				$materiales=$this->materiales_modificados_ao($id_pauta);
    			$asignados=$this->materiales_con_caracteristicas($id_pauta,$_SESSION['gerencia'], $_SESSION['division']);
    			$botonC = "<input type=\"button\" class='boton' value=\"Confirmar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
	    		$codHtml="";
	    		$codHtml="
	    		<div id='datosp2' align='center'>
	    		<div id='cargando2' align='center'></div>
	    		" . $reportes_assoc . "
	    		" . $materiales . "
	    		" . $asignados . "
	    		</div>
	    		";
	    		echo $codHtml;

			}


		} else {
			echo"<script>var pagina='classRegistro.php';
			alert('Disculpa no tiene permitido el acceso a esta pagina.');
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 0);
			</script>
			";
		}
	}

	function estatus_f5() {
		$pauta = $_GET['nsolicitud_aux'];
		$estatus = $this->ObjConsulta->selectestatus($this->conect_sistemas_vtv, $pauta);
		$id_estatus = $estatus[1][1];
		if (count($estatus) > 0) {
			$descestatus = $this->ObjConsulta->selectdescestatus($this->conect_sistemas_vtv, $id_estatus);
			$descripcion = $descestatus[1][1];
			$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El estatus de la pauta numero " . $pauta . " es : " . ucwords($descripcion) . " <div><br>";
			echo $this->ObjMensaje->InterfazExitosamente($mensaje);
			echo"<script>var pagina='classbienvenida.php';
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 3000);
			</script>";
		} else {
			$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La pauta " . $pauta . " no existe<div><br>";
			echo $this->ObjMensaje->InterfazExitosamente($mensaje);
			echo"<script>var pagina='classbienvenida.php';
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 3000);
			</script>";
		}
	}

	function combodivisionf5_hb() {
		$division = "<select id='iddivision' name='iddivision'>
		<option value=0 selected='selected' >Seleccione &nbsp;</option>";
		$data_division = $this->ObjConsulta->selectdivision($this->conect_sistemas_vtv, $_GET['idgerencia']);
		foreach ($data_division as $llave => $valor) {
			$division.="<option value='{$valor[1]}'>" . utf8_encode($valor[2]) . "</opcion>";
		}
		$division.="</select>";
		echo $division;
	}

	function combotipousuario() {
		if ($_GET["tipousuarioaux"] == 2 or $_GET["tipousuarioaux"] == 3) {
			$mensaje = "<div style='color: #009900;font-weight: bold;'><br>Se guardo con exito<div><br>";
			echo $this->ObjMensaje->InterfazExitosamente($mensaje);
		}
	}

	function comboperfil() {
		if ($_GET["perfilmodaux"] == 2 or $_GET["perfilmodaux"] == 3) {
			$codigo = "<input type='password' name='codigo' id='codigo' class='campo' size='6' maxlength='6'/><td>&nbsp;Ingrese su código de autorización</td>";
			echo $codigo;
		}
	}

	function guardarservicio_f5() {
		$tiposerv = $_GET["tiposerv_aux"];
		$descripcion = $_GET["descripcion_aux"];
		$insertserv = $this->ObjConsulta->insertservicio_f5($this->conect_sistemas_vtv, $descripcion, $tiposerv);
		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El servicio se guardo con exito<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
	}

	function guardarprograma() {
		$programa = $_GET["programa_aux"];
		$insertserv = $this->ObjConsulta->insertprograma($this->conect_sistemas_vtv, $programa);
		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El programa se guardo con exito<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
		echo"<script>var pagina='classlista.php?modulo=pag_listaprogra';
		function redireccionar() {
			location.href=pagina;
		}
		setTimeout ('redireccionar()', 3000);
		</script>";
	}

	function guardartipotraje() {
		$tipotraje = $_GET["tipotraje_aux"];
		$insertserv = $this->ObjConsulta->inserttipotraje($this->conect_sistemas_vtv, $tipotraje);
		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El tipo de traje se guardo con exito<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
		echo"<script>var pagina='classlista.php?modulo=pag_listatipotra';
		function redireccionar() {
			location.href=pagina;
		}
		setTimeout ('redireccionar()', 3000);
		</script>";
	}

	function combopauta_f5() {
		if ($_GET["pautaaux"] == 1) {
			$datosprograma = $this->ObjConsulta->selectprograma($this->conect_sistemas_vtv);
			$programa = "<select id='programa' name='programa' style='width:350px;'>";
			$programa.="<option value='0' selected >Seleccione </option>";
			foreach ($datosprograma as $llave => $valor) {
				if ($valor[1] > 0) {
					$programa.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
				}
			}
			$programa.="</SELECT> ";
			echo $programa;
		}
		if ($_GET["pautaaux"] == 2) {

			$evento = "<input type='text' name='evento' id='evento' size='30'  maxlength='40' class='campo' style='width:350px; height:10px;margin:0px; padding:0;' />";

			echo $evento;
		}
	}

	function combolocacion_f5() {
		$locacion = $_GET['locacion'];
		if ($_GET["locacion"] == 1) {
			$lugar_lef = "<input type='text' name='lugar_pt' id='lugar_pt' class='campo' size='20'  maxlength='30' style='width:250px;' />";
		}
		if ($_GET["locacion"] == 2) {
			$datoslugar2 = $this->ObjConsulta->selectlugar2($this->conect_sistemas_vtv, "");
			$lugar_lef = "<select id='lugar_lef' name='lugar_lef' style='width:250px;'>";
			$lugar_lef.="<option value='0' selected >Seleccione </option>";
			foreach ($datoslugar2 as $llave => $valor) {
				$lugar_lef.="<option value='" . $valor[2] . "' $selected >" . $valor[2] . "</option>";
			}
			$lugar_lef.="</SELECT> ";
		}
		echo $lugar_lef;
	}

	function cambiarestatus() {
		$id_pauta = $_GET['id_pauta'];
		$estatuspauta = $_GET['estatuspauta'];
		$cedula_sesion = $_SESSION['cedula'];

		$updateexpestatus = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $id_pauta);
		$insertestatusnuevo = $this->ObjConsulta->insertestatusnuevo($this->conect_sistemas_vtv, $id_pauta, $estatuspauta, $cedula_sesion);

		if($estatuspauta==21){
			//Generar solicitud de prestamo
			$id_destino=6;
			$id_remoto=0;
			$id_desc_destino="F5";
			$fecha_reg=date("Y-m-d");
			$id_estatus=4;
			$observaciones="Solicitud de Facilidades Tecnicas #".$id_pauta."";
			$tipo_prestamo=10;
			$hora_reg=date("H:i:s");
			$user_reg="Sistema F5";
			//resp_prestamo=user_reg de t_pauta
			$dat_pauta= $this->ObjConsulta->selectresppauta($this->conect_sistemas_vtv, $id_pauta);
			$resp_prestamo=$dat_pauta[1][1];
			$id_citacion=$dat_pauta[1][2];
			//fecha_sol=fecha_citacion
			//hora_prestamo=hora_citacion
			$dat_citacion= $this->ObjConsulta->selectdatcit($this->conect_sistemas_vtv, $id_citacion);
			$fecha_sol=$dat_citacion[1][1];
			$hora_prestamo=$dat_citacion[1][2];

			$insertsolalmtec = $this->ObjConsulta->insertsolalmtec($this->conect_sistemas_vtv,$id_destino,$id_desc_destino,
			$fecha_reg,$id_estatus,$observaciones,$tipo_prestamo,$hora_reg,$resp_prestamo,$fecha_sol,
			$hora_prestamo,$user_reg,$id_remoto);
			$id_prestamo=$insertsolalmtec[0];

			$datoslista = $this->ObjConsulta->selectlistmatasig($this->conect_sistemas_vtv, $id_pauta);
    		if(count($datoslista)>0){
	    		foreach ($datoslista as $llave3 => $valor3) {
	    			$id_recurso_asignado = $valor3[1];
	    			$id_tipo_rec = $valor3[4];
	    			if($id_tipo_rec==1){
	    				$estatus=2;
						//ingresar los articulo en t_materiales_sol
						$insertmatalmtec = $this->ObjConsulta->insertmatalmtec($this->conect_sistemas_vtv,$id_prestamo,$id_recurso_asignado,$fecha_sol,$user_reg);
						//ingresar en t_solicitudes_f5 el numero de la solicitud de prestamo
						$updatesolalmtec = $this->ObjConsulta->updatesolalmtecf5($this->conect_sistemas_vtv,$id_pauta,$id_prestamo,$id_recurso_asignado);
						//cambiar estatus de los articulos a 2 (prestado)
						$updatestart = $this->ObjConsulta->updatestart($this->conect_sistemas_vtv,$id_recurso_asignado,$estatus);
					}
				}
			}
		}


		if ($estatuspauta == 8) {
			$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La pauta fue validada con exito<div><br>";
			echo $this->ObjMensaje->InterfazExitosamente($mensaje);
			echo"<script>var pagina='classlista.php?modulo=listadepautas';
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 2500);
			</script>
			";
		} else {

			$pagina = '11';
			$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La pauta fue aprobada con exito<div><br>";
			echo $this->ObjMensaje->InterfazExitosamente($mensaje,($this->pasosdelproceso($pagina)));
			echo"<script>var pagina='classlista.php?modulo=listadepautas';
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 2500);
			</script>
			";
		}
	}

	function cambiarestatusrech() {
		$lista = $_GET['lista'];
		$id_pauta = $_GET['id_pauta'];
		$estatuspauta = $_GET['estatuspauta'];
		$obs = $_GET['obs_aux'];
		$ced_sesion = $_SESSION['cedula'];
		$verificarestatuspauta = $this->ObjConsulta->verificarestatuspauta($this->conect_sistemas_vtv, $id_pauta);
		$estatusactual = $verificarestatuspauta[1][1];
		if ($estatusactual == 26) {
			echo"<script>var pagina='" . $lista . "';
			alert('Disculpe la pauta ya fue realizada');
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 0);
			</script>
			";
		} else {
			$updateexpestatus2 = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $id_pauta);
			$insertestatusrech = $this->ObjConsulta->insertestatusnuevorech($this->conect_sistemas_vtv, $id_pauta, $estatuspauta, $obs, $ced_sesion);
			if ($estatuspauta == 18) {
				$estatus_recurso = 12;
				$recursosdelapauta = $this->ObjConsulta->selectrecursosdelapauta($this->conect_sistemas_vtv, $id_pauta);
				foreach ($recursosdelapauta as $llave => $valor) {
					$id_detalle_servicio = $valor[1];
					$updateexpestatusrecursos = $this->ObjConsulta->updateexpestatusrecursosf5($this->conect_sistemas_vtv, $id_pauta, $id_detalle_servicio);
					$verrecursoasignado = $this->ObjConsulta->selectverrecursoasignado($this->conect_sistemas_vtv, $id_detalle_servicio);
					$id_recurso_asignado = $verrecursoasignado[1][1];
					$insertestatusrecursoanulado = $this->ObjConsulta->insertestatusrecursoanulado($this->conect_sistemas_vtv, $id_pauta, $ced_sesion, $id_detalle_servicio, $estatus_recurso, $id_recurso_asignado); //insertado
				}

				$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La pauta fue anulada con exito<div><br>";
				echo $this->ObjMensaje->InterfazExitosamente($mensaje);

				echo"<script>var pagina='" . $lista . "';
				function redireccionar() {
					location.href=pagina;
				}
				setTimeout ('redireccionar()', 3000);
				</script>
				";
			} else {
				$estatus_recurso = 27;
				$recursosdelapauta = $this->ObjConsulta->selectrecursosdelapauta($this->conect_sistemas_vtv, $id_pauta);
				foreach ($recursosdelapauta as $llave => $valor) {
					$id_detalle_servicio = $valor[1];
					$updateexpestatusrecursos = $this->ObjConsulta->updateexpestatusrecursosf5($this->conect_sistemas_vtv, $id_pauta, $id_detalle_servicio);
					$verrecursoasignado = $this->ObjConsulta->selectverrecursoasignado($this->conect_sistemas_vtv, $id_detalle_servicio);
					$id_recurso_asignado = $verrecursoasignado[1][1];
					$insertestatusrecursoanulado = $this->ObjConsulta->insertestatusrecursoanulado($this->conect_sistemas_vtv, $id_pauta, $ced_sesion, $id_detalle_servicio, $estatus_recurso, $id_recurso_asignado); //insertado
				}

				$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La pauta fue rechazada con exito<div><br>";
				echo $this->ObjMensaje->InterfazExitosamente($mensaje);

				echo"<script>var pagina='" . $lista . "';
				function redireccionar() {
					location.href=pagina;
				}
				setTimeout ('redireccionar()', 3000);
				</script>
				";
			}
		}
	}

	function suspenderpauta(){
		$lista = $_GET['lista'];
		$id_pauta = $_GET['id_pauta'];
		$estatuspauta = $_GET['estatuspauta'];
		$obs = $_GET['obs_aux'];
		$ced_sesion = $_SESSION['cedula'];
		$verificarestatuspauta = $this->ObjConsulta->verificarestatuspauta($this->conect_sistemas_vtv, $id_pauta);
		$estatusactual = $verificarestatuspauta[1][1];
		if ($estatusactual == 32) {
			echo"<script>var pagina='" . $lista . "';
			alert('Disculpe la pauta ya fue suspendida');
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 0);
			</script>
			";
		} else {
			$updateexpestatus2 = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $id_pauta);
			$insertestatusrech = $this->ObjConsulta->insertestatusnuevorech($this->conect_sistemas_vtv, $id_pauta, $estatuspauta, $obs, $ced_sesion);
			$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La pauta fue suspendida con exito<div><br>";
			echo $this->ObjMensaje->InterfazExitosamente($mensaje);
			echo"<script>var pagina='" . $lista . "';
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 3000);
			</script>
			";
		}
	}

	function cambiarestatusanulada(){
		$id_pauta = $_GET['npauta'];
		$obs = $_GET['obs'];
		$estatuspauta = 18;
		$pautafinalizada = 19;
		$lista = $_GET['lista'];
		$estatus2 = $this->ObjConsulta->selectestatus($this->conect_sistemas_vtv, $id_pauta);
		$estatus = $estatus2[1][1];
		if ($estatus == "") {
			$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La pauta no existe, intente de nuevo<div><br>";
			echo $this->ObjMensaje->InterfazExitosamente($mensaje);
			echo"<script>var pagina='classanularpauta.php';
			function redireccionar() {
				location.href=pagina;
			}
			setTimeout ('redireccionar()', 3000);
			</script>
			";
		} else {
			if ($estatus == $pautafinalizada) {
				$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La pauta ya fue realizada<div><br>";
				echo $this->ObjMensaje->InterfazExitosamente($mensaje);
				echo"<script>var pagina='classanularpauta.php';
				function redireccionar() {
					location.href=pagina;
				}
				setTimeout ('redireccionar()', 3000);
				</script>
				";
			} else {
				$updateexpestatus2 = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $id_pauta);
				$updateexpestatusrecursos = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $id_pauta);
				$insertestatusrecursoanulado = $this->ObjConsulta->insertestatusrecursoanulado($this->conect_sistemas_vtv, $id_pauta);
				$insertestatusanulada = $this->ObjConsulta->insertestatusnuevoanulada($this->conect_sistemas_vtv, $id_pauta, $estatuspauta, $obs);

				$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La pauta fue anulada con exito<div><br>";
				echo $this->ObjMensaje->InterfazExitosamente($mensaje);
				echo"<script>var pagina='" . $lista . "';

				function redireccionar() {
					location.href=pagina;
				}
				setTimeout ('redireccionar()', 3000);
				</script>
				";
			}
		}
	}

	function descripciondegerencia(){
		$idgerencia = $_GET['gerencia'];
		$desc_gerencia = $this->ObjConsulta->selectestatus($this->conect_sistemas_vtv, $id_pauta);
		$estatus = $estatus2[1][1];
	}

	function botonesdereportes($id_pauta,$cedula,$estatus){
		if ($estatus== 18 or $estatus== 32 ) {
			$fec_estatus_anterior = $this->ObjConsulta->fechaestatusanterior($this->conect_sistemas_vtv, $id_pauta);
			$estatus_ant = $fec_estatus_anterior[2][3];
			$estatus = $estatus_ant;
		} else {
			$estatus = $estatus;
		}
		if ($estatus== 1 or $estatus== 3 or $estatus== 25){
			$botones_rep = "";
		}elseif ($estatus== 5 or $estatus== 6 or $estatus== 8 or $estatus== 9 or $estatus== 29 or $estatus== 34){
			$botones_rep = "<table class='tabla' style='width:850px'; id='reportes_asociados_pauta'>
			<tr><th class='titulo' colspan='8'>REPORTES</th></tr>
			<tr><th></th></tr>
			<tr><td align='center'></td><td align='center'><input type=\"button\" class=\"boton\" value=\"Modificaciones\" onclick=\"CancelarRegresar('../reportes/reporte_mod.php?id_pauta=$id_pauta&lista=classlistadereportes.php&cedula=" . $cedula . "');\" /></td></tr>
			</table>";
		}elseif ($estatus== 7 or $estatus== 10 or $estatus== 22){
			$botones_rep = "<table class='tabla' style='width:850px'; id='reportes_asociados_pauta'>
			<tr><th class='titulo' colspan='8'>REPORTES</th></tr>
			<tr><th></th></tr>
			<tr><td align='center'></td><td align='center'><input type=\"button\" class=\"boton\" value=\"Modificaciones\" onclick=\"CancelarRegresar('../reportes/reporte_mod.php?id_pauta=$id_pauta&lista=classlistadereportes.php&cedula=" . $cedula . "');\" /></td><td align='center'><input type=\"button\" class=\"boton\" value=\"Personal Asignado\" onclick=\"CancelarRegresar('../reportes/reporte_pers.php?id_pauta=$id_pauta&lista=classlistadereportes.php&cedula=" . $cedula . "');\" /></td><td align='center'><input type=\"button\" class=\"boton\" value=\"Recursos Asignado\" onclick=\"CancelarRegresar('../reportes/reporte_recur.php?id_pauta=$id_pauta&lista=classlistadereportes.php&cedula=" . $cedula . "');;\" /></td></tr>
			</table>";
		}elseif ($estatus== 21){
			$botones_rep = "<table class='tabla' style='width:850px'; id='reportes_asociados_pauta'>
			<tr><th class='titulo' colspan='8'>REPORTES</th></tr>
			<tr><th></th></tr>
			<tr><td align='center'></td><td align='center'><input type=\"button\" class=\"boton\" value=\"Modificaciones\" onclick=\"CancelarRegresar('../reportes/reporte_mod.php?id_pauta=$id_pauta&lista=classlistadereportes.php&cedula=" . $cedula . "');\" /></td><td align='center'><input type=\"button\" class=\"boton\" value=\"Personal Asignado\" onclick=\"CancelarRegresar('../reportes/reporte_pers.php?id_pauta=$id_pauta&lista=classlistadereportes.php&cedula=" . $cedula . "');\" /></td><td align='center'><input type=\"button\" class=\"boton\" value=\"Recursos Asignado\" onclick=\"CancelarRegresar('../reportes/reporte_recur.php?id_pauta=$id_pauta&lista=classlistadereportes.php&cedula=" . $cedula . "');;\" /></td><td align='center'><input type=\"button\" class=\"boton\" value=\"Remoto\" onclick=\"CancelarRegresar('../reportes/reporte_recur_aprob.php?id_pauta=$id_pauta&lista=classlistadereportes.php&cedula=" . $cedula . "');;\" /></td></tr>
			</table>";
		}elseif ($estatus== 19 or $estatus== 26){
			$botones_rep = "<table class='tabla' style='width:850px'; id='reportes_asociados_pauta'>
			<tr><th class='titulo' colspan='8'>REPORTES</th></tr>
			<tr><th></th></tr>
			<tr><td align='center'></td><td align='center'><input type=\"button\" class=\"boton\" value=\"Modificaciones\" onclick=\"CancelarRegresar('../reportes/reporte_mod.php?id_pauta=$id_pauta&lista=classlistadereportes.php&cedula=" . $cedula . "');\" /></td><td align='center'><input type=\"button\" class=\"boton\" value=\"Personal Asignado\" onclick=\"CancelarRegresar('../reportes/reporte_pers.php?id_pauta=$id_pauta&lista=classlistadereportes.php&cedula=" . $cedula . "');\" /></td><td align='center'><input type=\"button\" class=\"boton\" value=\"Recursos Asignado\" onclick=\"CancelarRegresar('../reportes/reporte_recur.php?id_pauta=$id_pauta&lista=classlistadereportes.php&cedula=" . $cedula . "');;\" /></td><td align='center'><input type=\"button\" class=\"boton\" value=\"Remoto\" onclick=\"CancelarRegresar('../reportes/reporte_recur_aprob.php?id_pauta=$id_pauta&lista=classlistadereportes.php&cedula=" . $cedula . "');;\" /></td><td align='center'><input type=\"button\" class=\"boton\" value=\"Informe de Cierre\" onclick=\"CancelarRegresar('../reportes/reporte_inf_prod.php?id_pauta=$id_pauta&lista=classlistadereportes.php&cedula=" . $cedula . "');\" /></td></tr>
			</table>";
		}else {
			$botones_rep = "";
		}
		return $botones_rep;
	}

	function pasosdelproceso($pagina){
		if ($pagina == '1'){
			$pasos=$this->ObjMensaje->interfazprocesotabla("Ingresando", 1, 1);
		}
		elseif ($pagina == '2') {
			$pasos=$this->ObjMensaje->interfazprocesotabla("Ingresando", 2, 1);
		}
		elseif ($pagina == '3') {
			$pasos=$this->ObjMensaje->interfazprocesotabla("Ingresando", 3, 1);
		}
		elseif ($pagina == '4') {
			$pasos=$this->ObjMensaje->interfazprocesotabla("Modificando", 1, 1);
		}
		elseif ($pagina == '5') {
			$pasos=$this->ObjMensaje->interfazprocesotabla("Modificando", 2, 1);
		}
		elseif ($pagina == '6') {
			$pasos=$this->ObjMensaje->interfazprocesotabla("Modificando", 3, 1);
		}

		elseif ($pagina == '7') {
			$pasos=$this->ObjMensaje->interfazprocesotabla("", 1, 2);
		}
		elseif ($pagina == '8') {
			$pasos=$this->ObjMensaje->interfazprocesotabla("", 2, 2);
		}
		elseif ($pagina == '9') {
			$pasos=$this->ObjMensaje->interfazprocesotabla("", 3, 2);
		}
		elseif ($pagina == '10') {
			$pasos=$this->ObjMensaje->interfazprocesotabla("", 4, 2);
		}
		elseif ($pagina == '11') {
			$pasos=$this->ObjMensaje->interfazprocesotabla("", 5, 2);
		}
		else{
			$pasos = " ";
		}

		return $pasos;
	}

	function leyenda($tipo=0) {
		switch ($tipo) {
			case 0:
			$leyenda = "";
			break;
			case 1:
			$leyenda = "<table class='tabla'>
			<tr><th class='titulo' colspan='19' align=\"center\">LEYENDA</th></tr>
			<tr align=\"center\"><th><td><img src='../estilos/imagenes/estatus/eliminado.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/buscar.gif'></th></td>
			<th><td><img src='../estilos/imagenes/estatus/nuevo.gif'></td></th>
			<th><td><img src='../estilos/imagenes/estatus/page_edit.png'></td></th>
			<th><td><img src='../estilos/imagenes/estatus/script_add.png'></td></th>
			<th><td><img src='../estilos/imagenes/estatus/aprobado.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/entregado.gif' ></td></th>
			</tr>
			<tr align=\"center\"><th><td>Anular Pauta &nbsp;</td></th>
			<th><td>Mostrar Detalles &nbsp;</td></th>
			<th><td>Agregar Recursos</td></th>
			<th><td>Modificar</td></th>
			<th><td>Asignar Recursos &nbsp;</td></th>
			<th><td>Pauta Aprobada</td></th>
			<th><td>Enviado a las<br>Areas Operativas &nbsp;</td></th>
			</tr>
			</table>";
			break;
			case 2:
			$leyenda = "<table class='tabla'>
			<tr><th class='titulo' colspan='19' align=\"center\">LEYENDA</th></tr>
			<tr align=\"center\">
			<!--<th><td><img src='../estilos/imagenes/estatus/por-entregar.gif'></td></th>
			<th><td><img src='../estilos/imagenes/estatus/aprobado.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/stop_round.png' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/entregado.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/page_edit.png'></td></th>-->
			<th><td><img src='../estilos/imagenes/estatus/script_add.png'></td></th>
			<th><td><img src='../estilos/imagenes/estatus/eliminado.gif'></td></th>
			<th><td><img src='../estilos/imagenes/estatus/buscar.gif'></td></th>
			<th><td><img src='../estilos/imagenes/estatus/script.png'></td></th></tr>
			<tr align=\"center\">
			<!--<th><td>Por Validar &nbsp;</td></th>
			<th><td>Validada &nbsp;</td></th>
			<th><td>Rechazada &nbsp;</td></th>
			<th><td>Enviado a las<br>Areas Operativas &nbsp;</td></th>
			<th><td>Modificado por<br> las Áreas Operativas&nbsp;</td></th>-->
			<th><td>Asignar Recursos &nbsp;</td></th>
			<th><td>Anular Pauta &nbsp;</td></th>
			<th><td>Observar detalles &nbsp;</td></th>
			<th><td>Informe en<br> Proceso</td></th></tr>
			</table>";
			break;
			case 3:
			$leyenda = "<table class='tabla'>
			<tr><th class='titulo' colspan='5' align=\"center\">LEYENDA</th></tr>
			<tr align=\"center\"><th><td><img src='../estilos/imagenes/estatus/buscar.gif' ></td></th>
			<tr align=\"center\"><th><td>Mostrar Detalles</td></th></tr>
			</table>";
			break;
			case 4:
			$leyenda = "<table class='tabla'>
			<tr><th class='titulo' colspan='10' align=\"center\">LEYENDA</th></tr>
			<tr align=\"center\"><th><td><img src='../estilos/imagenes/estatus/eliminado.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/buscar.gif'></th></td>
			<th><td><img src='../estilos/imagenes/estatus/stop_round.png'></th></td>
			<th><td><img src='../estilos/imagenes/estatus/nuevo.gif'></td></th>
			<th><td><img src='../estilos/imagenes/estatus/page_edit.png'></td></th>
			</tr>
			<tr align=\"center\"><th><td>Anular Pauta &nbsp;</td></th>
			<th><td>Mostrar Detalles &nbsp;</td></th>
			<th><td>Suspender&nbsp;</td></th>
			<th><td>Agregar Recursos</td></th>
			<th><td>Modificar</td></th>
			</tr>
			</table>";
			break;
			case 5:
			$leyenda = "<table class='tabla'>
			<tr><th class='titulo' colspan='6' align=\"center\">LEYENDA</th></tr>
			<tr align=\"center\"><th><td><img src='../estilos/imagenes/estatus/por-entregar.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/aprobado.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/buscar.gif' ></td></th></tr>
			<tr align=\"center\"><th><td>Por confirmar &nbsp;</td></th>
			<th><td>Pauta Aprobada</td></th>
			<th><td>Modificada por las<br> Áreas Operativas</td></th>
			</tr></table>";
			break;
			case 6:
			$leyenda = "<table class='tabla'>
			<tr><th class='titulo' colspan='6' align=\"center\">LEYENDA</th></tr>
			<tr align=\"center\"><th><td><img src='../estilos/imagenes/estatus/por-entregar.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/aprobado.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/buscar.gif' ></td></th></tr>
			<tr align=\"center\"><th><td>Por confirmar &nbsp;</td></th>
			<th><td>Pauta Aprobada</td></th>
			<th><td>Modificada por las<br> Áreas Operativas</td></th>
			</tr></table>";
			break;
			case 7:
			$leyenda = "<table class='tabla'>
			<tr><th class='titulo' colspan='6' align=\"center\">LEYENDA</th></tr>
			<tr align=\"center\"><th><td><img src='../estilos/imagenes/estatus/pdf.png' ></td></th></tr>
			<tr align=\"center\">
			<th><td>Imprimir PDF</td></th>
			</tr></table>";
			break;
			case 8:
			$leyenda = "<table class='tabla'>
			<tr><th class='titulo' colspan='6' align=\"center\">LEYENDA</th></tr>
			<tr align=\"center\"><th><td><img src='../estilos/imagenes/estatus/eliminado.gif' ></td></th></tr>
			<tr align=\"center\">
			<th><td>Eliminar</td></th>
			</tr></table>";
			break;
		}
		return $leyenda;
	}

	function listadeusuarios() {
		//print_r($_GET);
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}

		$pagina_ajax = "'listadeusuarios'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}

		if ($_SESSION['id_tipo_usuario'] == 27) {
			//super administrador.
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "usuarios", $campo_rn = "cedula", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "usuarios", $campo_rn = "cedula,nombre_usuario,apellido_usuario,desc_usuario,de_gerencia,de_division", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		} else {

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "usuarios", $campo_rn = "cedula", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and iddivision='{$_SESSION['division']}' and cedula<>'{$_SESSION['cedula']}' and (id_tipo_usuario=20 or id_tipo_usuario=22 or id_tipo_usuario=26 or id_tipo_usuario=25) order by cedula DESC", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "usuarios", $campo_rn = "cedula,nombre_usuario,apellido_usuario,desc_usuario,de_gerencia,de_division", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and iddivision='{$_SESSION['division']}' and cedula<>'{$_SESSION['cedula']}' and (id_tipo_usuario=20 or id_tipo_usuario=22 or id_tipo_usuario=26 or id_tipo_usuario=25) order by cedula DESC", $minimo);
		}

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}

		$boton_agreg = "<input type=\"button\" class='boton' value=\"Agregar Usuario\" OnClick='ir_a(\"classadmagregar.php?pag=1\")'>";
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">".$boton_agreg."</div></th>
		</table>";

		$lista.="
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='5'>USUARIOS</th></tr>
		<tr>
		<th style='200'><div align=\"center\">CEDULA</div></th>
		<th style='200'><div align=\"center\">NOMBRE Y APELLIDO</div></th>
		<th><div align=\"center\">GERENCIA/DIVISION</div></th>
		<th><div align=\"center\">TIPO DE USUARIO</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$ced_cod = base64_encode($valor2[1]);
				$boton_E =  "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Eliminar\" name='button' id='button' OnClick=eliminarusuario_f5($valor2[1]); ><img src='../estilos/imagenes/estatus/eliminado.gif' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_P = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Cambio de perfil\" name='button' id='button' OnClick=CancelarRegresar('classadmperfil.php?cedula=" . $ced_cod . "'); ><img src='../estilos/imagenes/usuario.jpeg' style=\"width:25px;height:25px;border-width:none;border-style:none;\" ></button>";
				$boton_R = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Reiniciar clave\" name='button' id='button' OnClick=reiniciar_clave($valor2[1]); ><img src='../estilos/imagenes/llave.jpeg' style=\"width:25px;height:25px;border-width:none;border-style:none;\" ></button>";
				$lista.="<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . strtoupper(ucwords(utf8_encode($valor2[2]))) . "<BR>" . strtoupper(ucwords(utf8_encode($valor2[3]))) . "</div></td>
				<td $color><div align=\"left\">" . strtoupper(ucwords(utf8_encode($valor2[5]))). "<BR>" . strtoupper(ucwords(utf8_encode($valor2[6]))) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[4]) . "</div></td>
				<td $color><div align=\"center\">" . $boton_E . "&nbsp;" . $boton_P . "&nbsp;" . $boton_R . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 0);
		$lista.="
		<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;usuario(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listatipotraje() {
		//print_r($_GET);
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listatipotraje'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "trajes", $campo_rn = "id_tipo_traje, descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
		$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "trajes", $campo_rn = "id_tipo_traje, descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}

		$boton_agreg = "<input type=\"button\" class='boton' value=\"Agregar Traje\" OnClick='ir_a(\"classtipotraje.php?pag=1\")'>";
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">".$boton_agreg."</div></th>
		</table>";
		$lista.="
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>
		<th style='200'><div align=\"center\">TIPO TRAJE</div></th>
		<th style='200'><div align=\"center\">ACCIONES</div></th>
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$boton_E =  "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Eliminar\" name='button' id='button' OnClick=eliminartraje($valor2[1]); ><img src='../estilos/imagenes/estatus/eliminado.gif' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$lista.="
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . strtoupper ($valor2[2]) . "</div></td>
				<td $color><div align=\"center\">" . $boton_E . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		//$lista.=$this->leyenda($tipo = 8);
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;Traje(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listaprog() {
		//print_r($_GET);
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listaprog'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "programa", $campo_rn = "id_program,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
		$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "programa", $campo_rn = "id_program,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$boton_agreg = "<input type=\"button\" class='boton' value=\"Agregar Programa\" OnClick='ir_a(\"classagrprograma.php?pag=1\")'>";
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">".$boton_agreg."</div></th>
		</table>";
		$lista.="
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>
		<th style='200'><div align=\"center\">PROGRAMAS</div></th>
		<th style='200'><div align=\"center\">ACCIONES</div></th>
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$boton_E =  "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Eliminar\" name='button' id='button' OnClick=eliminarprograma($valor2[1]); ><img src='../estilos/imagenes/estatus/eliminado.gif' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$lista.="
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . strtoupper ($valor2[2]) . "</div></td>
				<td $color><div align=\"center\">" . $boton_E . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		//$lista.=$this->leyenda($tipo = 8);
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;Programa(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listarecursosdisponibles() {
		$todas = $this->ObjConsulta->select_categorias_cargos($this->conect_sistemas_vtv); //presentacion para dummies
		for ($i = 1; $i <= count($todas); $i++) {
			$fila[] = Array("id" => $todas[$i][1], "valor" => utf8_encode($todas[$i][2]));
		}
		echo json_encode($fila);
	}

	function recursosdragandrog() {
		if ($_POST['idtipomaterial'] != "undefined" and $_POST['idtipomaterial'] != "null") {
			$ids = explode(" | ", $_POST['idtipomaterial']); //puede contener valores duplicados
			$array_valores_unicos = array_keys(array_count_values($ids));
			for ($tm = 0; $tm < count($array_valores_unicos); $tm++) {
				$recursos = $this->ObjConsulta->get_recursos_humanos($this->conect_sistemas_vtv,null,$array_valores_unicos[$tm]);
				unset($datospersonal);
				for ($i = 1; $i <= count($recursos); $i++) {
					//var_dump($datospersonal);
					$fila[] = Array("1" => $recursos[$i][1], "2" => "Cédula: " . $recursos[$i][1] . " <br>" . $recursos[$i][6] . " " . $recursos[$i][7] . "<br>" . $recursos[$i][10], "3" => $recursos[$i][1] , "4" => $array_valores_unicos[$tm]);
				}
			}
			//var_dump($fila);
			echo json_encode($fila);

		} else if($_POST["busqueda"]!="undefined" or $_POST["busqueda"]!=""){
			$recursos = $this->ObjConsulta->get_recursos_humanos($this->conect_sistemas_vtv,null,null,null,null,$_POST["busqueda"]);
			for ($i = 1; $i <= count($recursos); $i++) {
				$fila[] = Array("1" => $recursos[$i][1], "2" => "Cédula: " . $recursos[$i][1] . " <br>" . $recursos[$i][6] . " " . $recursos[$i][7] . "<br>" . $recursos[$i][10], "3" => $recursos[$i][1] , "4" => $recursos[$i][4]);
			}
			echo json_encode($fila);
		}else{
			echo "null";
		}
	}

	/**
	funcion que se encarga de guardar un recurso humano desde el asignador de recursos humanos.
	*/
	function guardardrop() {
		//preguntar si existe un valor
		//obtenerFuncion=guardardrop&idmaterialgeneral=6554&idpauta=10&idtmat=177
		//selecciona una pauta que tenga un idrecurso=idtmat y un id_recurso_asignado=1
		$existenoasignado = $this->ObjConsulta->selectlistmatnoasig($this->conect_sistemas_vtv, $_POST['idpauta'], (int)$_POST['idtmat']);

		//obtener gerencia y division del recurso.
		$idsgerenciadiv=$this->ObjConsulta->selectgerendiv($this->conect_sistemas_vtv, $_POST['idmaterialgeneral']);

		if (count($existenoasignado) > 0) {
			//guardar el idmaterial en el requerimiento..
			$guardar_req = $this->ObjConsulta->updatedelistaasignados($this->conect_sistemas_vtv, $existenoasignado[1][1], $_POST['idmaterialgeneral'], $_SESSION['cedula'], $idsgerenciadiv[1][1], $idsgerenciadiv[1][2]);
			$act_estatus = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $existenoasignado[1][1]);
			$act_estatus = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $_POST['idpauta'], $existenoasignado[1][1], '14', $_SESSION['cedula'], $_POST['idmaterialgeneral']);
		}else{
			//$this->conect_sistemas_vtv,1,$id_pauta,$cedula_sesion,$id_tipo_rec,$material,$id_gerencia,$id_division
			$idrequerimiento = $this->ObjConsulta->insertmaterial($this->conect_sistemas_vtv, $_POST['idmaterialgeneral'], $_POST['idpauta'], $_SESSION['cedula'], 2,$_POST['idtmat'],$idsgerenciadiv[1][1], $idsgerenciadiv[1][2]); //material en pauta_detalle_servicio
			$act_estatus = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $_POST['idpauta'], $idrequerimiento[0], '14', $_SESSION['cedula'], $_POST['idmaterialgeneral']); // estado del material en
		}

		//solo UAL
		$selectEstatusPauta=$this->ObjConsulta->selectestatus($this->conect_sistemas_vtv, $_POST['idpauta']);
		$datosobs=$this->ObjConsulta->selectobsrech($this->conect_sistemas_vtv, $_POST['idpauta']);
		
		if($selectEstatusPauta[1][2]!="7"){
			$pautaestatusexp=$this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $_POST['idpauta']);
			//si el usuario es un analista de area operativa entonces...
			if($_SESSION['id_tipo_usuario']=="25"){
				$pautaestatus=$this->ObjConsulta->insertestatusnuevo($this->conect_sistemas_vtv, $_POST['idpauta'], "22", $_SESSION['cedula'], $datosobs[1][1]);
			}else{
				$pautaestatus=$this->ObjConsulta->insertestatusnuevo($this->conect_sistemas_vtv, $_POST['idpauta'], "7", $_SESSION['cedula'], $datosobs[1][1]);
			}
		}
	}

	function eliminardop() {
		$selectEstatusPauta=$this->ObjConsulta->selectestatus($this->conect_sistemas_vtv, $_POST['idpauta']);
		$datosobs=$this->ObjConsulta->selectobsrech($this->conect_sistemas_vtv, $_POST['idpauta']);
		if($selectEstatusPauta[1][2]!="7"){
			$pautaestatusexp=$this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $_POST['idpauta']);
			if($_SESSION['id_tipo_usuario']=="25"){
				$pautaestatus=$this->ObjConsulta->insertestatusnuevo($this->conect_sistemas_vtv, $_POST['idpauta'], "22", $_SESSION['cedula'], $datosobs[1][1]);
			}else{
				$pautaestatus=$this->ObjConsulta->insertestatusnuevo($this->conect_sistemas_vtv, $_POST['idpauta'], "7", $_SESSION['cedula'], $datosobs[1][1]);
			}
		}

		if($_POST['id_tipo_rec']=='1'){
			$recursos_asignados = $this->ObjConsulta->invmaterial_asignados_pauta($this->conect_sistemas_vtv, $_POST['idpauta'], $_POST['idmaterialgeneral']);
			$act_estatus = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $recursos_asignados[1][1]);
			$act_estatus = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $_POST['idpauta'], $recursos_asignados[1][1], '16', $_SESSION['cedula'], $_POST['idmaterialgeneral']);
			$quitando_recursos = $this->ObjConsulta->desacsignar_recursos_mat($this->conect_sistemas_vtv, $_POST['idpauta'], $_POST['idmaterialgeneral'], $_SESSION['cedula']);
			//sistema almacen tecnico
			$updateestalmtec = $this->ObjConsulta->updateestalmtec($this->conect_sistemas_vtv, $_POST['idmaterialgeneral'],1);
			$updatesolicitudes = $this->ObjConsulta->updatesolicitudes($this->conect_sistemas_vtv, $_POST['idpauta'], $_POST['idmaterialgeneral']);
		}else{
			$recursos_asignados = $this->ObjConsulta->rrhh_personal_asignado($this->conect_sistemas_vtv, $_POST['idpauta'], $_POST['idmaterialgeneral']);
			$act_estatus = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $recursos_asignados[1][1]);
			$act_estatus = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $_POST['idpauta'], $recursos_asignados[1][1], '16', $_SESSION['cedula'], $_POST['idmaterialgeneral']);
			$quitando_recursos = $this->ObjConsulta->desacsignar_recursos_rrhh($this->conect_sistemas_vtv, $_POST['idpauta'], $_POST['idmaterialgeneral'], $_SESSION['cedula']);
		}
	}

	function select_asignados() {
		$recursos_asignados = $this->ObjConsulta->rrhh_personal_asignado($this->conect_sistemas_vtv, $_POST['idpauta']);
		for ($rc = 1; $rc <= count($recursos_asignados); $rc++) { //seleccionando cada recurso
			$recursos = $this->ObjConsulta->get_recursos_humanos($this->conect_sistemas_vtv,$recursos_asignados[$rc][4]);
			$fila[] = Array("1" =>  $recursos[1][1] , "2" => "Cédula: " . $recursos[1][1] . " <br>" . $recursos[1][6] . " " . $recursos[1][7] . "<br>" . $recursos[1][10], "3" => $recursos[1][1]  , "4" => $recursos_asignados[$rc][3]);
		}
		echo json_encode($fila);
	}

	/**
	*funcion centralizada y encargada de generar las listas de todo el sistema.
	*/
	function listadepautas($retornar=null){

		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}

		$pagina_ajax = "'listadepautas'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}

		//dependiendo de la session lo que va a consultar..
		//la vista para las listas ha estandarizado a sysmain

		if ($_SESSION['id_tipo_usuario'] == 27) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus, descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		} else if ($_SESSION['id_tipo_usuario'] == 20){
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus, descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);
		}else if ($_SESSION['id_tipo_usuario'] == 22){
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and iddivision='{$_SESSION['division']}'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus, descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "(id_estatus in(3,5,6,7,8,9,10,17,21,22,25,34,36) and id_gerencia='" . $_SESSION['gerencia'] . "' and iddivision='{$_SESSION['division']}') or productor='" . $_SESSION['cedula'] . "' order by id_pauta DESC ", $minimo);
		}else if ($_SESSION['id_tipo_usuario'] == 24){
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = 'id_estatus in(5,6,7,8,9,10,17,21,22,25,34)', "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus, descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = 'id_estatus in(5,6,7,8,9,10,17,21,22,25,34) order by id_pauta DESC ', $minimo);
		}else if($_SESSION['id_tipo_usuario'] == 25){
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_sinasignacion", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(10,21,22) and (id_gerencia='" . $_SESSION['gerencia'] . "' and id_division='{$_SESSION['division']}')", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_sinasignacion", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus, descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(10,21,22) and (id_gerencia='" . $_SESSION['gerencia'] . "' and id_division='{$_SESSION['division']}') order by id_pauta DESC", $minimo);
		}else{
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "productor='" . $_SESSION['cedula'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus, descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "productor='" . $_SESSION['cedula'] . "' order by id_pauta DESC ", $minimo);
		}

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$lista.="
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>
		<th style='200'><div align=\"center\">N&deg; DE PAUTA </div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$evento = $valor2[2] . $valor2[5];
				$id_estatus = $valor2[4];

				$boton_anular = "";
				$boton_mod = "";

				/**
				*administradores pueden afectar todo el sistema
				*/
				if($_SESSION['id_tipo_usuario'] == 27 or $_SESSION['id_tipo_usuario'] == 20){
					$adm_aff_sys=false;
				}else{
					$adm_aff_sys=true;
				}

				if($adm_aff_sys){
					if (($id_estatus == 1) or ($id_estatus == 2) or ($id_estatus == 3) or ($id_estatus == 24) or ($id_estatus == 29)) {
						$boton_modificar_pauta = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Modificar pauta\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmodificarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/page_edit.png\" style=\"border-width:none;border-style:none;\"></button>";
					} else {
						$boton_modificar_pauta = "";
					}
				}

				switch ($id_estatus) {
					//nueva pauta (cualquier perfil)
					case '1':
					case '29':
					case '34':{
						//administradores
						if($_SESSION['id_tipo_usuario'] == 27 or $_SESSION['id_tipo_usuario'] == 20){
							$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
						}

						//productor
						//jefe de area
						if($_SESSION['id_tipo_usuario'] == 26 or $_SESSION['id_tipo_usuario'] == 22){
							if ($valor2[4] == 1){
								$mod=2;
								$pagina=2;
							}else{
								$mod=1;
								$pagina=5;
							}
							$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas&pagina=".$pagina."&mod=".$mod."');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
							$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Agregar recursos\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classsolicitarservicios.php?id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas&pagina=".$pagina."&mod=".$mod."');><img src=\"../estilos/imagenes/estatus/nuevo.gif\" style=\"border-width:none;border-style:none;\"></button>";
							$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Anular\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classerechazada.php?id_pauta=" . $valor2[1] . "&estatus=18&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/eliminado.gif\" style=\"border-width:none;border-style:none;\"></button>";
						}
					}
					break;
					//enviado a jefe de area
					case '3': {
						//administradores
						if($_SESSION['id_tipo_usuario'] == 27 or $_SESSION['id_tipo_usuario'] == 20){
							$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
						}

						//productor
						elseif($_SESSION['id_tipo_usuario'] == 26){
							$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
							$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Agregar recursos\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classsolicitarservicios.php?id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas&pagina=2&mod=3');><img src=\"../estilos/imagenes/estatus/nuevo.gif\" style=\"border-width:none;border-style:none;\"></button>";
						}

						//jefedearea
						elseif($_SESSION['id_tipo_usuario'] == 22){
							$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
							$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Agregar recursos\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classsolicitarservicios.php?id_pauta=" . $valor2[1] . "&mod=3&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/nuevo.gif\" style=\"border-width:none;border-style:none;\"></button>";
							$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Anular\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classerechazada.php?id_pauta=" . $valor2[1] . "&estatus=18&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/eliminado.gif\" style=\"border-width:none;border-style:none;\"></button>";
						}
					}
					break;
					//aprobado por jefe de area
					case '5':{
						//administradores
						if($_SESSION['id_tipo_usuario'] == 27 or $_SESSION['id_tipo_usuario'] == 20){
							$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
						}elseif($_SESSION['id_tipo_usuario'] == 22){
							//Jefe de Area
							$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
							//$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Agregar recursos\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classsolicitarservicios.php?id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/nuevo.gif\" style=\"border-width:none;border-style:none;\"></button>";
							$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Anular\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classerechazada.php?id_pauta=" . $valor2[1] . "&estatus=18&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/eliminado.gif\" style=\"border-width:none;border-style:none;\"></button>";
						}elseif($_SESSION['id_tipo_usuario'] == 24){
								//apoyo logistico.
							$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Observar detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas&mod=3');><img src=\"../estilos/imagenes/estatus/buscar.gif\"></button>";
							$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Asignar Recursos\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classasignarrecursos.php?id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas&mod=3');><img src=\"../estilos/imagenes/estatus/script_add.png\" style=\"border-width:none;border-style:none;\"></button>";
							$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Anular\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classerechazada.php?id_pauta=" . $valor2[1] . "&estatus=18&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/eliminado.gif\" style=\"border-width:none;border-style:none;\"></button>";

						}elseif($_SESSION['id_tipo_usuario'] == 26){
								//Productor
							$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Observar detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\"></button>";						}
						}
						break;

						//Enviado a la la UAL
						case '6':{

						//administradores
							if($_SESSION['id_tipo_usuario'] == 27 or $_SESSION['id_tipo_usuario'] == 20){
								$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
							}
						//Apoyo logistico
							if($_SESSION['id_tipo_usuario'] == 24){
								$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Observar detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\"></button>";
								$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Asignar Recursos\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classasignarrecursos.php?id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas&mod=3');><img src=\"../estilos/imagenes/estatus/script_add.png\" style=\"border-width:none;border-style:none;\"></button>";
								$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Anular\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classerechazada.php?id_pauta=" . $valor2[1] . "&estatus=18&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/eliminado.gif\" style=\"border-width:none;border-style:none;\"></button>";
							}

						}
						break;
						//modificado por apoyo logistico
						case '7':{

						//administradores
							if($_SESSION['id_tipo_usuario'] == 27 or $_SESSION['id_tipo_usuario'] == 20){
								$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
							}
						//Apoyo logistico
							if($_SESSION['id_tipo_usuario'] == 24){
								$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Observar detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\"></button>";
								$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Asignar Recursos\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classasignarrecursos.php?id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas&mod=3');><img src=\"../estilos/imagenes/estatus/script_add.png\" style=\"border-width:none;border-style:none;\"></button>";
								$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Anular\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classerechazada.php?id_pauta=" . $valor2[1] . "&estatus=18&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/eliminado.gif\" style=\"border-width:none;border-style:none;\"></button>";
							}
						//Productor
							if($_SESSION['id_tipo_usuario'] == 26 ){
								$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
							}
						//Jefe da area
							if($_SESSION['id_tipo_usuario'] == 22){
								$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
							}
						}
						break;
						case '10':{
							//Apoyo logistico
							if($_SESSION['id_tipo_usuario'] == 24){
								$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Enviado\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/entregado.gif\" style=\"border-width:none;border-style:none;\"></button>";
							}elseif ($_SESSION['id_tipo_usuario'] == 25) {
								$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Enviado\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta_con_asig_ao&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/entregado.gif\" style=\"border-width:none;border-style:none;\"></button>";
							}else{
								$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
							}
						}
						break;
						case '21':{
							//Apoyo logistico
							if($_SESSION['id_tipo_usuario'] == 24){
								$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Aprobado\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/aprobado.gif\" style=\"border-width:none;border-style:none;\"></button>";
							}else{
								$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
							}
						}
						break;

						case '22':{
						//administradores
						//Apoyo logistico
						//Productor
						//Jefe de Area
							if($_SESSION['id_tipo_usuario'] == 25){
								$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
								$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Enviado\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta_con_asig_ao&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/entregado.gif\" style=\"border-width:none;border-style:none;\"></button>";
								$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Modificar\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/page_edit.png\" style=\"border-width:none;border-style:none;\"></button>";
							}else{
								$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";

							}
						}
						break;
						case '18':
						case '32':{
						//administradores
						//Apoyo logistico
						//Productor
						//Jefe de Area
							$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
						}
						break;
						case '19':
						case '25':
						case '26':{
						//administradores
						//Apoyo logistico
						//Productor
						//Jefe de Area
							$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_informe&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
						}
						break;

						default:
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
						break;
					}
					$lista.="
					<tr id='" . $valor2[1] . "'>
					<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
					<td $color><div align=\"left\">" . utf8_encode (ucwords($evento)) . "</div></td>
					<td $color><div align=\"center\">" . ucwords($valor2[3]) . "</div></td>
					<td $color><div align=\"center\">" . $boton_mod . "&nbsp;" . $boton_modificar_pauta . "&nbsp;" . $boton_anular . "</div></td>
					</tr>";
				}
			} else {
				$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
				$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
			}
			$lista.="</table>";
			if ($num_pag > $count) {
				$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Sig = "
				<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			}
			$lista.=$this->leyenda($tipo = 1);
			$lista.="<table class='tabla'>
			<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
			<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
			</tr></table>";
			if($retornar==null){
				echo $lista;
			}else{
				return $lista;
			}
	}

	function listadeinformes() {
			if (isset($_GET['parametrobusqueda'])) {
				$busquedaget = $_GET['parametrobusqueda'];
			} else {
				$busquedaget = $_GET['busqueda'];
			}
			$pagina_ajax = "'listadeinformes'";
			$nombre_fn = "0";
			if (isset($_GET['minimo'])) {
				$minimo = $_GET['minimo'];
				$count = $_GET['count'];
				if ($count > 1) {
					$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
				} else {
					$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
				}
			} else {
				$minimo = 0;
				$count = 1;
				$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
			}

			if ($_SESSION['id_tipo_usuario'] == "21" or $_SESSION['id_tipo_usuario'] == "22") {

				$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21,25) and id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
				$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta,nombre_programa, descripcion,pauta_estatus,id_estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21,25) and id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);
			} else {
				$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21,25) and productor='" . $_SESSION['cedula'] . "'", "NO");
				$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta,nombre_programa,descripcion,pauta_estatus,id_estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21,25) and productor='" . $_SESSION['cedula'] . "' order by id_pauta DESC", $minimo);
			}

			$num_pag = ceil(count($todas) / 10);
			for ($i = 1; $i <= $num_pag; $i++) {
				$cantidad_paginas[] = $i;
			}
			if (count($cantidad_paginas) > 0) {
				$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
				foreach ($cantidad_paginas as $llave => $valor) {
					if ($valor == $count) {
						$selected = 'selected';
					} else {
						$selected = "";
					}
					$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
				}
				$combo.="</SELECT> ";
			}
			$lista.="
			<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
			<input name='count' id='count' type='hidden' value=" . $count . " />
			<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>
			<table  class='tabla' id='tabla_corres'>
			<tr><th class='titulo' colspan='6'>DATOS</th></tr>
			<tr>
			<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
			<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
			<th><div align=\"center\">ESTATUS</div></th>
			<th><div align=\"center\">ACCIONES</div></th>
			</tr>";
			if (count($ObjListado) > 0) {
				foreach ($ObjListado as $llave2 => $valor2) {
					$resto = $llave2 % 2;
					if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$descripcionprog = $valor2[2];
				$descripcioneven = $valor2[3];
				if ($descripcionprog != '') {
					$evento = $descripcionprog;
				} else if ($descripcioneven != '') {
					$evento = $descripcioneven;
				}

				$estatus= $valor2[5];
				if ($estatus == 25){
					$boton_I = "<input type=\"button\" class='boton' value=\"Continuar Informe\" OnClick=CancelarRegresar('classinformedeprod.php?id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadereportesvalidacion');>";
				}else{
					$boton_I = "<input type=\"button\" class='boton' value=\"Generar Informe\" OnClick=CancelarRegresar('classinformedeprod.php?id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadereportesvalidacion');>";
				}

				$lista.="
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . $evento . "</div></td>
				<td $color><div align=\"center\">" . $valor2[4] . "</div></td>
				<td $color><div align=\"center\">" . $boton_I . "</div></td>
				</tr>";
				}
				} else {
					$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
					$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
				}
				$lista.="</table>";

				if ($num_pag > $count) {
					$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
				} else {
					$boton_Sig = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			}

			//$lista.=$this->leyenda($tipo = 6);
			$lista.="
			<table class='tabla'>
			<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
			<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
			</tr></table>";
			echo $lista;
	}

	function listadereportesvalidacion() {
		//print_r($_GET);
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadereportesvalidacion'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		if (($_SESSION['id_tipo_usuario'] == 24) or ($_SESSION['id_tipo_usuario'] == 27) or ($_SESSION['id_tipo_usuario'] == 20)) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = 'id_estatus in(5,6,7,8,9,10,21,22,24,25,26,29,32)', "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = 'id_estatus in(5,6,7,8,9,10,21,22,24,25,26,29,32)', $minimo);
		} elseif ($_SESSION['id_tipo_usuario'] == 25) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = " id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);
		} else if ($_SESSION['id_tipo_usuario'] == 22) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = " id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);
		} else {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "productor='" . $_SESSION['cedula'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "productor='" . $_SESSION['cedula'] . "' order by id_pauta DESC", $minimo);
		}
		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$lista.="
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>
		<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		<!--<th><div align=\"center\">DE LA UAL</div></th>-->
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$evento = $valor2[2] . " " . $valor2[5];
				$botonRg = "
				<a href='../reportes/reporte_val_ger.php?id_pauta=" . $valor2[1] . "&lista=classlistadereportesvalidacion.php&cedula=" . $_SESSION['cedula'] . "' target='_blank'>
				<img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$botonRu = "
				<a href='../reportes/reporte_val_ual.php?id_pauta=" . $valor2[1] . "&lista=classlistadereportesvalidacion.php&cedula=" . $_SESSION['cedula'] . "' target='_blank'>
				<img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$lista.="
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . ucwords($evento) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[3]) . "</div></td>
				<td $color><div align=\"center\">" . $botonRg . "</div></td>
				<!--<td $color><div align=\"center\">" . $botonRu . "</div></td>-->
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 7);
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadereportes() {
		//print_r($_GET);
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadereportes'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		if (($_SESSION['id_tipo_usuario'] == 24) or ($_SESSION['id_tipo_usuario'] == 27)) {

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		} elseif ($_SESSION['id_tipo_usuario'] == 25) {

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);
		} else {

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);
		}
		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}

		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}

		$lista.="
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>
		<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>

		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$evento = $valor2[2] . " " . $valor2[5];
				$botonRep = "
				<a href='../reportes/reporte_mod.php?id_pauta=" . $valor2[1] . "&lista=classlistadereportes.php&cedula=" . $_SESSION['cedula'] . "' target='_blank'>
				<img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$lista.="
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . ucwords($evento) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[3]) . "</div></td>
				<td $color><div align=\"center\">" . $botonRep . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 7);
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadereportes_sol_tec() {
		//print_r($_GET);
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadereportes'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		if (($_SESSION['id_tipo_usuario'] == 24) or ($_SESSION['id_tipo_usuario'] == 27)) {

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		} elseif ($_SESSION['id_tipo_usuario'] == 25) {

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);
		} else {

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);
		}
		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}

		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}

		$lista.="
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>
		<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>

		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$evento = $valor2[2] . " " . $valor2[5];
				$botonRep = "
				<a href='../reportes/reporte_sol_tec.php?id_pauta=" . $valor2[1] . "&lista=classlistadereportes.php&cedula=" . $_SESSION['cedula'] . "' target='_blank'>
				<img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$lista.="
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . ucwords($evento) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[3]) . "</div></td>
				<td $color><div align=\"center\">" . $botonRep . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 7);
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadereportespers() {
		//print_r($_GET);
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadereportespers'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		if (($_SESSION['id_tipo_usuario'] == 24) or ($_SESSION['id_tipo_usuario'] == 27)) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21) order by id_pauta DESC", $minimo);
		} elseif ($_SESSION['id_tipo_usuario'] == 25) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'  and id_estatus in(21)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'  and id_estatus in(21) order by id_pauta DESC", $minimo);
		} else {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and id_estatus in(21)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and id_estatus in(21) order by id_pauta DESC", $minimo);
		}

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$lista.="
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>
		<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$descripcionprog = $valor2[2];
				$descripcioneven = $valor2[5];
				$evento = trim($descripcionprog . "" . $descripcioneven);
				$botonRep = "
				<a href='../reportes/reporte_pers.php?id_pauta=" . $valor2[1] . "&lista=classlistadereportespers.php&cedula=" . $_SESSION['cedula'] . "' target='_blank'>
				<img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$lista.="
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . ucwords($evento) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[3]) . "</div></td>
				<td $color><div align=\"center\">" . $botonRep . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}

		$lista.=$this->leyenda($tipo = 7);
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadereportesrecur() {
		//print_r($_GET);
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadereportesrecur'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		if (($_SESSION['id_tipo_usuario'] == 24) or ($_SESSION['id_tipo_usuario'] == 27)) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21) order by id_pauta DESC", $minimo);
		} elseif ($_SESSION['id_tipo_usuario'] == 25) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'  and id_estatus in(21)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'  and id_estatus in(21) order by id_pauta DESC", $minimo);
		} else {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and id_estatus in(21)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and id_estatus in(21) order by id_pauta DESC", $minimo);
		}

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$lista.="
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>
		<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$descripcionprog = $valor2[2];
				$descripcioneven = $valor2[5];
				$evento = trim($descripcionprog . "" . $descripcioneven);
				$botonRep = "
				<a href='../reportes/reporte_recur.php?id_pauta=" . $valor2[1] . "&lista=classlistadereportesrecur.php&cedula=" . $_SESSION['cedula'] . "' target='_blank'>
				<img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$lista.="
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . ucwords($evento) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[3]) . "</div></td>
				<td $color><div align=\"center\">" . $botonRep . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}

		$lista.=$this->leyenda($tipo = 7);
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadereportesrecurasig() {
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadereportesrecurasig'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		if (($_SESSION['id_tipo_usuario'] == 24) or ($_SESSION['id_tipo_usuario'] == 27) or ($_SESSION['id_tipo_usuario'] == 25)) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21, 25, 26)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21, 25, 26) order by id_pauta DESC", $minimo);
		} else {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and id_estatus in(21, 25, 26)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and id_estatus in(21, 25, 26) order by id_pauta DESC", $minimo);
		}
		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$lista.="
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>
		<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$descripcionprog = $valor2[2];
				$descripcioneven = $valor2[5];
				$evento = trim($descripcionprog . "" . $descripcioneven);
				$botonRep = "
				<a href='../reportes/reporte_recur_aprob.php?id_pauta=" . $valor2[1] . "&lista=classlistadereportesrecurasig.php&cedula=" . $_SESSION['cedula'] . "' target='_blank'>
				<img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$lista.="
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . ucwords($evento) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[3]) . "</div></td>
				<td $color><div align=\"center\">" . $botonRep . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 7);
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadereportesinformes() {
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadereportesinformes'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}

		if (($_SESSION['id_tipo_usuario'] == 24) or ($_SESSION['id_tipo_usuario'] == 27)) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(26)", "NO");

			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa, descripcion ,pauta_estatus,id_estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(26) order by id_pauta DESC", $minimo);
		} elseif ($_SESSION['id_tipo_usuario'] == 25) {

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'  and id_estatus in(26)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta,nombre_programa, descripcion,pauta_estatus,id_estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'  and id_estatus in(26) order by id_pauta DESC", $minimo);
		} else {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and id_estatus in(26)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa, descripcion,pauta_estatus,id_estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and id_estatus in(26) order by id_pauta DESC", $minimo);
		}

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$lista.="
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>
		<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		</tr>";
		//print_r($ObjListadoVehiculos);
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {

				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$descripcionprog = $valor2[2];
				$descripcioneven = $valor2[3];
				if ($descripcionprog != '') {
					$evento = $descripcionprog;
				}
				if ($descripcioneven != '') {
					$evento = $descripcioneven;
				}
				$botonRep = "
				<a href='../reportes/reporte_inf_prod.php?id_pauta=" . $valor2[1] . "&lista=classlistadereportesinformes.php&cedula=" . $_SESSION['cedula'] . "' target='_blank'>
				<img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$lista.="
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . ucwords($evento) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[4]) . "</div></td>
				<td $color><div align=\"center\">" . $botonRep . "</div></td>
				</tr>";
			}//foreach
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}

		$lista.=$this->leyenda($tipo = 7);
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadepautascaducadas() {
			if (isset($_GET['parametrobusqueda'])) {
				$busquedaget = $_GET['parametrobusqueda'];
			} else {
				$busquedaget = $_GET['busqueda'];
			}
			$pagina_ajax = "'listadepautascaducadas'";
			$nombre_fn = "0";
			if (isset($_GET['minimo'])) {
				$minimo = $_GET['minimo'];
				$count = $_GET['count'];
				if ($count > 1) {
					$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
				} else {
					$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
				}
			} else {
				$minimo = 0;
				$count = 1;
				$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
			}

				$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(36) and id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
				$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta,nombre_programa,descripcion,pauta_estatus,id_estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(36) and id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);


			$num_pag = ceil(count($todas) / 10);
			for ($i = 1; $i <= $num_pag; $i++) {
				$cantidad_paginas[] = $i;
			}
			if (count($cantidad_paginas) > 0) {
				$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
				foreach ($cantidad_paginas as $llave => $valor) {
					if ($valor == $count) {
						$selected = 'selected';
					} else {
						$selected = "";
					}
					$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
				}
				$combo.="</SELECT> ";
			}
			$lista.="
			<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
			<input name='count' id='count' type='hidden' value=" . $count . " />
			<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>
			<table  class='tabla' id='tabla_corres'>
			<tr><th class='titulo' colspan='6'>DATOS</th></tr>
			<tr>
			<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
			<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
			<th><div align=\"center\">ESTATUS</div></th>
			<th><div align=\"center\">ACCIONES</div></th>
			</tr>";
			if (count($ObjListado) > 0) {
				foreach ($ObjListado as $llave2 => $valor2) {
					$resto = $llave2 % 2;
					if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$descripcionprog = $valor2[2];
				$descripcioneven = $valor2[3];
				if ($descripcionprog != '') {
					$evento = $descripcionprog;
				} else if ($descripcioneven != '') {
					$evento = $descripcioneven;
				}

				$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta&id_pauta=" . $valor2[1] . "&lista=classlista.php?modulo=listadepautascaducadas');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";

				$lista.="
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . $evento . "</div></td>
				<td $color><div align=\"center\">" . $valor2[4] . "</div></td>
				<td $color><div align=\"center\">" . $boton_mod . "</div></td>
				</tr>";
				}
				} else {
					$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
					$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
				}
				$lista.="</table>";

				if ($num_pag > $count) {
					$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
				} else {
					$boton_Sig = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			}

			//$lista.=$this->leyenda($tipo = 6);
			$lista.="
			<table class='tabla'>
			<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
			<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
			</tr></table>";
			echo $lista;
	}

	//valida el ingreso de usuarios en el sistema.
	function validaringresousuario() {
		$cedula = $_GET['login'];
		$clave = $_GET['clave'];
		$id_tipo_usuario = "";
		$id_submodulo = "";
		$datosusuario = $this->ObjConsulta->usuario($this->conect_sistemas_vtv, $cedula, $clave, id_aplicacion, id_modulo, $id_tipo_usuario, $id_submodulo);
		if (count($datosusuario) > 0) {// crea sesion
			$datospersonalesusuario = $this->ObjConsulta->datospersonalesusuario($this->conect_sistemas_vtv, $cedula);
			$campos['id_acceso'] = $datosusuario[1][1];
			$campos['cedula'] = $datosusuario[1][2];
			$campos['clave'] = $datosusuario[1][3];
			$campos['id_modulo'] = $datosusuario[1][4];
			$campos['id_aplicacion'] = $datosusuario[1][5];
			$campos['niv_con'] = $datosusuario[1][6];
			$campos['niv_eli'] = $datosusuario[1][7];
			$campos['niv_inc'] = $datosusuario[1][8];
			$campos['niv_mod'] = $datosusuario[1][9];
			$campos['ced_trans'] = $datosusuario[1][10];
			$campos['fecha_trans'] = $datosusuario[1][11];
			$campos['fecha_exp'] = $datosusuario[1][12];
			$campos["id_tipo_usuario"] = $datosusuario[1][13];
			$campos["id_submodulo"] = $datosusuario[1][14];
			$campos["nombres"] = $datospersonalesusuario[1][2];
			$campos["apellidos"] = $datospersonalesusuario[1][3];
			$campos["gerencia"] = $datosusuario[1][15];
			$campos["division"] = $datosusuario[1][16];
			$this->ObjSesion->flibCrearVariable($campos, 1);
			$cedula = $_SESSION['cedula'];
			$id_tipo_usuario = $_SESSION['id_tipo_usuario'];
			$nombres = $_SESSION['nombres'];
			$apellidos = $_SESSION['apellidos'];
			echo "<script type='text/javascript'>CancelarRegresar('classbienvenida.php');</script>";
		} else {
			echo "<script type='text/javascript'>CancelarRegresar('classRegistro.php?error=si');</script>";
		}
	}

	function combodivision() {
		//print_r($_GET);
		$gerencia = $_GET['gerencias'];
		$DatosDivision = $this->ObjConsulta->division($this->conect_sistemas_vtv, "", "", $gerencia);
		$division.="<SELECT name=\"division\" id=\"division\" class='lista' style='width:500px;' $onchage>";
		$division.="<option value=\"0\" selected>SELECCIONE</option>";
		foreach ($DatosDivision as $llave => $valor) {
			if ($valor[1] == $id_division) {
				$selected = "selected";
			} else {
				$selected = "";
			}
			$division.="<option value='" . $valor[1] . "' $selected >" . utf8_encode($valor[2]) . "</option>";
		}
		$division.="</SELECT> ";
		echo $division;
	}

	/***
	*funcion destinada a crear la ventana de bienvenida de las paginas.
	*/
	function bienvenida(){
		//datos de usuario
		if($_SESSION['id_tipo_usuario']=="22"){ //jefes de area envian a analistas de apoyo logistico
			$correo_personal = $this->ObjConsulta->tributa_a($this->conect_sistemas_vtv, null, null, '24');
		}else if($_SESSION['id_tipo_usuario']=="26"){ //productores envian a jefes de area.
			$correo_personal = $this->ObjConsulta->tributa_a($this->conect_sistemas_vtv, $_SESSION['gerencia'], $_SESSION['division'], '22');
		}else if($_SESSION['id_tipo_usuario']=="24"){ //analista de apoyo logistico envia a analistas operativos
			$correo_personal = $this->ObjConsulta->tributa_a($this->conect_sistemas_vtv, null, null, '25');
		}else if($_SESSION['id_tipo_usuario']=="25"){ //analista operativo envia a apoyo logistico
			$correo_personal = $this->ObjConsulta->tributa_a($this->conect_sistemas_vtv, null, null, '24');
		}else if($_SESSION['id_tipo_usuario']=="20"){ //envia a todos en su area
			$correo_personal = $this->ObjConsulta->tributa_a($this->conect_sistemas_vtv, $_SESSION['gerencia'], $_SESSION['division']);
		}

		switch ($_SESSION['id_tipo_usuario']) {
			case '20':
			$mensaje="<ul>
			<li type='square'>Configurar los usuarios del &aacute;rea dentro del sistema.</li>
			<li type='square'>Realizar el mantenimiento operativo del sistema en el &aacute;rea de su competencia.</li>
			<li type='square'>Coordinar con el Super Administrador cualquier consulta mayor</li>
			</ul>";
			break;

			case '26':
			$mensaje="<ul>
			<li type='square'>Generar la pauta a ejecutar seg&uacuten producci&oacute;n propuesta.</li>
			<li type='square'>Realizar el registro de la Solicitud de Facilidades T&eacute;cnicas dentro del sistema.</li>
			<li type='square'>Realizar seguimiento al proceso de la solicitud realizada.</li>
			</ul>";
			break;

			case '22':
			$mensaje="<ul>
			<li type='square'>Recibir las Solicitudes de Facilidades T&eacute;cnicas generadas por los productores.</li>
			<li type='square'>Revisar, Modificar y Aprobar las solicitudes realizadas por los productores.</li>
			<li type='square'>Enviar a la UAL las solicitudes aprobadas.</li>
			</ul>";
			break;

			case '24':
			$mensaje="<ul>
			<li type='square'>Recibir y gestionar todas las Solicitudes de Facilidades T&eacute;cnicas.</li>
			<li type='square'>Elaborar la propuesta de recursos a asignar a las solicitudes recibidas.</li>
			<li type='square'>Enviar las propuestas de asignaci&oacute;n de recursos al AO.</li>
			<li type='square'>Aprobar las solicitudes recibidas del AO.</li>
			</ul>";
			break;

			case '25':
			$mensaje="<ul>
			<li type='square'>Recibir las propuestas de Solicitud de Facilidades T&eacute;cnicas generadas por la UAL.</li>
			<li type='square'>Procesar y asignar los recursos definitivos para las propuestas recibidas.</li>
			<li type='square'>Enviar las propuestas definitivas de asignaci&oacute;n de recursos a la UAL.</li>
			</ul>";
			break;

			case '27':
			$mensaje = "<ul>
			<li type='square'>Garantizar la operatividad del sistema.</li>
			<li type='square'>Realizar la configuraci&oacute;n inicial del sistema.</li>
			<li type='square'>Configurar a los Administradores de &Aacute;rea.</li>
			</ul>";
			break;

		}

		$datausuario = $this->ObjConsulta->selectusuariof5($this->conect_sistemas_vtv, $_SESSION['cedula']);
		$htm=$this->ObjMensaje->interfazbienvenida(utf8_decode($datausuario[1][1])." ".utf8_decode($datausuario[1][2]), utf8_decode($datausuario[1][5]), utf8_decode($datausuario[1][6]), utf8_decode($datausuario[1][4]), $correo_personal, utf8_decode($mensaje));
		return $htm;
	}

	/**
	*componente usado en varias paginas.
	*/
	function comp_tablacomentario($titulo){
		$datosobs=$this->ObjConsulta->selectobsrech($this->conect_sistemas_vtv, $_GET['id_pauta']);
		$htm="<table class='tabla' id='tobservaciones_all' pauta='{$_GET['id_pauta']}' style='width:850px;' >
		<tr><th class='titulo' colspan='8'>$titulo</th></tr>
		<tr><td><textarea style='width:100%'>".$datosobs[1][1]."</textarea></td></tr>
		</table></div>";
		return $htm;
	}

	function comentarios_comp_tablacomentario(){
		$datosbdstatuspauta = $this->ObjConsulta->selectestatus($this->conect_sistemas_vtv, $_GET['id_pauta']);
		$id_estatus_pauta = $datosbdstatuspauta[1][2];
		$this->ObjConsulta->guardarcomentarios($this->conect_sistemas_vtv, $id_estatus_pauta, $_GET['mensaje']);
	}

	function opccion_com_pauta($empieza, $termina){
		$hora="";
		for ($e = $empieza; $e <= $termina; $e++){
			if ($e <= 9) {
				$cero = 0;
			} else {
				$cero = "";
			}
			$hora.="<option value='$e'>$cero$e</option>\n";
		}
		return $hora;
	}

	/**
	*componente que te genera los selects del formulario de fechas y horas del formulario de generar pauta
	*/
	function fecha_comp_pauta($idnomfech,$idnomHora,$idnommin){
		$fecha = "<input id='{$idnomfech}' name='{$idnomfech}' readonly='readonly' size='10' class='campo_vl' value='" . date("d/m/Y") . "'>";
		$hora = "<select name='{$idnomHora}' id='{$idnomHora}' onChange='validarhoras(this);' class='campo_vl'>";
		$hora.="<option value='24' selected='selected'>Hora</option>";
		$hora.= $this->opccion_com_pauta(0, 23);
		$hora.="</select>";
		$min = "<select name='{$idnommin}' id='{$idnommin}' onChange='validarhoras(this);' class='campo_vl'>";
		$min.="<option value='0' selected='selected'>00</option>";
		$min.=$this->opccion_com_pauta(1, 59);
		$min.="</select>";
		eval ('$data = array("'.$idnomfech.'" => $fecha, "'.$idnomHora.'" => $hora, "'.$idnommin.'" => $min);');
		return $data;
	}

	/**
	*componente que se encarga de generar un combo_select a partir de una consulta con argumentos.
	*/
	function basic_combo($consulta, $idnomcombo, $javascript_acc, $style='style="width:150px;"', $arg=null, $size=true, $defValuemsg="Seleccione"){
		if($arg!=null){
			$arg=",".$arg;
		}else{
			$arg="";
		}
		eval('$query = $this->ObjConsulta->$consulta($this->conect_sistemas_vtv'.$arg.');');
		if($size){
			$size='size="'.(count($query)+1).'"';
		}
		$select = '<select id="'.$idnomcombo.'" name="'.$idnomcombo.'" '.$style.' '.$size.' onChange="'.$javascript_acc.'">';
		$select.='<option value="0" selected >'.$defValuemsg.'</option>';
		foreach ($query as $llave => $valor) {
			$select.='<option value="'. $valor[1] . '" $selected >' . strtoupper($valor[2]) . '</option>';
		}
		$select.='</SELECT>';
		return $select;
	}

	/**
	*funcion destinada en crear la ventana de asignaciones de recursos.
	**/
	function ual_asignacionrecursos(){
		$cedula = $_SESSION['cedula'];
		$administrador = $_SESSION['id_tipo_usuario'];
		$nombres = $_SESSION['nombres'];
		$apellidos = $_SESSION['apellidos'];
		$pauta = $_GET['id_pauta'];
		//datos basicos de la pauta..
		$tabladatosbasicos=$this->datos_pauta($pauta);
		$datosservicios = $this->ObjConsulta->select_almacen($this->conect_sistemas_vtv);
		$servicios = "<select id='servicios' name='servicios' style='width:400px;'>";
		$servicios.="<option value='0' selected >Seleccione </option>";
		foreach ($datosservicios as $llave => $valor) {
			$servicios.="<option value='" . $valor[1] . "'>" . ucwords($valor[2]) . "</option>";
		}
		$servicios.="</SELECT> ";
		$cantidad = "<input type='text' name='cantidad' id='cantidad' onkeypress='return validaN(event)' size='4' maxlength='4'/>";
		$datosrec = $this->ObjConsulta->selectdatosrec($this->conect_sistemas_vtv, $pauta);
		$rectotal = count($datosrec);
        $materiales = "<div id='materiales_porasignar'></div>"; //quinatdo duplicidad en el codigo
        $datoslista2 = $this->ObjConsulta->selectlistmatasig($this->conect_sistemas_vtv, $pauta);
        if($_GET['tm']!="rm"){
        	$mod = '4';
        	$botones = "<input type=\"button\" class='boton' value=\"Siguiente\" OnClick=CancelarRegresar('classasignarrecursos.php?id_pauta=".$pauta."&lista=".$_GET['lista']."&tm=rm');>";
        }else{
        	$botones="<input type=\"button\" class='boton' value=\"Regresar\" OnClick=CancelarRegresar('classasignarrecursos.php?id_pauta=".$pauta."&lista=".$_GET['lista']."');> <input type=\"button\" class='boton' value=\"Vista Previa\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta_generada&id_pauta=" . $pauta . "&lista=classlista.php?modulo=listadepautas&id_pauta={$pauta}&mod=".$mod."');>";
        }
        $asignados = "<div id='recursos_hb_asignados'></div>";
        $botones.= "<input type=\"button\" class='boton' value=\"Guardar para futuros Cambios\" OnClick=\"CancelarRegresar('" . $_GET['lista'] . "', this);\" >";
        $botonAg = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick=\"agregarenlalista($pauta, '".$_GET['tm']."');\" >";
        $titulo1 = "DATOS GENERALES DE LA PAUTA";
        $titulo2 = "RECURSOS";
        if($_GET['tm']!="rm"){
        	//recurso humano..
        	$pagina = '7';
        	$tablaAdmPers = "<table class='tabla' style='width:850px';>
        	<tr><td ><div align='center' ><button class='boton modalInput' onclick='callback_select_personal(null,$pauta); ' rel='#agregar'>Administrador de personal</button></div></td></tr>
        	</table>";
        }else{
        	//recurso material.
        	$pagina = '8';
        	$tablaRecursoAsig = "<table class='tabla' style='width:850px';>
        	<tr><th class='titulo' colspan='8'>" . $titulo2 . "</th></tr>
        	<tr><th>Materiales:</th><td>" . $servicios . "</td><th>Cantidades:</th><td>" . $cantidad . "</td><td><div>" . $botonAg . "</td></tr>
        	</table>";
        }
        $htm.=$this->ObjMensaje->interfazasignarrecursos2($this->pasosdelproceso($pagina), $tabladatosbasicos, $tablaAdmPers, $tablaRecursoAsig,$materiales,$asignados,$this->comp_tablacomentario("Observaciones"),$botones);
        return $htm;
    }

	/**
	*funcion dedicada a crear el diseño de pagina para solicitar servicios.
	**/
	function pag_solicitarservicios(){
		$cedula = $_SESSION['cedula'];
		$administrador = $_SESSION['id_tipo_usuario'];
		$nombres = $_SESSION['nombres'];
		$apellidos = $_SESSION['apellidos'];
		$id_gerencia = $_SESSION['gerencia'];
		$pauta = $_GET['id_pauta'];
		$mod = $_GET['$mod'];
		$verificarestatuspauta = $this->ObjConsulta->verificarestatuspauta($this->conect_sistemas_vtv, $pauta);
		$estatusactual = $verificarestatuspauta[1][1];
		if (($estatusactual == 9) or ($estatusactual == 24)) {
			$estatuspauta = 29;
			$updateexpestatus2 = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $pauta);
			$insertestatusnuevo = $this->ObjConsulta->insertestatusnuevo($this->conect_sistemas_vtv, $pauta, $estatuspauta, $cedula);
			$estatus_recurso = 13;
			$recursosdelapauta = $this->ObjConsulta->selectrecursosdelapautaeditada($this->conect_sistemas_vtv, $pauta);
			foreach ($recursosdelapauta as $llave => $valor) {
				$id_detalle_servicio = $valor[1];
				$updateexpestatusrecursos = $this->ObjConsulta->updateexpestatusrecursosf5($this->conect_sistemas_vtv, $pauta, $id_detalle_servicio);
				$verrecursoasignado = $this->ObjConsulta->selectverrecursoasignado($this->conect_sistemas_vtv, $id_detalle_servicio);
				$id_recurso_asignado = $verrecursoasignado[1][1];
				$insertestatusrecursoanulado = $this->ObjConsulta->insertestatusrecursoanulado($this->conect_sistemas_vtv, $pauta, $cedula, $id_detalle_servicio, $estatus_recurso, $id_recurso_asignado); //insertado
			}
		}
		//tabla de la PAUTA BASICA..
		$tablapautabasica=$this->datos_pauta($pauta);
		//TABLA QUE LISTA LOS SERVICIOS
		$datosservicios = $this->ObjConsulta->select_almacen($this->conect_sistemas_vtv);
		$servicios = "<select id='servicios' name='servicios' style='width:380px;'>";
		$servicios.="<option value='0' selected >Seleccione </option>";
		foreach ($datosservicios as $llave => $valor) {
			$servicios.="<option value='" . $valor[1] . "' $selected >" . ucwords($valor[2]) . "</option>";
		}
		$servicios.="</SELECT> ";
		$cantidad = "<input type='text' name='cantidad' id='cantidad' onkeypress='return validaN(event)' size='2' maxlength='4'/>";
		$botonAg = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick=agregarenlista_f5(this,$pauta);>";
		$tr_materiales= "<tr><th>Materiales:</th><td>" . $servicios . "</td><th>Cant:</th><td><input type=\"button\" class=\"boton\" value=\"+\" OnClick=\"textsum(this,'+')\" style=\"margin-right:0px;\" />" . $cantidad . "<input type=\"button\" class=\"boton\" value=\"-\" OnClick=\"textsum(this,'-')\"/></td><td><div>" . $botonAg . "</div></td></tr>";
		//TABLA QUE LISTA EL PERSONAL
		$datosrecursoh = $this->ObjConsulta->select_cargos($this->conect_sistemas_vtv);
		$servicios = "<select id='servicios2' name='servicios2' style='width:380px;'>";
		$servicios.="<option value='0' selected >Seleccione </option>";
		foreach ($datosrecursoh as $llave => $valor) {
			$servicios.="<option value='" . $valor[1] . "' $selected >" . ucwords($valor[2]) . "</option>";
		}

		$servicios.="</SELECT> ";
		$botonAg = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick='agregarenlista_f5_rh(this,$pauta);'>";
		$cantidad = "<input type='text' name='cantidad2' id='cantidad2' onkeypress='return validaN(event)' size='2' maxlength='4'/>";
		$tr_recursoh="<tr><th>Recursos Humanos:</th><td>" . $servicios . "</td><th>Cant:</th><td><input type=\"button\" class=\"boton\" value=\"+\" OnClick=\"textsum(this,'+')\" style=\"margin-right:0px;\" />" . $cantidad . "<input type=\"button\" class=\"boton\" value=\"-\" OnClick=\"textsum(this,'-')\"/></td><td><div>" . $botonAg . "</div></td></tr>";
		$materiales = "<div id='hb_solicitarmateriales'></div>";
		//tabla de los servicios...
		$tablaservicios="<table class='tabla' style='width:850px';>
		<tr><th class='titulo' colspan='8'>RECURSOS</th></tr>
		".$tr_materiales."
		".$tr_recursoh."
		</table>";


		$verificarestatuspauta = $this->ObjConsulta->verificarestatuspauta($this->conect_sistemas_vtv, $pauta);
		$estatusactual = $verificarestatuspauta[1][1];
		if ($administrador == 26) {
			if ($estatusactual == 1) {
				$estatus_pt = 3;
				$botones = "<input type=\"button\" class='boton' value=\"Vista Previa\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta_generada&id_pauta=".$pauta."&mod={$_GET['mod']}',this);>";
				$botones.= "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
			} else {
				$botones= "<input type=\"button\" class='boton' value=\"Guardar para futuros Cambios\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas',this);>";
				$botones.= "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
			}
		}

		if ($administrador == 22) {
			if ($estatusactual==1 or $estatusactual == 3 or $estatusactual == 34) {
				$estatus_pt = 5;
				$botones="<input type=\"button\" class='boton' value=\"Vista Previa\" OnClick=CancelarRegresar('classmostrar.php?mostrar=mostrar_pauta_generada&id_pauta=" . $pauta . "&lista=classlistadepautasjarea.php?id_pauta={$pauta}&mod={$_GET['mod']}',this);>";
				$botones.= "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
			} else {
				$botones= "<input type=\"button\" class='boton' value=\"Guardar para futuros Cambios\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas',this);>";
				$botones.= "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
			}
		}

		$pasos = $this->pasosdelproceso($_GET['pagina']);
		$htm.=$this->ObjMensaje->interfazsolicitarservicios2($pasos, $tablapautabasica, $tablaservicios, $materiales, $this->comp_tablacomentario("Observaciones"), $botones);
		//$htm.=$this->ObjMensaje->interfazsolicitarservicios($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $tr_materiales, $tr_recursoh, $evento, $materiales, $titulo1, $titulo2, $titulo3, $botonA, $botonC, $pasos, $this->comp_tablacomentario("Observaciones"));
		return $htm;
	}

	/**
	*pagina responsable en crear la interfaz de la pauta.
	**/
	function pag_crearpauta(){
		$cedula = $_SESSION['cedula'];
		$administrador = $_SESSION['id_tipo_usuario'];
		$nombres = $_SESSION['nombres'];
		$apellidos = $_SESSION['apellidos'];
        /**
         * jefe de area = 22
         * productor = 26
         * analista de apoyo logistico = 24
         */
        if (($administrador == 26) || ($administrador == 22) || ($administrador == 24)) {
        	$pauta=$this->basic_combo("selectpauta", "pauta", "combopauta_f5()");
        	$locacion=$this->basic_combo("selectlocacion", "locacion", "combolocacion_f5()");
        	$tipoevento=$this->basic_combo("selecttipoevento", "tipoevento", "cambianomenclatura(this)");
        	$tipotraje=$this->basic_combo("selecttipotraje", "tipotraje", "");
        	$datoslugarcitacionlef = $this->ObjConsulta->selectlugar2($this->conect_sistemas_vtv, 2);
        	$lugar_citacion_lef = "<select id='lugar_citacion_lef' name='lugar_citacion_lef' style='width:150px;'>";
        	$lugar_citacion_lef.="<option value='0' selected >Seleccione </option>";
        	foreach ($datoslugarcitacionlef as $llave => $valor) {
        		$lugar_citacion_lef.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
        	}
        	$lugar_citacion_lef.="</SELECT> ";
			//horas y minutos del formulario de pautas.
        	$data=$this->fecha_comp_pauta("fecha1","hora1","min1");
        	extract($data);
        	$data=$this->fecha_comp_pauta("fecha2","hora2","min2");
        	extract($data);
        	$data=$this->fecha_comp_pauta("fecha3","hora3","min3");
        	extract($data);
        	$data=$this->fecha_comp_pauta("fecha4","hora4","min4");
        	extract($data);
        	$botonA = "<input type=\"button\" class='boton' value=\"Continuar\" OnClick='guardarpauta_f5()';>";
        	if ($administrador == 24) {
        		$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick='ir_a(\"classlista.php?modulo=listadepautas\")'>";
        	}else if ($administrador == 22){
        		$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick='ir_a(\"classlista.php?modulo=listadepautas\")'>";
        	}else {
        		$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick='ir_a(\"classlista.php?modulo=listadepautas\")'>";
        	}
        	if($_SESSION["id_tipo_usuario"]==22){
        		$elegirProductor=$this->ObjMensaje->interfazelegirprod($this->basic_combo("tributa_a", 'selectProductor', "","", $_SESSION['gerencia'].", ".$_SESSION['division'].", '26'", false,utf8_decode("T&uacute;")));
        	}elseif ($_SESSION["id_tipo_usuario"]==24) {
        		$elegirProductor=$this->ObjMensaje->interfazelegirprod($this->basic_combo("tributa_a", 'selectProductor', "","", "null, null, '26'", false,utf8_decode("T&uacute;")));
        	}

        	$html="<div id='datosp' align='center'>
        	<div id='cargando' align='center'></div>
        	".$this->pasosdelproceso("1")."
        	".$elegirProductor."
        	".$this->ObjMensaje->form_solfactecn_bas($pauta,$locacion,$tipoevento,$tipotraje)."
        	".$this->ObjMensaje->form_solfactecn_bas_citaciones($fecha1,$fecha2,$fecha3,$fecha4,$hora1,$hora2,$hora3,$hora4,$min1,$min2,$min3,$min4,$lugar_citacion_lef)."
        	".$this->ObjMensaje->tablabotones($botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC)."
        	</div>";
        	return $html;
        }
    }

	//funcion que hace todos listados.
    function pag_lista(){
    	$campo_busqueda="<input type=\"text\" name=\"busqueda\" id=\"busqueda\" class=\"campo\" onkeypress=\"return enter(event,function(){ listar('".$_GET["modulo"]."'); })\" size=\"70\">";
    	$botonB="<input type=\"button\" class=\"boton\" value=\"Buscar\" OnClick=\"listar('".$_GET["modulo"]."');\">";
    	$htm=$this->ObjMensaje->InterfazListaPersona($campo_busqueda,$botonB);
    	return $htm;
    }

	//datos basicos de la pauta.
    function datos_pauta($pauta=null){
    	$cedula = $_SESSION['cedula'];
    	$administrador = $_SESSION['id_tipo_usuario'];
    	$nombres = $_SESSION['nombres'];
    	$apellidos = $_SESSION['apellidos'];

    	if($pauta==null){
    		$pauta = $_GET['id_pauta'];
    	}
    	$idspauta = $this->ObjConsulta->selectidspauta($this->conect_sistemas_vtv, $pauta);
    	$id_pauta = $idspauta[1][1];
    	$id_tipo_pauta = $idspauta[1][2];
    	$id_locacion = $idspauta[1][3];
    	$id_tipo_traje = $idspauta[1][4];
    	$id_program = $idspauta[1][5];
    	$id_citacion = $idspauta[1][6];
    	$id_montaje = $idspauta[1][7];
    	$id_emision_grabacion = $idspauta[1][8];
    	$id_retorno = $idspauta[1][9];
    	$id_tipo_evento = $idspauta[1][10];
    	$user_reg = $idspauta[1][11];
    	$descripcionevenbd = ucwords($idspauta[1][13]);
    	$pauta_lugarlef = ucwords($idspauta[1][12]);

        ///////////////////////////////////////////////////////////////////////////////////////////////////////
    	$descprograma = $this->ObjConsulta->selectdescprograma($this->conect_sistemas_vtv, $id_program);
    	$idprograma = $descprograma[1][1];
    	$descripcionprog = ucwords($descprograma[1][2]);
    	$desclocacion = $this->ObjConsulta->selectdesclocacion($this->conect_sistemas_vtv, $id_locacion);
    	$idlocacion = $desclocacion[1][1];
    	$descripcionloc = ucwords($desclocacion[1][2]);
    	$desctipotraje = $this->ObjConsulta->selectdesctipotraje($this->conect_sistemas_vtv, $id_tipo_traje);
    	$descripciontraje = $desctipotraje[1][1];
    	$descripciontraje = ucwords($desctipotraje[1][1]);

    	$descproductor = $this->ObjConsulta->selectusuariof5($this->conect_sistemas_vtv, $user_reg);
    	$nombre = strtoupper($descproductor[1][1]);
    	$apellido = strtoupper($descproductor[1][2]);
    	$descripcionprod = $nombre . " " . $apellido;


    	$desctipoevento = $this->ObjConsulta->selectdesctipoevento($this->conect_sistemas_vtv, $id_tipo_evento);
    	$idtipoevento = $desctipoevento[1][1];
    	$descripcionteven = ucwords($desctipoevento[1][2]);
        //////////////////////////////////////////////////////////////////////////////////////////////////////
    	$datoscitacion = $this->ObjConsulta->selectcitacion($this->conect_sistemas_vtv, $id_citacion);
    	$idcitacion = $datoscitacion[1][1];
    	$fechacitacion = $datoscitacion[1][2];
    	$horacitacion = $datoscitacion[1][3];
    	$id_lugar_citacion = $datoscitacion[1][4];
    	$fechacitacion = $this->Objfechahora->flibInvertirInEs($fechacitacion);
    	$lugarcitacion = $this->ObjConsulta->selectlugar($this->conect_sistemas_vtv, $id_lugar_citacion);
		$lugarcitacion = $lugarcitacion[1][2];
		$descripcitacion = ucwords($lugarcitacion);

    	$datosmontaje = $this->ObjConsulta->selectmontaje($this->conect_sistemas_vtv, $id_montaje);
    	$idmontaje = $datosmontaje[1][1];
    	$fechamontaje = $datosmontaje[1][2];
    	$horamontaje = $datosmontaje[1][3];
    	$fechamontaje = $this->Objfechahora->flibInvertirInEs($fechamontaje);
    	$descripmontaje = $pauta_lugarlef;

    	$datosemision = $this->ObjConsulta->selectemision($this->conect_sistemas_vtv, $id_emision_grabacion);
    	$idemision = $datosemision[1][1];
    	$fechaemision = $datosemision[1][2];
    	$horaemision = $datosemision[1][3];
    	$fechaemision = $this->Objfechahora->flibInvertirInEs($fechaemision);
    	$descripemision = $pauta_lugarlef;

    	$datosretorno = $this->ObjConsulta->selectretorno($this->conect_sistemas_vtv, $id_retorno);
    	$idretorno = $datosretorno[1][1];
    	$fecharetorno = $datosretorno[1][2];
    	$horaretorno = $datosretorno[1][3];
    	$fecharetorno = $this->Objfechahora->flibInvertirInEs($fecharetorno);
    	$descripretorno = $pauta_lugarlef;

    	$estatuspauta = $this->ObjConsulta->selectestatuspautas($this->conect_sistemas_vtv, $pauta);
    	$estatus = $estatuspauta[1][1];
    	if ($estatus == '9' or $estatus == '18' or $estatus == '24') {
    		$obsrech = $this->ObjConsulta->selectobsrech($this->conect_sistemas_vtv, $pauta);
    		$observaciones = $obsrech[1][1];
    		$obs = "<table class='tabla' align='center'><tr><th>Observaciones:</th><td>" . $observaciones . "</td></tr></table>";
    	} else {
    		$obs = "";
    	}
        ///////////////////////////////////////////////////////////////////////////////////////////////
    	if ($idlocacion == '2') {
    		$descripretorno = "";
    	} else {
    		$descripretorno = "<tr><th>Retorno:</th><td>" . ucwords($descripretorno) . "</td><th>Fecha:</th><td>" . $fecharetorno . "</td><th>Hora:</th><td>" . $horaretorno . "</td></tr>";
    	}
        ////////////////////////////////////////////////////////////////////////////////////////////
    	if ($id_program == 0) {
    		$evento = "<th>Evento:</th><td>" . $descripcionevenbd . "</td>";
    	} else {
    		$evento = "<th>Programa:</th><td>" . $descripcionprog . "</td>";
    	}

    	if($administrador == 24 and ($estatus ==10 or $estatus ==22)){
    		$pagina = '10';
    		$pasos = $this->pasosdelproceso($pagina);
    	}else{
    		$pasos = " ";
    	}

    	$codHtml = "";
    	$codHtml = "
    	<br><br>
    	".$pasos."
    	<div id='datosp' align='center'>
    	<div id='cargando' align='center'></div>
    	<table class='tabla' style='width:850px';>
    	<tr><th class='titulo' colspan='8'>DATOS GENERALES DE LA PAUTA</th></tr>
    	<tr><th>N&deg; de pauta:</th><td>" . $pauta . "</td></tr>
    	<tr>" . $evento . "</tr>
    	<tr><th>Productor:</th><td>" . $descripcionprod . "</td></tr>
    	<tr><th>Locaci&oacute;n:</th><td>" . $descripcionloc . "</td></tr>
    	<tr><th>Tipo de evento:</th><td>" . $descripcionteven . "</td></tr>
    	<tr><th>Tipo de traje:</th><td>" . $descripciontraje . "</td></tr>
    	<tr><th>Citaci&oacute;n:</th><td>" . $descripcitacion . "</td><th>Fecha:</th><td>" . $fechacitacion . "</td><th>Hora:</th><td>" . $horacitacion . "</td></tr>
    	<tr><th>Montaje:</th><td>" . $descripmontaje . "</td><th>Fecha:</th><td>" . $fechamontaje . "</td><th>Hora:</th><td>" . $horamontaje . "</td></tr>
    	<tr><th>Emisi&oacute;n:</th><td>" . $descripemision . "</td><th>Fecha:</th><td>" . $fechaemision . "</td><th>Hora:</th><td>" . $horaemision . "</td></tr>
    	" . $descripretorno . "
    	</table>
    	</div>
    	";
    	return $codHtml;
    }

    //Mostrar pautas unificados
    function mostrar_pautas($pauta=null,$modulo=null,$botones=true){
    	if($pauta==null){
    		$pauta = $_GET['id_pauta'];
    	}

    	if($modulo==null){
    		$modulo = $_GET['mostrar'];
    	}

    	$cedula = $_SESSION['cedula'];
    	$administrador = $_SESSION['id_tipo_usuario'];
    	$nombres = $_SESSION['nombres'];
    	$apellidos = $_SESSION['apellidos'];
    	$gerencia = $_SESSION['gerencia'];
    	$division = $_SESSION['division'];
    	$mod= $_GET['$mod'];

    	$estatuspauta = $this->ObjConsulta->selectestatuspautas($this->conect_sistemas_vtv, $pauta);
    	$estatus = $estatuspauta[1][1];

    	if ($estatus == '9' or $estatus == '18' or $estatus == '24') {
    		$obsrech = $this->ObjConsulta->selectobsrech($this->conect_sistemas_vtv, $pauta);
    		$observaciones = $obsrech[1][1];
    		$obs = "<table class='tabla' align='center'><tr><th>Observaciones:</th><td>" . $observaciones . "</td></tr></table>";
    	} else {
    		$obs = " ";
    	}

    	$reportes_assoc = $this->botonesdereportes($pauta,$cedula,$estatus);

    	if($modulo == 'mostrar_pauta_con_asig_ao') {
    		//para que modifique la ao
    		$materiales=$this->materiales_modificados_ao($pauta);
    		$asignados=$this->materiales_con_caracteristicas($pauta,$_SESSION['gerencia'], $_SESSION['division']);
    		$botonC = "<input type=\"button\" class='boton' value=\"Confirmar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
    		$codHtml="";
    		$codHtml="
    		<div id='datosp2' align='center'>
    		<div id='cargando2' align='center'></div>
    		" . $reportes_assoc . "
    		" . $materiales . "
    		" . $asignados . "
    		</div>
    		<table class='tabla'>
    		<tr><th colspan='2'><div align='center'>" . $botonC . "</div></tr>
    		</table>
    		</div>
    		<br>
    		<div id='agregar' style='display:none'>
		        <table class='tabla'>
			        <thead>
			        	<tr><th colspan='2'>ASIGNACION PERSONAL</th></tr>
			        </thead>
			        <tbody>
				        <tr><th colspan='2'>RECURSOS DISPONIBLE</th></tr>
				        <tr><td colspan='2'>
				        <div class='demo ui-widget ui-helper-clearfix'>
				        <ul id='gallery' class='gallery ui-helper-reset ui-helper-clearfix' style='overflow: auto; height: 300px'>
				        </ul>
				        <div id='trash' class='ui-widget-content ui-state-default' style='overflow: auto; height: 300px'>
				        <h4 class='ui-widget-header'><span class='ui-icon ui-icon-plus'>Personal Asignado</span> Asignados</h4>
				        </div>
				        </div>
				        </td>
				        </tr>
			        </tbody>
			        <tfoot>
				        <tr>
				        <td colspan='2'>
				        <form>
				        <button class='close boton' type='button'>Listo</button>
				        </form>
				        </td>
				        </tr>
			        </tfoot>
		        </table>
        	</div>";
    		return $codHtml;

    	}elseif ($modulo == 'mostrar_pauta' OR $modulo == 'mostrar_pauta_generada'){
    			$recsol = $this->ObjConsulta->selectrecsolporest($this->conect_sistemas_vtv,$pauta);
				if(count($recsol)!=0){
					foreach ($recsol as $llave2 => $valor2) {
						$id_rec = $valor2[1];
						$id_tipo_rec = $valor2[2];
						$cant = $valor2[3];
						if($id_tipo_rec==1){
							$datosmaterial = $this->ObjConsulta->selectlistmatdescnuevo($this->conect_sistemas_vtv, $id_rec);
							$desc_id_recurso = $datosmaterial[1][1];
						}else{
							$datosmaterial = $this->ObjConsulta->select_cargos($this->conect_sistemas_vtv, $id_rec);
							$desc_id_recurso=$datosmaterial[1][2];
						}

						$materiales2.="<tr id='$id' $color><td  align='center'>" . ucwords(strtoupper($desc_id_recurso)) . "</td><td  align='center'>" . $cant . "</td>";
					}

					$materiales = "<table class='tabla' align='center' style='width:850px'; id='$idtablacontenedor'>
						<tr><th class='titulo' colspan='8'>LISTA DE RECURSOS SOLICITADOS</th></tr>
						<tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Cantidad</div></th></tr>
						<tr><td>" . $materiales2 . "</td></tr>
						</table>";
	    		}

	    		if($estatus>6){
	    			$materialesna=$this->materiales_no_asig($pauta);
	    		}
	    		//NO SE COLOCA LA TABLA DE LOS RECURSOS ELIMINADOS POR PARTE DEL
	    		//PROD O DEL JEFE POR QUE REALMENTE SI LOS ELIMINAN ES POR QUE NO
	    		//LOS ESTAN SOLICITANDO
	    		$materialesa=$this->materiales_asignados($pauta);
	    		$eliminados=$this->materiales_eliminados($pauta);
	    		$modao = $this->materiales_modificados_porao($pauta);

	    		$mod = '3';
	    		switch ($estatus) {
	    			case '1':{
	    				if ($administrador == 26){
	    					$botonAcc = "<input type=\"button\" class='boton' value=\"Agregar recursos\" OnClick=CancelarRegresar('classsolicitarservicios.php?id_pauta=" . $pauta . "&lista=classlista.php?modulo=listadepautas&pagina=2&mod=".$mod."');>";
	    					$botonAcc.="<input type=\"button\" class='boton' value=\"Anular\" OnClick=CancelarRegresar('classerechazada.php?id_pauta=" . $pauta . "&estatus=18&lista=classlista.php?modulo=listadepautas');>";
	    					$botonAcc.="<input type=\"button\" class='boton' value=\"Modificar pauta\" OnClick=CancelarRegresar('classmodificarpauta.php?id_pauta=" . $pauta . "&lista=classlista.php?modulo=listadepautas');>";

	    					$estatus_pt = 3;
	    					$botonAcc.="<input type=\"button\" class='boton' value=\"Enviar a jefe inmediato\" OnClick=\"enviarsolicitud($pauta,$estatus_pt,$mod);\">";
	    					$botonAcc.= "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
	    				}
	    				else if ($administrador == 22)
	    				{
	    					$botonAcc = "<input type=\"button\" class='boton' value=\"Agregar recursos\" OnClick=CancelarRegresar('classsolicitarservicios.php?id_pauta=" . $pauta . "&lista=classlista.php?modulo=listadepautas&pagina=2&mod=".$mod."');>";
	    					$botonAcc.="<input type=\"button\" class='boton' value=\"Anular\" OnClick=CancelarRegresar('classerechazada.php?id_pauta=" . $pauta . "&estatus=18&lista=classlista.php?modulo=listadepautas');>";
	    					$botonAcc.="<input type=\"button\" class='boton' value=\"Modificar pauta\" OnClick=CancelarRegresar('classmodificarpauta.php?id_pauta=" . $pauta . "&lista=classlista.php?modulo=listadepautas');>";
	    					$botonAcc.= "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
	    					$estatus_pt = 5;
	    					$botonAcc.="<input type=\"button\" class='boton' value=\"Aprobar y Enviar\" OnClick=enviarsolicitud($pauta,$estatus_pt,$mod);>";
	    				}

	    				else if ($administrador == 22) {
	    					$estatus_pt = 5;
	    					$botonAcc ="<input type=\"button\" class='boton' value=\"Aprobar y Enviar\" OnClick=enviarsolicitud($pauta,$estatus_pt,$mod);>";
	    					$botonAcc.= "<input type=\"button\" class='boton' value=\"Agregar recursos\" OnClick=CancelarRegresar('classsolicitarservicios.php?id_pauta=" . $pauta . "&lista=classlista.php?modulo=listadepautas&pagina=2&mod=".$mod."');>";
	    					$botonAcc.="<input type=\"button\" class='boton' value=\"Anular\" OnClick=CancelarRegresar('classerechazada.php?id_pauta=" . $pauta . "&estatus=18&lista=classlista.php?modulo=listadepautas');>";
	    					$botonAcc.="<input type=\"button\" class='boton' value=\"Modificar pauta\" OnClick=CancelarRegresar('classmodificarpauta.php?id_pauta=" . $pauta . "&lista=classlista.php?modulo=listadepautas');>";
	    					$botonAcc.="<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
	    				}
	    			}
	    			break;
	    			case '3':{
	    				$botonAcc = "<input type=\"button\" class='boton' value=\"Agregar recursos\" OnClick=CancelarRegresar('classsolicitarservicios.php?id_pauta=" . $pauta . "&lista=classmostrarpauta.php?id_pauta={$pauta}&pagina=2&mod=".$mod."');>";
	    				if ($administrador == 22) {
	    					$estatus_pt = 5;
	    					$botonAcc.= "<input type=\"button\" class='boton' value=\"Aprobar y Enviar\" OnClick=enviarsolicitud($pauta,$estatus_pt,$mod);>";
	    				}
	    				$botonAcc.="<input type=\"button\" class='boton' value=\"Modificar pauta\" OnClick=CancelarRegresar('classmodificarpauta.php?id_pauta=" . $pauta . "&lista=classlista.php?modulo=listadepautas&mod=".$mod."');>";
	    				$botonAcc.= "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";

	    			}
	    			break;
	    			case '5':{
	    				if ($administrador == 22){
	    					//$botonAcc = "<input type=\"button\" class='boton' value=\"Agregar recursos\" OnClick=CancelarRegresar('classsolicitarservicios.php?id_pauta=" . $id_pauta . "&lista=classlista.php?modulo=listadepautas&pagina=2&mod=".$mod."');>";
	    					$botonAcc.="<input type=\"button\" class='boton' value=\"Anular\" OnClick=CancelarRegresar('classerechazada.php?id_pauta=" . $pauta . "&estatus=18&lista=classlista.php?modulo=listadepautas');>";
	    					$botonAcc.= "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
	    					//$botonAcc.="<input type=\"button\" class='boton' value=\"Modificar pauta\" OnClick=CancelarRegresar('classmodificarpauta.php?id_pauta=" . $id_pauta . "&lista=classlista.php?modulo=listadepautas');>";
	    				}
	    				elseif ($administrador == 24){
	    					$estatus_pt= 10;
	    					$botonAcc ="<input type=\"button\" class='boton' value=\"Asignar Recursos\" OnClick=CancelarRegresar('classasignarrecursos.php?id_pauta={$pauta}&lista=classlista.php?modulo=listadepautas&mod=".$mod."');>";
	    					$botonAcc.="<input type=\"button\" class='boton' value=\"Anular\" OnClick=CancelarRegresar('classerechazada.php?id_pauta={$pauta}&estatus=12&lista=classlista.php?modulo=listadepautas');>";
	    					$botonAcc.= "<input type=\"button\" class='boton' value=\"Enviar a areas operativas\" OnClick='enviarsolicitud($pauta,$estatus_pt,2);'><input name='datosrec'  id='datosrec' type='hidden' value=" . count($datoslista2) . " /><input name='datosasig'  id='datosasig' type='hidden' value=" . count($datoslista) . " />";
	    					$botonAcc.= "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
	    				}
	    				else{
	    					$botonAcc.= "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
	    				}
	    			}
	    			break;
	    			case '6':
	    			case '7':
	    			$estatus_pt= 10;
	    			$botonAcc.= "<input type=\"button\" class='boton' value=\"Enviar a areas operativas\" OnClick='enviarsolicitud($pauta,$estatus_pt,2);'><input name='datosrec'  id='datosrec' type='hidden' value=" . count($datoslista2) . " /><input name='datosasig'  id='datosasig' type='hidden' value=" . count($datoslista) . " />";
	    			$botonAcc.="<input type=\"button\" class='boton' value=\"Asignar Recursos\" OnClick=CancelarRegresar('classasignarrecursos.php?id_pauta={$pauta}&lista=classlista.php?modulo=listadepautas');>";
	    			$botonAcc.="<input type=\"button\" class='boton' value=\"Anular\" OnClick=CancelarRegresar('classerechazada.php?id_pauta={$pauta}&estatus=12&lista=classlista.php?modulo=listadepautas');>";
	    			$botonAcc.= "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
	    			break;
	    			case "34":
	    			if ($administrador == 22) {
	    				$botonAcc = "<input type=\"button\" class='boton' value=\"Agregar recursos\" OnClick=CancelarRegresar('classsolicitarservicios.php?id_pauta=" . $pauta . "&lista=classlista.php?modulo=listadepautas&pagina=5&mod=1');>";
	    				$estatus_pt = 5;
	    				$botonAcc.= "<input type=\"button\" class='boton' value=\"Aprobar y Enviar\" OnClick=enviarsolicitud($pauta,$estatus_pt,1);>";
	    				$botonAcc.="<input type=\"button\" class='boton' value=\"Modificar pauta\" OnClick=CancelarRegresar('classmodificarpauta.php?id_pauta=" . $pauta . "&lista=classlista.php?modulo=listadepautas');>";
	    				$botonAcc.= "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
	    			}
	    			break;
	    			case "36":

	    				$botonAcc.= "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadereportesestadisticos');>";

	    			break;
	    			default:
						$botonAcc.= "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
						break;
	    		}
	    		if ($administrador == 27 or $administrador == 20){

	    				$botonAcc = " ";
	    				$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('".$_GET['lista']."');>";

	    		}

	    		if ($administrador == 24 and ($estatus ==10 or $estatus ==22)){
	    			$estatuspauta=21;
	    			$botonAcc = " ";
	    			$botonC="<input type=\"button\" class='boton' value=\"Aprobar\" OnClick=cambiarestatus($pauta,$estatuspauta);>";
	    			$botonC.="<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";

	    		}

	    		$codHtml = "";
	    		$codHtml = "
	    		<div id='datosp2' align='center'>
	    		<div id='cargando2' align='center'></div>
	    		" . $reportes_assoc . "
	    		" . $materiales . "
	    		" . $materialesna . "
	    		" . $asignados . "
	    		" . $materialesa . "
	    		" . $eliminados . "
	    		" . $modao . "
	    		</div>
	    		<div id='datosp3' align='center'>
	    		<div id='cargando' align='center'></div>
	    		" . $obs . "
	    		</div>
	    		<div id='datosp4' align='center'>

	    		<table class='tabla'>
	    		<tr><th colspan='2'><div align='center'>" . $botonAcc . " " . $botonC . "</div></tr>
	    		</table>
	    		</div>
	    		</div>";
	    		return $codHtml;


    	}elseif ($modulo == 'mostrar_informe'){

    		$justificados=$this->materiales_justificados($pauta,false);
    		$nojustificados=$this->materiales_no_justificados($pauta,false);

    		$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadepautas');>";
    		$codHtml = "";
    		$codHtml = "
    		<div id='datosp2' align='center'>
    		<div id='cargando2' align='center'></div>
    		" . $reportes_assoc . "
    		" . $justificados . "
    		" . $nojustificados . "
    		</div>
    		<table class='tabla'>
    		<tr><th colspan='2'><div align='center'>" . $botonC . "</div></tr>
    		</table>
    		</div>
    		<br>
    		";
    		return $codHtml;
    	}
    }

    //Funcion de los materiales asignados
    function materiales_asignados($pauta){
    	$datoslista = $this->ObjConsulta->selectlistmatasig($this->conect_sistemas_vtv, $pauta);
    	if(count($datoslista)>0){
    		foreach ($datoslista as $llave3 => $valor3) {
    			$id_recurso_asignado = $valor3[1];
    			$id_recurso = $valor3[2];
    			$id_tipo_rec = $valor3[4];
    			if($id_tipo_rec==1){
    				$datosmaterial = $this->ObjConsulta->selectlistmatdescnuevo($this->conect_sistemas_vtv, $id_recurso);
	    			$desc_id_recurso = $datosmaterial[1][1];
	    			$datoscaract = $this->ObjConsulta->selectdatosrecasig($this->conect_sistemas_vtv, $id_recurso_asignado);
	    			$marca=$datoscaract[1][1];
	    			$modelo=$datoscaract[1][2];
	    			$bien_nac=$datoscaract[1][3];
	    				$primero = "Marca: " . $marca . "";
	    				$segundo = "Modelo: " . $modelo . "";
	    				$tercero = "Bien Nac.:: " . $bien_nac . "";
	    				$desc_caracteristicas = "" . $primero . "&nbsp;" . $segundo . "&nbsp;" . $tercero . " ";
	    			$asignados2.="<tr id='$id'><td  align='left'>" . strtoupper($desc_id_recurso) . "</td><td  align='left'>" . $desc_caracteristicas . "</td></tr>";
    			}else{
    				$datosmaterial = $this->ObjConsulta->select_cargos($this->conect_sistemas_vtv, $id_recurso);
					$desc_id_recurso=$datosmaterial[1][2];
					$recursos = $this->ObjConsulta->get_recursos_humanos($this->conect_sistemas_vtv,$id_recurso_asignado);
	    			$asignados2.="<tr id='$id'><td  align='left'>" . strtoupper($desc_id_recurso) . "</td><td  align='left'>" . utf8_encode($recursos[1][6]) ." ".utf8_encode($recursos[1][7])."</td></tr>";
    			}
    		}
    		$asignados .= "<table class='tabla' style='width:850px'; id='listarecursosasignados'>
    		<tr><th class='titulo' colspan='8'>LISTA DE RECURSOS ASIGNADOS</th></tr>
    		<tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Detalle</div></th></tr>
    		<tr><td>" . $asignados2 . "</td></tr>
    		</table>
    		";
    	}
    	else{
    		$asignados= " ";
    	}

    	return $asignados;
    }

    //Funcion de los materiales no asignados
    function materiales_no_asig($pauta){
    	$datoslista=$this->ObjConsulta->selectrecnoasig($this->conect_sistemas_vtv,$pauta);
    	if(count($datoslista) == 0){
    		$materiales = " ";
    	}
    	else{
    		foreach($datoslista as $llave => $valor){
    			$id_recurso=$valor[1];
    			$cantidad=$valor[2];
    			$id_tipo_rec=$valor[3];
    			if($id_tipo_rec==1){
    				$datosmaterial=$this->ObjConsulta->selectlistmatdescnuevo($this->conect_sistemas_vtv,$id_recurso);
    				$desc_id_recurso=$datosmaterial[1][1];
    			}else{
    				$datosmaterial = $this->ObjConsulta->select_cargos($this->conect_sistemas_vtv, $id_recurso);
					$desc_id_recurso=$datosmaterial[1][2];
    			}
    			$materiales2.="<tr id='$id'><td  align='left'>".ucwords (strtoupper($desc_id_recurso))."</td><td  align='left'>".$cantidad."</td>";
    		}
    		$materiales="<table class='tabla' style='width:850px'; id='listarecursos'>
    		<tr><th class='titulo' colspan='8'>LISTA DE RECURSOS SIN ASIGNAR</th></tr>
    		<tr><th><div align='left'>Descripci&oacute;n</div></th></tr>
    		<tr><td>".$materiales2."</td></tr>
    		</table>";
    	}
    	return $materiales;
    }

	//Funcion de los materiales para modificar por las areas operativas
    function materiales_modificados_ao($pauta){
    	//LUEGO DE DARLE A MODIFICAR MUESTRA LOS RECURSOS DISPONIBLE PARA MODIFICAR EL ASIGNADO
    	$datoslista1 = $this->ObjConsulta->selectlistreccambiados($this->conect_sistemas_vtv, $pauta);//todos
    	if(count($datoslista1) == 0){
    		$materiales = "";
    	}else{
    		foreach ($datoslista1 as $llave4 => $valor4) {
    			$id_detalle_servicio = $valor4[1];
    			$datoslista2 = $this->ObjConsulta->selectlistmatcambiado($this->conect_sistemas_vtv, $id_detalle_servicio);
    			$id_rec = $datoslista2[1][1];
    			$id_tipo_rec = $datoslista2[1][2];

    			if($id_tipo_rec=="1"){
					//con el $id_rec busco todas las caractieristicas disponible para los recursos con ese id
					$desc_tipo = $this->ObjConsulta->selectdesctipo($this->conect_sistemas_vtv, $id_rec);
					$descripcion=$desc_tipo[1][1];
					$caractrec = $this->ObjConsulta->selectcaractrec($this->conect_sistemas_vtv, $id_rec);

					$caracteristicas = "<select id='caracteristicas".$id_detalle_servicio."' name='caracteristicas' style='width:500px;'>";
					$caracteristicas.="<option value='0' selected >Seleccione </option>";
		       		foreach ($caractrec as $llave => $valor) {//MARCA MODELO BIEN NAC
		        		$caracteristicas.="<option value='" . $valor[1] . "' $selected >Marca:" . $valor[3] . "&nbsp;&nbsp;Modelo:" . $valor[4] . "&nbsp;&nbsp;Bien Nac.:" . $valor[5] . "</option>";
		        	}
		        	$caracteristicas.="</SELECT> ";
		        	$botonAs = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick=agregarenlista_cambiado($pauta,$id_detalle_servicio);>";
					$materiales2.="<tr id='".$id_detalle_servicio."'><td align='left'>" . strtoupper(utf8_encode($descripcion)) . "</td>
					<td align='left'>" . utf8_encode($caracteristicas) . "&nbsp;" . $botonAs . "</td></tr>";
				}else{
					//ES UN RECURSO HUMANO
					$botonPErs = "<button class='boton modalInput2' onclick=\"callback_select_personal(".$id_rec.",$pauta);\" rel=\"#agregar\" >Asignar</button>";
					//$botonE = "<input type=\"button\" class='boton' value='Eliminar' OnClick='eliminardelalista($id_pauta,$id_detalle_servicio,$id_rec);' />";
					$datosmaterial = $this->ObjConsulta->select_cargos($this->conect_sistemas_vtv, $id_rec);
					$desc_id_recurso=$datosmaterial[1][2];
					$materiales2.="<tr id='".$id_detalle_servicio."'><td align='left'>" . strtoupper(utf8_encode($desc_id_recurso))."</td><td align='left'>" . $botonPErs . $botonE . "</td></tr>";
				}

    		}
    		$materiales = "<table class='tabla' style='width:850px'; id='listarecursos'>
    		<tr><th class='titulo' colspan='8'>LISTA DE RECURSOS</th></tr>
    		<tr><th><div align='left'>Descripci&oacute;n</div></th></tr>
    		<tr><td>" . $materiales2 . "</td></tr>
    		</table>
    		";
    	}
    	return $materiales;
    }

    //Funcion de las caracteristicas de los materiales por cada gerencia...
    function materiales_con_caracteristicas($pauta, $gerencia=null, $division=null){
    	$pauta = $_GET['id_pauta'];
    	$modulo = $_GET['mostrar'];
    	$cedula = $_SESSION['cedula'];
    	$administrador = $_SESSION['id_tipo_usuario'];
    	$nombres = $_SESSION['nombres'];
    	$apellidos = $_SESSION['apellidos'];
    	if($gerencia==null){
    		$gerencia=$_SESSION['gerencia'];
    	}
    	$datoslista = $this->ObjConsulta->selectlistmatasignados($this->conect_sistemas_vtv, $pauta, $gerencia, $division);
    	if (count($datoslista) == 0) {
    		echo"<script>var pagina='classlista.php?modulo=listadepautas';
    		alert('Disculpa esta pauta no requiere ningun recurso de su gerencia.');
    		function redireccionar() {
    			location.href=pagina;
    		}
    		setTimeout ('redireccionar()', 0);
    		</script>
    		";
    	}else{
    		//MUESTRA UN LISTA DE LOS RECURSOS QUE FUERON ASIGNADOS DE SU GERENCIA Y TIENE LA OPCION DE MODIFICARLOS
    		foreach ($datoslista as $llave3 => $valor3) {
    			$id_recurso_asignado = $valor3[1];
    			$id_recurso = $valor3[2];
    			$id_detalle_servicio = $valor3[3];
    			$id_tipo_rec = $valor3[4];

    			$botonM = "<input type=\"button\" class='boton' value=\"Modificar\" OnClick=modificarasignado($pauta,$id_detalle_servicio,$id_tipo_rec,$cedula);>"; // un update

    			$verificarestatus = $this->ObjConsulta->verificarestatus($this->conect_sistemas_vtv, $id_detalle_servicio);
    			$estatusactual = $verificarestatus[1][1];
    			if($id_tipo_rec==1){//es material
    				$datosmaterial = $this->ObjConsulta->selectlistmatdescnuevo($this->conect_sistemas_vtv, $id_recurso);
	    			$desc_id_recurso = $datosmaterial[1][1];
	    			$datoscaract = $this->ObjConsulta->selectdatosrecasig($this->conect_sistemas_vtv, $id_recurso_asignado);
	    			$marca=$datoscaract[1][1];
	    			$modelo=$datoscaract[1][2];
	    			$bien_nac=$datoscaract[1][3];
	    			$primero = "Marca: " . $marca . "";
	    			$segundo = "Modelo: " . $modelo . "";
	    			$tercero = "Bien Nac.:: " . $bien_nac . "";
	    			$desc_caracteristicas = "" . $primero . "&nbsp;" . $segundo . "&nbsp;" . $tercero . " ";
	    			$asignados2.="<tr id='$id'><td  align='left'>" . strtoupper($desc_id_recurso) . "</td><td  align='left'>" . $desc_caracteristicas . "</td><td  align='center'>" . $botonM . "</td></tr>";
    			}else{
    				$datosmaterial = $this->ObjConsulta->select_cargos($this->conect_sistemas_vtv, $id_recurso);
					$desc_id_recurso=$datosmaterial[1][2];
					$recursos = $this->ObjConsulta->get_recursos_humanos($this->conect_sistemas_vtv,$id_recurso_asignado);
					if($id_recurso_asignado<>"1"){
	    				$asignados2.="<tr id='$id'><td  align='left'>" . strtoupper($desc_id_recurso) . "</td><td  align='left'>" . utf8_encode($recursos[1][6]) ." ".utf8_encode($recursos[1][7])."</td><td  align='center'>" . $botonM . "</td></tr>";
					}
    			}
    			if ($estatusactual == 11) {
    				$asignados = "";
    			} else {
    				$asignados2=$asignados2;
    			}
    		}

    		$asignados = "<table class='tabla' style='width:850px'; id='listarecursosasignados'>
    		<tr><th class='titulo' colspan='8'>LISTA DE RECURSOS ASIGNADOS</th></tr>
    		<tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Detalle</div></th><th><div align='center'>Accion</div></th></tr>
    		<tr><td>" . $asignados2 . "</td></tr>
    		</table>";
    	}
    	return $asignados;
    }

    //Funcion de los materiales modificados por las areas operativas
    function materiales_modificados_porao($pauta){

    	$est_mod_ao=28;
    	$datoslista7=$this->ObjConsulta->selectlistmatactrep($this->conect_sistemas_vtv,$pauta,$est_mod_ao);
    	//print_r($datoslista7);
    	if(count($datoslista7) == 0){

    		$modao = " ";
    	}else{
    		foreach($datoslista7 as $llave7 => $valor7){
    			$id_detalle_servicio=$valor7[1];
    			$id_recurso_asignado=$valor7[2];

    			$idsmat=$this->ObjConsulta->selectidmat($this->conect_sistemas_vtv,$id_detalle_servicio);
    			$id_recurso=$idsmat[1][1];
    			$id_tipo_rec=$idsmat[1][2];

    			if($id_tipo_rec==1){
    				$datosmaterial=$this->ObjConsulta->selectlistmatdescnuevo($this->conect_sistemas_vtv,$id_recurso);
	    			$desc_id_recurso=$datosmaterial[1][1];

	    			$datoscaract=$this->ObjConsulta->selectdatosrecasig($this->conect_sistemas_vtv,$id_recurso_asignado);
	    			$marca=$datoscaract[1][1];
	    			$modelo=$datoscaract[1][2];
	    			$bien_nac=$datoscaract[1][3];

	    				$primero="Marca: ".$marca."";
	    				$segundo="Modelo: ".$modelo."";
	    				$tercero="Bien Nac.: ".$bien_nac."";

	    				$desc_caracteristicas="".$primero."&nbsp;".$segundo."&nbsp;".$tercero." ";

    				$modao2.="<tr id='$id'><td  align='left'>".strtoupper($desc_id_recurso)."</td><td  align='left'>".$desc_caracteristicas."</td></tr>";

    			}//else recurso humano

    		}

    		$modao="<table class='tabla' style='width:850px'; id='listarecursosasignados'>
    		<tr><th class='titulo' colspan='8'>RECURSOS MODIFICADO POR LAS AREAS OPERATIVAS</th></tr>
    		<tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Detalle</div></th></tr>
    		".$modao2."
    		</table>
    		";
    	}
    	/*else{

    				$datoscaract2=$this->ObjConsulta->selectdatospnombre($this->conect_sistemas_vtv,$id_recurso_asignado);
    				$segundo=$datoscaract2[1][1];

    				$datoscaract3=$this->ObjConsulta->selectdatospapellido($this->conect_sistemas_vtv,$id_recurso_asignado);
    				$tercero=$datoscaract3[1][1];

    				$id_foto= base64_encode ($id_foto);
    				$foto="<img style='width:60px; height:60px; 'src='http://intranet/sistemas/directorio/paginas/download_foto.php?id=".$id_foto."=' >";
    				$primero="Cedula: ".$primero."";
    				$segundo="Nombre: ".$segundo."";
    				$tercero="Apellido: ".$tercero."";
    				$desc_caracteristicas="".$foto."&nbsp;".$primero."&nbsp;".$segundo."&nbsp;".$tercero." ";

    			}

    			$modao2.="<tr id='$id'><td  align='left'>".strtoupper($desc_id_recurso)."</td><td  align='left'>".$desc_caracteristicas."</td></tr>";
    		}*/




    	return $modao;
    }

    //Funcion de los materiales eliminados
    function materiales_eliminados($pauta){
    	$datosideliminados=$this->ObjConsulta->selectlistdeeliminadosual($this->conect_sistemas_vtv,$pauta);
    	if(count($datosideliminados) == 0){
    		$eliminados = " ";
    	}else{
    		foreach($datosideliminados as $llave4 => $valor4){
    			$id_recursos=$valor4[1];
    			$cant=$valor4[2];
    			$id_tipo_rec=$valor4[3];
    			if($id_tipo_rec==1){
    				$datosmatelim=$this->ObjConsulta->selectlistmatdescnuevo($this->conect_sistemas_vtv,$id_recursos);
    				$desc_id_recursos=$datosmatelim[1][1];
    			}else{
    				$desc_id_recursos="RECURSO HUMANO";;
    			}

    			$eliminados2.="<tr id='$id'><td  align='left'>".strtoupper($desc_id_recursos)."</td><td  align='center'>".$cant."</td></tr>";
    		}
    		$eliminados="<table class='tabla' style='width:850px'; id='listarecursosasignados'>
    		<tr><th class='titulo' colspan='8'>LISTA DE RECURSOS ELIMINADOS</th></tr>
    		<tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Cantidad</div></th></tr>
    		<tr><td>".$eliminados2."</td></tr>
    		</table>
    		";
    	}
    	return $eliminados;
    }

    //Funciones de los materiales justificados
    function materiales_justificados($id_pauta, $botonM=true){
    	$pauta=$id_pauta;
    	$ced_sesion=$_SESSION['cedula'];

    	 $datoslistajust = $this->ObjConsulta->selectlistmatjust($this->conect_sistemas_vtv, $id_pauta);
            foreach ($datoslistajust as $llave4 => $valor4) {
                $id_detalle_servicio = $valor4[1];
                $id_informe = $valor4[2];
                $id_estado_informe = $valor4[3];
                $observaciones = $valor4[4];

                $datosnojust = $this->ObjConsulta->selectlistnojust($this->conect_sistemas_vtv, $id_detalle_servicio, $id_pauta);
                $id_estatus_actual = $datosnojust[1][1];
                $id_tipo_recurso=$datosnojust[1][2];

                if ($id_estatus_actual == 35) {
                    $datos_estado_inf = $this->ObjConsulta->selectestadoinf($this->conect_sistemas_vtv, $id_estado_informe);
                    $desc_estado = $datos_estado_inf[1][1];
                    $datosidrecurso = $this->ObjConsulta->selectidrecurso($this->conect_sistemas_vtv, $id_detalle_servicio);
                    $id_recurso_asignado = $datosidrecurso[1][1];
                    $id_recurso = $datosidrecurso[1][2];
                    $id_tipo_recurso = $datosidrecurso[1][3];
                    if($id_tipo_recurso==1){
                        $datosmaterial = $this->ObjConsulta->selectlistmatdescnuevo($this->conect_sistemas_vtv, $id_recurso);
                        $desc_id_recurso = $datosmaterial[1][1];
                        $datoscaract = $this->ObjConsulta->selectdatosrecasig($this->conect_sistemas_vtv, $id_recurso_asignado);
                        $marca=$datoscaract[1][1];
                        $modelo=$datoscaract[1][2];
                        $bien_nac=$datoscaract[1][3];
                        $primero = "Marca: " . $marca . "";
                        $segundo = "Modelo: " . $modelo . "";
                        $tercero = "Bien Nac.:: " . $bien_nac . "";
                        $desc_caracteristicas = "" . $primero . "&nbsp;" . $segundo . "&nbsp;" . $tercero . " ";
                    }else{
                        $datosmaterial = $this->ObjConsulta->select_cargos($this->conect_sistemas_vtv, $id_recurso);
                        $desc_id_recurso=$datosmaterial[1][2];
                        $recursos = $this->ObjConsulta->get_recursos_humanos($this->conect_sistemas_vtv,$id_recurso_asignado);
                        $desc_caracteristicas= utf8_encode($recursos[1][6]) ." ".utf8_encode($recursos[1][7]);
                    }

                    if($botonM){
                        $botonM = "<input type=\"button\" class='boton' value=\"Modificar\" OnClick=modificarinforme($pauta,$id_informe,$id_detalle_servicio,$llave4,$ced_sesion);>"; // un update
                    }
                    $justificados2.="<tr id='$id_recurso'><td  align='left'>" . strtoupper($desc_id_recurso) . "</td><td  align='left'>" . strtoupper($desc_caracteristicas) . "</td><td  align='center'>" . $desc_estado . "</td><td  align='left'>" . $observaciones . "</td><td  align='left'>" . $botonM . "</td></tr>";
                }

                $datos_especiales="<input name='cantjust'  id='cantjust' type='hidden' value=" . count($datoslistajust) . " />";

                    $titulo3 = "LISTA DE RECURSOS JUSTIFICADOS";
                    $justificados = "<table class='tabla' style='width:850px'; id='listarecursosasignados'>
                    <tr><th class='titulo' colspan='8'>" . $titulo3 . "</th></tr>
                    <tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Detalle</div></th><th><div align='center'>Asistencia&frasl;Estado</div></th><th><div align='center'>Observaciones</div></th></tr>
                    <tr><td>" . $justificados2 . "</td></tr>
                    </table>".$datos_especiales;
            }
	    return $justificados;
	}

	function materiales_no_justificados ($id_pauta, $botonM=true){
			$pauta=$id_pauta;
			$ced_sesion=$_SESSION['cedula'];
            $estatuspauta = $this->ObjConsulta->selectestatuspautas($this->conect_sistemas_vtv, $id_pauta);
            $estatus = $estatuspauta[1][1];
            $datoslista = $this->ObjConsulta->selectlistmatasig2($this->conect_sistemas_vtv, $id_pauta);
            foreach ($datoslista as $llave3 => $valor3) {
                $id_detalle_servicio = $valor3[1];
                $id_recurso_asignado = $valor3[2];
                $id_recurso = $valor3[3];
                $id_tipo_recurso = $valor3[4];


                $datosnojust = $this->ObjConsulta->selectlistnojust($this->conect_sistemas_vtv, $id_detalle_servicio, $id_pauta);
                $id_estatus_actual = $datosnojust[1][1];

                if ($id_estatus_actual == 13 or $id_estatus_actual == 14 or  $id_estatus_actual == 20 or $id_estatus_actual == 23 or $id_estatus_actual == 28) {
                if($id_tipo_recurso==1){
                    $datosmaterial = $this->ObjConsulta->selectlistmatdescnuevo($this->conect_sistemas_vtv, $id_recurso);
                    $desc_id_recurso = $datosmaterial[1][1];
                    $datoscaract = $this->ObjConsulta->selectdatosrecasig($this->conect_sistemas_vtv, $id_recurso_asignado);
                    $marca=$datoscaract[1][1];
                    $modelo=$datoscaract[1][2];
                    $bien_nac=$datoscaract[1][3];
                        $primero = "Marca: " . $marca . "";
                        $segundo = "Modelo: " . $modelo . "";
                        $tercero = "Bien Nac.:: " . $bien_nac . "";
                        $desc_caracteristicas = "" . $primero . "&nbsp;" . $segundo . "&nbsp;" . $tercero . " ";
                }else{
                    $datosmaterial = $this->ObjConsulta->select_cargos($this->conect_sistemas_vtv, $id_recurso);
                    $desc_id_recurso=$datosmaterial[1][2];
                    $recursos = $this->ObjConsulta->get_recursos_humanos($this->conect_sistemas_vtv,$id_recurso_asignado);
                    $desc_caracteristicas= utf8_encode($recursos[1][6]) ." ".utf8_encode($recursos[1][7]);
                }

                if($botonM)
                    if ($id_tipo_recurso == 2) {
                        $asistio = "<select id='asistio" . $llave3 . "' style='width:110px';>
                        <option value='0' selected>Seleccione</option>
                        <option value='1'>Si asistio</option>
                        <option value='2'>No asistio</option>
                        </select>";
                    } else {
                        $asistio = "<select id='asistio" . $llave3 . "'>
                        <option value='0' selected>Seleccione</option>
                        <option value='3'>Bueno</option>
                        <option value='4'>Regular</option>
                        <option value='5'>Malo</option>
                        <option value='6'>No entregado</option>
                        </select>";
                    }
                    $observaciones = "<textarea name='observaciones" . $llave3 . "' id='observaciones" . $llave3 . "' rows='2' cols='30'></textarea>";
                    $botonG = "<input type=\"button\" class='boton' value=\"Guardar\" OnClick=guardarinforme($pauta,$id_detalle_servicio,$llave3,$ced_sesion);>";
                }else{
                	$asistio="sin evaluar";
                	$observaciones="sin evaluar";
                }

                if($id_estatus_actual!="35"){
                    $asignados2.="<tr id='$llave3'><td  align='left'>" ." " .strtoupper($desc_id_recurso) . "</td><td  align='left'>".$desc_caracteristicas."</td><td  align='left'>" . $asistio . "</td><td  align='left'>" . $observaciones . "</td><td  align='left'>" . $botonG . "</td></tr>";
                
                }

                $datos_especiales="<input name='canttotal'  id='canttotal' type='hidden' value=" . (count($datoslista)) . " />";

                $titulo2 = "LISTA DE RECURSOS";
                $asignados = "<table class='tabla' style='width:850px'; id='listarecursosasignados'>
                <tr><th class='titulo' colspan='8'>" . $titulo2 . "</th></tr>
                <tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Detalle</div></th><th><div align='center'>Asistencia&frasl;Estado</div></th><th><div align='center'>Observaciones</div></th></tr>
                <tr><td>" . $asignados2 . "</td></tr>
                </table>".$datos_especiales;
            }
            return $asignados;
	}

    //Funciones de los materiales tabla de los procesos
	function tabla_proceso($pagina){

		$pasos = $this->pasosdelproceso($pagina);
		$tabproc="
		<div id='datosp' align='center'>
		<div id='cargando' align='center'></div>
		".$pasos."
		</div>";

		return $tabproc;
	}

	function reporte_estadistico(){

    	$estad1="<input type='text' name='fecha_ini' id='fecha_ini' class='campo' size='10' maxlength='10' readonly='readonly'/>";
    	$estad2="<input type='text' name='fecha_fin' id='fecha_fin' class='campo' size='10' maxlength='10' readonly='readonly'/>";
    	$estad="<table class='tabla' style='width:850px'; id='busqueda'>
    		<tr><th class='titulo' colspan='8'>BUSCADOR</th></tr>
    		<tr><th><div align='center'>Fecha inicio : ".$estad1."</div></th></tr>
    		<tr><th><div align='center'>Fecha fin : &nbsp;&nbsp;&nbsp;&nbsp;".$estad2."</div></th></tr>
    		</table>
    		";

    	$botonAcc.="<div id=\"p1\"><input type=\"button\" class='boton' value=\"Consultar\" OnClick=\"rep_estad();\">
    	<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classbienvenida.php');></div>";

	 	$botonAcc.="<div style=\"display:none\" id=\"p2\"><a class='boton' OnClick=\"reportejs('../reportes/reporte_estadistico.php');\" >Generar PDF</a>
	 	<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classestadisticas.php');></div>";


		$codHtml = "";
    	$codHtml = "
    	<div id='datosp2' align='center'>
    	<div id='cargando2' align='center'></div>
    	" . $estad . "
    	</div>
    	<div id='datosp4' align='center'>
    	<table class='tabla'>
    	<tr><th colspan='2'><div align='center'>" . $botonAcc . "</div></tr>
    	</table>
    	</div>
    	";

    	return $codHtml;
    }

    function rep_estad(){

    	$gerencia=$_SESSION['gerencia'];
    	$fecha_ini=$_GET['fecha_ini'];
    	$fecha_ini = $this->Objfechahora->flibInvertirEsIn($fecha_ini);
    	$fecha_fin=$_GET['fecha_fin'];
    	$fecha_fin = $this->Objfechahora->flibInvertirEsIn($fecha_fin);

    	///////////////////////////Busqueda de las pautas con estatus 26(Informe generado)//////////////////////////////////
      	$finalizadas=$this->ObjConsulta->selectpautasfinalizadas($this->conect_sistemas_vtv, $fecha_ini, $fecha_fin, $gerencia);
    	$cont_f=count($finalizadas);
    	if ($cont_f!= 0){

    		foreach ($finalizadas as $llave => $valor) {
	        $nom_evento = $valor[1];
	        $nom_programa = $valor[2];
	        $productor = $valor[3];
	        $gerencia_prod = $valor[4];
	        $fecha_citacion = $valor[5];
	     	$fecha_citacion = $this->Objfechahora->flibInvertirInEs($fecha_citacion);
	     	$id_pauta = $valor[6];


	        if($nom_evento!= ""){
	        	$descripcion=$nom_evento;
	        }else{
	        	$descripcion=$nom_programa;
	        }
	        $botonAcc ="<button style=\"width:35px;height:35px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Remoto\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('../reportes/reporte_recur_aprob.php?id_pauta=" . $valor[6] . "');><img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"border-width:none;border-style:none;\"></button>";
	    	$botonAcc.="<button style=\"width:35px;height:35px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Solicitudes Tecnicas\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('../reportes/reporte_sol_tec.php?id_pauta=" . $valor[6] . "');><img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"border-width:none;border-style:none;\"></button>";
	    	$botonAcc.="<button style=\"width:35px;height:35px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Informe de Cierre\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('../reportes/reporte_inf_prod.php?id_pauta=" . $valor[6] . "');><img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"border-width:none;border-style:none;\"></button>";

	        $p_finalizada.="<tr id='$id'><td  align='left'>" .utf8_encode(strtoupper($descripcion)) . "</td><td  align='center'>".utf8_encode(strtoupper($productor))."</td><td  align='center'>".utf8_encode(strtoupper($gerencia_prod))."</td><td  align='center'>".utf8_encode(strtoupper($fecha_citacion))."</td><td  align='center'>".$botonAcc."</td></tr>";
	    	}

	 	}else{

    		$p_finalizada = "<tr><th  colspan='6' align='center'><div style='color: #FF0000;font-weight: bold;' align='center'>Disculpe NO se encontro PAUTAS FINALIZADAS para la fecha indicada.</div></th></tr>";
    	}

    	$p_finalizadas = "<table class='tabla' style='width:850px'; id='resultados'>
        <tr><th class='titulo' colspan='6'>PAUTAS FINALIZADAS</th></tr>
        <tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Productor</div></th><th><div align='center'>Gerencia</div></th><th><div align='center'>Fecha de citaci&oacute;n</div></th><th><div align='center'>Acciones</div></th></tr>
        " . $p_finalizada . "
        </table>
        ";

        //////////////////Busqueda de las pautas con estatus 25 (Informe en proceso)//////////////////////////////////
	    $pautasconinf=$this->ObjConsulta->selectpautasinformesenproceso($this->conect_sistemas_vtv, $fecha_ini, $fecha_fin, $gerencia);

	    $cont_inf= count($pautasconinf);
    	if ($cont_inf!= 0){
    		foreach ($pautasconinf as $llave3 => $valor3) {
	        $nom_evento = $valor3[1];
	        $nom_programa = $valor3[2];
	        $productor = $valor3[3];
	        $gerencia_prod = $valor3[4];
	        $fecha_citacion = $valor3[5];
	     	$fecha_citacion = $this->Objfechahora->flibInvertirInEs($fecha_citacion);
	     	$id_pauta = $valor3[6];

	        if($nom_evento!= ""){
	        	$descripcion=$nom_evento;
	        }else{
	        	$descripcion=$nom_programa;
	        }
	        $botonAcc ="<button style=\"width:35px;height:35px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Remoto\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('../reportes/reporte_recur_aprob.php?id_pauta=" . $valor3[6] . "');><img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"border-width:none;border-style:none;\"></button>";
	    	$botonAcc.="<button style=\"width:35px;height:35px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Solicitudes Tecnicas\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('../reportes/reporte_sol_tec.php?id_pauta=" . $valor3[6] . "');><img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"border-width:none;border-style:none;\"></button>";


	        $p_coninf.="<tr id='$id'><td  align='left'>" .utf8_encode(strtoupper($descripcion)) . "</td><td  align='center'>".utf8_encode(strtoupper($productor))."</td><td  align='center'>".utf8_encode(strtoupper($gerencia_prod))."</td><td  align='center'>".utf8_encode(strtoupper($fecha_citacion))."</td><td  align='center'>".$botonAcc."</td></tr>";
	    	}

    	}else{

    		$p_coninf = "<tr><th  colspan='6' align='center'><div style='color: #FF0000;font-weight: bold;' align='center'>Disculpe NO se encontro PAUTAS CON INFORMES EN PROCESO para la fecha indicada.</div></th></tr>";
    	}

    	$p_coninfs = "<table class='tabla' style='width:850px'; id='resultados'>
        <tr><th class='titulo' colspan='6'>PAUTAS CON INFORMES EN PROCESO</th></tr>
        <tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Productor</div></th><th><div align='center'>Gerencia</div></th><th><div align='center'>Fecha de citaci&oacute;n</div></th><th><div align='center'>Acciones</div></th></tr>
        " . $p_coninf . "
        </table>
        ";


	    //////////////////Busqueda de las pautas con estatus XX(Pauta caducada)/////////////////////////////////////////////////
	    $nofinalizadas=$this->ObjConsulta->selectpautasnofinalizadas($this->conect_sistemas_vtv, $fecha_ini, $fecha_fin, $gerencia);
    	$cont_nf= count($nofinalizadas);
    	if ($cont_nf!= 0){
    		foreach ($nofinalizadas as $llave2 => $valor2) {
	        $nom_evento = $valor2[1];
	        $nom_programa = $valor2[2];
	        $productor = $valor2[3];
	        $gerencia_prod = $valor2[4];
	        $fecha_citacion = $valor2[5];
	     	$fecha_citacion = $this->Objfechahora->flibInvertirInEs($fecha_citacion);
	     	$id_pauta = $valor2[5];


	        if($nom_evento!= ""){
	        	$descripcion=$nom_evento;
	        }else{
	        	$descripcion=$nom_programa;
	        }

	        $p_nofinalizada.="<tr id='$llave2'><td  align='left'>" .utf8_encode(strtoupper($descripcion)) . "</td><td  align='center'>".utf8_encode(strtoupper($productor))."</td><td  align='center'>".utf8_encode(strtoupper($gerencia_prod))."</td><td  align='center'>".utf8_encode(strtoupper($fecha_citacion))."</td></tr>";
	    	}

    		}else{

    		$p_nofinalizada = "<tr><th  colspan='5' align='center'><div style='color: #FF0000;font-weight: bold;' align='center'>Disculpe NO se encontro PAUTAS CADUCADAS para la fecha indicada.</div></th></tr>";
    	}

    	$p_nofinalizadas = "<table class='tabla' style='width:850px'; id='resultados'>
        <tr><th class='titulo' colspan='5'>PAUTAS CADUCADAS</th></tr>
        <tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Productor</div></th><th><div align='center'>Gerencia</div></th><th><div align='center'>Fecha de citaci&oacute;n</div></th></tr>
        " . $p_nofinalizada . "
        </table>
        ";

    	if ($cont_f!= 0 or $cont_nf!= 0){
    		echo "$p_finalizadas";
    		echo "$p_coninfs";
    		echo "$p_nofinalizadas";
    	}else{
    		$sin_pautas= "<table class='tabla' style='width:850px'; id='resultados'>
            <tr><th class='titulo' colspan='5'>PAUTAS</th></tr>
            <tr><th  align='center'><div style='color: #FF0000;font-weight: bold;' align='center'>Error: Disculpe su consulta NO arrojo ningun resultado.</div></th></tr>
            </table>
            ";
            echo $sin_pautas;
    	}

    }

    function tarea_programada(){

    	$prueba=$this->ObjConsulta->selectpautascaducas($this->conect_sistemas_vtv);
    	foreach ($prueba as $llave => $valor) {
	        $nom_pauta = $valor[1];
	        $descripcion = $valor[2];
	        $fecha_citacion = $valor[3];
    		$hora_citacion = $valor[4];
    		$id_estatus = $valor[5];
    		$id_pauta = $valor[6];

    		if ($id_estatus == 36){
    			$pautas.= "";

    		}else{
	    		if($descripcion!= ""){
		       	$nom_evento=$descripcion;
		    	}else{
		       	$nom_evento=$nom_pauta;
		    	}

		  		$descestatus = $this->ObjConsulta->selectdescestatus($this->conect_sistemas_vtv, $id_estatus);
				$descripestatus = $descestatus[1][1];

				$pautas.= $id_pauta."|".$nom_evento."|".$fecha_citacion."|".$hora_citacion."|".$descripestatus."\n";

				$mensaje = "Pauta caducada por el sistema ";
				$estatus=36;
				$user_exp ="El sistema";
				$expestatus=$this->ObjConsulta->updateestatus($this->conect_sistemas_vtv, $id_pauta, $mensaje);
				$cambiarestatus=$this->ObjConsulta->insertestatusnuevo($this->conect_sistemas_vtv, $id_pauta, $estatus, $user_exp, null);
    		}

        }

    	@print_r($pautas);

    }

}
if (isset($_GET['obtenerFuncion'])) {
	$ObjBienvenida = new classDirectorioFunciones();
}else if (isset($_POST['obtenerFuncion'])) {
	$ObjBienvenida = new classDirectorioFunciones();
}
?>
