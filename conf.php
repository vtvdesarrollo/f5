<?php
/*
archivo que configura algunas constantes de sistema
pretende organizar un poco algunas cosas hardcodeadas que tiene el sistema.
*/

//sobre el sistema de inventario
//estos valores van acorde con las tablas f5.rrhh_gerencia y f5.rrhh_division
define("id_gerencia_inv","9");
define("id_division_inv","33");

//definir valores de aplicacion
//estos valores tienen que ver al esquema usuario.t_aplicacion y t_modulo
define("id_aplicacion", 12);
define("id_modulo", 107);


?>