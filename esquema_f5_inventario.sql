--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.5
-- Dumped by pg_dump version 9.3.5
-- Started on 2014-08-07 09:05:15 VET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP DATABASE "SISTEMAS";
--
-- TOC entry 2795 (class 1262 OID 16384)
-- Name: SISTEMAS; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE "SISTEMAS" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'es_VE.UTF-8' LC_CTYPE = 'es_VE.UTF-8';


\connect "SISTEMAS"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 7 (class 2615 OID 26065)
-- Name: almacen_tecnico; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA almacen_tecnico;


--
-- TOC entry 8 (class 2615 OID 26066)
-- Name: f5; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA f5;


--
-- TOC entry 5 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA public;


--
-- TOC entry 2796 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 9 (class 2615 OID 26067)
-- Name: usuario; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA usuario;


--
-- TOC entry 2797 (class 0 OID 0)
-- Dependencies: 9
-- Name: SCHEMA usuario; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA usuario IS 'esquema que define los permisos en sistemas f5 e inventario';


--
-- TOC entry 314 (class 3079 OID 11829)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2798 (class 0 OID 0)
-- Dependencies: 314
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = f5, pg_catalog;

--
-- TOC entry 327 (class 1255 OID 26068)
-- Name: concatenar(text, text); Type: FUNCTION; Schema: f5; Owner: -
--

CREATE FUNCTION concatenar(text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$

DECLARE
t text;
BEGIN
     IF character_length($1) > 0 THEN
         IF character_length($2) > 0 THEN
             t = $1 || ' | ' || $2;
         else
             t = $1;
         end if;
     ELSE
         t = $2;
     END IF;
RETURN t;
END;
$_$;


--
-- TOC entry 1035 (class 1255 OID 26069)
-- Name: concatena(text); Type: AGGREGATE; Schema: f5; Owner: -
--

CREATE AGGREGATE concatena(text) (
    SFUNC = concatenar,
    STYPE = text
);


SET search_path = almacen_tecnico, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 173 (class 1259 OID 26070)
-- Name: t_articulo; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_articulo (
    id_articulo integer NOT NULL,
    tipo_articulo character varying,
    descripcion character varying,
    modelo character varying,
    marca character varying,
    observacion character varying,
    bien_nac character varying,
    serial character varying,
    costo character varying,
    siglas character varying,
    id_grupo character varying,
    cantidad integer,
    id_unidad_medida integer,
    id_estado integer,
    id_categoria integer,
    fecha_reg date,
    user_reg integer,
    fecha_exp date DEFAULT '2222-12-31'::date,
    user_exp integer,
    id_estatus integer,
    peldano character varying,
    estante character varying,
    imagen character varying,
    cant_min integer,
    id_descripcion integer
);


--
-- TOC entry 174 (class 1259 OID 26077)
-- Name: t_categoria; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_categoria (
    id_categoria integer NOT NULL,
    descripcion character varying,
    user_reg character varying,
    fecha_reg date,
    user_exp character varying,
    fecha_exp date DEFAULT '2222-12-31'::date
);


--
-- TOC entry 175 (class 1259 OID 26084)
-- Name: t_des_estado; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_des_estado (
    desc_estado character varying,
    id_desc_estado integer NOT NULL,
    user_reg character varying,
    fecha_reg date,
    user_exp character varying,
    fecha_exp date DEFAULT '2222-12-31'::date
);


--
-- TOC entry 176 (class 1259 OID 26091)
-- Name: t_estatus; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_estatus (
    id_estatus integer NOT NULL,
    descripcion character varying
);


--
-- TOC entry 177 (class 1259 OID 26097)
-- Name: t_grupos; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_grupos (
    id_del_grupo integer NOT NULL,
    descripcion character varying,
    id_estatus integer,
    id_categoria integer,
    user_reg integer,
    fecha_reg date,
    fecha_exp date DEFAULT '2222-12-31'::date,
    user_exp integer,
    id_asig integer,
    resp_asig integer,
    id_desc_estado integer,
    imagen character varying,
    asig integer,
    id_grupo character varying
);


--
-- TOC entry 178 (class 1259 OID 26104)
-- Name: t_unidad_medida; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_unidad_medida (
    id_unidad_medida integer NOT NULL,
    descripcion character varying,
    user_reg character varying,
    fecha_reg date,
    user_exp character varying,
    fecha_exp date DEFAULT '2222-12-31'::date
);


--
-- TOC entry 179 (class 1259 OID 26111)
-- Name: articulos_almacen; Type: VIEW; Schema: almacen_tecnico; Owner: -
--

CREATE VIEW articulos_almacen AS
 SELECT a.id_articulo,
    a.tipo_articulo,
    a.descripcion,
    a.marca,
    a.modelo,
    a.bien_nac,
    a.serial,
    a.siglas,
    e.descripcion AS grupo,
    a.costo,
    a.cantidad,
    c.descripcion AS desc_unidad_medida,
    b.desc_estado AS estado,
    a.id_estatus,
    b.id_desc_estado,
    d.descripcion AS estatus,
    f.descripcion AS categoria,
    (((((((((((((((((((a.descripcion)::text || ' '::text) || (a.marca)::text) || ' '::text) || (a.modelo)::text) || ' '::text) || (a.bien_nac)::text) || ' '::text) || (a.serial)::text) || ' '::text) || (a.cantidad)::text) || ' '::text) || (c.descripcion)::text) || ' '::text) || (e.descripcion)::text) || ' '::text) || (f.descripcion)::text) || ' '::text) || (d.descripcion)::text) AS busqueda
   FROM (((((t_articulo a
     LEFT JOIN t_des_estado b ON ((a.id_estado = b.id_desc_estado)))
     LEFT JOIN t_unidad_medida c ON (((a.id_unidad_medida)::text = (c.id_unidad_medida)::text)))
     LEFT JOIN t_estatus d ON (((a.id_estatus)::text = (d.id_estatus)::text)))
     LEFT JOIN t_grupos e ON (((a.id_grupo)::text = (e.id_grupo)::text)))
     LEFT JOIN t_categoria f ON (((a.id_categoria)::text = (f.id_categoria)::text)))
  WHERE (a.fecha_exp = '2222-12-31'::date)
  ORDER BY a.descripcion;


--
-- TOC entry 180 (class 1259 OID 26116)
-- Name: articulos_almacen_prestados; Type: VIEW; Schema: almacen_tecnico; Owner: -
--

CREATE VIEW articulos_almacen_prestados AS
 SELECT a.id_articulo,
    a.tipo_articulo,
    a.descripcion,
    a.marca,
    a.modelo,
    a.bien_nac,
    a.serial,
    a.siglas,
    e.descripcion AS grupo,
    a.costo,
    a.cantidad,
    c.descripcion AS desc_unidad_medida,
    b.desc_estado AS estado,
    a.id_estatus,
    b.id_desc_estado,
    d.descripcion AS estatus,
    f.descripcion AS categoria,
    (((((((((((((((((((a.descripcion)::text || ' '::text) || (a.marca)::text) || ' '::text) || (a.modelo)::text) || ' '::text) || (a.bien_nac)::text) || ' '::text) || (a.serial)::text) || ' '::text) || (a.cantidad)::text) || ' '::text) || (c.descripcion)::text) || ' '::text) || (e.descripcion)::text) || ' '::text) || (f.descripcion)::text) || ' '::text) || (d.descripcion)::text) AS busqueda
   FROM (((((t_articulo a
     LEFT JOIN t_des_estado b ON ((a.id_estado = b.id_desc_estado)))
     LEFT JOIN t_unidad_medida c ON (((a.id_unidad_medida)::text = (c.id_unidad_medida)::text)))
     LEFT JOIN t_estatus d ON (((a.id_estatus)::text = (d.id_estatus)::text)))
     LEFT JOIN t_grupos e ON (((a.id_grupo)::text = (e.id_grupo)::text)))
     LEFT JOIN t_categoria f ON (((a.id_categoria)::text = (f.id_categoria)::text)))
  WHERE (((a.fecha_exp = '2222-12-31'::date) AND (a.id_estatus = 2)) AND ((a.tipo_articulo)::text = '1'::text))
  ORDER BY a.descripcion;


--
-- TOC entry 181 (class 1259 OID 26121)
-- Name: articulos_consumibles; Type: VIEW; Schema: almacen_tecnico; Owner: -
--

CREATE VIEW articulos_consumibles AS
 SELECT a.id_articulo,
    a.tipo_articulo,
    a.descripcion,
    a.marca,
    a.modelo,
    a.bien_nac,
    a.serial,
    a.siglas,
    e.descripcion AS grupo,
    a.costo,
    a.cantidad,
    c.descripcion AS desc_unidad_medida,
    b.desc_estado AS estado,
    a.id_estatus,
    b.id_desc_estado,
    d.descripcion AS estatus,
    f.descripcion AS categoria,
    (((((((((((((((((((a.descripcion)::text || ' '::text) || (a.marca)::text) || ' '::text) || (a.modelo)::text) || ' '::text) || (a.bien_nac)::text) || ' '::text) || (a.serial)::text) || ' '::text) || (a.cantidad)::text) || ' '::text) || (c.descripcion)::text) || ' '::text) || (e.descripcion)::text) || ' '::text) || (f.descripcion)::text) || ' '::text) || (d.descripcion)::text) AS busqueda
   FROM (((((t_articulo a
     LEFT JOIN t_des_estado b ON ((a.id_estado = b.id_desc_estado)))
     LEFT JOIN t_unidad_medida c ON (((a.id_unidad_medida)::text = (c.id_unidad_medida)::text)))
     LEFT JOIN t_estatus d ON (((a.id_estatus)::text = (d.id_estatus)::text)))
     LEFT JOIN t_grupos e ON (((a.id_grupo)::text = (e.id_grupo)::text)))
     LEFT JOIN t_categoria f ON (((a.id_categoria)::text = (f.id_categoria)::text)))
  WHERE ((a.fecha_exp = '2222-12-31'::date) AND ((a.tipo_articulo)::text = '2'::text))
  ORDER BY a.descripcion;


--
-- TOC entry 182 (class 1259 OID 26126)
-- Name: articulos_consumibles_disp; Type: VIEW; Schema: almacen_tecnico; Owner: -
--

CREATE VIEW articulos_consumibles_disp AS
 SELECT a.id_articulo,
    a.descripcion,
    a.marca,
    a.modelo,
    a.bien_nac,
    a.serial,
    a.imagen,
    a.cantidad,
    a.id_unidad_medida,
    a.tipo_articulo,
    a.id_grupo,
    a.id_estatus,
    a.id_estado,
    a.id_categoria,
    a.cant_min,
    c.descripcion AS categoria,
    b.descripcion AS estatus,
    d.desc_estado AS estado
   FROM (((t_articulo a
     LEFT JOIN t_estatus b ON (((a.id_estatus)::text = (b.id_estatus)::text)))
     LEFT JOIN t_categoria c ON (((a.id_categoria)::text = (c.id_categoria)::text)))
     LEFT JOIN t_des_estado d ON ((a.id_estado = d.id_desc_estado)))
  WHERE (((a.fecha_exp = '2222-12-31'::date) AND (a.cantidad > 0)) AND ((a.tipo_articulo)::text = '2'::text));


--
-- TOC entry 183 (class 1259 OID 26131)
-- Name: articulos_equipos; Type: VIEW; Schema: almacen_tecnico; Owner: -
--

CREATE VIEW articulos_equipos AS
 SELECT a.id_articulo,
    a.tipo_articulo,
    a.descripcion,
    a.marca,
    a.modelo,
    a.bien_nac,
    a.serial,
    a.siglas,
    e.descripcion AS grupo,
    a.costo,
    a.cantidad,
    c.descripcion AS desc_unidad_medida,
    b.desc_estado AS estado,
    a.id_estatus,
    b.id_desc_estado,
    d.descripcion AS estatus,
    f.descripcion AS categoria,
    (((((((((((((((((((a.descripcion)::text || ' '::text) || (a.marca)::text) || ' '::text) || (a.modelo)::text) || ' '::text) || (a.bien_nac)::text) || ' '::text) || (a.serial)::text) || ' '::text) || (a.cantidad)::text) || ' '::text) || (c.descripcion)::text) || ' '::text) || (e.descripcion)::text) || ' '::text) || (f.descripcion)::text) || ' '::text) || (d.descripcion)::text) AS busqueda
   FROM (((((t_articulo a
     LEFT JOIN t_des_estado b ON ((a.id_estado = b.id_desc_estado)))
     LEFT JOIN t_unidad_medida c ON (((a.id_unidad_medida)::text = (c.id_unidad_medida)::text)))
     LEFT JOIN t_estatus d ON (((a.id_estatus)::text = (d.id_estatus)::text)))
     LEFT JOIN t_grupos e ON (((a.id_grupo)::text = (e.id_grupo)::text)))
     LEFT JOIN t_categoria f ON (((a.id_categoria)::text = (f.id_categoria)::text)))
  WHERE ((a.fecha_exp = '2222-12-31'::date) AND ((a.tipo_articulo)::text = '1'::text))
  ORDER BY a.descripcion;


--
-- TOC entry 184 (class 1259 OID 26136)
-- Name: articulos_existencia_min; Type: VIEW; Schema: almacen_tecnico; Owner: -
--

CREATE VIEW articulos_existencia_min AS
 SELECT a.id_articulo,
    a.descripcion,
    a.modelo,
    a.marca,
    a.estante,
    a.peldano,
    a.cantidad,
    b.descripcion AS desc_unidad_medida,
    a.cant_min,
    a.costo,
    c.descripcion AS categoria
   FROM ((t_articulo a
     LEFT JOIN t_unidad_medida b ON (((a.id_unidad_medida)::text = (b.id_unidad_medida)::text)))
     LEFT JOIN t_categoria c ON (((a.id_categoria)::text = (c.id_categoria)::text)))
  WHERE ((a.cantidad <= a.cant_min) AND ((a.tipo_articulo)::text = '2'::text));


--
-- TOC entry 185 (class 1259 OID 26141)
-- Name: t_descripciones; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_descripciones (
    id_descripcion integer NOT NULL,
    descripcion character varying,
    tipo_articulo integer,
    user_reg character varying,
    fecha_reg date,
    user_exp character varying,
    fecha_exp date DEFAULT '2222-12-31'::date
);


--
-- TOC entry 186 (class 1259 OID 26148)
-- Name: descripciones; Type: VIEW; Schema: almacen_tecnico; Owner: -
--

CREATE VIEW descripciones AS
 SELECT a.id_descripcion,
    a.descripcion
   FROM t_descripciones a
  ORDER BY a.descripcion;


--
-- TOC entry 187 (class 1259 OID 26152)
-- Name: t_despacho; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_despacho (
    id_despacho integer NOT NULL,
    resp_despacho character varying,
    id_destino character varying,
    id_desc_dest character varying,
    user_reg character varying,
    fecha_reg date,
    fecha_sol date,
    id_estatus_despacho integer,
    observacion character varying,
    hora_despacho time without time zone,
    hora_reg time without time zone,
    id_remoto character varying
);


--
-- TOC entry 188 (class 1259 OID 26158)
-- Name: despachos; Type: VIEW; Schema: almacen_tecnico; Owner: -
--

CREATE VIEW despachos AS
 SELECT a.id_despacho,
    a.resp_despacho,
    a.id_destino,
    a.id_desc_dest,
    a.fecha_sol,
    (((((((((a.id_despacho)::text || ' '::text) || (a.resp_despacho)::text) || ' '::text) || (a.id_destino)::text) || ' '::text) || (a.id_desc_dest)::text) || ' '::text) || (a.fecha_sol)::text) AS busqueda
   FROM t_despacho a
  ORDER BY a.id_despacho DESC;


--
-- TOC entry 189 (class 1259 OID 26162)
-- Name: t_entrada; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_entrada (
    id_entrada integer NOT NULL,
    id_solicitud integer,
    resp_entrada character varying,
    user_reg character varying,
    fecha_entrada date,
    hora_entrada time without time zone,
    id_est_entrada integer,
    observacion text,
    hora_reg time with time zone,
    fecha_reg date
);


--
-- TOC entry 190 (class 1259 OID 26168)
-- Name: entradas; Type: VIEW; Schema: almacen_tecnico; Owner: -
--

CREATE VIEW entradas AS
 SELECT a.id_entrada,
    a.id_solicitud,
    a.resp_entrada,
    a.fecha_entrada,
    (((((((a.id_entrada)::text || ' '::text) || (a.id_solicitud)::text) || ' '::text) || (a.resp_entrada)::text) || ' '::text) || (a.fecha_entrada)::text) AS busqueda
   FROM t_entrada a;


--
-- TOC entry 191 (class 1259 OID 26172)
-- Name: grupo_camaras; Type: VIEW; Schema: almacen_tecnico; Owner: -
--

CREATE VIEW grupo_camaras AS
 SELECT a.id_grupo,
    a.descripcion,
    a.id_estatus,
    b.descripcion AS estatus,
    a.id_categoria,
    c.descripcion AS categoria,
    a.id_desc_estado,
    d.desc_estado,
    a.resp_asig,
    a.fecha_exp,
    a.imagen,
    (((((((a.id_grupo)::text || ' '::text) || (a.descripcion)::text) || ' '::text) || (a.id_estatus)::text) || ' '::text) || (a.id_categoria)::text) AS busqueda
   FROM (((t_grupos a
     LEFT JOIN t_estatus b ON (((a.id_estatus)::text = (b.id_estatus)::text)))
     LEFT JOIN t_categoria c ON (((a.id_categoria)::text = (c.id_categoria)::text)))
     LEFT JOIN t_des_estado d ON ((a.id_desc_estado = d.id_desc_estado)))
  WHERE ((((((a.id_grupo)::text <> 'GP1'::text) AND (a.fecha_exp = '2222-12-31'::date)) AND ((a.descripcion)::text ~~* '%camara%'::text)) AND (a.id_asig = 1)) AND (a.asig = 1));


--
-- TOC entry 192 (class 1259 OID 26177)
-- Name: grupo_camaras_sa; Type: VIEW; Schema: almacen_tecnico; Owner: -
--

CREATE VIEW grupo_camaras_sa AS
 SELECT a.id_grupo,
    a.descripcion,
    a.id_estatus,
    b.descripcion AS estatus,
    c.descripcion AS categoria,
    a.id_desc_estado,
    d.desc_estado,
    a.resp_asig,
    a.fecha_exp,
    a.imagen,
    (((((((a.id_grupo)::text || ' '::text) || (a.descripcion)::text) || ' '::text) || (a.id_estatus)::text) || ' '::text) || (a.id_categoria)::text) AS busqueda
   FROM (((t_grupos a
     LEFT JOIN t_estatus b ON (((a.id_estatus)::text = (b.id_estatus)::text)))
     LEFT JOIN t_categoria c ON (((a.id_categoria)::text = (c.id_categoria)::text)))
     LEFT JOIN t_des_estado d ON ((a.id_desc_estado = d.id_desc_estado)))
  WHERE ((((a.id_grupo)::text <> 'GP1'::text) AND (a.fecha_exp = '2222-12-31'::date)) AND ((a.descripcion)::text ~~* '%camara%'::text));


--
-- TOC entry 193 (class 1259 OID 26182)
-- Name: grupos; Type: VIEW; Schema: almacen_tecnico; Owner: -
--

CREATE VIEW grupos AS
 SELECT a.id_grupo,
    a.descripcion,
    a.id_estatus,
    b.descripcion AS estatus,
    c.descripcion AS categoria,
    a.id_asig,
    a.id_desc_estado,
    d.desc_estado,
    a.fecha_exp,
    (((((((((a.id_grupo)::text || ' '::text) || (a.descripcion)::text) || ' '::text) || (a.id_estatus)::text) || ' '::text) || (d.desc_estado)::text) || ' '::text) || (a.id_categoria)::text) AS busqueda
   FROM (((t_grupos a
     LEFT JOIN t_estatus b ON (((a.id_estatus)::text = (b.id_estatus)::text)))
     LEFT JOIN t_categoria c ON (((a.id_categoria)::text = (c.id_categoria)::text)))
     LEFT JOIN t_des_estado d ON ((a.id_desc_estado = d.id_desc_estado)))
  WHERE (((a.id_grupo)::text <> 'GP1'::text) AND (a.fecha_exp = '2222-12-31'::date))
  ORDER BY a.descripcion;


--
-- TOC entry 194 (class 1259 OID 26187)
-- Name: grupos_sc; Type: VIEW; Schema: almacen_tecnico; Owner: -
--

CREATE VIEW grupos_sc AS
 SELECT a.id_grupo,
    a.descripcion,
    a.id_estatus,
    b.descripcion AS estatus,
    a.id_categoria,
    c.descripcion AS categoria,
    a.id_asig,
    a.id_desc_estado,
    d.desc_estado,
    a.fecha_exp,
    a.imagen,
    (((((((((a.id_del_grupo)::text || ' '::text) || (a.descripcion)::text) || ' '::text) || (a.id_estatus)::text) || ' '::text) || (d.desc_estado)::text) || ' '::text) || (a.id_categoria)::text) AS busqueda
   FROM (((t_grupos a
     LEFT JOIN t_estatus b ON (((a.id_estatus)::text = (b.id_estatus)::text)))
     LEFT JOIN t_categoria c ON (((a.id_categoria)::text = (c.id_categoria)::text)))
     LEFT JOIN t_des_estado d ON ((a.id_desc_estado = d.id_desc_estado)))
  WHERE (((a.id_del_grupo <> 1) AND (a.fecha_exp = '2222-12-31'::date)) AND (a.id_asig = 2))
  ORDER BY a.descripcion;


--
-- TOC entry 195 (class 1259 OID 26192)
-- Name: t_prestamo; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_prestamo (
    id_prestamo integer NOT NULL,
    id_destino integer,
    id_desc_dest character varying,
    resp_prestamo character varying,
    user_reg character varying,
    fecha_reg date,
    fecha_sol date,
    fecha_exp date,
    id_estatus_prestamo integer,
    observacion character varying,
    hora_prestamo time without time zone,
    fecha_ent date DEFAULT '2222-12-31'::date,
    tipo_prestamo integer,
    placa character varying,
    hora_reg time without time zone,
    id_remoto character varying,
    resp_susp character varying,
    obs_susp character varying
);


--
-- TOC entry 196 (class 1259 OID 26199)
-- Name: prestamos; Type: VIEW; Schema: almacen_tecnico; Owner: -
--

CREATE VIEW prestamos AS
 SELECT a.id_prestamo,
    a.id_destino,
    a.id_desc_dest,
    a.resp_prestamo,
    b.descripcion AS estatus,
    a.tipo_prestamo,
    (((((((((a.id_prestamo)::text || ' '::text) || (a.id_destino)::text) || ' '::text) || (a.id_desc_dest)::text) || ' '::text) || (a.resp_prestamo)::text) || ' '::text) || (b.descripcion)::text) AS busqueda
   FROM (t_prestamo a
     LEFT JOIN t_estatus b ON (((a.id_estatus_prestamo)::text = (b.id_estatus)::text)))
  ORDER BY a.id_prestamo DESC;


--
-- TOC entry 197 (class 1259 OID 26204)
-- Name: t_articulo_id_articulo_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_articulo_id_articulo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2799 (class 0 OID 0)
-- Dependencies: 197
-- Name: t_articulo_id_articulo_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_articulo_id_articulo_seq OWNED BY t_articulo.id_articulo;


--
-- TOC entry 198 (class 1259 OID 26206)
-- Name: t_camara_sol; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_camara_sol (
    id_camara_sol integer NOT NULL,
    id_solicitud integer,
    fecha_sol date,
    user_reg character varying,
    fecha_ent date DEFAULT '2222-12-31'::date,
    user_exp character varying,
    hora_exp time without time zone,
    id_grupo character varying,
    id_incidencia character varying
);


--
-- TOC entry 199 (class 1259 OID 26213)
-- Name: t_camara_sol_id_camara_sol_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_camara_sol_id_camara_sol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2800 (class 0 OID 0)
-- Dependencies: 199
-- Name: t_camara_sol_id_camara_sol_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_camara_sol_id_camara_sol_seq OWNED BY t_camara_sol.id_camara_sol;


--
-- TOC entry 200 (class 1259 OID 26215)
-- Name: t_categoria_id_catgoria_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_categoria_id_catgoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2801 (class 0 OID 0)
-- Dependencies: 200
-- Name: t_categoria_id_catgoria_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_categoria_id_catgoria_seq OWNED BY t_categoria.id_categoria;


--
-- TOC entry 201 (class 1259 OID 26217)
-- Name: t_des_estado_id_desc_estado_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_des_estado_id_desc_estado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2802 (class 0 OID 0)
-- Dependencies: 201
-- Name: t_des_estado_id_desc_estado_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_des_estado_id_desc_estado_seq OWNED BY t_des_estado.id_desc_estado;


--
-- TOC entry 202 (class 1259 OID 26219)
-- Name: t_descripciones_id_descripcion_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_descripciones_id_descripcion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2803 (class 0 OID 0)
-- Dependencies: 202
-- Name: t_descripciones_id_descripcion_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_descripciones_id_descripcion_seq OWNED BY t_descripciones.id_descripcion;


--
-- TOC entry 203 (class 1259 OID 26221)
-- Name: t_despacho_id_despacho_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_despacho_id_despacho_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2804 (class 0 OID 0)
-- Dependencies: 203
-- Name: t_despacho_id_despacho_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_despacho_id_despacho_seq OWNED BY t_despacho.id_despacho;


--
-- TOC entry 204 (class 1259 OID 26223)
-- Name: t_entrada_id_entrada_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_entrada_id_entrada_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2805 (class 0 OID 0)
-- Dependencies: 204
-- Name: t_entrada_id_entrada_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_entrada_id_entrada_seq OWNED BY t_entrada.id_entrada;


--
-- TOC entry 205 (class 1259 OID 26225)
-- Name: t_estatus_id_estatus_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_estatus_id_estatus_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2806 (class 0 OID 0)
-- Dependencies: 205
-- Name: t_estatus_id_estatus_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_estatus_id_estatus_seq OWNED BY t_estatus.id_estatus;


--
-- TOC entry 206 (class 1259 OID 26227)
-- Name: t_grupo_sol; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_grupo_sol (
    id_grupo_sol integer NOT NULL,
    id_solicitud integer,
    fecha_sol date,
    user_reg character varying,
    fecha_ent date DEFAULT '2222-12-31'::date,
    user_exp character varying,
    hora_exp time without time zone,
    id_grupo character varying,
    id_incidencia character varying
);


--
-- TOC entry 207 (class 1259 OID 26234)
-- Name: t_grupo_sol_id_grupo_sol_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_grupo_sol_id_grupo_sol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2807 (class 0 OID 0)
-- Dependencies: 207
-- Name: t_grupo_sol_id_grupo_sol_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_grupo_sol_id_grupo_sol_seq OWNED BY t_grupo_sol.id_grupo_sol;


--
-- TOC entry 208 (class 1259 OID 26236)
-- Name: t_grupos_id_grupo_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_grupos_id_grupo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2808 (class 0 OID 0)
-- Dependencies: 208
-- Name: t_grupos_id_grupo_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_grupos_id_grupo_seq OWNED BY t_grupos.id_del_grupo;


--
-- TOC entry 209 (class 1259 OID 26238)
-- Name: t_incidencia; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_incidencia (
    resp_incidencia character varying,
    user_reg character varying,
    fecha_solicitud date,
    hora_solicitud time without time zone,
    observacion text,
    hora_reg time without time zone,
    fecha_reg date,
    id_solicitud integer,
    id_incidencia character varying,
    id_serial_incidencia integer NOT NULL
);


--
-- TOC entry 210 (class 1259 OID 26244)
-- Name: t_incidencia_id_serial_incidencia_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_incidencia_id_serial_incidencia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2809 (class 0 OID 0)
-- Dependencies: 210
-- Name: t_incidencia_id_serial_incidencia_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_incidencia_id_serial_incidencia_seq OWNED BY t_incidencia.id_serial_incidencia;


--
-- TOC entry 211 (class 1259 OID 26246)
-- Name: t_ingresos; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_ingresos (
    id_ingresos integer NOT NULL,
    id_articulo character varying,
    cantidad character varying,
    costo character varying,
    fecha_reg date,
    user_reg character varying,
    tipo character varying,
    hora_reg time without time zone
);


--
-- TOC entry 212 (class 1259 OID 26252)
-- Name: t_ingresos_id_ingresos_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_ingresos_id_ingresos_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2810 (class 0 OID 0)
-- Dependencies: 212
-- Name: t_ingresos_id_ingresos_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_ingresos_id_ingresos_seq OWNED BY t_ingresos.id_ingresos;


--
-- TOC entry 213 (class 1259 OID 26254)
-- Name: t_material_desp; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_material_desp (
    id_material_desp integer NOT NULL,
    id_articulo integer,
    id_despacho integer NOT NULL,
    fecha_reg date,
    user_reg character varying,
    cantidad character varying,
    fecha_exp date DEFAULT '2222-12-31'::date,
    user_exp character varying,
    resp_devol character varying,
    hora_exp time without time zone,
    hora_dev time without time zone,
    sol_ppal character varying
);


--
-- TOC entry 214 (class 1259 OID 26261)
-- Name: t_material_desp_id_material_desp_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_material_desp_id_material_desp_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2811 (class 0 OID 0)
-- Dependencies: 214
-- Name: t_material_desp_id_material_desp_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_material_desp_id_material_desp_seq OWNED BY t_material_desp.id_material_desp;


--
-- TOC entry 215 (class 1259 OID 26263)
-- Name: t_material_sol; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_material_sol (
    id_material_sol integer NOT NULL,
    id_articulo integer,
    id_solicitud integer,
    fecha_sol date,
    user_reg character varying,
    fecha_ent date DEFAULT '2222-12-31'::date,
    user_exp character varying,
    hora_exp time without time zone,
    id_incidencia character varying
);


--
-- TOC entry 216 (class 1259 OID 26270)
-- Name: t_material_sol_id_material_sol_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_material_sol_id_material_sol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2812 (class 0 OID 0)
-- Dependencies: 216
-- Name: t_material_sol_id_material_sol_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_material_sol_id_material_sol_seq OWNED BY t_material_sol.id_material_sol;


--
-- TOC entry 217 (class 1259 OID 26272)
-- Name: t_prestamo_id_prestamo_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_prestamo_id_prestamo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2813 (class 0 OID 0)
-- Dependencies: 217
-- Name: t_prestamo_id_prestamo_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_prestamo_id_prestamo_seq OWNED BY t_prestamo.id_prestamo;


--
-- TOC entry 218 (class 1259 OID 26274)
-- Name: t_programa; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_programa (
    id_programa integer NOT NULL,
    descripcion character varying,
    user_reg character varying,
    fecha_reg date,
    user_exp character varying,
    fecha_exp date DEFAULT '2222-12-31'::date
);


--
-- TOC entry 219 (class 1259 OID 26281)
-- Name: t_programa_id_programa_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_programa_id_programa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2814 (class 0 OID 0)
-- Dependencies: 219
-- Name: t_programa_id_programa_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_programa_id_programa_seq OWNED BY t_programa.id_programa;


--
-- TOC entry 220 (class 1259 OID 26283)
-- Name: t_solicitudes_f5; Type: TABLE; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

CREATE TABLE t_solicitudes_f5 (
    id_solicitud integer NOT NULL,
    id_pauta integer,
    id_articulo integer,
    id_prestamo integer,
    fecha_exp date DEFAULT '2222-12-31'::date
);


--
-- TOC entry 221 (class 1259 OID 26287)
-- Name: t_solicitudes_f5_id_solicitud_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_solicitudes_f5_id_solicitud_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2815 (class 0 OID 0)
-- Dependencies: 221
-- Name: t_solicitudes_f5_id_solicitud_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_solicitudes_f5_id_solicitud_seq OWNED BY t_solicitudes_f5.id_solicitud;


--
-- TOC entry 222 (class 1259 OID 26289)
-- Name: t_unidad_medida_id_unidad_medida_seq; Type: SEQUENCE; Schema: almacen_tecnico; Owner: -
--

CREATE SEQUENCE t_unidad_medida_id_unidad_medida_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2816 (class 0 OID 0)
-- Dependencies: 222
-- Name: t_unidad_medida_id_unidad_medida_seq; Type: SEQUENCE OWNED BY; Schema: almacen_tecnico; Owner: -
--

ALTER SEQUENCE t_unidad_medida_id_unidad_medida_seq OWNED BY t_unidad_medida.id_unidad_medida;


--
-- TOC entry 223 (class 1259 OID 26291)
-- Name: ubicacion_consumibles; Type: VIEW; Schema: almacen_tecnico; Owner: -
--

CREATE VIEW ubicacion_consumibles AS
 SELECT a.id_articulo,
    a.tipo_articulo,
    a.descripcion,
    a.marca,
    a.modelo,
    a.bien_nac,
    a.serial,
    a.siglas,
    e.descripcion AS grupo,
    a.costo,
    a.cantidad,
    c.descripcion AS desc_unidad_medida,
    b.desc_estado AS estado,
    a.id_estatus,
    b.id_desc_estado,
    d.descripcion AS estatus,
    f.descripcion AS categoria,
    a.peldano,
    a.estante,
    (((((((((((((((((((a.descripcion)::text || ' '::text) || (a.marca)::text) || ' '::text) || (a.modelo)::text) || ' '::text) || (a.bien_nac)::text) || ' '::text) || (a.serial)::text) || ' '::text) || (a.cantidad)::text) || ' '::text) || (c.descripcion)::text) || ' '::text) || (e.descripcion)::text) || ' '::text) || (f.descripcion)::text) || ' '::text) || (d.descripcion)::text) AS busqueda
   FROM (((((t_articulo a
     LEFT JOIN t_des_estado b ON ((a.id_estado = b.id_desc_estado)))
     LEFT JOIN t_unidad_medida c ON (((a.id_unidad_medida)::text = (c.id_unidad_medida)::text)))
     LEFT JOIN t_estatus d ON (((a.id_estatus)::text = (d.id_estatus)::text)))
     LEFT JOIN t_grupos e ON (((a.id_grupo)::text = (e.id_grupo)::text)))
     LEFT JOIN t_categoria f ON (((a.id_categoria)::text = (f.id_categoria)::text)))
  WHERE ((a.fecha_exp = '2222-12-31'::date) AND ((a.tipo_articulo)::text = '2'::text))
  ORDER BY a.descripcion;


SET search_path = f5, pg_catalog;

--
-- TOC entry 227 (class 1259 OID 26317)
-- Name: t_estatus_pauta; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_estatus_pauta (
    id_estatus_pauta integer NOT NULL,
    id_estatus integer,
    id_pauta integer,
    fecha_exp date DEFAULT '2222-12-31'::date,
    observaciones text,
    fecha_reg date DEFAULT now(),
    user_reg character varying
)
WITH (autovacuum_enabled=true);


--
-- TOC entry 2817 (class 0 OID 0)
-- Dependencies: 227
-- Name: TABLE t_estatus_pauta; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_estatus_pauta IS 'Tabla que almacena los esatutus por los que ha pasado una pauta desde su ecomienzo hasta que se expira la pauta';


--
-- TOC entry 228 (class 1259 OID 26325)
-- Name: cant_estatus_por_pauta; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW cant_estatus_por_pauta AS
 SELECT t_estatus_pauta.id_pauta,
    t_estatus_pauta.id_estatus,
    t_estatus_pauta.fecha_reg,
    count(t_estatus_pauta.id_estatus) AS cantidad
   FROM t_estatus_pauta
  GROUP BY t_estatus_pauta.id_estatus, t_estatus_pauta.id_pauta, t_estatus_pauta.fecha_reg
  ORDER BY t_estatus_pauta.id_pauta;


--
-- TOC entry 229 (class 1259 OID 26329)
-- Name: t_detalle_servicio; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_detalle_servicio (
    id_detalle_servicio integer NOT NULL,
    id_pauta integer NOT NULL,
    fecha_reg date,
    user_reg character varying,
    fecha_exp date DEFAULT '2222-12-31'::date,
    user_exp character varying,
    id_recurso integer,
    id_recurso_asignado integer DEFAULT 1,
    id_tipo_recurso integer,
    id_gerencia integer,
    id_division integer
)
WITH (autovacuum_enabled=true);


--
-- TOC entry 2818 (class 0 OID 0)
-- Dependencies: 229
-- Name: TABLE t_detalle_servicio; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_detalle_servicio IS 'Tabla que almacena los recursos de una pauta';


--
-- TOC entry 230 (class 1259 OID 26337)
-- Name: cantidades_recursos; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW cantidades_recursos AS
 SELECT t_detalle_servicio.id_recurso,
    t_detalle_servicio.id_pauta,
    count(t_detalle_servicio.id_recurso) AS cantidad
   FROM t_detalle_servicio
  WHERE ((t_detalle_servicio.fecha_exp = '2222-12-31'::date) AND (t_detalle_servicio.id_recurso_asignado = 1))
  GROUP BY t_detalle_servicio.id_recurso, t_detalle_servicio.id_pauta
  ORDER BY t_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso;


--
-- TOC entry 231 (class 1259 OID 26341)
-- Name: rrhh_cargos; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE rrhh_cargos (
    id_cargo integer NOT NULL,
    de_cargo character varying,
    fecha_expiracion date DEFAULT '2222-12-31'::date
);


--
-- TOC entry 232 (class 1259 OID 26348)
-- Name: rrhh_categoria; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE rrhh_categoria (
    id_categoria integer NOT NULL,
    desc_categoria character varying,
    fecha_expiracion date DEFAULT '2222-12-31'::date NOT NULL
);


--
-- TOC entry 233 (class 1259 OID 26355)
-- Name: rrhh_rel_categoria_cargo; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE rrhh_rel_categoria_cargo (
    id_categoria integer,
    id_cargo integer
);


--
-- TOC entry 234 (class 1259 OID 26358)
-- Name: categorias_cargo; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW categorias_cargo AS
 SELECT concatena((rrhh_cargos.id_cargo)::text) AS ids_cargos,
    rrhh_categoria.desc_categoria
   FROM ((rrhh_rel_categoria_cargo
     LEFT JOIN rrhh_cargos ON ((rrhh_cargos.id_cargo = rrhh_rel_categoria_cargo.id_cargo)))
     LEFT JOIN rrhh_categoria ON ((rrhh_categoria.id_categoria = rrhh_rel_categoria_cargo.id_categoria)))
  GROUP BY rrhh_categoria.desc_categoria
  ORDER BY rrhh_categoria.desc_categoria;


--
-- TOC entry 236 (class 1259 OID 26370)
-- Name: contador; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW contador AS
 SELECT t_estatus_pauta.id_estatus,
    count(t_estatus_pauta.id_pauta) AS cantidad
   FROM t_estatus_pauta
  GROUP BY t_estatus_pauta.id_estatus
  ORDER BY t_estatus_pauta.id_estatus;


--
-- TOC entry 237 (class 1259 OID 26374)
-- Name: invmaterial_asignados_pauta; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW invmaterial_asignados_pauta AS
 SELECT t_detalle_servicio.id_detalle_servicio AS serial,
    t_detalle_servicio.id_pauta,
    t_detalle_servicio.fecha_reg,
    t_detalle_servicio.user_reg,
    t_detalle_servicio.fecha_exp,
    t_detalle_servicio.user_exp,
    t_detalle_servicio.id_recurso,
    t_detalle_servicio.id_recurso_asignado,
    t_detalle_servicio.id_tipo_recurso
   FROM t_detalle_servicio
  WHERE (((t_detalle_servicio.id_recurso_asignado <> 1) AND (t_detalle_servicio.id_tipo_recurso = 1)) AND (t_detalle_servicio.fecha_exp = '2222-12-31'::date));


--
-- TOC entry 252 (class 1259 OID 26458)
-- Name: rrhh_division; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE rrhh_division (
    id_division integer NOT NULL,
    id_gerencia integer,
    desc_division character varying,
    fecha_expiracion date DEFAULT '2222-12-31'::date NOT NULL
);


--
-- TOC entry 253 (class 1259 OID 26465)
-- Name: rrhh_gerencia; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE rrhh_gerencia (
    id_gerencia integer NOT NULL,
    desc_gerencia character varying,
    fecha_expiracion date DEFAULT '2222-12-31'::date NOT NULL
);


--
-- TOC entry 238 (class 1259 OID 26378)
-- Name: t_citacion; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_citacion (
    id_citacion integer NOT NULL,
    fecha date,
    hora time without time zone,
    id_lugar integer NOT NULL
)
WITH (autovacuum_enabled=true);


--
-- TOC entry 2819 (class 0 OID 0)
-- Dependencies: 238
-- Name: TABLE t_citacion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_citacion IS 'Tabla que almacena los distintos tipos de citacion de la pauta';


--
-- TOC entry 2820 (class 0 OID 0)
-- Dependencies: 238
-- Name: COLUMN t_citacion.id_citacion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_citacion.id_citacion IS 'Campo identificacion de la tabla t_citacion';


--
-- TOC entry 2821 (class 0 OID 0)
-- Dependencies: 238
-- Name: COLUMN t_citacion.fecha; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_citacion.fecha IS 'Fecha de la citacion';


--
-- TOC entry 2822 (class 0 OID 0)
-- Dependencies: 238
-- Name: COLUMN t_citacion.hora; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_citacion.hora IS 'Hora de la citacion';


--
-- TOC entry 2823 (class 0 OID 0)
-- Dependencies: 238
-- Name: COLUMN t_citacion.id_lugar; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_citacion.id_lugar IS 'lugar donde se realizara la citacion ';


--
-- TOC entry 239 (class 1259 OID 26381)
-- Name: t_emision_grabacion; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_emision_grabacion (
    id_emision_grabacion integer NOT NULL,
    fecha date,
    hora time without time zone
)
WITH (autovacuum_enabled=true);


--
-- TOC entry 2824 (class 0 OID 0)
-- Dependencies: 239
-- Name: TABLE t_emision_grabacion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_emision_grabacion IS 'Tabla que almacena la fecha de la emision o grabacion de una pauta';


--
-- TOC entry 240 (class 1259 OID 26384)
-- Name: t_estatus; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_estatus (
    id_estatus integer NOT NULL,
    descripcion character varying,
    id_tipo_estatus integer
);


--
-- TOC entry 2825 (class 0 OID 0)
-- Dependencies: 240
-- Name: TABLE t_estatus; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_estatus IS 'Tabla que almacena los tipos de estatus para la pauta y para el servicio';


--
-- TOC entry 2826 (class 0 OID 0)
-- Dependencies: 240
-- Name: COLUMN t_estatus.id_estatus; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_estatus.id_estatus IS 'Campo identificativo de la tabla estatus';


--
-- TOC entry 2827 (class 0 OID 0)
-- Dependencies: 240
-- Name: COLUMN t_estatus.descripcion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_estatus.descripcion IS 'Descripcion del estatus';


--
-- TOC entry 2828 (class 0 OID 0)
-- Dependencies: 240
-- Name: COLUMN t_estatus.id_tipo_estatus; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_estatus.id_tipo_estatus IS 'Id del tipo de estatus al que va hacer refeencia esta descripcion';


--
-- TOC entry 241 (class 1259 OID 26390)
-- Name: t_locacion; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_locacion (
    id_locacion integer NOT NULL,
    descripcion character varying,
    CONSTRAINT t_locacion_descripcion_check CHECK ((((descripcion)::text = 'REMOTO'::text) OR ((descripcion)::text = 'ESTUDIO'::text)))
);


--
-- TOC entry 2829 (class 0 OID 0)
-- Dependencies: 241
-- Name: TABLE t_locacion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_locacion IS 'Tabla que almacena las locaciones de la pauta';


--
-- TOC entry 2830 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN t_locacion.id_locacion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_locacion.id_locacion IS 'Campo identificativo de la tabla locacion';


--
-- TOC entry 2831 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN t_locacion.descripcion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_locacion.descripcion IS 'Descripcion de la locacion';


--
-- TOC entry 242 (class 1259 OID 26397)
-- Name: t_lugar; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_lugar (
    id_lugar integer NOT NULL,
    descripcion character varying,
    CONSTRAINT t_lugar_descripcion_check CHECK ((((((((descripcion)::text = 'VTV'::text) OR ((descripcion)::text = 'EN SITIO'::text)) OR ((descripcion)::text = 'ESTUDIO 1'::text)) OR ((descripcion)::text = 'ESTUDIO 2'::text)) OR ((descripcion)::text = 'ESTUDIO 3'::text)) OR ((descripcion)::text = 'ESTUDIO 4'::text)))
)
WITH (autovacuum_enabled=true);


--
-- TOC entry 2832 (class 0 OID 0)
-- Dependencies: 242
-- Name: TABLE t_lugar; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_lugar IS 'Tabla que almacena los distintos lugares de la citacion para la pauta';


--
-- TOC entry 2833 (class 0 OID 0)
-- Dependencies: 242
-- Name: COLUMN t_lugar.id_lugar; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_lugar.id_lugar IS 'Campo identificativo de la tabla lugar';


--
-- TOC entry 2834 (class 0 OID 0)
-- Dependencies: 242
-- Name: COLUMN t_lugar.descripcion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_lugar.descripcion IS 'Descripcion del lugar';


--
-- TOC entry 243 (class 1259 OID 26404)
-- Name: t_montaje; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_montaje (
    id_montaje integer NOT NULL,
    fecha date,
    hora time without time zone
);


--
-- TOC entry 2835 (class 0 OID 0)
-- Dependencies: 243
-- Name: TABLE t_montaje; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_montaje IS 'Tabla donde se almacenan los montajes de la pauta';


--
-- TOC entry 2836 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN t_montaje.id_montaje; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_montaje.id_montaje IS 'Campo identificativo de la tabla montaje';


--
-- TOC entry 2837 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN t_montaje.fecha; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_montaje.fecha IS 'Fecha a realizarse el montaje de la pauta';


--
-- TOC entry 2838 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN t_montaje.hora; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_montaje.hora IS 'Hora a realizarse el montaje de la pauta';


--
-- TOC entry 244 (class 1259 OID 26407)
-- Name: t_pauta; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_pauta (
    id_pauta integer NOT NULL,
    fecha_reg date DEFAULT now(),
    fecha_exp date DEFAULT '2222-12-31'::date,
    user_reg character varying,
    user_exp character varying,
    id_tipo_pauta integer NOT NULL,
    id_locacion integer NOT NULL,
    id_tipo_traje integer NOT NULL,
    id_program integer NOT NULL,
    id_citacion integer NOT NULL,
    id_montaje integer NOT NULL,
    id_emision_grabacion integer NOT NULL,
    id_retorno integer NOT NULL,
    id_tipo_evento integer NOT NULL,
    lugar character varying,
    descripcion character varying
)
WITH (autovacuum_enabled=true);


--
-- TOC entry 2839 (class 0 OID 0)
-- Dependencies: 244
-- Name: TABLE t_pauta; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_pauta IS 'Tabla donde se almacenan todos los datos relacionados a la pauta';


--
-- TOC entry 2840 (class 0 OID 0)
-- Dependencies: 244
-- Name: COLUMN t_pauta.id_pauta; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_pauta.id_pauta IS 'Campo identificativo de la tabla pauta';


--
-- TOC entry 2841 (class 0 OID 0)
-- Dependencies: 244
-- Name: COLUMN t_pauta.fecha_reg; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_pauta.fecha_reg IS 'Fecha en la se reliza el registro de la pauta';


--
-- TOC entry 2842 (class 0 OID 0)
-- Dependencies: 244
-- Name: COLUMN t_pauta.fecha_exp; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_pauta.fecha_exp IS 'Fecha en la que se realiza el eliminado logico de la pauta ';


--
-- TOC entry 2843 (class 0 OID 0)
-- Dependencies: 244
-- Name: COLUMN t_pauta.user_reg; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_pauta.user_reg IS 'Usuario que hace el registro de la pauta';


--
-- TOC entry 2844 (class 0 OID 0)
-- Dependencies: 244
-- Name: COLUMN t_pauta.user_exp; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_pauta.user_exp IS 'Usuario que realiza el eliminado logico de la pauta';


--
-- TOC entry 2845 (class 0 OID 0)
-- Dependencies: 244
-- Name: COLUMN t_pauta.id_tipo_pauta; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_pauta.id_tipo_pauta IS 'id del tipo de pauta para la pauta';


--
-- TOC entry 2846 (class 0 OID 0)
-- Dependencies: 244
-- Name: COLUMN t_pauta.id_locacion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_pauta.id_locacion IS 'id de la locacion de la pauta';


--
-- TOC entry 2847 (class 0 OID 0)
-- Dependencies: 244
-- Name: COLUMN t_pauta.id_tipo_traje; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_pauta.id_tipo_traje IS 'id del tipo de traje para la pauta';


--
-- TOC entry 2848 (class 0 OID 0)
-- Dependencies: 244
-- Name: COLUMN t_pauta.id_program; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_pauta.id_program IS 'id del programa al que pertenece la pauta';


--
-- TOC entry 2849 (class 0 OID 0)
-- Dependencies: 244
-- Name: COLUMN t_pauta.id_citacion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_pauta.id_citacion IS 'id de la citacion para la pauta';


--
-- TOC entry 2850 (class 0 OID 0)
-- Dependencies: 244
-- Name: COLUMN t_pauta.id_montaje; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_pauta.id_montaje IS 'id del montaje para la pauta';


--
-- TOC entry 2851 (class 0 OID 0)
-- Dependencies: 244
-- Name: COLUMN t_pauta.id_emision_grabacion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_pauta.id_emision_grabacion IS 'id de la emision de la grabacion ';


--
-- TOC entry 2852 (class 0 OID 0)
-- Dependencies: 244
-- Name: COLUMN t_pauta.id_retorno; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_pauta.id_retorno IS 'id del retorno de la pauta';


--
-- TOC entry 2853 (class 0 OID 0)
-- Dependencies: 244
-- Name: COLUMN t_pauta.id_tipo_evento; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_pauta.id_tipo_evento IS 'id del tipo de evento que sera la pauta';


--
-- TOC entry 2854 (class 0 OID 0)
-- Dependencies: 244
-- Name: COLUMN t_pauta.lugar; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_pauta.lugar IS 'Descricpcion del lugar donde se llevara a cabo la pauta';


--
-- TOC entry 2855 (class 0 OID 0)
-- Dependencies: 244
-- Name: COLUMN t_pauta.descripcion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_pauta.descripcion IS 'Descripcion de la pauta';


--
-- TOC entry 245 (class 1259 OID 26415)
-- Name: t_programa; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_programa (
    id_program integer NOT NULL,
    descripcion character varying,
    fecha_exp date DEFAULT '2222-12-31'::date
);


--
-- TOC entry 2856 (class 0 OID 0)
-- Dependencies: 245
-- Name: TABLE t_programa; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_programa IS 'Tabla en la que se almacena los distintos programas en los que puede hacer o a los que corresponde una pauta';


--
-- TOC entry 2857 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN t_programa.id_program; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_programa.id_program IS 'Campo identificativo de la tabla programa';


--
-- TOC entry 2858 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN t_programa.descripcion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_programa.descripcion IS 'Descripcion del nombre del programa ';


--
-- TOC entry 246 (class 1259 OID 26422)
-- Name: t_retorno; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_retorno (
    id_retorno integer NOT NULL,
    fecha date,
    hora time without time zone
);


--
-- TOC entry 2859 (class 0 OID 0)
-- Dependencies: 246
-- Name: TABLE t_retorno; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_retorno IS 'Tabla donde se almacena los datos del retorno de la pauta';


--
-- TOC entry 2860 (class 0 OID 0)
-- Dependencies: 246
-- Name: COLUMN t_retorno.id_retorno; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_retorno.id_retorno IS 'Campo identificativo de la tabla retorno';


--
-- TOC entry 2861 (class 0 OID 0)
-- Dependencies: 246
-- Name: COLUMN t_retorno.fecha; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_retorno.fecha IS 'Fecha del retorno de la pauta';


--
-- TOC entry 2862 (class 0 OID 0)
-- Dependencies: 246
-- Name: COLUMN t_retorno.hora; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_retorno.hora IS 'Hora del retorno de la pauta';


--
-- TOC entry 247 (class 1259 OID 26425)
-- Name: t_tipo_estatus; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_tipo_estatus (
    id_tipo_estatus integer NOT NULL,
    descripcion character varying,
    CONSTRAINT t_tipo_estatus_descripcion_check CHECK ((((descripcion)::text = 'DE PAUTA'::text) OR ((descripcion)::text = 'DE SERVICIO'::text)))
);


--
-- TOC entry 2863 (class 0 OID 0)
-- Dependencies: 247
-- Name: TABLE t_tipo_estatus; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_tipo_estatus IS 'Tabla que almacena a que tabla se refiere el tipo de estatus';


--
-- TOC entry 2864 (class 0 OID 0)
-- Dependencies: 247
-- Name: COLUMN t_tipo_estatus.id_tipo_estatus; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_tipo_estatus.id_tipo_estatus IS 'Campo identificativo de la tabla tipo_estatus';


--
-- TOC entry 2865 (class 0 OID 0)
-- Dependencies: 247
-- Name: COLUMN t_tipo_estatus.descripcion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_tipo_estatus.descripcion IS 'Descricpcion del tipo de estatus';


--
-- TOC entry 248 (class 1259 OID 26432)
-- Name: t_tipo_evento; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_tipo_evento (
    id_tipo_evento integer NOT NULL,
    descripcion character varying,
    CONSTRAINT t_tipo_evento_descripcion_check CHECK ((((descripcion)::text = 'EN VIVO'::text) OR ((descripcion)::text = 'GRABADO'::text)))
);


--
-- TOC entry 2866 (class 0 OID 0)
-- Dependencies: 248
-- Name: TABLE t_tipo_evento; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_tipo_evento IS 'Tabla que almacena los distintos tipos de eventos para la pauta';


--
-- TOC entry 2867 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN t_tipo_evento.id_tipo_evento; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_tipo_evento.id_tipo_evento IS 'Campo identificativo de la tabla tipo_evento';


--
-- TOC entry 2868 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN t_tipo_evento.descripcion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_tipo_evento.descripcion IS 'Descripcion del tipo de evento';


--
-- TOC entry 249 (class 1259 OID 26439)
-- Name: t_tipo_pauta; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_tipo_pauta (
    id_tipo_pauta integer NOT NULL,
    descripcion character varying,
    CONSTRAINT chk_tipo_pauta CHECK ((((descripcion)::text = 'PROGRAMA'::text) OR ((descripcion)::text = 'EVENTO'::text)))
);


--
-- TOC entry 2869 (class 0 OID 0)
-- Dependencies: 249
-- Name: TABLE t_tipo_pauta; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_tipo_pauta IS 'Tabla que almcena los distintos tipos de pauta';


--
-- TOC entry 2870 (class 0 OID 0)
-- Dependencies: 249
-- Name: COLUMN t_tipo_pauta.id_tipo_pauta; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_tipo_pauta.id_tipo_pauta IS 'Campo identificativo de la tabla tipo_pauta';


--
-- TOC entry 2871 (class 0 OID 0)
-- Dependencies: 249
-- Name: COLUMN t_tipo_pauta.descripcion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_tipo_pauta.descripcion IS 'Descripcion del tipo de pauta';


--
-- TOC entry 250 (class 1259 OID 26446)
-- Name: t_tipo_traje; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_tipo_traje (
    id_tipo_traje integer NOT NULL,
    descripcion character varying,
    fecha_exp date DEFAULT '2222-12-31'::date
);


--
-- TOC entry 2872 (class 0 OID 0)
-- Dependencies: 250
-- Name: TABLE t_tipo_traje; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_tipo_traje IS 'Tabla que almacena los distintos tipos de traje que se utilizaran para la pauta';


--
-- TOC entry 2873 (class 0 OID 0)
-- Dependencies: 250
-- Name: COLUMN t_tipo_traje.id_tipo_traje; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_tipo_traje.id_tipo_traje IS 'Campo identificativo de la tabla tipo_traje';


--
-- TOC entry 2874 (class 0 OID 0)
-- Dependencies: 250
-- Name: COLUMN t_tipo_traje.descripcion; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_tipo_traje.descripcion IS 'Descripcion del tipo de traje';


--
-- TOC entry 251 (class 1259 OID 26453)
-- Name: sys_main; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW sys_main AS
 SELECT a.id_pauta,
    a.lugar,
    a.descripcion,
    a.user_reg AS productor,
    k.descripcion AS tipo_pauta,
    m.descripcion AS tipo_evento,
    d.descripcion AS nombre_programa,
    x.descripcion AS pauta_locacion,
    j.descripcion AS pauta_traje,
    n0.descripcion AS citacion_lugar,
    g.fecha AS fecha_citacion,
    g.hora AS hora_citacion,
    h.fecha AS fecha_retorno,
    h.hora AS hora_retorno,
    i.fecha AS fecha_montaje,
    i.hora AS hora_montaje,
    c.fecha AS fecha_emision,
    c.hora AS hora_emision,
    f.descripcion AS pauta_estatus,
        CASE
            WHEN ((a.descripcion)::text = ''::text) THEN ((((((((((((((((((((((((((((((((((a.id_pauta || ' | '::text) || (a.lugar)::text) || ' | '::text) || (a.user_reg)::text) || ' | '::text) || (k.descripcion)::text) || ' | '::text) || (m.descripcion)::text) || ' | '::text) || (d.descripcion)::text) || ' | '::text) || (x.descripcion)::text) || ' | '::text) || (j.descripcion)::text) || ' | '::text) || (n0.descripcion)::text) || ' | '::text) || g.fecha) || ' | '::text) || g.hora) || ' | '::text) || h.fecha) || ' | '::text) || h.hora) || ' | '::text) || i.fecha) || ' | '::text) || i.hora) || ' | '::text) || c.fecha) || ' | '::text) || c.hora) || ' | '::text) || (f.descripcion)::text)
            ELSE ((((((((((((((((((((((((((((((((((a.id_pauta || ' | '::text) || (a.lugar)::text) || ' | '::text) || (a.descripcion)::text) || ' | '::text) || (a.user_reg)::text) || ' | '::text) || (k.descripcion)::text) || ' | '::text) || (m.descripcion)::text) || ' | '::text) || (x.descripcion)::text) || ' | '::text) || (j.descripcion)::text) || ' | '::text) || (n0.descripcion)::text) || ' | '::text) || g.fecha) || ' | '::text) || g.hora) || ' | '::text) || h.fecha) || ' | '::text) || h.hora) || ' | '::text) || i.fecha) || ' | '::text) || i.hora) || ' | '::text) || c.fecha) || ' | '::text) || c.hora) || ' | '::text) || (f.descripcion)::text)
        END AS busqueda,
    f.id_estatus
   FROM (((((((((((((t_pauta a
     LEFT JOIN t_estatus_pauta b USING (id_pauta))
     LEFT JOIN t_emision_grabacion c USING (id_emision_grabacion))
     LEFT JOIN t_programa d USING (id_program))
     LEFT JOIN t_estatus f USING (id_estatus))
     LEFT JOIN t_citacion g USING (id_citacion))
     LEFT JOIN t_retorno h USING (id_retorno))
     LEFT JOIN t_montaje i USING (id_montaje))
     LEFT JOIN t_tipo_traje j USING (id_tipo_traje))
     LEFT JOIN t_tipo_pauta k USING (id_tipo_pauta))
     LEFT JOIN t_tipo_evento m USING (id_tipo_evento))
     LEFT JOIN t_tipo_estatus l USING (id_tipo_estatus))
     LEFT JOIN t_locacion x USING (id_locacion))
     LEFT JOIN t_lugar n0 ON ((n0.id_lugar = g.id_lugar)))
  WHERE (b.fecha_exp = '2222-12-31'::date)
  ORDER BY a.id_pauta DESC;


SET search_path = usuario, pg_catalog;

SET default_with_oids = true;

--
-- TOC entry 224 (class 1259 OID 26296)
-- Name: t_acceso; Type: TABLE; Schema: usuario; Owner: -; Tablespace: 
--

CREATE TABLE t_acceso (
    id_acceso integer NOT NULL,
    cedula character varying(12),
    clave character varying,
    id_modulo integer,
    id_aplicacion integer,
    niv_con integer,
    niv_eli integer,
    niv_inc integer,
    niv_mod integer,
    ced_trans character varying(12),
    fecha_trans date,
    fecha_exp date DEFAULT '2222-12-31'::date,
    id_tipo_usuario integer,
    id_submodulo integer,
    idgerencia integer DEFAULT 0,
    iddivision integer DEFAULT 0
);


--
-- TOC entry 254 (class 1259 OID 26472)
-- Name: t_aplicacion; Type: TABLE; Schema: usuario; Owner: -; Tablespace: 
--

CREATE TABLE t_aplicacion (
    id_aplicacion integer NOT NULL,
    nombre character varying(50) NOT NULL,
    version character varying(20) NOT NULL,
    fecha_trans date NOT NULL,
    ced_trans character varying(12) NOT NULL,
    fecha_exp date DEFAULT '2222-12-31'::date
);


SET default_with_oids = false;

--
-- TOC entry 225 (class 1259 OID 26305)
-- Name: t_datos_personales; Type: TABLE; Schema: usuario; Owner: -; Tablespace: 
--

CREATE TABLE t_datos_personales (
    nombre_usuario character varying,
    apellido_usuario character varying,
    cedula integer,
    id_datos_personales integer NOT NULL,
    correo character varying,
    telefono1 character varying,
    telefono2 character varying
);


SET default_with_oids = true;

--
-- TOC entry 255 (class 1259 OID 26476)
-- Name: t_modulo; Type: TABLE; Schema: usuario; Owner: -; Tablespace: 
--

CREATE TABLE t_modulo (
    id_modulo integer NOT NULL,
    id_aplicacion integer NOT NULL,
    nombre character varying(100) NOT NULL,
    fecha_trans date NOT NULL,
    fecha_exp date DEFAULT '2222-12-31'::date
);


SET default_with_oids = false;

--
-- TOC entry 226 (class 1259 OID 26311)
-- Name: t_tipo_usuario; Type: TABLE; Schema: usuario; Owner: -; Tablespace: 
--

CREATE TABLE t_tipo_usuario (
    id_tipo_usuario integer NOT NULL,
    desc_usuario character varying,
    id_aplicacion integer NOT NULL
);


SET search_path = f5, pg_catalog;

--
-- TOC entry 256 (class 1259 OID 26480)
-- Name: usuarios; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW usuarios AS
 SELECT t_datos_personales.cedula,
    t_datos_personales.nombre_usuario,
    t_datos_personales.apellido_usuario,
    t_tipo_usuario.desc_usuario,
    t_aplicacion.nombre AS aplicacion,
    t_aplicacion.version,
    t_modulo.nombre AS modulo,
    ((((((((((t_datos_personales.cedula || ' '::text) || (t_datos_personales.nombre_usuario)::text) || ' '::text) || (t_datos_personales.apellido_usuario)::text) || ' '::text) || (t_tipo_usuario.desc_usuario)::text) || ' '::text) || (rrhh_gerencia.desc_gerencia)::text) || ' '::text) || (rrhh_division.desc_division)::text) AS busqueda,
    t_acceso.idgerencia AS id_gerencia,
    t_acceso.iddivision,
    t_datos_personales.correo,
    t_acceso.id_tipo_usuario,
    rrhh_gerencia.desc_gerencia AS de_gerencia,
    rrhh_division.desc_division AS de_division
   FROM ((((((usuario.t_acceso
     LEFT JOIN usuario.t_aplicacion ON ((t_aplicacion.id_aplicacion = t_acceso.id_aplicacion)))
     LEFT JOIN usuario.t_datos_personales ON (((t_datos_personales.cedula)::text = (t_acceso.cedula)::text)))
     LEFT JOIN usuario.t_tipo_usuario ON ((t_tipo_usuario.id_tipo_usuario = t_acceso.id_tipo_usuario)))
     LEFT JOIN usuario.t_modulo ON ((t_modulo.id_modulo = t_acceso.id_modulo)))
     LEFT JOIN rrhh_gerencia ON ((rrhh_gerencia.id_gerencia = t_acceso.idgerencia)))
     LEFT JOIN rrhh_division ON ((rrhh_division.id_division = t_acceso.iddivision)))
  WHERE ((t_acceso.fecha_exp = '2222-12-31'::date) AND (t_acceso.id_aplicacion = 12))
  ORDER BY t_datos_personales.cedula;


--
-- TOC entry 257 (class 1259 OID 26485)
-- Name: lista_pauta_gerencia; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW lista_pauta_gerencia AS
 SELECT a.id_pauta,
    a.tipo_pauta,
    a.nombre_programa,
    a.pauta_estatus,
    a.id_estatus,
    a.productor,
    a.busqueda,
    a.descripcion,
    c.id_gerencia,
    c.iddivision
   FROM ((sys_main a
     LEFT JOIN usuarios c ON ((c.cedula = (a.productor)::integer)))
     LEFT JOIN t_detalle_servicio b ON ((b.id_pauta = a.id_pauta)))
  WHERE (b.user_exp IS NULL)
  GROUP BY a.id_pauta, a.tipo_pauta, a.nombre_programa, a.pauta_estatus, a.id_estatus, a.productor, a.busqueda, a.descripcion, c.id_gerencia, c.iddivision
  ORDER BY a.id_pauta DESC;


--
-- TOC entry 312 (class 1259 OID 27264)
-- Name: lista_pauta_gerencia_ao_rep; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW lista_pauta_gerencia_ao_rep AS
 SELECT sys_main.id_pauta,
    sys_main.descripcion,
    sys_main.tipo_pauta,
    sys_main.nombre_programa,
    sys_main.pauta_estatus,
    sys_main.id_estatus,
    t_detalle_servicio.id_gerencia,
    sys_main.busqueda
   FROM (sys_main sys_main
     LEFT JOIN t_detalle_servicio USING (id_pauta))
  WHERE ((t_detalle_servicio.user_exp IS NULL) AND (sys_main.id_estatus = ANY (ARRAY[5, 6, 7, 8, 9, 10, 21, 22, 24, 25, 26, 29, 32])))
  GROUP BY sys_main.id_pauta, sys_main.descripcion, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, t_detalle_servicio.id_gerencia, sys_main.busqueda
  ORDER BY sys_main.id_pauta;


--
-- TOC entry 259 (class 1259 OID 26495)
-- Name: lista_pauta_gerencia_ao_sinasignacion; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW lista_pauta_gerencia_ao_sinasignacion AS
 SELECT sys_main.id_pauta,
    sys_main.descripcion,
    sys_main.tipo_pauta,
    sys_main.nombre_programa,
    sys_main.pauta_estatus,
    sys_main.id_estatus,
    t_detalle_servicio.id_gerencia,
    t_detalle_servicio.id_division,
    sys_main.busqueda
   FROM (sys_main sys_main
     LEFT JOIN t_detalle_servicio USING (id_pauta))
  WHERE (t_detalle_servicio.user_exp IS NULL)
  GROUP BY sys_main.id_pauta, sys_main.descripcion, sys_main.tipo_pauta, sys_main.nombre_programa, sys_main.pauta_estatus, sys_main.id_estatus, t_detalle_servicio.id_gerencia, t_detalle_servicio.id_division, sys_main.busqueda
  ORDER BY sys_main.id_pauta;


--
-- TOC entry 313 (class 1259 OID 27269)
-- Name: lista_pauta_gerencia_rep; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW lista_pauta_gerencia_rep AS
 SELECT a.id_pauta,
    a.tipo_pauta,
    a.nombre_programa,
    a.pauta_estatus,
    a.id_estatus,
    a.productor,
    a.busqueda,
    a.descripcion,
    c.id_gerencia,
    c.iddivision
   FROM ((sys_main a
     LEFT JOIN usuarios c ON ((c.cedula = (a.productor)::integer)))
     LEFT JOIN t_detalle_servicio b ON ((b.id_pauta = a.id_pauta)))
  WHERE ((b.user_exp IS NULL) AND (a.id_estatus = ANY (ARRAY[5, 6, 7, 8, 9, 10, 21, 22, 24, 25, 26, 29, 32])))
  GROUP BY a.id_pauta, a.tipo_pauta, a.nombre_programa, a.pauta_estatus, a.id_estatus, a.productor, a.busqueda, a.descripcion, c.id_gerencia, c.iddivision
  ORDER BY a.id_pauta DESC;


--
-- TOC entry 260 (class 1259 OID 26500)
-- Name: pautas_por_fechas; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW pautas_por_fechas AS
 SELECT t_estatus_pauta.id_estatus,
    t_estatus_pauta.fecha_reg,
    count(t_estatus_pauta.id_pauta) AS cantidad,
    t_estatus_pauta.id_pauta
   FROM t_estatus_pauta
  GROUP BY t_estatus_pauta.id_estatus, t_estatus_pauta.fecha_reg, t_estatus_pauta.id_pauta
  ORDER BY t_estatus_pauta.id_estatus;


--
-- TOC entry 261 (class 1259 OID 26504)
-- Name: programa; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW programa AS
 SELECT t_programa.descripcion,
    (((t_programa.id_program)::text || ' '::text) || (t_programa.descripcion)::text) AS busqueda,
    t_programa.id_program,
    t_programa.fecha_exp
   FROM t_programa
  WHERE (t_programa.fecha_exp = '2222-12-31'::date)
  ORDER BY t_programa.descripcion;


--
-- TOC entry 235 (class 1259 OID 26362)
-- Name: t_estatus_detalle_servicio; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_estatus_detalle_servicio (
    id_estatus_detalle_servicio integer NOT NULL,
    id_pauta integer,
    id_estatus integer,
    id_detalle_servicio integer,
    user_exp character varying,
    fecha_exp date DEFAULT '2222-12-31'::date,
    fecha_reg date DEFAULT now(),
    id_recurso_asignado integer
)
WITH (autovacuum_enabled=true);


--
-- TOC entry 2875 (class 0 OID 0)
-- Dependencies: 235
-- Name: TABLE t_estatus_detalle_servicio; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_estatus_detalle_servicio IS 'Tabla que almacena los historicos de los servicios relacionados a una pauta';


--
-- TOC entry 262 (class 1259 OID 26508)
-- Name: rec_por_estatus_cant; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW rec_por_estatus_cant AS
 SELECT t_estatus_detalle_servicio.id_pauta,
    t_detalle_servicio.id_recurso,
    t_detalle_servicio.id_tipo_recurso,
    count(t_detalle_servicio.id_recurso) AS cantidad,
    t_estatus_detalle_servicio.id_estatus
   FROM (t_estatus_detalle_servicio
     LEFT JOIN t_detalle_servicio ON ((t_estatus_detalle_servicio.id_detalle_servicio = t_detalle_servicio.id_detalle_servicio)))
  GROUP BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, t_estatus_detalle_servicio.id_estatus, t_detalle_servicio.id_tipo_recurso
  ORDER BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, t_estatus_detalle_servicio.id_estatus;


--
-- TOC entry 263 (class 1259 OID 26512)
-- Name: recur_asig; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW recur_asig AS
 SELECT t_detalle_servicio.id_pauta,
    t_detalle_servicio.id_detalle_servicio,
    t_detalle_servicio.id_recurso_asignado,
    t_detalle_servicio.fecha_reg,
    t_detalle_servicio.user_reg AS user_exp
   FROM t_detalle_servicio
  WHERE ((t_detalle_servicio.id_recurso_asignado <> 1) AND (t_detalle_servicio.fecha_exp = '2222-12-31'::date))
  GROUP BY t_detalle_servicio.id_pauta, t_detalle_servicio.id_detalle_servicio, t_detalle_servicio.id_recurso_asignado, t_detalle_servicio.fecha_reg, t_detalle_servicio.user_reg
  ORDER BY t_detalle_servicio.id_pauta, t_detalle_servicio.id_detalle_servicio, t_detalle_servicio.id_recurso_asignado, t_detalle_servicio.fecha_reg, t_detalle_servicio.user_reg;


--
-- TOC entry 264 (class 1259 OID 26516)
-- Name: recur_elim_ual; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW recur_elim_ual AS
 SELECT t_estatus_detalle_servicio.id_pauta,
    t_detalle_servicio.id_recurso,
    count(t_detalle_servicio.id_recurso) AS cantidad,
    t_detalle_servicio.id_tipo_recurso
   FROM (t_estatus_detalle_servicio
     LEFT JOIN t_detalle_servicio ON ((t_estatus_detalle_servicio.id_detalle_servicio = t_detalle_servicio.id_detalle_servicio)))
  WHERE (t_estatus_detalle_servicio.id_estatus = 16)
  GROUP BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, t_detalle_servicio.id_tipo_recurso
  ORDER BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso;


--
-- TOC entry 265 (class 1259 OID 26520)
-- Name: recur_sin_asig; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW recur_sin_asig AS
 SELECT t_detalle_servicio.id_pauta,
    t_detalle_servicio.id_recurso,
    t_detalle_servicio.id_tipo_recurso,
    count(t_detalle_servicio.id_recurso) AS cantidad
   FROM t_detalle_servicio
  WHERE ((t_detalle_servicio.id_recurso_asignado = 1) AND (t_detalle_servicio.fecha_exp = '2222-12-31'::date))
  GROUP BY t_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso, t_detalle_servicio.id_tipo_recurso
  ORDER BY t_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso;


--
-- TOC entry 266 (class 1259 OID 26524)
-- Name: recur_val_ger; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW recur_val_ger AS
 SELECT t_estatus_detalle_servicio.id_pauta,
    t_detalle_servicio.id_recurso,
    count(t_detalle_servicio.id_recurso) AS cantidad
   FROM (t_estatus_detalle_servicio
     LEFT JOIN t_detalle_servicio ON ((t_estatus_detalle_servicio.id_detalle_servicio = t_detalle_servicio.id_detalle_servicio)))
  WHERE ((t_estatus_detalle_servicio.id_estatus = ANY (ARRAY[13, 20])) OR (t_estatus_detalle_servicio.id_estatus = 13))
  GROUP BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso
  ORDER BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso;


--
-- TOC entry 267 (class 1259 OID 26528)
-- Name: recur_val_ual; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW recur_val_ual AS
 SELECT t_estatus_detalle_servicio.id_pauta,
    t_detalle_servicio.id_recurso,
    count(t_detalle_servicio.id_recurso) AS cantidad
   FROM (t_estatus_detalle_servicio
     LEFT JOIN t_detalle_servicio ON ((t_estatus_detalle_servicio.id_detalle_servicio = t_detalle_servicio.id_detalle_servicio)))
  WHERE (t_estatus_detalle_servicio.id_estatus = 14)
  GROUP BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso
  ORDER BY t_estatus_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso;


--
-- TOC entry 268 (class 1259 OID 26532)
-- Name: rrhh_asignados_pauta; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW rrhh_asignados_pauta AS
 SELECT t_detalle_servicio.id_detalle_servicio AS serial,
    t_detalle_servicio.id_pauta,
    t_detalle_servicio.fecha_reg,
    t_detalle_servicio.user_reg,
    t_detalle_servicio.fecha_exp,
    t_detalle_servicio.user_exp,
    t_detalle_servicio.id_recurso,
    t_detalle_servicio.id_recurso_asignado,
    t_detalle_servicio.id_tipo_recurso
   FROM t_detalle_servicio
  WHERE (((t_detalle_servicio.id_recurso_asignado <> 1) AND (t_detalle_servicio.id_tipo_recurso = 2)) AND (t_detalle_servicio.fecha_exp = '2222-12-31'::date));


--
-- TOC entry 269 (class 1259 OID 26536)
-- Name: rrhh_categoria_id_categoria_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE rrhh_categoria_id_categoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2876 (class 0 OID 0)
-- Dependencies: 269
-- Name: rrhh_categoria_id_categoria_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE rrhh_categoria_id_categoria_seq OWNED BY rrhh_categoria.id_categoria;


--
-- TOC entry 270 (class 1259 OID 26538)
-- Name: rrhh_personal; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE rrhh_personal (
    cedula integer NOT NULL,
    nombre character varying,
    apellido character varying,
    id_gerencia integer,
    id_division integer,
    id_cargo integer
);


--
-- TOC entry 271 (class 1259 OID 26544)
-- Name: rrhh_modulogeneral; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW rrhh_modulogeneral AS
 SELECT rrhh_personal.cedula,
    rrhh_personal.id_gerencia,
    rrhh_personal.id_division,
    rrhh_personal.id_cargo,
    rrhh_rel_categoria_cargo.id_categoria,
    rrhh_personal.nombre,
    rrhh_personal.apellido,
    rrhh_gerencia.desc_gerencia,
    rrhh_division.desc_division,
    rrhh_cargos.de_cargo,
    rrhh_categoria.desc_categoria,
    (((((((((rrhh_personal.nombre)::text || ' '::text) || (rrhh_personal.apellido)::text) || ' '::text) || (rrhh_gerencia.desc_gerencia)::text) || ' '::text) || (rrhh_division.desc_division)::text) || ' '::text) || (rrhh_cargos.de_cargo)::text) AS busqueda
   FROM (((((rrhh_personal
     JOIN rrhh_gerencia USING (id_gerencia))
     JOIN rrhh_cargos USING (id_cargo, fecha_expiracion))
     JOIN rrhh_division USING (fecha_expiracion, id_gerencia, id_division))
     LEFT JOIN rrhh_rel_categoria_cargo USING (id_cargo))
     LEFT JOIN rrhh_categoria USING (fecha_expiracion, id_categoria));


--
-- TOC entry 272 (class 1259 OID 26549)
-- Name: selectlistmat_group; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW selectlistmat_group AS
 SELECT concatena((t_detalle_servicio.id_detalle_servicio)::text) AS ids_detalles_servicios,
    t_detalle_servicio.id_pauta,
    t_detalle_servicio.id_recurso,
    count(t_detalle_servicio.id_detalle_servicio) AS cantidad
   FROM t_detalle_servicio
  WHERE ((t_detalle_servicio.id_recurso_asignado = 1) AND (t_detalle_servicio.fecha_exp = '2222-12-31'::date))
  GROUP BY t_detalle_servicio.id_pauta, t_detalle_servicio.id_recurso;


--
-- TOC entry 258 (class 1259 OID 26490)
-- Name: sys_main_old; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW sys_main_old AS
 SELECT a.id_pauta,
    a.user_reg AS productor,
    k.descripcion AS tipo_pauta,
    m.descripcion AS tipo_evento,
    d.descripcion AS nombre_programa,
    x.descripcion AS pauta_locacion,
    j.descripcion AS pauta_traje,
    n0.descripcion AS citacion_lugar,
    g.fecha AS fecha_citacion,
    g.hora AS hora_citacion,
    h.fecha AS fecha_retorno,
    h.hora AS hora_retorno,
    i.fecha AS fecha_montaje,
    i.hora AS hora_montaje,
    c.fecha AS fecha_emision,
    c.hora AS hora_emision,
    f.descripcion AS pauta_estatus,
        CASE
            WHEN ((k.descripcion)::text = 'remoto'::text) THEN ((((((((((((((((((((((((((((((((a.id_pauta || ' | '::text) || (a.user_reg)::text) || ' | '::text) || (k.descripcion)::text) || ' | '::text) || (m.descripcion)::text) || ' | '::text) || (d.descripcion)::text) || ' | '::text) || (x.descripcion)::text) || ' | '::text) || (j.descripcion)::text) || ' | '::text) || (n0.descripcion)::text) || ' | '::text) || g.fecha) || ' | '::text) || g.hora) || ' | '::text) || h.fecha) || ' | '::text) || h.hora) || ' | '::text) || i.fecha) || ' | '::text) || i.hora) || ' | '::text) || c.fecha) || ' | '::text) || c.hora) || ' | '::text) || (f.descripcion)::text)
            ELSE ((((((((((((((((((((a.id_pauta || ' | '::text) || (a.user_reg)::text) || ' | '::text) || (k.descripcion)::text) || ' | '::text) || (m.descripcion)::text) || ' | '::text) || (d.descripcion)::text) || ' | '::text) || (x.descripcion)::text) || ' | '::text) || (j.descripcion)::text) || ' | '::text) || (n0.descripcion)::text) || ' | '::text) || g.fecha) || ' | '::text) || g.hora) || ' | '::text) || (f.descripcion)::text)
        END AS busqueda,
    f.id_estatus
   FROM (((((((((((((t_pauta a
     LEFT JOIN t_estatus_pauta b USING (id_pauta))
     LEFT JOIN t_emision_grabacion c USING (id_emision_grabacion))
     LEFT JOIN t_programa d USING (id_program))
     LEFT JOIN t_estatus f USING (id_estatus))
     LEFT JOIN t_citacion g USING (id_citacion))
     LEFT JOIN t_retorno h USING (id_retorno))
     LEFT JOIN t_montaje i USING (id_montaje))
     LEFT JOIN t_tipo_traje j USING (id_tipo_traje))
     LEFT JOIN t_tipo_pauta k USING (id_tipo_pauta))
     LEFT JOIN t_tipo_evento m USING (id_tipo_evento))
     LEFT JOIN t_tipo_estatus l USING (id_tipo_estatus))
     LEFT JOIN t_locacion x USING (id_locacion))
     LEFT JOIN t_lugar n0 ON ((n0.id_lugar = g.id_lugar)))
  WHERE (b.fecha_exp = '2222-12-31'::date)
  ORDER BY a.id_pauta DESC;


--
-- TOC entry 273 (class 1259 OID 26553)
-- Name: t_citacion_id_citacion_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_citacion_id_citacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2877 (class 0 OID 0)
-- Dependencies: 273
-- Name: t_citacion_id_citacion_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_citacion_id_citacion_seq OWNED BY t_citacion.id_citacion;


--
-- TOC entry 274 (class 1259 OID 26555)
-- Name: t_correo; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_correo (
    id_gerencia integer,
    id_division integer DEFAULT 0,
    correo character varying,
    user_reg character varying,
    fecha_reg date DEFAULT now(),
    fecha_exp date DEFAULT '2222-12-31'::date,
    id_correo integer NOT NULL
);


--
-- TOC entry 2878 (class 0 OID 0)
-- Dependencies: 274
-- Name: TABLE t_correo; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_correo IS 'Tabla que almacena los correos electronicos del personal involucrado en una pauta';


--
-- TOC entry 2879 (class 0 OID 0)
-- Dependencies: 274
-- Name: COLUMN t_correo.id_gerencia; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_correo.id_gerencia IS 'Id de la gerencia a la que petenece la cuenta de correo registrada';


--
-- TOC entry 2880 (class 0 OID 0)
-- Dependencies: 274
-- Name: COLUMN t_correo.id_division; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_correo.id_division IS 'Id de la division a la que petenece la cuenta de correo registrada';


--
-- TOC entry 2881 (class 0 OID 0)
-- Dependencies: 274
-- Name: COLUMN t_correo.correo; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_correo.correo IS 'cuenta de correo';


--
-- TOC entry 2882 (class 0 OID 0)
-- Dependencies: 274
-- Name: COLUMN t_correo.user_reg; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_correo.user_reg IS 'Usuario que registro la cuenta de correo en el sistema';


--
-- TOC entry 2883 (class 0 OID 0)
-- Dependencies: 274
-- Name: COLUMN t_correo.fecha_reg; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_correo.fecha_reg IS 'Fecha en la que se registro la cuenta de correo en el sistema';


--
-- TOC entry 2884 (class 0 OID 0)
-- Dependencies: 274
-- Name: COLUMN t_correo.fecha_exp; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_correo.fecha_exp IS 'Fecha en la que se realizo el eliminado logico de la cuenta de correo del sistema';


--
-- TOC entry 2885 (class 0 OID 0)
-- Dependencies: 274
-- Name: COLUMN t_correo.id_correo; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_correo.id_correo IS 'Campo identificativo de la tabla correo';


--
-- TOC entry 275 (class 1259 OID 26564)
-- Name: t_correo_id_correo_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_correo_id_correo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2886 (class 0 OID 0)
-- Dependencies: 275
-- Name: t_correo_id_correo_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_correo_id_correo_seq OWNED BY t_correo.id_correo;


--
-- TOC entry 276 (class 1259 OID 26566)
-- Name: t_datos_persona; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_datos_persona (
    id_datos_persona integer NOT NULL,
    id_persona integer,
    telefono1 character varying,
    telefono2 character varying,
    correo character varying
);


--
-- TOC entry 2887 (class 0 OID 0)
-- Dependencies: 276
-- Name: TABLE t_datos_persona; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_datos_persona IS 'Tabla donde se almacenan los datos de contacto de las personas relacionadas a la pauta. ';


--
-- TOC entry 2888 (class 0 OID 0)
-- Dependencies: 276
-- Name: COLUMN t_datos_persona.id_datos_persona; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_datos_persona.id_datos_persona IS 'Campo identificativo de la tabla';


--
-- TOC entry 2889 (class 0 OID 0)
-- Dependencies: 276
-- Name: COLUMN t_datos_persona.id_persona; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_datos_persona.id_persona IS 'Este es el id_persona proveniente de la tabla grlpersona del esquema sigai';


--
-- TOC entry 2890 (class 0 OID 0)
-- Dependencies: 276
-- Name: COLUMN t_datos_persona.telefono1; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_datos_persona.telefono1 IS 'Numero de telefono de la persona';


--
-- TOC entry 2891 (class 0 OID 0)
-- Dependencies: 276
-- Name: COLUMN t_datos_persona.telefono2; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_datos_persona.telefono2 IS 'Numero de telefono de la persona';


--
-- TOC entry 2892 (class 0 OID 0)
-- Dependencies: 276
-- Name: COLUMN t_datos_persona.correo; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_datos_persona.correo IS 'Direccion electronica de la persona';


--
-- TOC entry 277 (class 1259 OID 26572)
-- Name: t_datos_persona_id_datos_persona_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_datos_persona_id_datos_persona_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2893 (class 0 OID 0)
-- Dependencies: 277
-- Name: t_datos_persona_id_datos_persona_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_datos_persona_id_datos_persona_seq OWNED BY t_datos_persona.id_datos_persona;


--
-- TOC entry 278 (class 1259 OID 26574)
-- Name: t_detalle_servicio_id_detalle_servicio_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_detalle_servicio_id_detalle_servicio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2894 (class 0 OID 0)
-- Dependencies: 278
-- Name: t_detalle_servicio_id_detalle_servicio_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_detalle_servicio_id_detalle_servicio_seq OWNED BY t_detalle_servicio.id_detalle_servicio;


--
-- TOC entry 279 (class 1259 OID 26576)
-- Name: t_emision_grabacion_id_emision_grabacion_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_emision_grabacion_id_emision_grabacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2895 (class 0 OID 0)
-- Dependencies: 279
-- Name: t_emision_grabacion_id_emision_grabacion_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_emision_grabacion_id_emision_grabacion_seq OWNED BY t_emision_grabacion.id_emision_grabacion;


--
-- TOC entry 280 (class 1259 OID 26578)
-- Name: t_estado_informe; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_estado_informe (
    id_estado_informe integer NOT NULL,
    descripcion character varying,
    clasificacion character varying
);


--
-- TOC entry 2896 (class 0 OID 0)
-- Dependencies: 280
-- Name: TABLE t_estado_informe; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_estado_informe IS 'Tabla que almacena los estados en los que se puede evaluar el informe en la tabla informe';


--
-- TOC entry 281 (class 1259 OID 26584)
-- Name: t_estado_informe_id_estado_informe_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_estado_informe_id_estado_informe_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2897 (class 0 OID 0)
-- Dependencies: 281
-- Name: t_estado_informe_id_estado_informe_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_estado_informe_id_estado_informe_seq OWNED BY t_estado_informe.id_estado_informe;


--
-- TOC entry 282 (class 1259 OID 26586)
-- Name: t_estatus_detalle_servicio_id_estatus_detalle_servicio_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_estatus_detalle_servicio_id_estatus_detalle_servicio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2898 (class 0 OID 0)
-- Dependencies: 282
-- Name: t_estatus_detalle_servicio_id_estatus_detalle_servicio_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_estatus_detalle_servicio_id_estatus_detalle_servicio_seq OWNED BY t_estatus_detalle_servicio.id_estatus_detalle_servicio;


--
-- TOC entry 283 (class 1259 OID 26588)
-- Name: t_estatus_id_estatus_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_estatus_id_estatus_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2899 (class 0 OID 0)
-- Dependencies: 283
-- Name: t_estatus_id_estatus_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_estatus_id_estatus_seq OWNED BY t_estatus.id_estatus;


--
-- TOC entry 284 (class 1259 OID 26590)
-- Name: t_estatus_pauta_id_estatus_pauta_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_estatus_pauta_id_estatus_pauta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2900 (class 0 OID 0)
-- Dependencies: 284
-- Name: t_estatus_pauta_id_estatus_pauta_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_estatus_pauta_id_estatus_pauta_seq OWNED BY t_estatus_pauta.id_estatus_pauta;


--
-- TOC entry 285 (class 1259 OID 26592)
-- Name: t_informe; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_informe (
    id_informe integer NOT NULL,
    id_pauta integer,
    id_detalle_servicio integer,
    observaciones character varying,
    user_reg character varying,
    user_exp character varying,
    fecha_reg date,
    fecha_exp date,
    id_estado_informe integer
)
WITH (autovacuum_enabled=true);


--
-- TOC entry 2901 (class 0 OID 0)
-- Dependencies: 285
-- Name: TABLE t_informe; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_informe IS 'Tabla para almacenar los comentario y/o evaluacion de un servicio de una pauta';


--
-- TOC entry 286 (class 1259 OID 26598)
-- Name: t_informe_id_informe_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_informe_id_informe_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2902 (class 0 OID 0)
-- Dependencies: 286
-- Name: t_informe_id_informe_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_informe_id_informe_seq OWNED BY t_informe.id_informe;


--
-- TOC entry 287 (class 1259 OID 26600)
-- Name: t_locacion_id_locacion_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_locacion_id_locacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2903 (class 0 OID 0)
-- Dependencies: 287
-- Name: t_locacion_id_locacion_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_locacion_id_locacion_seq OWNED BY t_locacion.id_locacion;


--
-- TOC entry 288 (class 1259 OID 26602)
-- Name: t_lugar_id_lugar_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_lugar_id_lugar_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2904 (class 0 OID 0)
-- Dependencies: 288
-- Name: t_lugar_id_lugar_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_lugar_id_lugar_seq OWNED BY t_lugar.id_lugar;


--
-- TOC entry 289 (class 1259 OID 26604)
-- Name: t_montaje_id_montaje_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_montaje_id_montaje_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2905 (class 0 OID 0)
-- Dependencies: 289
-- Name: t_montaje_id_montaje_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_montaje_id_montaje_seq OWNED BY t_montaje.id_montaje;


--
-- TOC entry 290 (class 1259 OID 26606)
-- Name: t_pauta_id_pauta_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_pauta_id_pauta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2906 (class 0 OID 0)
-- Dependencies: 290
-- Name: t_pauta_id_pauta_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_pauta_id_pauta_seq OWNED BY t_pauta.id_pauta;


--
-- TOC entry 291 (class 1259 OID 26608)
-- Name: t_productor; Type: TABLE; Schema: f5; Owner: -; Tablespace: 
--

CREATE TABLE t_productor (
    id_productor integer NOT NULL,
    id_persona integer,
    fecha_reg date DEFAULT now(),
    user_reg character varying,
    fecha_exp date,
    user_exp character varying
);


--
-- TOC entry 2907 (class 0 OID 0)
-- Dependencies: 291
-- Name: TABLE t_productor; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON TABLE t_productor IS 'Tabla donde se almacenan los productores de las pautas';


--
-- TOC entry 2908 (class 0 OID 0)
-- Dependencies: 291
-- Name: COLUMN t_productor.id_productor; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_productor.id_productor IS 'Campo identificativo de la tabla productor';


--
-- TOC entry 2909 (class 0 OID 0)
-- Dependencies: 291
-- Name: COLUMN t_productor.id_persona; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_productor.id_persona IS 'id  de la persona que va hacer registrada como productor, este id_persona proviene de la tabla rglpersona de sigai';


--
-- TOC entry 2910 (class 0 OID 0)
-- Dependencies: 291
-- Name: COLUMN t_productor.fecha_reg; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_productor.fecha_reg IS 'Fecha en la que se creo el registro';


--
-- TOC entry 2911 (class 0 OID 0)
-- Dependencies: 291
-- Name: COLUMN t_productor.user_reg; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_productor.user_reg IS 'Usuario que creo el registro en la tabla';


--
-- TOC entry 2912 (class 0 OID 0)
-- Dependencies: 291
-- Name: COLUMN t_productor.fecha_exp; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_productor.fecha_exp IS 'Fecha en la que se realizo el eliminado logico del registro';


--
-- TOC entry 2913 (class 0 OID 0)
-- Dependencies: 291
-- Name: COLUMN t_productor.user_exp; Type: COMMENT; Schema: f5; Owner: -
--

COMMENT ON COLUMN t_productor.user_exp IS 'Usuario que realizo el eliminado logico del registro';


--
-- TOC entry 292 (class 1259 OID 26615)
-- Name: t_productor_id_productor_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_productor_id_productor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2914 (class 0 OID 0)
-- Dependencies: 292
-- Name: t_productor_id_productor_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_productor_id_productor_seq OWNED BY t_productor.id_productor;


--
-- TOC entry 293 (class 1259 OID 26617)
-- Name: t_programa_id_program_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_programa_id_program_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2915 (class 0 OID 0)
-- Dependencies: 293
-- Name: t_programa_id_program_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_programa_id_program_seq OWNED BY t_programa.id_program;


--
-- TOC entry 294 (class 1259 OID 26619)
-- Name: t_retorno_id_retorno_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_retorno_id_retorno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2916 (class 0 OID 0)
-- Dependencies: 294
-- Name: t_retorno_id_retorno_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_retorno_id_retorno_seq OWNED BY t_retorno.id_retorno;


--
-- TOC entry 295 (class 1259 OID 26621)
-- Name: t_tipo_estatus_id_tipo_estatus_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_tipo_estatus_id_tipo_estatus_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2917 (class 0 OID 0)
-- Dependencies: 295
-- Name: t_tipo_estatus_id_tipo_estatus_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_tipo_estatus_id_tipo_estatus_seq OWNED BY t_tipo_estatus.id_tipo_estatus;


--
-- TOC entry 296 (class 1259 OID 26623)
-- Name: t_tipo_evento_id_tipo_evento_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_tipo_evento_id_tipo_evento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2918 (class 0 OID 0)
-- Dependencies: 296
-- Name: t_tipo_evento_id_tipo_evento_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_tipo_evento_id_tipo_evento_seq OWNED BY t_tipo_evento.id_tipo_evento;


--
-- TOC entry 297 (class 1259 OID 26625)
-- Name: t_tipo_pauta_id_tipo_pauta_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_tipo_pauta_id_tipo_pauta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2919 (class 0 OID 0)
-- Dependencies: 297
-- Name: t_tipo_pauta_id_tipo_pauta_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_tipo_pauta_id_tipo_pauta_seq OWNED BY t_tipo_pauta.id_tipo_pauta;


--
-- TOC entry 298 (class 1259 OID 26627)
-- Name: t_tipo_traje_id_tipo_traje_seq; Type: SEQUENCE; Schema: f5; Owner: -
--

CREATE SEQUENCE t_tipo_traje_id_tipo_traje_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2920 (class 0 OID 0)
-- Dependencies: 298
-- Name: t_tipo_traje_id_tipo_traje_seq; Type: SEQUENCE OWNED BY; Schema: f5; Owner: -
--

ALTER SEQUENCE t_tipo_traje_id_tipo_traje_seq OWNED BY t_tipo_traje.id_tipo_traje;


--
-- TOC entry 299 (class 1259 OID 26629)
-- Name: trajes; Type: VIEW; Schema: f5; Owner: -
--

CREATE VIEW trajes AS
 SELECT t_tipo_traje.descripcion,
    (((t_tipo_traje.id_tipo_traje)::text || ' '::text) || (t_tipo_traje.descripcion)::text) AS busqueda,
    t_tipo_traje.id_tipo_traje,
    t_tipo_traje.fecha_exp
   FROM t_tipo_traje
  WHERE (t_tipo_traje.fecha_exp = '2222-12-31'::date)
  ORDER BY t_tipo_traje.descripcion;


SET search_path = usuario, pg_catalog;

SET default_with_oids = true;

--
-- TOC entry 300 (class 1259 OID 26633)
-- Name: t_submodulo; Type: TABLE; Schema: usuario; Owner: -; Tablespace: 
--

CREATE TABLE t_submodulo (
    id_submodulo integer NOT NULL,
    id_modulo integer NOT NULL,
    nombre character varying(100) NOT NULL,
    fecha_trans date NOT NULL,
    fecha_exp date DEFAULT '2222-12-31'::date
);


--
-- TOC entry 301 (class 1259 OID 26637)
-- Name: main_modulosys; Type: VIEW; Schema: usuario; Owner: -
--

CREATE VIEW main_modulosys AS
 SELECT a.id_aplicacion,
    b.id_modulo,
    c.id_submodulo,
    ((((a.nombre)::text || ' ('::text) || (a.version)::text) || ')'::text) AS aplicacion,
    b.nombre AS modulo,
    c.nombre AS sub_modulo
   FROM ((t_modulo b
     LEFT JOIN t_aplicacion a ON ((b.id_aplicacion = a.id_aplicacion)))
     LEFT JOIN t_submodulo c ON ((c.id_modulo = b.id_modulo)));


--
-- TOC entry 302 (class 1259 OID 26641)
-- Name: main_usuariossys; Type: VIEW; Schema: usuario; Owner: -
--

CREATE VIEW main_usuariossys AS
 SELECT t_acceso.cedula AS id,
    t_acceso.id_modulo,
    t_acceso.id_aplicacion,
    t_acceso.niv_con,
    t_datos_personales.nombre_usuario,
    t_datos_personales.apellido_usuario,
    ((((t_aplicacion.nombre)::text || '('::text) || (t_aplicacion.version)::text) || ')'::text) AS aplicacion,
    t_modulo.nombre AS modulo,
    t_tipo_usuario.desc_usuario AS perfil,
    t_acceso.fecha_exp
   FROM t_acceso,
    t_aplicacion,
    t_modulo,
    t_datos_personales,
    t_tipo_usuario
  WHERE (((((t_acceso.fecha_exp = '2222-12-31'::date) AND (t_acceso.id_aplicacion = t_aplicacion.id_aplicacion)) AND (t_acceso.id_modulo = t_modulo.id_modulo)) AND ((t_acceso.cedula)::text = (t_datos_personales.cedula)::text)) AND (t_acceso.niv_con = t_tipo_usuario.id_tipo_usuario))
  ORDER BY t_aplicacion.nombre;


--
-- TOC entry 303 (class 1259 OID 26646)
-- Name: t_acceso_id_acceso_seq; Type: SEQUENCE; Schema: usuario; Owner: -
--

CREATE SEQUENCE t_acceso_id_acceso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2921 (class 0 OID 0)
-- Dependencies: 303
-- Name: t_acceso_id_acceso_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: -
--

ALTER SEQUENCE t_acceso_id_acceso_seq OWNED BY t_acceso.id_acceso;


--
-- TOC entry 304 (class 1259 OID 26648)
-- Name: t_aplicacion_id_aplicacion_seq; Type: SEQUENCE; Schema: usuario; Owner: -
--

CREATE SEQUENCE t_aplicacion_id_aplicacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2922 (class 0 OID 0)
-- Dependencies: 304
-- Name: t_aplicacion_id_aplicacion_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: -
--

ALTER SEQUENCE t_aplicacion_id_aplicacion_seq OWNED BY t_aplicacion.id_aplicacion;


--
-- TOC entry 305 (class 1259 OID 26650)
-- Name: t_datos_personales_id_datos_personales_seq; Type: SEQUENCE; Schema: usuario; Owner: -
--

CREATE SEQUENCE t_datos_personales_id_datos_personales_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2923 (class 0 OID 0)
-- Dependencies: 305
-- Name: t_datos_personales_id_datos_personales_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: -
--

ALTER SEQUENCE t_datos_personales_id_datos_personales_seq OWNED BY t_datos_personales.id_datos_personales;


SET default_with_oids = false;

--
-- TOC entry 306 (class 1259 OID 26652)
-- Name: t_firma; Type: TABLE; Schema: usuario; Owner: -; Tablespace: 
--

CREATE TABLE t_firma (
    id_firma integer NOT NULL,
    cedula integer,
    firma bit(1),
    id_aplicacion integer,
    fecha_reg date DEFAULT now(),
    fecha_exp date DEFAULT '2222-12-31'::date
);


--
-- TOC entry 307 (class 1259 OID 26657)
-- Name: t_modulo_id_modulo_seq; Type: SEQUENCE; Schema: usuario; Owner: -
--

CREATE SEQUENCE t_modulo_id_modulo_seq
    START WITH 100
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2924 (class 0 OID 0)
-- Dependencies: 307
-- Name: t_modulo_id_modulo_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: -
--

ALTER SEQUENCE t_modulo_id_modulo_seq OWNED BY t_modulo.id_modulo;


--
-- TOC entry 308 (class 1259 OID 26659)
-- Name: t_submodulo_id_submodulo_seq; Type: SEQUENCE; Schema: usuario; Owner: -
--

CREATE SEQUENCE t_submodulo_id_submodulo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2925 (class 0 OID 0)
-- Dependencies: 308
-- Name: t_submodulo_id_submodulo_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: -
--

ALTER SEQUENCE t_submodulo_id_submodulo_seq OWNED BY t_submodulo.id_submodulo;


--
-- TOC entry 309 (class 1259 OID 26661)
-- Name: t_tipo_usuario_id_usuario_seq; Type: SEQUENCE; Schema: usuario; Owner: -
--

CREATE SEQUENCE t_tipo_usuario_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2926 (class 0 OID 0)
-- Dependencies: 309
-- Name: t_tipo_usuario_id_usuario_seq; Type: SEQUENCE OWNED BY; Schema: usuario; Owner: -
--

ALTER SEQUENCE t_tipo_usuario_id_usuario_seq OWNED BY t_tipo_usuario.id_tipo_usuario;


--
-- TOC entry 310 (class 1259 OID 26663)
-- Name: usuarios_almacen; Type: VIEW; Schema: usuario; Owner: -
--

CREATE VIEW usuarios_almacen AS
 SELECT a.id_aplicacion,
    b.cedula,
    b.nombre_usuario,
    b.apellido_usuario,
    a.id_tipo_usuario,
    c.desc_usuario,
    b.correo
   FROM ((t_datos_personales b
     LEFT JOIN t_acceso a ON (((b.cedula)::text = (a.cedula)::text)))
     LEFT JOIN t_tipo_usuario c ON (((a.id_tipo_usuario)::text = (c.id_tipo_usuario)::text)))
  WHERE ((a.id_aplicacion = 15) AND (a.fecha_exp = '2222-12-31'::date));


--
-- TOC entry 311 (class 1259 OID 26668)
-- Name: usuarios_almacen_tecnico; Type: VIEW; Schema: usuario; Owner: -
--

CREATE VIEW usuarios_almacen_tecnico AS
 SELECT a.id_aplicacion,
    b.cedula,
    b.nombre_usuario,
    b.apellido_usuario,
    a.id_tipo_usuario,
    c.desc_usuario,
    b.correo,
    (((((((((b.cedula)::text || ' '::text) || (b.nombre_usuario)::text) || ' '::text) || (b.apellido_usuario)::text) || ' '::text) || (c.desc_usuario)::text) || ' '::text) || (b.correo)::text) AS busqueda
   FROM ((t_datos_personales b
     LEFT JOIN t_acceso a ON (((b.cedula)::text = (a.cedula)::text)))
     LEFT JOIN t_tipo_usuario c ON (((a.id_tipo_usuario)::text = (c.id_tipo_usuario)::text)))
  WHERE ((a.id_aplicacion = 15) AND (a.fecha_exp = '2222-12-31'::date))
  ORDER BY b.nombre_usuario;


SET search_path = almacen_tecnico, pg_catalog;

--
-- TOC entry 2415 (class 2604 OID 26688)
-- Name: id_articulo; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_articulo ALTER COLUMN id_articulo SET DEFAULT nextval('t_articulo_id_articulo_seq'::regclass);


--
-- TOC entry 2432 (class 2604 OID 26689)
-- Name: id_camara_sol; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_camara_sol ALTER COLUMN id_camara_sol SET DEFAULT nextval('t_camara_sol_id_camara_sol_seq'::regclass);


--
-- TOC entry 2417 (class 2604 OID 26690)
-- Name: id_categoria; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_categoria ALTER COLUMN id_categoria SET DEFAULT nextval('t_categoria_id_catgoria_seq'::regclass);


--
-- TOC entry 2419 (class 2604 OID 26691)
-- Name: id_desc_estado; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_des_estado ALTER COLUMN id_desc_estado SET DEFAULT nextval('t_des_estado_id_desc_estado_seq'::regclass);


--
-- TOC entry 2426 (class 2604 OID 26692)
-- Name: id_descripcion; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_descripciones ALTER COLUMN id_descripcion SET DEFAULT nextval('t_descripciones_id_descripcion_seq'::regclass);


--
-- TOC entry 2427 (class 2604 OID 26693)
-- Name: id_despacho; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_despacho ALTER COLUMN id_despacho SET DEFAULT nextval('t_despacho_id_despacho_seq'::regclass);


--
-- TOC entry 2428 (class 2604 OID 26694)
-- Name: id_entrada; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_entrada ALTER COLUMN id_entrada SET DEFAULT nextval('t_entrada_id_entrada_seq'::regclass);


--
-- TOC entry 2420 (class 2604 OID 26695)
-- Name: id_estatus; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_estatus ALTER COLUMN id_estatus SET DEFAULT nextval('t_estatus_id_estatus_seq'::regclass);


--
-- TOC entry 2434 (class 2604 OID 26696)
-- Name: id_grupo_sol; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_grupo_sol ALTER COLUMN id_grupo_sol SET DEFAULT nextval('t_grupo_sol_id_grupo_sol_seq'::regclass);


--
-- TOC entry 2422 (class 2604 OID 26697)
-- Name: id_del_grupo; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_grupos ALTER COLUMN id_del_grupo SET DEFAULT nextval('t_grupos_id_grupo_seq'::regclass);


--
-- TOC entry 2435 (class 2604 OID 26698)
-- Name: id_serial_incidencia; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_incidencia ALTER COLUMN id_serial_incidencia SET DEFAULT nextval('t_incidencia_id_serial_incidencia_seq'::regclass);


--
-- TOC entry 2436 (class 2604 OID 26699)
-- Name: id_ingresos; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_ingresos ALTER COLUMN id_ingresos SET DEFAULT nextval('t_ingresos_id_ingresos_seq'::regclass);


--
-- TOC entry 2438 (class 2604 OID 26700)
-- Name: id_material_desp; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_material_desp ALTER COLUMN id_material_desp SET DEFAULT nextval('t_material_desp_id_material_desp_seq'::regclass);


--
-- TOC entry 2440 (class 2604 OID 26701)
-- Name: id_material_sol; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_material_sol ALTER COLUMN id_material_sol SET DEFAULT nextval('t_material_sol_id_material_sol_seq'::regclass);


--
-- TOC entry 2430 (class 2604 OID 26702)
-- Name: id_prestamo; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_prestamo ALTER COLUMN id_prestamo SET DEFAULT nextval('t_prestamo_id_prestamo_seq'::regclass);


--
-- TOC entry 2442 (class 2604 OID 26703)
-- Name: id_programa; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_programa ALTER COLUMN id_programa SET DEFAULT nextval('t_programa_id_programa_seq'::regclass);


--
-- TOC entry 2444 (class 2604 OID 26704)
-- Name: id_solicitud; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_solicitudes_f5 ALTER COLUMN id_solicitud SET DEFAULT nextval('t_solicitudes_f5_id_solicitud_seq'::regclass);


--
-- TOC entry 2424 (class 2604 OID 26705)
-- Name: id_unidad_medida; Type: DEFAULT; Schema: almacen_tecnico; Owner: -
--

ALTER TABLE ONLY t_unidad_medida ALTER COLUMN id_unidad_medida SET DEFAULT nextval('t_unidad_medida_id_unidad_medida_seq'::regclass);


SET search_path = f5, pg_catalog;

--
-- TOC entry 2459 (class 2604 OID 26706)
-- Name: id_categoria; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY rrhh_categoria ALTER COLUMN id_categoria SET DEFAULT nextval('rrhh_categoria_id_categoria_seq'::regclass);


--
-- TOC entry 2463 (class 2604 OID 26707)
-- Name: id_citacion; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_citacion ALTER COLUMN id_citacion SET DEFAULT nextval('t_citacion_id_citacion_seq'::regclass);


--
-- TOC entry 2494 (class 2604 OID 26708)
-- Name: id_correo; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_correo ALTER COLUMN id_correo SET DEFAULT nextval('t_correo_id_correo_seq'::regclass);


--
-- TOC entry 2495 (class 2604 OID 26709)
-- Name: id_datos_persona; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_datos_persona ALTER COLUMN id_datos_persona SET DEFAULT nextval('t_datos_persona_id_datos_persona_seq'::regclass);


--
-- TOC entry 2456 (class 2604 OID 26710)
-- Name: id_detalle_servicio; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_detalle_servicio ALTER COLUMN id_detalle_servicio SET DEFAULT nextval('t_detalle_servicio_id_detalle_servicio_seq'::regclass);


--
-- TOC entry 2464 (class 2604 OID 26711)
-- Name: id_emision_grabacion; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_emision_grabacion ALTER COLUMN id_emision_grabacion SET DEFAULT nextval('t_emision_grabacion_id_emision_grabacion_seq'::regclass);


--
-- TOC entry 2496 (class 2604 OID 26712)
-- Name: id_estado_informe; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_estado_informe ALTER COLUMN id_estado_informe SET DEFAULT nextval('t_estado_informe_id_estado_informe_seq'::regclass);


--
-- TOC entry 2465 (class 2604 OID 26713)
-- Name: id_estatus; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_estatus ALTER COLUMN id_estatus SET DEFAULT nextval('t_estatus_id_estatus_seq'::regclass);


--
-- TOC entry 2462 (class 2604 OID 26714)
-- Name: id_estatus_detalle_servicio; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_estatus_detalle_servicio ALTER COLUMN id_estatus_detalle_servicio SET DEFAULT nextval('t_estatus_detalle_servicio_id_estatus_detalle_servicio_seq'::regclass);


--
-- TOC entry 2453 (class 2604 OID 26715)
-- Name: id_estatus_pauta; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_estatus_pauta ALTER COLUMN id_estatus_pauta SET DEFAULT nextval('t_estatus_pauta_id_estatus_pauta_seq'::regclass);


--
-- TOC entry 2497 (class 2604 OID 26716)
-- Name: id_informe; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_informe ALTER COLUMN id_informe SET DEFAULT nextval('t_informe_id_informe_seq'::regclass);


--
-- TOC entry 2466 (class 2604 OID 26717)
-- Name: id_locacion; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_locacion ALTER COLUMN id_locacion SET DEFAULT nextval('t_locacion_id_locacion_seq'::regclass);


--
-- TOC entry 2468 (class 2604 OID 26718)
-- Name: id_lugar; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_lugar ALTER COLUMN id_lugar SET DEFAULT nextval('t_lugar_id_lugar_seq'::regclass);


--
-- TOC entry 2470 (class 2604 OID 26719)
-- Name: id_montaje; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_montaje ALTER COLUMN id_montaje SET DEFAULT nextval('t_montaje_id_montaje_seq'::regclass);


--
-- TOC entry 2473 (class 2604 OID 26720)
-- Name: id_pauta; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_pauta ALTER COLUMN id_pauta SET DEFAULT nextval('t_pauta_id_pauta_seq'::regclass);


--
-- TOC entry 2499 (class 2604 OID 26721)
-- Name: id_productor; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_productor ALTER COLUMN id_productor SET DEFAULT nextval('t_productor_id_productor_seq'::regclass);


--
-- TOC entry 2475 (class 2604 OID 26722)
-- Name: id_program; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_programa ALTER COLUMN id_program SET DEFAULT nextval('t_programa_id_program_seq'::regclass);


--
-- TOC entry 2476 (class 2604 OID 26723)
-- Name: id_retorno; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_retorno ALTER COLUMN id_retorno SET DEFAULT nextval('t_retorno_id_retorno_seq'::regclass);


--
-- TOC entry 2477 (class 2604 OID 26724)
-- Name: id_tipo_estatus; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_tipo_estatus ALTER COLUMN id_tipo_estatus SET DEFAULT nextval('t_tipo_estatus_id_tipo_estatus_seq'::regclass);


--
-- TOC entry 2479 (class 2604 OID 26725)
-- Name: id_tipo_evento; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_tipo_evento ALTER COLUMN id_tipo_evento SET DEFAULT nextval('t_tipo_evento_id_tipo_evento_seq'::regclass);


--
-- TOC entry 2481 (class 2604 OID 26726)
-- Name: id_tipo_pauta; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_tipo_pauta ALTER COLUMN id_tipo_pauta SET DEFAULT nextval('t_tipo_pauta_id_tipo_pauta_seq'::regclass);


--
-- TOC entry 2484 (class 2604 OID 26727)
-- Name: id_tipo_traje; Type: DEFAULT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_tipo_traje ALTER COLUMN id_tipo_traje SET DEFAULT nextval('t_tipo_traje_id_tipo_traje_seq'::regclass);


SET search_path = usuario, pg_catalog;

--
-- TOC entry 2448 (class 2604 OID 26728)
-- Name: id_acceso; Type: DEFAULT; Schema: usuario; Owner: -
--

ALTER TABLE ONLY t_acceso ALTER COLUMN id_acceso SET DEFAULT nextval('t_acceso_id_acceso_seq'::regclass);


--
-- TOC entry 2488 (class 2604 OID 26729)
-- Name: id_aplicacion; Type: DEFAULT; Schema: usuario; Owner: -
--

ALTER TABLE ONLY t_aplicacion ALTER COLUMN id_aplicacion SET DEFAULT nextval('t_aplicacion_id_aplicacion_seq'::regclass);


--
-- TOC entry 2449 (class 2604 OID 26730)
-- Name: id_datos_personales; Type: DEFAULT; Schema: usuario; Owner: -
--

ALTER TABLE ONLY t_datos_personales ALTER COLUMN id_datos_personales SET DEFAULT nextval('t_datos_personales_id_datos_personales_seq'::regclass);


--
-- TOC entry 2490 (class 2604 OID 26731)
-- Name: id_modulo; Type: DEFAULT; Schema: usuario; Owner: -
--

ALTER TABLE ONLY t_modulo ALTER COLUMN id_modulo SET DEFAULT nextval('t_modulo_id_modulo_seq'::regclass);


--
-- TOC entry 2501 (class 2604 OID 26732)
-- Name: id_submodulo; Type: DEFAULT; Schema: usuario; Owner: -
--

ALTER TABLE ONLY t_submodulo ALTER COLUMN id_submodulo SET DEFAULT nextval('t_submodulo_id_submodulo_seq'::regclass);


--
-- TOC entry 2450 (class 2604 OID 26733)
-- Name: id_tipo_usuario; Type: DEFAULT; Schema: usuario; Owner: -
--

ALTER TABLE ONLY t_tipo_usuario ALTER COLUMN id_tipo_usuario SET DEFAULT nextval('t_tipo_usuario_id_usuario_seq'::regclass);


SET search_path = almacen_tecnico, pg_catalog;

--
-- TOC entry 2505 (class 2606 OID 26989)
-- Name: id_articulo_pkey; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_articulo
    ADD CONSTRAINT id_articulo_pkey PRIMARY KEY (id_articulo);


--
-- TOC entry 2507 (class 2606 OID 26991)
-- Name: id_categoria_pkey; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_categoria
    ADD CONSTRAINT id_categoria_pkey PRIMARY KEY (id_categoria);


--
-- TOC entry 2517 (class 2606 OID 26993)
-- Name: id_descripcion_pk; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_descripciones
    ADD CONSTRAINT id_descripcion_pk PRIMARY KEY (id_descripcion);


--
-- TOC entry 2513 (class 2606 OID 26995)
-- Name: id_grupo_pkey; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_grupos
    ADD CONSTRAINT id_grupo_pkey PRIMARY KEY (id_del_grupo);


--
-- TOC entry 2531 (class 2606 OID 26997)
-- Name: id_ingresos_pkey; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_ingresos
    ADD CONSTRAINT id_ingresos_pkey PRIMARY KEY (id_ingresos);


--
-- TOC entry 2533 (class 2606 OID 26999)
-- Name: id_material_desp_pkey; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_material_desp
    ADD CONSTRAINT id_material_desp_pkey PRIMARY KEY (id_material_desp);


--
-- TOC entry 2523 (class 2606 OID 27001)
-- Name: id_pretamo_pkey; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_prestamo
    ADD CONSTRAINT id_pretamo_pkey PRIMARY KEY (id_prestamo);


--
-- TOC entry 2529 (class 2606 OID 27003)
-- Name: id_serial_incidencia_pkey; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_incidencia
    ADD CONSTRAINT id_serial_incidencia_pkey PRIMARY KEY (id_serial_incidencia);


--
-- TOC entry 2539 (class 2606 OID 27005)
-- Name: id_solicitudes_pk; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_solicitudes_f5
    ADD CONSTRAINT id_solicitudes_pk PRIMARY KEY (id_solicitud);


--
-- TOC entry 2515 (class 2606 OID 27007)
-- Name: id_unidad_medida_pkey; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_unidad_medida
    ADD CONSTRAINT id_unidad_medida_pkey PRIMARY KEY (id_unidad_medida);


--
-- TOC entry 2525 (class 2606 OID 27009)
-- Name: t_camara_sol_pkey; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_camara_sol
    ADD CONSTRAINT t_camara_sol_pkey PRIMARY KEY (id_camara_sol);


--
-- TOC entry 2509 (class 2606 OID 27011)
-- Name: t_des_estado_pkey; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_des_estado
    ADD CONSTRAINT t_des_estado_pkey PRIMARY KEY (id_desc_estado);


--
-- TOC entry 2519 (class 2606 OID 27013)
-- Name: t_despacho_pkey; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_despacho
    ADD CONSTRAINT t_despacho_pkey PRIMARY KEY (id_despacho);


--
-- TOC entry 2521 (class 2606 OID 27015)
-- Name: t_entrada_pkey; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_entrada
    ADD CONSTRAINT t_entrada_pkey PRIMARY KEY (id_entrada);


--
-- TOC entry 2511 (class 2606 OID 27017)
-- Name: t_estatus_pkey; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_estatus
    ADD CONSTRAINT t_estatus_pkey PRIMARY KEY (id_estatus);


--
-- TOC entry 2527 (class 2606 OID 27019)
-- Name: t_grupo_sol_pkey; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_grupo_sol
    ADD CONSTRAINT t_grupo_sol_pkey PRIMARY KEY (id_grupo_sol);


--
-- TOC entry 2535 (class 2606 OID 27021)
-- Name: t_material_sol_pkey; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_material_sol
    ADD CONSTRAINT t_material_sol_pkey PRIMARY KEY (id_material_sol);


--
-- TOC entry 2537 (class 2606 OID 27023)
-- Name: t_programa_pkey; Type: CONSTRAINT; Schema: almacen_tecnico; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_programa
    ADD CONSTRAINT t_programa_pkey PRIMARY KEY (id_programa);


SET search_path = f5, pg_catalog;

--
-- TOC entry 2554 (class 2606 OID 27025)
-- Name: pk_cargo; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY rrhh_cargos
    ADD CONSTRAINT pk_cargo PRIMARY KEY (id_cargo);


--
-- TOC entry 2595 (class 2606 OID 27027)
-- Name: pk_cedula_empleado; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY rrhh_personal
    ADD CONSTRAINT pk_cedula_empleado PRIMARY KEY (cedula);


--
-- TOC entry 2556 (class 2606 OID 27029)
-- Name: pk_id_categoria; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY rrhh_categoria
    ADD CONSTRAINT pk_id_categoria PRIMARY KEY (id_categoria);


--
-- TOC entry 2597 (class 2606 OID 27031)
-- Name: pk_id_correo; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_correo
    ADD CONSTRAINT pk_id_correo PRIMARY KEY (id_correo);


--
-- TOC entry 2587 (class 2606 OID 27033)
-- Name: pk_id_division; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY rrhh_division
    ADD CONSTRAINT pk_id_division PRIMARY KEY (id_division);


--
-- TOC entry 2589 (class 2606 OID 27035)
-- Name: pk_id_gerencia; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY rrhh_gerencia
    ADD CONSTRAINT pk_id_gerencia PRIMARY KEY (id_gerencia);


--
-- TOC entry 2561 (class 2606 OID 27037)
-- Name: t_citacion_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_citacion
    ADD CONSTRAINT t_citacion_pkey PRIMARY KEY (id_citacion);


--
-- TOC entry 2599 (class 2606 OID 27039)
-- Name: t_datos_persona_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_datos_persona
    ADD CONSTRAINT t_datos_persona_pkey PRIMARY KEY (id_datos_persona);


--
-- TOC entry 2552 (class 2606 OID 27041)
-- Name: t_detalle_servicio_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_detalle_servicio
    ADD CONSTRAINT t_detalle_servicio_pkey PRIMARY KEY (id_detalle_servicio);


--
-- TOC entry 2563 (class 2606 OID 27043)
-- Name: t_emision_grabacion_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_emision_grabacion
    ADD CONSTRAINT t_emision_grabacion_pkey PRIMARY KEY (id_emision_grabacion);


--
-- TOC entry 2601 (class 2606 OID 27045)
-- Name: t_estado_informe_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_estado_informe
    ADD CONSTRAINT t_estado_informe_pkey PRIMARY KEY (id_estado_informe);


--
-- TOC entry 2559 (class 2606 OID 27047)
-- Name: t_estatus_detalle_servicio_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_estatus_detalle_servicio
    ADD CONSTRAINT t_estatus_detalle_servicio_pkey PRIMARY KEY (id_estatus_detalle_servicio);


--
-- TOC entry 2550 (class 2606 OID 27049)
-- Name: t_estatus_pauta_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_estatus_pauta
    ADD CONSTRAINT t_estatus_pauta_pkey PRIMARY KEY (id_estatus_pauta);


--
-- TOC entry 2565 (class 2606 OID 27051)
-- Name: t_estatus_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_estatus
    ADD CONSTRAINT t_estatus_pkey PRIMARY KEY (id_estatus);


--
-- TOC entry 2603 (class 2606 OID 27053)
-- Name: t_informe_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_informe
    ADD CONSTRAINT t_informe_pkey PRIMARY KEY (id_informe);


--
-- TOC entry 2567 (class 2606 OID 27055)
-- Name: t_locacion_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_locacion
    ADD CONSTRAINT t_locacion_pkey PRIMARY KEY (id_locacion);


--
-- TOC entry 2569 (class 2606 OID 27057)
-- Name: t_lugar_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_lugar
    ADD CONSTRAINT t_lugar_pkey PRIMARY KEY (id_lugar);


--
-- TOC entry 2571 (class 2606 OID 27059)
-- Name: t_montaje_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_montaje
    ADD CONSTRAINT t_montaje_pkey PRIMARY KEY (id_montaje);


--
-- TOC entry 2573 (class 2606 OID 27061)
-- Name: t_pauta_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_pkey PRIMARY KEY (id_pauta);


--
-- TOC entry 2605 (class 2606 OID 27063)
-- Name: t_productor_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_productor
    ADD CONSTRAINT t_productor_pkey PRIMARY KEY (id_productor);


--
-- TOC entry 2575 (class 2606 OID 27065)
-- Name: t_programa_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_programa
    ADD CONSTRAINT t_programa_pkey PRIMARY KEY (id_program);


--
-- TOC entry 2577 (class 2606 OID 27067)
-- Name: t_retorno_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_retorno
    ADD CONSTRAINT t_retorno_pkey PRIMARY KEY (id_retorno);


--
-- TOC entry 2579 (class 2606 OID 27069)
-- Name: t_tipo_estatus_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_tipo_estatus
    ADD CONSTRAINT t_tipo_estatus_pkey PRIMARY KEY (id_tipo_estatus);


--
-- TOC entry 2581 (class 2606 OID 27071)
-- Name: t_tipo_evento_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_tipo_evento
    ADD CONSTRAINT t_tipo_evento_pkey PRIMARY KEY (id_tipo_evento);


--
-- TOC entry 2583 (class 2606 OID 27073)
-- Name: t_tipo_pauta_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_tipo_pauta
    ADD CONSTRAINT t_tipo_pauta_pkey PRIMARY KEY (id_tipo_pauta);


--
-- TOC entry 2585 (class 2606 OID 27075)
-- Name: t_tipo_traje_pkey; Type: CONSTRAINT; Schema: f5; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_tipo_traje
    ADD CONSTRAINT t_tipo_traje_pkey PRIMARY KEY (id_tipo_traje);


SET search_path = usuario, pg_catalog;

--
-- TOC entry 2544 (class 2606 OID 27077)
-- Name: datos_personales_pk; Type: CONSTRAINT; Schema: usuario; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_datos_personales
    ADD CONSTRAINT datos_personales_pk PRIMARY KEY (id_datos_personales);


--
-- TOC entry 2546 (class 2606 OID 27079)
-- Name: pk_cedula; Type: CONSTRAINT; Schema: usuario; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_datos_personales
    ADD CONSTRAINT pk_cedula UNIQUE (cedula);


--
-- TOC entry 2542 (class 2606 OID 27081)
-- Name: t_acceso_pkey; Type: CONSTRAINT; Schema: usuario; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_acceso
    ADD CONSTRAINT t_acceso_pkey PRIMARY KEY (id_acceso);


--
-- TOC entry 2591 (class 2606 OID 27083)
-- Name: t_aplicacion_pkey; Type: CONSTRAINT; Schema: usuario; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_aplicacion
    ADD CONSTRAINT t_aplicacion_pkey PRIMARY KEY (id_aplicacion);


--
-- TOC entry 2609 (class 2606 OID 27085)
-- Name: t_firma_pkey; Type: CONSTRAINT; Schema: usuario; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_firma
    ADD CONSTRAINT t_firma_pkey PRIMARY KEY (id_firma);


--
-- TOC entry 2593 (class 2606 OID 27087)
-- Name: t_modulo_pkey; Type: CONSTRAINT; Schema: usuario; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_modulo
    ADD CONSTRAINT t_modulo_pkey PRIMARY KEY (id_modulo);


--
-- TOC entry 2607 (class 2606 OID 27089)
-- Name: t_submodulo_pkey; Type: CONSTRAINT; Schema: usuario; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_submodulo
    ADD CONSTRAINT t_submodulo_pkey PRIMARY KEY (id_submodulo);


--
-- TOC entry 2548 (class 2606 OID 27091)
-- Name: t_tipo_usuario_pkey; Type: CONSTRAINT; Schema: usuario; Owner: -; Tablespace: 
--

ALTER TABLE ONLY t_tipo_usuario
    ADD CONSTRAINT t_tipo_usuario_pkey PRIMARY KEY (id_tipo_usuario);


SET search_path = f5, pg_catalog;

--
-- TOC entry 2557 (class 1259 OID 27092)
-- Name: fki_; Type: INDEX; Schema: f5; Owner: -; Tablespace: 
--

CREATE INDEX fki_ ON t_estatus_detalle_servicio USING btree (id_detalle_servicio);


SET search_path = usuario, pg_catalog;

--
-- TOC entry 2540 (class 1259 OID 27093)
-- Name: fki_; Type: INDEX; Schema: usuario; Owner: -; Tablespace: 
--

CREATE INDEX fki_ ON t_acceso USING btree (id_tipo_usuario);


SET search_path = f5, pg_catalog;

--
-- TOC entry 2635 (class 2606 OID 27094)
-- Name: fk_id_cargo; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY rrhh_personal
    ADD CONSTRAINT fk_id_cargo FOREIGN KEY (id_cargo) REFERENCES rrhh_cargos(id_cargo) MATCH FULL DEFERRABLE;


--
-- TOC entry 2617 (class 2606 OID 27099)
-- Name: fk_id_cargo; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY rrhh_rel_categoria_cargo
    ADD CONSTRAINT fk_id_cargo FOREIGN KEY (id_cargo) REFERENCES rrhh_cargos(id_cargo) MATCH FULL DEFERRABLE;


--
-- TOC entry 2618 (class 2606 OID 27104)
-- Name: fk_id_categoria; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY rrhh_rel_categoria_cargo
    ADD CONSTRAINT fk_id_categoria FOREIGN KEY (id_categoria) REFERENCES rrhh_categoria(id_categoria);


--
-- TOC entry 2636 (class 2606 OID 27109)
-- Name: fk_id_division; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY rrhh_personal
    ADD CONSTRAINT fk_id_division FOREIGN KEY (id_division) REFERENCES rrhh_division(id_division) MATCH FULL DEFERRABLE;


--
-- TOC entry 2633 (class 2606 OID 27114)
-- Name: fk_id_gerencia; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY rrhh_division
    ADD CONSTRAINT fk_id_gerencia FOREIGN KEY (id_gerencia) REFERENCES rrhh_gerencia(id_gerencia) MATCH FULL DEFERRABLE;


--
-- TOC entry 2637 (class 2606 OID 27119)
-- Name: fk_id_gerencia; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY rrhh_personal
    ADD CONSTRAINT fk_id_gerencia FOREIGN KEY (id_gerencia) REFERENCES rrhh_gerencia(id_gerencia) MATCH FULL DEFERRABLE;


--
-- TOC entry 2622 (class 2606 OID 27124)
-- Name: t_citacion_id_lugar_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_citacion
    ADD CONSTRAINT t_citacion_id_lugar_fkey FOREIGN KEY (id_lugar) REFERENCES t_lugar(id_lugar) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2616 (class 2606 OID 27129)
-- Name: t_detalle_servicio_id_pauta_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_detalle_servicio
    ADD CONSTRAINT t_detalle_servicio_id_pauta_fkey FOREIGN KEY (id_pauta) REFERENCES t_pauta(id_pauta) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2619 (class 2606 OID 27134)
-- Name: t_estatus_detalle_servicio_id_detalle_servicio_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_estatus_detalle_servicio
    ADD CONSTRAINT t_estatus_detalle_servicio_id_detalle_servicio_fkey FOREIGN KEY (id_detalle_servicio) REFERENCES t_detalle_servicio(id_detalle_servicio);


--
-- TOC entry 2620 (class 2606 OID 27139)
-- Name: t_estatus_detalle_servicio_id_estatus_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_estatus_detalle_servicio
    ADD CONSTRAINT t_estatus_detalle_servicio_id_estatus_fkey FOREIGN KEY (id_estatus) REFERENCES t_estatus(id_estatus);


--
-- TOC entry 2621 (class 2606 OID 27144)
-- Name: t_estatus_detalle_servicio_id_pauta_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_estatus_detalle_servicio
    ADD CONSTRAINT t_estatus_detalle_servicio_id_pauta_fkey FOREIGN KEY (id_pauta) REFERENCES t_pauta(id_pauta);


--
-- TOC entry 2623 (class 2606 OID 27149)
-- Name: t_estatus_id_tipo_estatus_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_estatus
    ADD CONSTRAINT t_estatus_id_tipo_estatus_fkey FOREIGN KEY (id_tipo_estatus) REFERENCES t_tipo_estatus(id_tipo_estatus);


--
-- TOC entry 2614 (class 2606 OID 27154)
-- Name: t_estatus_pauta_id_estatus_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_estatus_pauta
    ADD CONSTRAINT t_estatus_pauta_id_estatus_fkey FOREIGN KEY (id_estatus) REFERENCES t_estatus(id_estatus);


--
-- TOC entry 2615 (class 2606 OID 27159)
-- Name: t_estatus_pauta_id_pauta_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_estatus_pauta
    ADD CONSTRAINT t_estatus_pauta_id_pauta_fkey FOREIGN KEY (id_pauta) REFERENCES t_pauta(id_pauta);


--
-- TOC entry 2638 (class 2606 OID 27164)
-- Name: t_informe_id_detalle_servicio_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_informe
    ADD CONSTRAINT t_informe_id_detalle_servicio_fkey FOREIGN KEY (id_detalle_servicio) REFERENCES t_detalle_servicio(id_detalle_servicio);


--
-- TOC entry 2639 (class 2606 OID 27169)
-- Name: t_informe_id_pauta_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_informe
    ADD CONSTRAINT t_informe_id_pauta_fkey FOREIGN KEY (id_pauta) REFERENCES t_pauta(id_pauta);


--
-- TOC entry 2624 (class 2606 OID 27174)
-- Name: t_pauta_id_citacion_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_citacion_fkey FOREIGN KEY (id_citacion) REFERENCES t_citacion(id_citacion) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2625 (class 2606 OID 27179)
-- Name: t_pauta_id_emision_grabacion_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_emision_grabacion_fkey FOREIGN KEY (id_emision_grabacion) REFERENCES t_emision_grabacion(id_emision_grabacion) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2626 (class 2606 OID 27184)
-- Name: t_pauta_id_locacion_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_locacion_fkey FOREIGN KEY (id_locacion) REFERENCES t_locacion(id_locacion) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2627 (class 2606 OID 27189)
-- Name: t_pauta_id_montaje_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_montaje_fkey FOREIGN KEY (id_montaje) REFERENCES t_montaje(id_montaje) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2628 (class 2606 OID 27194)
-- Name: t_pauta_id_program_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_program_fkey FOREIGN KEY (id_program) REFERENCES t_programa(id_program) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2629 (class 2606 OID 27199)
-- Name: t_pauta_id_retorno_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_retorno_fkey FOREIGN KEY (id_retorno) REFERENCES t_retorno(id_retorno) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2630 (class 2606 OID 27204)
-- Name: t_pauta_id_tipo_evento_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_tipo_evento_fkey FOREIGN KEY (id_tipo_evento) REFERENCES t_tipo_evento(id_tipo_evento) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2631 (class 2606 OID 27209)
-- Name: t_pauta_id_tipo_pauta_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_tipo_pauta_fkey FOREIGN KEY (id_tipo_pauta) REFERENCES t_tipo_pauta(id_tipo_pauta) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2632 (class 2606 OID 27214)
-- Name: t_pauta_id_tipo_traje_fkey; Type: FK CONSTRAINT; Schema: f5; Owner: -
--

ALTER TABLE ONLY t_pauta
    ADD CONSTRAINT t_pauta_id_tipo_traje_fkey FOREIGN KEY (id_tipo_traje) REFERENCES t_tipo_traje(id_tipo_traje) ON UPDATE RESTRICT ON DELETE RESTRICT;


SET search_path = usuario, pg_catalog;

--
-- TOC entry 2613 (class 2606 OID 27219)
-- Name: id_aplicacion; Type: FK CONSTRAINT; Schema: usuario; Owner: -
--

ALTER TABLE ONLY t_tipo_usuario
    ADD CONSTRAINT id_aplicacion FOREIGN KEY (id_aplicacion) REFERENCES t_aplicacion(id_aplicacion);


--
-- TOC entry 2610 (class 2606 OID 27224)
-- Name: t_acceso_fk1; Type: FK CONSTRAINT; Schema: usuario; Owner: -
--

ALTER TABLE ONLY t_acceso
    ADD CONSTRAINT t_acceso_fk1 FOREIGN KEY (id_modulo) REFERENCES t_modulo(id_modulo);


--
-- TOC entry 2611 (class 2606 OID 27229)
-- Name: t_acceso_fk2; Type: FK CONSTRAINT; Schema: usuario; Owner: -
--

ALTER TABLE ONLY t_acceso
    ADD CONSTRAINT t_acceso_fk2 FOREIGN KEY (id_aplicacion) REFERENCES t_aplicacion(id_aplicacion);


--
-- TOC entry 2612 (class 2606 OID 27234)
-- Name: t_acceso_id_tipo_usuario_fkey; Type: FK CONSTRAINT; Schema: usuario; Owner: -
--

ALTER TABLE ONLY t_acceso
    ADD CONSTRAINT t_acceso_id_tipo_usuario_fkey FOREIGN KEY (id_tipo_usuario) REFERENCES t_tipo_usuario(id_tipo_usuario);


--
-- TOC entry 2634 (class 2606 OID 27239)
-- Name: t_modulo_id_aplicacion_fkey; Type: FK CONSTRAINT; Schema: usuario; Owner: -
--

ALTER TABLE ONLY t_modulo
    ADD CONSTRAINT t_modulo_id_aplicacion_fkey FOREIGN KEY (id_aplicacion) REFERENCES t_aplicacion(id_aplicacion);


--
-- TOC entry 2640 (class 2606 OID 27244)
-- Name: t_submodulo_id_modulo_fkey; Type: FK CONSTRAINT; Schema: usuario; Owner: -
--

ALTER TABLE ONLY t_submodulo
    ADD CONSTRAINT t_submodulo_id_modulo_fkey FOREIGN KEY (id_modulo) REFERENCES t_modulo(id_modulo);


-- Completed on 2014-08-07 09:05:15 VET

--
-- PostgreSQL database dump complete
--
